@extends('layouts.user.base')
@section('content')
<style type="text/css">
    .card {
        background-color: transparent;
        border: 0px solid;
    }
    .card-header {
        padding: 0px;
    }
    .card-header a {
        background: #784d98;
        color: #fff;
        padding: 0.75rem 1.25rem;
        width: 100%;
        display: block;
    }
    .p-radio-btn {
        position: absolute;
        top: 3px;
    }
    .card-header a::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/plus.png);
    }
    .card-header a[aria-expanded="true"]::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/minus.png);
    }
</style>
<section class="container-fluid my-3 my-lg-5">
   <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12 pb-5">
                        <div>
                            <img src="images/banners/matrimony.jpg" class="img-fluid w-100" alt="">
                        </div>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                   <button type="button" class="close" data-dismiss="alert">×</button>
                   <strong>{{ $message }}</strong>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                   <button type="button" class="close" data-dismiss="alert">×</button>
                   <strong>{{ $message }}</strong>
                </div>
                @endif
                 @if ($message = Request::get('error'))
                <div class="alert alert-danger alert-block">
                   <button type="button" class="close" data-dismiss="alert">×</button>
                   <strong>This member not registred with nriva eedujodu. Please register in Nriva.org with eedujodu membership.</strong>
                </div>
                @endif
                <div class="row px-1 mx-0 mx-lg-5 px-lg-0">
                    <div class="col-12 mb-5">
                        <h5 class="text-violet text-center mb-0">Find your perfect Match at NRIVA Matrimony!</h5>
                    </div>
                    <div class="col-12 col-lg-12 shadow-small px-sm-20 p40 p-md-4 mb-4">
                        <form id='form' action="{{url('matrimony_registration')}}" method="post" enctype="multipart/form-data">
                            <div class="row">
                            @csrf
                            <!-- Personal Information -->
                                <div class="col-12">
                                    <h6 class="mb-3 text-violet">Event Registration to Participate In-Person at the 6th NRIVA Global Convention:</h6>
                                   <div class="form-row">
                                      <div class="form-group col-12 col-md-7 col-lg-7 my-auto">
                                        <label>Eedu-Jodu Profile ID:</label><span class="text-red">*</span>
                                        <div>
                                            <input type="text" name="profile_id" id="profile_id" value="{{  $user->member_id ?? '' }}" class="form-control" required placeholder="Profile Id" @if($user) readonly @endif/>
                                        </div>
                                      </div>
                                      <div class="form-group col-12 col-md-5 col-lg-5 my-auto">
                                        <label></label>
                                        <div class="pt10">
                                            <a href="#" class="btn btn-violet">Register Here</a>
                                        </div>
                                      </div>
                                    </div>
                                    <p class="mt20 mb10">
                                        <strong>Note :</strong><span> If you do not have Eedu-Jodu Profile ID, please go to the following link to register first.</span>
                                    </p>
                                    <p>
                                        <a href="#">https://matrimony.nriva.org/</a>
                                    </p>
                                    <!-- <div class="form-row">
                                        <div class="col-12 col-sm-6 col-md-12 my-2 my-md-auto">
                                            <div class="text-center text-sm-right">
                                                <input type="submit" class="btn btn-lg btn-danger text-uppercase px-5 " value="Submit" name="">
                                            </div>
                                        </div>
                                    </div> -->
                                </div>  
                                <div class="col-12 py-3">
                                    <div class="row">
                                        <div class="col-12 col-md-12 col-lg-6">
                                            <h6 class="text-violet">PLANNED EVENTS \ SCHEDULE:</h6>
                                            <ul class="fs16">
                                                <li>
                                                    Informal Social Hangout and <Top Golf> On July xxx, 7pm To 11pm
                                                </li>
                                                <li>
                                                    Speed Dating On July
                                                </li>
                                                <li>
                                                    Speed Dating On July
                                                </li>
                                                <li>
                                                    Profile Introductions, July xxxx , 10 Am To 9pm
                                                </li>
                                                <li>
                                                    One-On-One Sessions, July xxx
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 px-md-2">
                                            <div class="text-right">
                                                <div>
                                                    <iframe
                                                        width="100%"
                                                        class="border-radius-5 mr-video"
                                                        src="https://www.youtube.com/embed/CkMNaUjieWM"
                                                        title="YouTube video player"
                                                        frameborder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowfullscreen
                                                    ></iframe>
                                                </div>
                                                <!-- <div>
                                                    <iframe
                                                        width="100%"
                                                        class="border-radius-5 mr-video"
                                                        src="https://www.youtube.com/embed/CkMNaUjieWM"
                                                        title="YouTube video player"
                                                        frameborder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowfullscreen
                                                    ></iframe>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 px-md-2">
                                            <div class="text-right">
                                                <div>
                                                    <iframe
                                                        width="100%"
                                                        class="border-radius-5 mr-video"
                                                        src="https://www.youtube.com/embed/CkMNaUjieWM"
                                                        title="YouTube video player"
                                                        frameborder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowfullscreen
                                                    ></iframe>
                                                </div>
                                                <!-- <div>
                                                    <iframe
                                                        width="100%"
                                                        class="border-radius-5 mr-video"
                                                        src="https://www.youtube.com/embed/CkMNaUjieWM"
                                                        title="YouTube video player"
                                                        frameborder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowfullscreen
                                                    ></iframe>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 py-3">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                            <div>
                                                <img src="images/no-image1.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                            <div>
                                                <img src="images/no-image1.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                            <div>
                                                <img src="images/no-image1.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                            <div>
                                                <img src="images/no-image1.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 py-3">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-7 col-lg-7">
                                             <h6 class="text-violet">REGISTRATION PRIVILEGES</h6>
                                             <ul class="fs16">
                                                 <li>Designated Priority Seating</li>
                                                 <li>Better Reach for Your Profile By early access</li>
                                                 <li>Assigned Relationship Manager Assistance</li>
                                                 <li>Support of Relationship Manager before, during and after event</li>
                                            </ul>
                                            <h6 class="text-violet">DURING THE EVENT:</h6>
                                            <p>Choose One on One Sessions---- Please suggest options to enable meetings in timeslots and rooms</p>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                                             <!-- <h6 class="text-violet">IMPORTANT LINKS</h6> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div id="accordion" class="o-accordion">
                                        <ul class="list-unstyled">
                                            <li class="pb15 mb15 border-bottom">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <a class="card-link fs18" data-toggle="collapse" href="#collapseOne">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        </a>
                                                    </div>
                                                    <div id="collapseOne" class="collapse" data-parent="#accordion">
                                                        <div class="card-body p20">
                                                            <ul class="list-unstyled matrimony-details-list fs16">
                                                                <li>
                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="pb15 mb15 border-bottom">
                                                <div class="card">
                                                    <div class="card-header fs18">
                                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        </a>
                                                    </div>
                                                    <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <ul class="list-unstyled matrimony-details-list fs16">
                                                                <!-- <li>Click on the <a class="text-danger font-weight-bold" href="#matrimony-registration">(Registration link)</a>&nbsp;on this page</li> -->
                                                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="pb15 mb15 border-bottom">
                                                <div class="card">
                                                    <div class="card-header fs18">
                                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        </a>
                                                    </div>
                                                    <div id="collapseThree" class="collapse" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <p class="mb-0">
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="pb15 mb15 border-bottom">
                                                <div class="card">
                                                    <div class="card-header fs18">
                                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        </a>
                                                    </div>
                                                    <div id="collapseFour" class="collapse" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <p class="mb-0">
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Registration</h4>
        </div>

        <div class="modal-body">
            <div class="alert alert-success">Please enter nriva password and procced to registration</div>
            <form class="" action="{{url('newAuthorize')}}" method="post">
                 @csrf
                <input type="hidden" name="email" id="new_email">
             <div class="form-row">
                    
                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Password:</label><span style="color:red;">*</span>
                        <div>
                            <input type="password" name="password" id="password"  class="form-control" placeholder="Enter Password" required />
                        </div>
                    </div>
                </div>
                 <button type="submit" class="btn btn-primary">Submit</button>
            </form>

          
        </div>
         <div class="modal-footer">
        <a href="{{url('/')}}" class="btn btn-danger" >Close</a>
      </div>
        
      </div>
      
    </div>
  </div>

  <div class="modal fade" id="t_and_c" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Terms and Conditions</h4>
        </div>

        <div class="modal-body">
            <div class="">
                <ul>
                    <li>Terms and Conditions</li>
                    <li>Terms and Conditions</li>
                    <li>Terms and Conditions</li>
                    <li>Terms and Conditions</li>
                </ul>
            </div>
        </div>
         <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
        
      </div>
      
    </div>
  </div>


@section('javascript')
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
<script>

     $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        });
          $.validator.addMethod("numberss", function(value, element) {
            return this.optional(element) || value == value.match(/^[0-9) (-]+$/);
        });


        // $("#form").validate({
           
            
        //     rules: {
                
        //         first_name: "required alpha",
        //         last_name: "required alpha",
        //         age: "required numberss",
        //         gothram: "required",
        //         rasi: "required",
        //         state: "required",
        //         country: "required",
        //         address: "required",
        //         email: "required",
        //         mobile :{
        //             maxlength: 14,
        //             numberss:true
        //         },
        //         'zip_code': {
        //                 digits: true
        //             },
        //     },
        //      messages: {
               
        //           "first_name": {
        //             alpha: "First name should not contain numbers.",
        //         },
        //          "last_name": {
        //             alpha: "Last name should not contain numbers.",
        //         },
                 
        //          "mobile": {
        //             maxlength: "Max length is  14.",
        //             numberss: "Numbers only"
        //         }
        //     },
        // });

 @if(!Auth::user())
$('#email').on('change',function(){
        checking();

    });
$('#profile_id').on('change',function(){
        checking();

    });
function checking() {
  $('.email_valid_msg').text('')
        var email = $('#email').val();
        var profile_id = $('#profile_id').val();
        $.ajax({
            type: 'GET',
            url: "{{url('check_matrimony_email')}}?email="+email+"&profile_id="+profile_id,
            success:function(data){
              if(data!="failed"){
                $('#new_email').val(data);
                $('#myModal').modal({backdrop: 'static', keyboard: false});
                $('#myModal').modal('show');
              }else{
                window.location.href = "{{url('matrimony_registration')}}?error=not_registered";
              }
             
           
        }
      });
}
@endif
</script>

@endsection

@endsection