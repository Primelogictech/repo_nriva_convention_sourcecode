<style type="text/css">
	.fade:not(.show) {
        opacity: 1;
    }
    .nav-link {
	    color: #050a30;
	}
    .sc-tabs .nav-link.active {
	    border-bottom: none !important;
	    background-color: #050A30;
	    border-left: 0px;
	    border-right: 0px;
	    border-top: 0px;
	    color: #ffffff;
	}
</style>

<div class="container">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-10 offset-lg-1">
            <div class="row">
                <div class="col-12">
                    <h4 class="text-center mb-4 text-orange">Nearby Services</h4>
                </div>
                <div class="col-12">
                    <div>
                        <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll" role="tablist">
                            <li class="nav-item d-inline-flex">
                                <a class="nav-link px-2 px-sm-3 px-md-4 border active" href="#grocery" role="tab" data-toggle="tab">Grocery</a>
                            </li>
                            <li class="nav-item d-inline-flex">
                                <a class="nav-link px-2 px-sm-3 px-md-4 border" href="#pharmacy" role="tab" data-toggle="tab">Pharmacy</a>
                            </li>
                            <li class="nav-item d-inline-flex">
                                <a class="nav-link px-2 px-sm-3 px-md-4 border" id="reg-page" href="#shopping-centers" role="tab" data-toggle="tab">Shopping&nbsp;centers</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="grocery">
                            <div class="col-12 shadow-small py-2">
                                <div class="row px-3">
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/nearby-services/patel-brothers.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 py-lg-0">
                                                <div class="pb-3">
                                                    <div class="text-violet fs18 font-weight-bold">Patel Brothers</div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>830 W Golf Rd, Schaumburg, IL 60194</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">+1(847)519-3200</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/nearby-services/aldi-supermarket.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 py-lg-0">
                                                <div class="pb-3">
                                                    <div class="text-violet fs18 font-weight-bold">ALDI Supermarket</div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>1412 E Algonquin Rd, Schaumburg, IL 60173</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">+1(855)955-2534</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/nearby-services/vishnu-groceries.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 py-lg-0">
                                                <div class="pb-3">
                                                    <div class="text-violet fs18 font-weight-bold">Vishnu Foods</div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>641 E Algonquin Rd, Schaumburg, IL 60173</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">+1(847)303-5116</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Attractions -->

                        <div role="tabpanel" class="tab-pane fade in" id="pharmacy">
                            <div class="col-12 shadow-small py-2">
                                <div class="row px-3">
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/nearby-services/jewel_osco.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 py-lg-0">
                                                <div class="pb-3">
                                                    <div class="text-violet fs18 font-weight-bold">24-hour Jewel Osco Pharmacy</div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>1151 S Roselle Rd, Schaumburg, IL 60193</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">+1(847) 895-1600</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/nearby-services/Costco_Pharmacy.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 py-lg-0">
                                                <div class="pb-3">
                                                    <div class="text-violet fs18 font-weight-bold">Costco Pharmacy</div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>1375 N Meacham Rd, Schaumburg, IL 60173</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">+1(847)969-0790</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/nearby-services/cvs_pharmacy.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 py-lg-0">
                                                <div class="pb-3">
                                                    <div class="text-violet fs18 font-weight-bold">CVS Pharmacy</div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>1235 E Higgins Rd, Schaumburg, IL 60173</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">+1(847)413-1091</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Beaches -->

                        <div role="tabpanel" class="tab-pane fade in" id="shopping-centers">
                            <div class="col-12 shadow-small py-2">
                                <div class="row px-3">
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/nearby-services/woodfield-mall.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 py-lg-0">
                                                <div class="pb-3">
                                                    <div class="text-violet fs18 font-weight-bold">Woodfield Mall</div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>5 Woodfield Mall, Schaumburg, IL 60173</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">+1(847)330-1537</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/nearby-services/ikea.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 py-lg-0">
                                                <div class="pb-3">
                                                    <div class="text-violet fs18 font-weight-bold">Ikea</div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>1800 McConnor Pkwy, Schaumburg, IL 60173</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">+1(888)888-4532</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>