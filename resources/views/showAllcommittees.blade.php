@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-5">

<div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/convention-committees.jpg" class="img-fluid w-100" alt="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row pb-5">
                            @foreach($committes as $key=>$committe)
                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                    <a  href="{{$committe->slug}}"
                                    {{-- href="{{url('more-committe-members', $committe->id)}}" --}} class="text-decoration-none">
                                        <div class="gc-{{ ($loop->iteration % 16)+1 }} border-radius-10">
                                            <div class="convention-cc-bg">
                                                <div class="p-3">
                                                    <div>
                                                        <img src="{{asset(config('conventions.committe_display').$committe->image_url)}}" class="img-fluid mx-auto d-block" alt="">
                                                    </div>
                                                    <h6 class="committeess-names">{{ $committe->name }}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>



@section('javascript')

@endsection


@endsection
