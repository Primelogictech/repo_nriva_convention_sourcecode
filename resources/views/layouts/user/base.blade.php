<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
   
    <link rel="shortcut icon" href="https://convention.nriva.org/public/storage/logo/logo_31.png">
    <link rel="apple-touch-icon" href="https://convention.nriva.org/public/storage/logo/logo_31.png">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://convention.nriva.org/"/>
    <meta property="og:title" content="NRIVA 6th Convention"/>
    <meta property="og:description" content="NRI Vasavi Association (NRIVA), USA 6th Global Convention"/>
    <meta property="og:image" content="https://convention.nriva.org/public/storage/logo/logo_31.png"/>
    <meta property="og:image:width" content="300"/>
    <meta property="og:image:height" content="300"/>

    <title>{{ config('app.name', 'Laravel') }}</title>
    <title>Home Page</title>

    <link rel="icon" href="images/logos/nriva-convention-favicon.png" type="image/png"/>

    <!-- Bootstrap CSS -->

    <link href="{{ asset('css/bootstrapcss.css') }}" rel="stylesheet" />

    <!-- Fontawesome icons -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css" />

    <!-- Animations -->

    <link href="{{ asset('css/Animations.css') }} " rel="stylesheet" />

    <!-- Font family -->

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap" rel="stylesheet">

    <!--Datatables CSS-->

    <link href="{{ asset('css/datatables.css') }}" rel="stylesheet" />

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />

    <!--Custom CSS-->

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/nriva.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/gallery.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/jquery.countdown.css') }}" rel="stylesheet" />

    <script src="{{ asset('js/app.js') }}" defer></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js" defer></script>

    <style>
        .pointer{
            cursor:pointer
        }
    </style>

</head>

<body>
    @include('layouts.user.header')
    <div class="page-wrapper">
        <div class=" container-fluid">
            @if ($errors->any())
                <div class="row">
                    @foreach ($errors->all() as $error)
                        <div class="col-12 my-3">
                            <p class="alert alert-danger">{!! $error !!}</p>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
        @if(Session::has('message-suc'))
        <script type="text/javascript">
            swal({
                title: "success!",
                text: "{{ Session::get('message-suc') }}!",
                icon: "success",
            });
        </script>
        @endif
    </div>
    <!-- Page Content -->
    @yield('content')
    </div>
</body>

@include('layouts.user.footer')

@yield('javascript')
@yield('eaxtra-javascript')

</html>
