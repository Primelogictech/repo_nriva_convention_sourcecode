        <header class="">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 d-none d-lg-block">
                        <div class="row header-bg-yellow justify-content-center pl-5 pr-3">
                            <div class="col-lg-10 offset-lg-1 mx-5">
                                <div class="row">
                                    <div class="col-lg-3 py-1 d-none d-lg-block">
                                        <div class="location-clr text-center text-lg-left fs15">July 2nd, 3rd & 4th 2022</div>
                                    </div>
                                    <div class="col-lg-6 py-1 d-none d-lg-block">
                                        <div class="location-clr text-center fs15">
                                            {{$data['venue']->location?? ""}}
                                        </div>
                                    </div>
                                    <div class="col-lg-3 d-none d-lg-block my-auto">
                                        <div class="d-flex pl-3 py-1 float-right">
                                            <div class="my-auto">
                                                <img src="{{asset('images/phone-icon.png')}}" width="20" alt="">
                                            </div>
                                            <div class="my-auto">
                                                <div class="mbn-clr ml-2 fs14">1-855-WE NRIVA</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row header-bg-yellow">
                            <article class="left-logo-positions">
                                <a href="{{url('/')}}">
                                    <img src="{{asset(config('conventions.logo_display').$data['left_logo']->image_url)}}" alt="" border="0" class="img-fluid" width="125" />
                                </a>
                            </article>
                            <div class="col-12 py-1">
                                <div class="text-left">
                                    <div class="text-uppercase mb-0 web-name-font text-center lh30">NRI VASAVI ASSOCIATION (NRIVA), USA</div>
                                    <!-- <div class="text-center text-white fs20 text-capitalize">6th Global Convention
                                    </div> -->
                                    <div class="text-center pt-2">
                                        <img src="{{asset('images/8.png')}}" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center bg-navyblue"  data-toggle="sticky-onscroll">
                            <div>
                                <nav class="navbar navbar-expand-lg navbar-light p-0 justify-content-center d-none d-lg-block">
                                    <!-- Navbar links -->
                                    <div class="collapse navbar-collapse" id="collapsibleNavbar">
                                        <ul class="navbar-nav">
                                             <li class="nav-item active">
                                                <a class="nav-link lh20" href="{{url('/')}}">
                                                    <img src="{{asset('images/home.png')}}" alt="" />
                                                </a>
                                            </li>
                                     <li class="nav-item dropdown dropdownn">
                                        <a class="nav-link" href="">
                                            Registration
                                        </a>
                                        <div class="dropdown-menu submenu s-row">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-unstyled">
                                                          <li>
                                                            <a href="{{ url('bookticket') }}" class="">Convention Registration</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ url('exhibits-reservation') }}" class="">Exhibit Registration</a>
                                                        </li>
                                                        {{--   <li>
                                                            <a href="{{ url('youthActivities-regestration') }}" class="">Youth Activities Registration</a>
                                                        </li>  --}}
                                                          <li>
                                                            <a href="{{ url('shathamanam-bhavathi-registration') }}" class="">Shathamanam Bhavathi Registration</a>
                                                        </li> 
                                                          <li>
                                                            <a href="{{ url('awards') }}?page=regpage" class="">Registration For NRIVA Awards</a>
                                                        </li>  
                                                         <li>
                                                            <a href="{{ url('matrimony_registration') }}" class="">Matrimony Registration</a>
                                                        </li> 
                                                         <li>
                                                            <a href="{{ url('corporate_sponsors_registration') }}" class="">Corporate Sponsors Registration</a>
                                                        </li> 
                                                         <li>
                                                            <a href="{{ url('youth_banquet_registration') }}" class="">Youth Banquet Registration</a>
                                                        </li>

                                                        <li>
                                                            <a href="{{ url('business_conference_registration') }}" class="">Business Conference Registration</a>
                                                        </li> 
 

                                                    {{--
                                                         <li>
                                                            <a href="{{ url('souvenir-registration') }}" class="">Souvenir Registration</a>
                                                        </li>   --}}
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>

                                            @foreach($data['menu'] as $menu) 

                                                @if($loop->iteration==2)
                                                {{--
                                                     <li class="nav-item dropdown dropdownn">
                                                    <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                                        Programs
                                                    </a>
                                                   
                                                   <div class="dropdown-menu submenu l-menu">
                                                             <div class="row">
                                                                @foreach( $data['programsMenu']->chunk(4) as $chunk)
                                                               <div class="col-md-6">
                                                                     <ul class="list-unstyled">
                                                                        @foreach ($chunk as $submenu)
                                                                            <li>
                                                                                <a href="{{url('programs/'.$submenu->id)}}" class="">{{  $submenu->program_name }}</a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                         
                                                                </div> 
                                                                @endforeach
                                                            </div> 
                                                        </div>--}}
                                                </li>

                                                @endif

                                                <li class="nav-item {{$menu->slug==null? 'dropdown dropdownn': ''}}">
                                                    <a class="nav-link"  href="{{config('conventions.APP_URL')}}/{{$menu->slug==null ? '#': $menu->slug }}"  id="navbardrop" {{$menu->slug==null? 'data-toggle="dropdown"': ''}}>
                                                        {{$menu->name}}
                                                    </a>
                                                    <div class="dropdown-menu submenu  s-row ">
                                                            <div class="row">
                                                                @foreach( $menu->submenus->chunk(4) as $chunk)
                                                                <div class="col-md-12">
                                                                    <ul class="list-unstyled">
                                                                        @foreach ($chunk as $submenu)
                                                                             <li>
                                                                                <a href="{{config('conventions.APP_URL')}}/{{$submenu->slug }}" class="">{{  $submenu->name }}</a>
                                                                            </li> 
                                                                        @endforeach
                                                                    </ul>

                                                                </div> 
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                </li>
                                              @endforeach
                                            
                                         <li class="nav-item ">
                                                    <a class="nav-link lh25 text-white" href="{{url('contact-us')}}">
                                                        Contact Us
                                                    </a>
                                                </li>
                                            @guest
                                                 <li class="nav-item ">
                                                    <a class="nav-link lh25 text-white" href="{{route('login')}}">
                                                        Login
                                                    </a>
                                                </li> 
                                            @endguest

                                             @auth
                                            <li class="nav-item dropdown dropdownn">
                                                <a class="nav-link py-1" href="#" id="navbardrop" data-toggle="dropdown">
                                                     <img src="images/avtar1.png" alt="" > Welcome {{ ucwords(Auth::user()->first_name)}}
                                                </a>
                                                <div class="dropdown-menu user-icon submenu s-row">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    <a href="{{ url('myaccount') }}">My Dashboard</a>
                                                                </li>
                                                                 <li>
                                                                    <a href="{{ url('change_password') }}">Change Password</a>
                                                                </li>
                                                                <li>
                                                                    <form method="POST" action="{{ route('logout') }}">
                                                                        @csrf

                                                                        <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                                                this.closest('form').submit();">
                                                                            {{ __('Log Out') }}
                                                                        </x-dropdown-link>
                                                                    </form>
                                                                    <!--  <a href="#">Logout</a> -->
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                             @endauth
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>


                        <article class="tabhorizontal-hide right-logo-positions r-p8">
                            <a href="{{url('/')}}">
                            @if($data['right_logo'] !=null )
                            <img src="{{asset(config('conventions.logo_display').$data['right_logo']->image_url)}}" class="img-fluid rounded-circle" width="125" border="0" alt="" />
                           @endif
                            </a>
                        </article>
                    </div>
                </div>
            </div>
        </header>

        <!-- Header Mobile View -->

        <div class="container-fluid d-block d-lg-none header-bg" data-toggle="sticky-onscroll">
            <div class="row">
                <div class="col-12 px-0">
                   
                    <div class=" mobile-header-bg">
                        <div class="row">
                            <div class="col-12 pt-1">
                                <div class="text-right px-3 px-lg-4">
                                    <div class="mobile-location">
                                        <div>July 2nd, 3rd & 4th 2022</div>
                                        <div>
                                             {{$data['venue']->location?? ""}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <nav class="navbar-dark d-block d-lg-none pt-0" data-toggle="sticky-onscroll">
                            <div class="navbar px-0 px-sm-1 pt-0 pb-1">
                                <div class="col-2 col-md-2">
                                    <div>
                                        <a class="navbar-brand py-0 mr-0" href="index.php">
                                            <img src="{{asset(config('conventions.logo_display').$data['left_logo']->image_url)}}" alt="" border="0" class="img-fluid logo-size-responsive" />
                                        </a>
                                    </div>
                                </div>

                                <div class="col-8 col-md-8 px-0">
                                    <div class="my-auto">
                                        <div class="text-center pt-0 pb-1">
                                            <div class="web-name-font">NRI VASAVI ASSOCIATION (NRIVA), USA</div>
                                            <div>
                                    <img src="{{asset('images/8.png')}}" class="img-fluid" alt="">
                                </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-2 col-md-2">
                                    <div class="text-right">
                                        <a href="index.php">
                                            <img src="{{asset(config('conventions.logo_display').$data['right_logo']->image_url)}}" class="img-fluid logo-size-responsive rounded-circle" border="0" alt="" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </nav>

                        <div class="row pb-2"> 
                            <div class="col-12 my-auto pr-0">
                                <div class="nav-item pl-0">
                                    <a class="nav-link pl-2 pr-0 py-0" href="#">
                                        <span class="text-navyblue font-weight-bold" onclick="openNav()"><i class="fas fs30">&#xf039;</i><span class="menuu-text">MENU</span></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="mySidenav" class="sidenav d-block d-lg-none">
                        <a href="javascript:void(0)" class="closebtn px-3 py-2" onclick="closeNav()">&times;</a>
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-headerr collapsed" data-toggle="collapse" aria-expanded="false">
                                    <span class="title"> 
                                        <a href="{{url('/')}}">Home
                                        </a>
                                    </span>
                                </div>
                            </div>
                             <div class="card">
                                <div class="card-headerr collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    <span class="title">Registration</span>
                                    <span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>
                                </div>
                                <div id="collapseOne" class="collapse" data-parent="#accordionExample">
                                    <div class="card-bodyy p0">
                                        <ul>
                                            <li>
                                                <a href="{{ url('bookticket') }}" class="">Convention Registration</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('exhibits-reservation') }}" class="">Exhibit Registration</a>
                                            </li>
                                          {{--   <li>
                                                <a href="{{ url('youthActivities-regestration') }}" class="">Youth Activities Registration</a>
                                            </li> --}} 
                                            <li>
                                                <a href="{{ url('shathamanam-bhavathi-registration') }}" class="">Shathamanam Bhavathi Registration</a>
                                            </li> 
                                             <li>
                                                <a href="{{ url('matrimony_registration') }}" class="">Matrimony Registration</a>
                                            </li> 
                                              <li>
                                                <a href="{{ url('awards') }}?page=regpage" class="">Registration For NRIVA Awards</a>
                                            </li> 
                                             <li>
                                                <a href="{{ url('corporate_sponsors_registration') }}" class="">Corporate Sponsors Registration</a>
                                            </li> 
                                            <li>
                                                            <a href="{{ url('youth_banquet_registration') }}" class="">Youth Banquet Registration</a>
                                                        </li>
                                            <li>
                                                <a href="{{ url('business_conference_registration') }}" class="">Business Conference Registration</a>
                                            </li> 

                                            
                                        </ul>
                                    </div>
                                </div>
                            </div> 
                            @foreach($data['menu'] as $menu)
                            <div class="card">
                                <div class="card-headerr collapsed" data-toggle="collapse" data-target="#{{$menu->name}}" aria-expanded="false" aria-controls="{{$menu->name}}">
                                    <span class="title">
                                        @if($menu->slug==null)
                                            <span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>
                                        @endif
                                        <a class="nav-link p-0"  href="{{config('conventions.APP_URL')}}/{{$menu->slug==null ? '#': $menu->slug }}"  id="navbardrop" {{$menu->slug==null? 'data-toggle="dropdown"': '' }}>
                                       {{$menu->name}}
                                        </a>
                                    </span>
                                </div>
                                <div id="{{$menu->name}}" class="collapse" data-parent="#accordionExample">
                                    <div class="card-bodyy p0">
                                        {{--  $menu->submenus --}}
                                        @foreach( $menu->submenus->chunk(4) as $chunk)
                                        <div class="col-12">
                                            <ul class="list-unstyled">
                                                @foreach ($chunk as $submenu)
                                                     <li>
                                                        <a href="{{config('conventions.APP_URL')}}/{{$submenu->slug }}" class="">{{  $submenu->name }}</a>
                                                    </li> 
                                                @endforeach
                                            </ul>
                                        </div> 
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                               @endforeach

                        <div class="card">
                            <div class="card-headerr collapsed" data-toggle="collapse" aria-expanded="false">
                                <span class="title">
                                    <a href="{{url('contact-us')}}">Contact us
                                    </a>
                                </span>
                            </div>
                        </div>

                               @guest

                         <div class="card">
                            <div class="card-headerr collapsed" data-toggle="collapse" aria-expanded="false">
                                <span class="title">
                                    <a href="{{route('login')}}">Login
                                    </a>
                                </span>
                            </div>
                        </div> 
                        @endguest
                           @auth
                        <div class="card">
                            <div class="card-headerr collapsed" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="false" aria-controls="collapseOne1">
                                <span class="title">{{Auth::user()->first_name}}</span>
                                <span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>
                            </div>
                            <div id="collapseOne1" class="collapse" data-parent="#accordionExample">
                                <div class="card-bodyy p0">
                                    <ul>
                                        <li>
                                             <a href="{{ url('myaccount') }}">My Dashboard</a>
                                        </li>
                                        <li>
                                            <form method="POST" action="{{ route('logout') }}">
                                                        @csrf
                                            <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                            this.closest('form').submit();">
                                                            {{ __('Log Out') }}
                                                    </x-dropdown-link>
                                        </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endauth

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- End of Mobile Version -->