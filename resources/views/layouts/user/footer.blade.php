<footer>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-5">
                <div class="row registration-block">
                    <div class="col-12">
                        <h5 class="pb-3">REGISTER</h5>
                    </div>
                    <div class="col-12 col-sm-6 col-md-12">
                       <ul class="list-unstyled">
                            <li>
                                <a href="{{url('bookticket')}}"><span>Convention Registration</span></a>
                            </li>
                            <li>
                                <a href="{{url('exhibits-reservation')}}"><span>Exhibit Registration</span></a>
                            </li>
                            <li>
                                <a href="{{url('shathamanam-bhavathi-registration')}}"><span>shathamanam Bhavathi Registration</span></a>
                            </li>
                            <li>
                                <a href="{{ url('awards') }}?page=regpage" class="">Registration For NRIVA Awards</a>
                            </li>
                              <li>
                                <a href="{{url('matrimony_registration')}}"><span>Matrimony Registration</span></a>
                            </li> 
                            <li>
                                <a href="{{url('corporate_sponsors_registration')}}"><span>Corporate Sponsors Registration</span></a>
                            </li> 
                           {{--  <li>
                                <a href="{{url('souvenir-registration')}}"><span>Souvenir Registration</span></a>
                            </li> --}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-7">
                <div class="row leadership-block">
                    <div class="col-12 col-sm-6 col-md-6">
                        <h5 class="pb-3">LEADERSHIP</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="{{url('/')}}#Our_Leadership_Team"><span>Our Leadership Team</span></a>
                            </li>
                          {{--   <li>
                                <a href="#"><span>Convention Advisors</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Executive Committee</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Convention Management Office</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Board Of Directors</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Advisory Council</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Convenor Circle</span></a>
                            </li> --}}
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6">
                        <h5 class="pb-3">CONVENTION</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="https://nriva.org/about-us" target="_blank" ><span>About NRIVA</span></a>
                            </li>
                            <li>
                                <a href="{{url('/')}}#our-Donors"><span>Our Donors</span></a>
                            </li>
                            <!-- <li>
                                <a href="#"><span>Contact Us</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Flyers</span></a>
                            </li> -->
                            <!-- <li>
                                <a href="page_under_construction.php"><span>Souvenir Sponsorship Package</span></a>
                            </li> -->
                            <li>
                                <a href="{{url('documents/2022-Chicago-Convention-Exhibit_Booths_Package.pdf')}}" target="_blank" ><span>Exhibits Package Details</span></a>
                            </li>
                            <li>
                               Total Visitor Count : {{4100+$data['visitors_count'] }}
                            </li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </footer>
    <section class="copy-right-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-10 col-md-8 col-lg-6 my-2 my-sm-auto">
                <div>
                    &copy; 2022 NRI Vasavi Association, Inc.. All rights reserved.
                </div>
            </div>
            <div class="col-12 col-sm-2 col-md-4 col-lg-6 my-3 my-sm-auto">
                <div class="float-center float-sm-right">
                    <ul class="social-media-icons mb-0">
                        <li>
                            <a href="https://www.facebook.com/groups/nriva/"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCkr4pstLIjp9bb48MNBUT5g"><i class="fab fa-youtube"></i></a>
                        </li>
                        

                    </ul>
                  

                </div>
            </div>
        </div>
    </div>
    </section>

    <!-- All scripts -->

    <!-- jQuery library -->

    <script src="{{ asset('js/jquery.js') }}"></script>

    <!-- Bootstrap JavaScript -->

    <script src="{{ asset('js/bootstrapjs.js') }}"></script>

    <!--Popups js-->

    <script src="{{ asset('js/popperjs.js') }}"></script>

    <!--Datatables js-->

    <script src="{{ asset('js/datatables.js') }}"></script>

    <!-- Extra js -->

    <script src="{{ asset('js/extrajquery.js') }}"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script src="{{ asset('js/common.js') }}"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
    
	<script type="text/javascript" src="{{ asset('js/jquery.countdown.min.js') }}"></script>
    
    <script src="js/gallery.js"></script> 

    	<script class="source" type="text/javascript">
	
		$('#example').countdown({
			date: '07/02/22 17:00:00', // TODO Date format: 07/27/2017 17:00:00
			offset: +2, // TODO Your Timezone Offset
			day: 'Day',
			days: 'Days',
			hideOnComplete: true
		}, function (container) {
			//alert('Done!');
		});
	</script>  

    <script type="text/javascript">


        $(document).ready(function () {
           

            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none",
            });

            $(".zoom").hover(
                function () {
                    $(this).addClass("transition");
                },
                function () {
                    $(this).removeClass("transition");
                }
            );

            
        $('[data-countdown]').each(function() {
            var $this = $(this), finalDate = $(this).data('countdown');
            $this.countdown(finalDate, function(event) {
                $this.html(event.strftime('%D days %H:%M:%S'));
            });
        });

        });
    </script>

    <script>
    $(document).ready(function(){
        $("#myAlert").on('closed.bs.alert', function(){
            
        });
    });
    </script>

    <script type="text/javascript">
        // Sticky navbar
        // =========================
        $(document).ready(function () {
            // Custom function which toggles between sticky class (is-sticky)
            var stickyToggle = function (sticky, stickyWrapper, scrollElement) {
                var stickyHeight = sticky.outerHeight();
                var stickyTop = stickyWrapper.offset().top;
                if (scrollElement.scrollTop() >= stickyTop) {
                    stickyWrapper.height(stickyHeight);
                    sticky.addClass("is-sticky");
                } else {
                    sticky.removeClass("is-sticky");
                    stickyWrapper.height("auto");
                }
            };

            // Find all data-toggle="sticky-onscroll" elements
            $('[data-toggle="sticky-onscroll"]').each(function () {
                var sticky = $(this);
                var stickyWrapper = $("<div>").addClass("sticky-wrapper"); // insert hidden element to maintain actual top offset on page
                sticky.before(stickyWrapper);
                sticky.addClass("sticky");

                // Scroll & resize events
                $(window).on("scroll.sticky-onscroll resize.sticky-onscroll", function () {
                    stickyToggle(sticky, stickyWrapper, $(this));
                });

                // On page load
                stickyToggle(sticky, stickyWrapper, $(window));
            });
        });
    </script>
    
    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "280px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }
    </script>

    <script>
        AOS.init({
            duration: 2000,
        });
    </script>

    <!-- Venue Carousel -->

    <script type="text/javascript">
        $('.carousel-item', '.show-neighbors').each(function(){
          var next = $(this).next();
          if (! next.length) {
            next = $(this).siblings(':first');
          }
          next.children(':first-child').clone().appendTo($(this));
        }).each(function(){
          var prev = $(this).prev();
          if (! prev.length) {
            prev = $(this).siblings(':last');
          }
          prev.children(':nth-last-child(2)').clone().prependTo($(this));
        });
    </script>

    <script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
    </script>

    <script>
        $(".btn-small").click(function() {
         $("#"+$(this).data("attribute")).modal("show");
         var a = $(this).data("location")
         $("#"+a).trigger('mouseover');
        });

    </script>


