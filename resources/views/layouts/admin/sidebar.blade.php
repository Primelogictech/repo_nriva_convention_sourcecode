	<!-- Sidebar -->

	{{--@dd(in_array(Auth::user()->email,Config::get('conventions.admin_emails')))
	@dd($data['user_menu_array']) --}}
	<div class="sidebar" id="sidebar">
	    <div class="sidebar-inner slimscroll">
	        <div id="sidebar-menu" class="sidebar-menu">
	        	
	            <ul>
	            	<li class="">
			                	<a href="{{url('admin')}}"><i class="fe fe-home"></i> <span>Dashboard</span></a>
			        </li>
			        @if(!in_array(Auth::user()->email,Config::get('conventions.admin_emails')))
			        <?php $data['menu_array']=$data['user_menu_array'][Auth::user()->member_id]; ?>
			        @endif
	            		@forelse($data['menu_array'] as $key=>$menus )
		            	
			            	<li class="submenu">
			            	     <a href="#"><i class="fe fe-document"></i> <span>{{$data['main_menu_name'][$key]}}</span></a>
			            	     <ul style="display: none;">
			            	     	@forelse($menus as $item )
			            	     	<li><a href="{{url($item['slug'])}}">{{$item['name']}}</a></li>
			            	     	@empty
				                    <li>No Pages To Show</li>
				                    @endforelse
								</ul>
		            		</li>
	                @empty
                    <li>No Pages To Show</li>
                    @endforelse

	                <!--
	                <li class="submenu">
	                    <a href="#"><i class="fe fe-document"></i> <span>Registrations</span> <span class="menu-arrow"></span></a>
	                    <ul style="display: none;">
	                        <li><a href="{{url('admin/registrations')}}">Member Registrations</a></li>
	                        <li><a href="{{url('admin/exhibit-registrations')}}">Exhibits Registration</a></li>
	                        {{-- <li><a href="{{url('admin/youth-activities-registrations')}}">Youth Activities Registration</a></li> --}}
	                        <li><a href="{{url('admin/satamanam-bhavati-registrations')}}">Shathamanam Bhavathi Registration</a></li>
	                        <li><a href="{{url('admin/matrimonyRegistrations')}}">Matrimony Registration</a></li>
	                        <li><a href="{{url('admin/mr_missRegistrations')}}">Mr, Mrs, Miss and Master NRIVA Registration</a></li>
	                        <li><a href="{{url('admin/souvenirRegistrations')}}">Souvenir Registration</a></li>
	                    </ul>
	                </li>
	                <li class="submenu">
	                    <a href="#"><i class="fe fe-document"></i> <span> Home Page </span> <span class="menu-arrow"></span></a>
	                    <ul style="display: none;">
	                        <li><a href="{{route('banner.index')}}">Upload Banners</a></li>
	                        <li><a href="{{route('venue.index')}}">Change Venue Date & Time</a></li>
	                        <li><a href="{{route('message.index')}}">Add Messages</a></li>
	                        <li><a href="{{route('member.index')}}">Members</a></li>
	                        <li><a href="{{route('event.index')}}">Events</a></li>
	                        <li><a href="{{route('schedule.index')}}">Schedule</a></li>
	                        <li><a href="{{route('video.index')}}">Youtube Videos</a></li>
	                    </ul>
	                </li>
	                <li class="submenu">
	                    <a href="#"><i class="fe fe-document"></i> <span> Content Management </span> <span class="menu-arrow"></span></a>
	                    <ul style="display: none;">
	                        <li><a href="{{route('menu.index')}}">Menu Management</a></li>
	                        <li><a href="{{route('submenu.index')}}">Menu Items Management</a></li>
	                        <li><a href="{{route('page.index')}}">Manage Pages</a></li>
	                        <li><a href="{{route('registration-page-content-update')}}">Registration Page Content</a></li>
	                    </ul>
	                </li>
	                <li class="submenu">
	                    <a href="#"><i class="fe fe-document subdrop"></i> <span> Master </span> <span class="menu-arrow"></span></a>
	                    <ul>
	                        <li><a href="{{route('sponsor-category-type.index') }}">Sponsor Category Type</a></li>
	                        <li><a href="{{route('sponsor-category.index')}}">Manage Sponsor Category</a></li>
	                        <li><a href="{{route('right-logo.index') }}">Upload Right Side Logo</a></li>
	                        <li><a href="{{route('left-logo.index') }}">Upload Left Side Logo</a></li>
	                        <li><a href="{{ route('leadership-type.index')}}">Leadership Type</a></li>
	                        <li><a href="{{ route('invitee.index')}}">Invitees Type</a></li>
	                        <li><a href="{{ route('donortype.index')}}">Donor Type</a></li>
	                        <li><a href="{{route('designation.index')}}">Designation Type</a></li>
	                        <li><a href="{{route('paymenttype.index')}}">Payment Type</a></li>
	                        <li><a href="{{route('benefittype.index')}}">Benifits Type</a></li>
	                        <li><a href="{{route('exhibitor-type.index')}}">Exhibitor Type</a></li>
	                        <li><a href="{{route('committe.index')}}">Committes </a></li>
	                        <li><a href="{{route('mrandmisstype.index')}}">Mr And Miss Age </a></li>
	                        <li><a href="{{route('souvenir-sponsortype.index')}}">Souvenir Sponsor Type </a></li>
	                    </ul>
	                </li>-->
	            </ul>
	        </div>
	    </div>
	</div>
	<!-- /Sidebar -->
