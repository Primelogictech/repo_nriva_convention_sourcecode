@extends('layouts.user.base') @section('content')

<style type="text/css">
    @media (min-width: 1200px) {
        .container,
        .container-lg,
        .container-md,
        .container-sm,
        .container-xl {
            max-width: 1250px;
        }
    }
    .nav-link {
        color: #050a30;
    }
    .awards li {
        padding: 5px;
    }
    .card {
        background-color: transparent;
        border: 0px solid;
    }
    .card-header {
        padding: 0px;
    }
    .card-header a {
        background: #784d98;
        color: #fff;
        padding: 0.75rem 1.25rem;
        width: 100%;
        display: block;
    }
    .p-radio-btn {
        position: absolute;
        top: 3px;
    }
    .card-header a::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(../images/plus.png);
    }
    .card-header a[aria-expanded="true"]::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(../images/minus.png);
    }
</style>

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">{{$committe->name}} Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact">
                                    <span>Committee Contact : </span>
                                    <span class="overflow-wrap_break-word">{{$committe->committe_email}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll" role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                @if($committe->name=='Awards and Recognition')
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#guidelines" id="Awards_and_Recognition_Guidelines" role="tab" data-toggle="tab">Guidelines and Selection Criteria</a>
                                </li>
                                @endif
                                @if($committe->name=='Awards and Recognition')

                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#registration" id="reg-page" role="tab" data-toggle="tab">Register Here</a>
                                </li>
                                @endif
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2 px-3 px-lg-4">
                                    <div class="row">
                                        @forelse($Committe_members as $member)
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                                            <div class="committee-member-block">
                                                <div>
                                                    <img src=" @if($member->image_url) {{asset(config('conventions.member_display'))}}/{{$member->image_url??""}} @else ../images/no-image1.jpg @endif " alt="" />
                                                </div>
                                                <h6 class="committee-member-name">{{ucwords( strtolower($member->name) ) }}</h6>
                                                <div class="committee-member-designation">{{ $member->designation->name }}</div>
                                            </div>
                                        </div>
                                        @empty
                                        <h5>No Members</h5>
                                        @endforelse
                                    </div>
                                </div>
                            </div>

                            <!-- Guidelines -->

                            @if($committe->name=='Awards and Recognition')
                            <div role="tabpanel" class="tab-pane fade" id="guidelines">
                                <div class="col-12 shadow-small py-4 px-3 px-lg-4">
                                    <div class="row">
                                        <div class="col-12">
                                            <h5 class="mb-4 text-warningg text-center">Frequently Asked Questions</h5>
                                            <div id="accordion" class="o-accordion">
                                                <ul class="list-unstyled">
                                                    <li class="pb15 mb15 border-bottom">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <a class="card-link fs18" data-toggle="collapse" href="#collapseOne">
                                                                    Who is eligible for nomination?
                                                                </a>
                                                            </div>
                                                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                                                <div class="card-body">
                                                                    <p class="mb-0">
                                                                        All contestants must be vasavites, preferably NRIVA members. Please register at <a href="http://nriva.org" target="_blank">nriva.org</a>. We encourage all to become
                                                                        Life members of NRIVA.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="pb15 mb15 border-bottom">
                                                        <div class="card">
                                                            <div class="card-header fs18">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                                                    Any limit on the number of awards for each category?
                                                                </a>
                                                            </div>
                                                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                                                <div class="card-body">
                                                                    <p class="mb-0">
                                                                        NRIVA objective is to recognize the significant accomplishments by Vasavites. There are multiple awards for each category.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="pb15 mb15 border-bottom">
                                                        <div class="card">
                                                            <div class="card-header fs18">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                                                    How old do accomplishments or contributions need to be?
                                                                </a>
                                                            </div>
                                                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                                                <div class="card-body">
                                                                    <p class="mb-0">
                                                                        Accomplishments and contributions should be between 2020 and 2022.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="pb15 mb15 border-bottom">
                                                        <div class="card">
                                                            <div class="card-header fs18">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                                                                    Are the prior award winners eligible for nominations this term?
                                                                </a>
                                                            </div>
                                                            <div id="collapseFour" class="collapse" data-parent="#accordion">
                                                                <div class="card-body">
                                                                    <p class="mb-0">
                                                                        We encourage the new nominees who were not awarded in the same category during prior conventions.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="pb15 mb15 border-bottom">
                                                        <div class="card">
                                                            <div class="card-header fs18">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseFive">
                                                                    What is the selection process?
                                                                </a>
                                                            </div>
                                                            <div id="collapseFive" class="collapse" data-parent="#accordion">
                                                                <div class="card-body">
                                                                    <p class="mb-0">
                                                                        All nominations along with the supporting documents are reviewed thoroughly. Award winners are chosen by a Judge panel established by NRIVA Convention leadership.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="pb15 mb15 border-bottom">
                                                        <div class="card">
                                                            <div class="card-header fs18">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseSix">
                                                                    I know someone who is the right fit for an award. Can I submit a nomination on their behalf?
                                                                </a>
                                                            </div>
                                                            <div id="collapseSix" class="collapse" data-parent="#accordion">
                                                                <div class="card-body">
                                                                    <p class="mb-0">
                                                                        Yes, you can submit nominations for anybody in the Vasavite community. Please provide supporting documentation on their behalf.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="pb15 mb15 border-bottom">
                                                        <div class="card">
                                                            <div class="card-header fs18">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseSeven">
                                                                    I am not sure if I will attend the convention in person, can I apply?
                                                                </a>
                                                            </div>
                                                            <div id="collapseSeven" class="collapse" data-parent="#accordion">
                                                                <div class="card-body">
                                                                    <p class="mb-0">
                                                                        Yes. In person presence is not mandatory and has no impact on award selection. But we highly recommend you attend the convention and showcase your achivements.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="pb15 mb15 border-bottom">
                                                        <div class="card">
                                                            <div class="card-header fs18">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseEight">
                                                                    Can I submit a nomination for more than one category?
                                                                </a>
                                                            </div>
                                                            <div id="collapseEight" class="collapse" data-parent="#accordion">
                                                                <div class="card-body">
                                                                    <p class="mb-0">
                                                                        Yes but a separate nomination is required for each category. Supporting documents applicable to that category are required.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="pb15 mb15 border-bottom">
                                                        <div class="card">
                                                            <div class="card-header fs18">
                                                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseNine">
                                                                    What is the criteria for the individual award category?
                                                                </a>
                                                            </div>
                                                            <div id="collapseNine" class="collapse" data-parent="#accordion">
                                                                <div class="card-body">
                                                                    <p class="mb-0">
                                                                        The nomination form will present the details upon selection of the individual award category.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                            <h5 class="text-warningg py-3 text-center">
                                                NRIVA Mission 20ONE Chapter Award - during NRIVA Mission 20ONE term
                                            </h5>
                                            <p class="pt-2">
                                                Selection Criteria - guidelines (Score card - each section will have 5 points max):
                                            </p>
                                            <div class="fs16 py-1">
                                                Chapter achievements measurable - during NRIVA Mission 20ONE term in no certain order :
                                            </div>
                                            <ul class="faqs-ul">
                                                <li>Programs initiated at chapter level that are adopted to national level</li>
                                                <li>Number of new life members (this can be retrieved from technology team)</li>
                                                <li>How many AAS sponsorships (Dollar Amount and/or Number of sponsorships - AAS Standing committee help can be obtained for this if needed)</li>
                                                <li>How many AAP sponsorships (Dollar Amount and/or Number of sponsorships - AAP Standing committee help can be obtained for this if needed)</li>
                                                <li>How many AAV sponsorships (Dollar Amount and/or Number of sponsorships - AAV Standing committee help can be obtained for this if needed)</li>
                                                <li>RAH fundraising participation (Dollar Amount - Treasury Team help can be obtained for this if needed)</li>
                                                <li>Seva participation - training support - educational support - community services -</li>
                                                <li>How many vasavites volunteered at national level for committees, special programs like webinars</li>
                                                <li>How many and what events conducted at chapter level - (needs pictures, videos, how many attended).</li>
                                                <li>How many families joined in NRIVA website as members - (this can be retrieved from technology team)</li>
                                                <li>Women participation at chapter level - brief description to quantify - pictures/videos' etc.,)</li>
                                                <li>Youth participation at chapter level.</li>
                                                <li>Kids/Youth activities promoted at chapter level - brief description to quantify - pictures/videos' etc.,)</li>
                                                <li>How many PVSA recipients - (Bronze/Silver/Gold medal wise numbers)</li>
                                                <li>Vasavi Jayanthi participation</li>
                                                <li>Contribution towards Global Convention(volunteer hours)</li>
                                            </ul>
                                            <p>
                                                All above metrics are measured relative to chapter size.
                                            </p>
                                            <p>
                                                Our aim is to ensure a level playing field between all chapters regardless of their size. NRIVA will choose multiple winners based on chapter size and the activities performed during the term.
                                            </p>
                                            <p>
                                                When we announce, it should give a wow factor
                                            </p>
                                            <h5 class="text-warningg py-3 text-center">
                                                NRIVA Mission 20ONE Community Impact Award - during NRIVA Mission 20ONE term
                                            </h5>
                                            <p class="pt-3">
                                                Selection Criteria - guidelines in no certain order (Score card - each section will have 10 points max):
                                            </p>
                                            <ul class="faqs-ul mb-0">
                                                <li>
                                                    What are unique aspects initiated within the committee in this term?
                                                </li>
                                                <li>
                                                    How many events organized - explain details (pictures - videos - significance)
                                                </li>
                                                <li>
                                                    How many vasavites (families) participated
                                                </li>
                                                <li>
                                                    What is the impact - briefly explain for the each of the below and quantify the impact

                                                    <ol type="i">
                                                        <li>value to the community</li>
                                                        <li>value or benefit to the vasavites</li>
                                                        <li>Support to build one NRIVA</li>
                                                    </ol>
                                                </li>
                                                <li>
                                                    Any Targets given by Mission 20ONE and where this committee stands on that.
                                                </li>
                                                <li>
                                                    Program impact towards NRIVA goal : educate, empower, elevate
                                                </li>
                                            </ul>
                                            <h5 class="text-warningg py-3 text-center">Note to all committee members</h5>
                                            <p class="text-center">
                                                These two awards are to encourage and keep healthy/friendly competition among chapters and their leads and core members to take their chapters to next level and also make more and more
                                                awareness about NRIVA and its Motto - <strong>Connecting to Serve</strong> and develop extended family relationships among vasavites wherever they live.
                                                <strong>You are not alone and We are here to support you</strong>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif

                            <!-- Registration -->

                            @if($committe->name=='Awards and Recognition')
                            <div role="tabpanel" class="tab-pane fade text-white" id="registration">
                                <div class="col-12 shadow-small py-4 px-3 px-lg-4">
                                    <div class="row px-2 px-md-3 px-lg-3">
                                        <div class="col-12 awards-reg-bg p-4">
                                            <div class="text-center">
                                                <h4 class="font-weight-normal">
                                                    <i>Awards and Recognitions Committee</i>
                                                </h4>
                                                <h1 class="font-weight-bold">
                                                    HONORING FOR EXCELLENCE IN FOLLOWING CATEGORIES
                                                </h1>
                                                <ul class="list-unstyled fs20 awards">
                                                    <li>
                                                        NRIVA Professional Excellence Award
                                                    </li>
                                                    <li>
                                                        NRIVA Women Excellence Award
                                                    </li>
                                                    <li>
                                                        NRIVA Youth Excellence Award (Academics)
                                                    </li>
                                                    <li>
                                                        NRIVA Youth Excellence Award (Non-Academics)
                                                    </li>
                                                    <li>
                                                        NRIVA Entrepreneur Award
                                                    </li>
                                                    <li>
                                                        NRIVA Community Service Award
                                                    </li>
                                                    <li>
                                                        NRIVA Mission 20ONE Chapter Award
                                                    </li>
                                                    <li>
                                                        NRIVA Mission 20ONE Community Impact Award
                                                    </li>
                                                </ul>
                                                <p class="py-4">
                                                    The nominating period for the <span class="text-decoration-underline">2022 NRIVA Awards</span> will remain open until May 15, 2022. Awards will be announced and presented and facilitated
                                                    during the Convention on July 2nd to 4th, 2022.
                                                </p>
                                                <div>
                                                    <p class="fs20 text-white">
                                                        Please submit your nomination
                                                        <a href="https://docs.google.com/forms/d/e/1FAIpQLSckaHyuxZR8kVcjKtWogIuXqttaYCaLSOqBxMqdSkdVEYipCA/viewform" target="_blank" class="text-decoration-underline">here</a>
                                                    </p>
                                                    <p>Refer to <a class="text-decoration-underline" id="faq_btn">FAQ</a> for additional details.</p>
                                                    <p>
                                                        You can reach out to the awards committee: awards@nriva.org
                                                    </p>
                                                </div>

                                                <p class="fs13 pt-4">
                                                    Only one application per entity is permitted. Please submit any appropriate materials(video, audio clips, articles, certificates, documents, etc.) along with nomination. All communication
                                                    regarding credentials will be directed to the individual submitting the credential request(s) on behalf of your or your chapter via email. The Awards are administered by the Awards and
                                                    Recognition Committee, and all decisions regarding the eligibility of works and the administration of nominations are exclusively the convention leadership responsibility.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('javascript')
<script>
    //Awards_and_Recognition_Guidelines
    $("#faq_btn").click(function () {
        $("#Awards_and_Recognition_Guidelines").trigger("click");
    });

    $(document).ready(function () {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split("&"),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split("=");

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };

        var page = getUrlParameter("page");

        if (page == "regpage") {
            $("#reg-page").trigger("click");
        }
    });
</script>
@endsection @endsection
