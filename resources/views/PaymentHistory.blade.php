@extends('layouts.user.base')
@section('content')



<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12">
                <h5>Transaction Details</h5>
                <a href="{{route('myaccount')}}" data-toggle="tooltip" title="" class="float-right-back-btn btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>

            </div>
            <div class="col-12">
                <div class="card-body px-0">
                    <div class="table-responsive">
                        @if(Auth::user()->total_amount>0)
                        <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    <th>SNo</th>
                                    <th>Transaction Data</th>
                                    <th>Payment Methord</th>
                                    <th>Amount</th>
                                    <th>Payment Status</th>
                                    <th>Account Status</th>
                                    <th>Paid Towards</th>
                                    <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($payments as $payment)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                    <b>Date</b>
                                    {{paymentDate($payment) }}
                                    <br>
                                    <b>Transaction Id </b>
                                    {{ $payment->unique_id_for_payment }}
                                    @if(isset($payment->paymentmethord) && $payment->paymentmethord->name == config('conventions.other_name_db'))
                                         <br>
                                        <b>Payee Name</b>
                                        {{ $payment->more_info['payee_name']??""  }}
                                        <br>
                                    <b>Company Name</b>
                                        {{ $payment->more_info['company_name'] ??"" }}
                                        
                                         @if(isset($payment->more_info['document']))
                                            @if(!empty($payment->more_info['document']))
                                            <a href="{{asset('public/storage/user/'.$payment->more_info['document'])}}" download>Download Document</a>
                                            @endif
                                        @endif
                                    @endif

                                    @if(isset($payment->paymentmethord) && $payment->paymentmethord->name == config('conventions.check_name_db'))
                                         <br>
                                        <b>Bank Name</b>
                                        {{ $payment->more_info['bank_name'] ??"" }}
                                        <br>
                                    <b>Check Received By</b>
                                        {{ $payment->more_info['check_received_by'] ??"" }}
                                    @endif
                                    </td>
                                    <td>{{ ucfirst($payment->paymentmethord->name??"Free")  }}</td>
                                    
                                    @if ( $payment->payment_made_towards=='Paid Towards Exhibit registration')
                                        <td> $ {{  number_format($payment->paid_amount)  }} </td>
                                    @else
                                        <td> $ {{  number_format($payment->payment_amount)  }} </td>
                                    @endif
                                    
                                    <td> {{ ucfirst($payment->payment_status) }} </td>
                                    <td> {{ ucfirst($payment->account_status) }} </td>
                                    <td> {{ ucfirst($payment->payment_made_towards) }} </td>
                                </tr>
                                @empty

                                @endforelse
                            </tbody>
                        </table>
                        @else
                        <h5> You have not Registered for the event Please click <a href="bookticket">here<a /> for register</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@section('javascript')

@endsection


@endsection
