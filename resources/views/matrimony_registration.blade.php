@extends('layouts.user.base')
@section('content')

<link href="{{ asset('css/slick.min.css') }}" rel="stylesheet" />

<link href="{{ asset('css/slick-theme.min.css') }}" rel="stylesheet" />

<!-- <link href="{{ asset('css/vertical-slick-carousel.css') }}" rel="stylesheet" /> -->

<style type="text/css">
p{
    font-size: 15px;
}
.card {
    background-color: transparent;
    border: 0px solid;
}
.card-header {
    padding: 0px;
}
.card-header a {
    background: #784d98;
    color: #fff;
    padding: 0.75rem 1.25rem;
    width: 100%;
    display: block;
}
.p-radio-btn {
    position: absolute;
    top: 3px;
}
.card-header a::after {
    position: absolute;
    top: 0px;
    right: 0px;
    width: 60px;
    height: 52px;
    content: "";
    background-image: url(images/plus.png);
}
.card-header a[aria-expanded="true"]::after {
    position: absolute;
    top: 0px;
    right: 0px;
    width: 60px;
    height: 52px;
    content: "";
    background-image: url(images/minus.png);
}
textarea:focus{
    outline: none;
}
.sc-image{
    width: 200px;
}
.blink{
        text-align: center;
        padding: 10px;
    }
    .blink-text{
        font-size: 16px;
        color: red;
        animation: blink 2s linear infinite;
    }
@keyframes blink{
0%{opacity: 0;}
50%{opacity: .5;}
100%{opacity: 1;}
}
@media (max-width: 576px){
    .sc-image{
        width: 250px;
    }
}
</style>
<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12 pb-4">
                        <div>
                            <img src="images/banners/matrimony.jpg" class="img-fluid w-100" alt="" />
                        </div>
                       {{--  <p class="text-center text-red fs16 pt25 mb-2">
                            Convention registration is closed, we reached our capacity. Thank you for your great support.
                        </p>
                        <p class="text-center text-red fs16 mb-0">
                            Please continue to watch for any further updates.
                        </p> --}}
                    </div>
                </div>
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif  @if ($message = Request::get('error'))
                <div class="row px-1 mx-0 mx-lg-5 px-lg-0">
                    <div class="col-12 px-0 mb-3">
                        <div class="alert alert-danger alert-block py-2">
                            <button type="button" class="close lh15" data-dismiss="alert">×</button>
                            <strong class="mr-2">This member not registred with nriva eedujodu. Please register in Nriva.org with eedujodu membership.</strong>
                        </div>
                    </div>
                </div>
                @endif
                <div class="row px-1 mx-0 mx-lg-5 px-lg-0">
                    <div class="col-12 mb-4">
                        <h5 class="text-violet text-center mb-0">Convention Registration and Eedu-Jodu profile are required for matrimony event registration</h5>
                        <div class="blink"><span class="blink-text">Last day to register for Convention Matrimony is June 10th 2022</span></div>
                    </div>

                    <!-- <div class="col-12 mb-3">
                        <h6 class="text-violet px-0">Early Registration Privileges
                            <span>(Register On or Before June 10th 2022)</span>
                        </h6>
                    </div>  -->
                    <div class="col-12 col-lg-12 shadow-small px-sm-20 p40 p-md-4 mb-4">
                        <form id="form" action="{{url('check_matrimony_email')}}" method="post" enctype="multipart/form-data">
                            <div class="row">
                                @csrf

                                <div class="col-12 col-md-7 col-lg-8">
                                    <div class="row">
                                        <div class="col-12">
                                            <h6 class="mb-3 text-violet">Matrimony Event Registration for In-Person Participation</h6>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-7 col-lg-8 my-auto">
                                                    <label>Eedu-Jodu Profile ID:</label><span class="text-red">*</span>
                                                    <div>
                                                        <input type="text" name="profile_id" id="profile_id" class="form-control" 
                                                        @guest
                                                            disabled
                                                        @endguest
                                                        required placeholder="Profile Id" />
                                                    </div>

                                                    @guest
                                                    <div class="alert px-1 py-1">
                                                        <div class="text-red">
                                                           <strong>*Convention Registration is mandatory to register for matrimony convention. </strong>
                                                        </div>
                                                       <div>
                                                           <strong>If you have already registered for convention, please click <a href="{{url('login')}}"> here </a> to login</strong>
                                                       </div>
                                                    </div>
                                                    @endguest
                                                    
                                                    @if ($message = Session::get('user_error'))
                                                    <div class="alert px-1 py-1" style="color: #fb0c22;">
                                                      
                                                       <strong>{{ $message }}</strong>
                                                    </div>
                                                    @endif
                                                </div>
                                                @guest
                                                <div class="form-group col-12 col-md-5 col-lg-4">
                                                    <label class="d-none d-md-block"></label>
                                                    <div class="mat-reg-btn">
                                                        <input type="submit" class="btn btn-violet" value="Register Here" />
                                                    </div>
                                                </div>
                                                @endguest
                                                @auth
                                                <div class="form-group col-12 col-md-5 col-lg-4
                                                @if (Session::get('user_error'))
                                                @else
                                                my-auto
                                                @endif
                                                 ">
                                                    <label class="d-none d-md-block"></label>
                                                    <div class="mat-reg-btn">
                                                        <input type="submit" class="btn btn-violet" value="Register Here" />
                                                    </div>
                                                </div>
                                                @endauth
                                            </div>
                                            <p class="mt20 mb10"><strong>Note :</strong><span> If you do not have Eedu-Jodu Profile ID, please go to the following link to register first.</span></p>
                                            <p class="mb15">
                                                <a href="https://nriva.org/eedu-jodu/register" target="_blank">https://nriva.org/eedu-jodu/register/</a>
                                            </p>
                                        </div>
                                        <div class="col-12 py-3">
                                            <h6 class="text-violet">Planned Events / Schedule</h6>
                                            <ul class="fs15 pl-4 pl-lg-5">
                                                <li>
                                                    <strong>Ashirvachanam - </strong>Participants only - Saturday July 2nd - From 9 AM to 10 AM
                                                </li>
                                                <li>
                                                    <strong>Profile Introductions - </strong>Participants are <strong>REQUIRED (Family Welcome)</strong>
                                                    <ul>
                                                        <li class="list-style-square">Saturday July 2nd - From 11 AM to 4 PM </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <strong>Formal Banquet - </strong>Participants Only - Saturday July 2nd at 7 PM
                                                </li>
                                                <li>
                                                    <strong>Parents Meet - </strong>Only Parents - Sunday July 3rd at 3 PM
                                                </li>
                                                <li>
                                                    <strong>Connect Over Coffee - </strong>Sessions for USA Raised at 3 PM and Global Participants at 4 PM On Sunday July 3rd
                                                </li>
                                                <li>
                                                    <strong>In Person Private Meetings - </strong> July 3rd and 4th From 10 AM to 12 PM
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-12 pt-1 pb-3">
                                            <h6 class="text-violet">Early Registration Privileges
                                                <span>(Register On or Before June 10th 2022)</span>
                                            </h6>
                                            <ul class="fs15">
                                                <li>Designated Priority Seating</li>
                                                <li>Better Reach for Your Profile By early access</li>
                                                <li>Assigned Relationship Manager Assistance</li>
                                                <li>Support of Relationship Manager before, during and after event</li>
                                                <li>Choose One on One Sessions</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-5 col-lg-4">
                                    <div class="row">
                                        <div class="col-12">
                                            <h6 class="text-violet mb-3">Videos</h6>
                                        </div>
                                        <div class="col-12 shadow-small py-3">
                                            <div class="my-2">
                                                <iframe
                                                    width="100%"
                                                    class="border-radius-5 mr-video"
                                                    src="https://www.youtube.com/embed/AtrlMdAo6hU"
                                                    title="YouTube video player"
                                                    frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen
                                                ></iframe>
                                            </div>
                                            <div class="my-2">
                                                <iframe
                                                    width="100%"
                                                    class="border-radius-5 mr-video"
                                                    src="https://www.youtube.com/embed/aaeHe-7aNus"
                                                    title="YouTube video player"
                                                    frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen
                                                ></iframe>
                                            </div>
                                            <div class="my-2">
                                                <iframe
                                                    width="100%"
                                                    class="border-radius-5 mr-video"
                                                    src="https://www.youtube.com/embed/EXIwgFHwbn4"
                                                    title="YouTube video player"
                                                    frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen
                                                ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-12">
                                    <h6 class="text-violet mb-3">Gallery</h6>
                                </div>
                                <div class="col-12 mb-4">
                                    <div id="slick-carousel" class="shadow-small p-3">
                                        <div><a><img src="{{asset('images/matrimony-gallery/couple-1.jpg')}}" class="sc-image" /></a></div>
                                        <div><a><img src="{{asset('images/matrimony-gallery/couple-2.jpg')}}" class="sc-image" /></a></div>
                                        <div><a><img src="{{asset('images/matrimony-gallery/couple-3.jpg')}}" class="sc-image" /></a></div>
                                        <div><a><img src="{{asset('images/matrimony-gallery/couple-4.jpg')}}" class="sc-image" /></a></div>
                                        <div><a><img src="{{asset('images/matrimony-gallery/couple-5.jpg')}}" class="sc-image" /></a></div>
                                        <div><a><img src="{{asset('images/matrimony-gallery/couple-6.jpg')}}" class="sc-image" /></a></div>
                                        <div><a><img src="{{asset('images/matrimony-gallery/couple-7.jpg')}}" class="sc-image" /></a></div>
                                        <div><a><img src="{{asset('images/matrimony-gallery/couple-8.jpg')}}" class="sc-image" /></a></div>
                                        <div><a><img src="{{asset('images/matrimony-gallery/couple-9.jpg')}}" class="sc-image" /></a></div>
                                        <div><a><img src="{{asset('images/matrimony-gallery/couples.jpg')}}" class="sc-image" /></a></div>
                                    </div>
                               
                            </div>
                                </div>
                                <!-- <div class="col-12 mb-3">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 thumb">
                                            <a href="{{asset('images/matrimony-gallery/couple-1.jpg')}}" class="fancybox" rel="ligthbox">
                                                <img src="{{asset('images/matrimony-gallery/couple-1.jpg')}}" class="zoom img-fluid" alt="" />
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 thumb">
                                            <a href="{{asset('images/matrimony-gallery/couple-2.jpg')}}" class="fancybox" rel="ligthbox">
                                                <img src="{{asset('images/matrimony-gallery/couple-2.jpg')}}" class="zoom img-fluid" alt="" />
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 thumb">
                                            <a href="{{asset('images/matrimony-gallery/couple-3.jpg')}}" class="fancybox" rel="ligthbox">
                                                <img src="{{asset('images/matrimony-gallery/couple-3.jpg')}}" class="zoom img-fluid" alt="" />
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 thumb">
                                            <a href="{{asset('images/matrimony-gallery/couple-4.jpg')}}" class="fancybox" rel="ligthbox">
                                                <img src="{{asset('images/matrimony-gallery/couple-4.jpg')}}" class="zoom img-fluid" alt="" />
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 thumb">
                                            <a href="{{asset('images/matrimony-gallery/couple-5.jpg')}}" class="fancybox" rel="ligthbox">
                                                <img src="{{asset('images/matrimony-gallery/couple-5.jpg')}}" class="zoom img-fluid" alt="" />
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 thumb">
                                            <a href="{{asset('images/matrimony-gallery/couple-6.jpg')}}" class="fancybox" rel="ligthbox">
                                                <img src="{{asset('images/matrimony-gallery/couple-6.jpg')}}" class="zoom img-fluid" alt="" />
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 thumb">
                                            <a href="{{asset('images/matrimony-gallery/couples.jpg')}}" class="fancybox" rel="ligthbox">
                                                <img src="{{asset('images/matrimony-gallery/couples.jpg')}}" class="zoom img-fluid" alt="" />
                                            </a>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-12 pt-3">
                                    <h5 class="text-violet text-center mb-4">Frequently Asked Questions</h5>
                                    <div id="accordion" class="o-accordion">
                                        <ul class="list-unstyled">
                                            <li class="pb15 mb15 border-bottom">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <a class="card-link fs18" data-toggle="collapse" href="#collapseOne">
                                                            Ashirvachanam
                                                        </a>
                                                    </div>
                                                    <div id="collapseOne" class="collapse" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <h6 class="text-danger mb-2">Ashirvachanam / Blessings Ceremony</h6>
                                                            <div class="fs16 font-weight-bold text-violet mb-2">Saturday July 2nd - at 9 AM to 10 AM</div>
                                                            <div class="fs16 text-violet mb-3">
                                                                <div>
                                                                    <strong>Participant Dress Code: </strong>Indian Traditional
                                                                </div>
                                                            </div>
                                                            <p>
                                                                Event will start with the Ganapathi Pooja performed by the honorable priests and blessings will be cherished to all the prospective Brides and Grooms.    
                                                            </p>
                                                            <p>
                                                                Brides and Grooms are encouraged to participate in this event to take blessings with Indian traditional attire.
                                                            </p>
                                                            <p>
                                                                Only Matrimony Participants can attend the ceremony. 
                                                            </p>
                                                            <p>
                                                                "NRIVA Wishes you all Shigrameva Kalyanam Masthu" 
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="pb15 mb15 border-bottom">
                                                <div class="card">
                                                    <div class="card-header fs18">
                                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                                            Profile Introductions / Pelli Choopulu
                                                        </a>
                                                    </div>
                                                    <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <h6 class="text-danger mb-2">Participant introductions - Pelli Choopulu</h6>
                                                            <div class="fs16 font-weight-bold text-violet mb-2">Saturday July 2nd - From 12 PM to 5 PM</div>
                                                            <div class="fs16 text-violet mb-3">
                                                                <div>
                                                                    <strong>Participant Dress Code :   </strong> Indian Formal
                                                                </div>
                                                            </div>
                                                            <p>
                                                                Registered Participants and family members can take part in Profile Introductions.     
                                                            </p>
                                                            <p>
                                                                Please <strong class="text-maroon">"Check In"</strong> at the counter outside NIRVANA room to pick up your badge and know your time slot for participation.
                                                            </p>
                                                            <p>
                                                                This is the Core segment for Matrimony and all the Participants are required to attend. Families are welcome but not mandatory. The participants' profiles are displayed on the screen and are called on the stage to present themselves for 2 mins, parents/family members are welcome to accompany them on the stage.   
                                                            </p>
                                                            <p>
                                                                <h6 class="text-danger"> Suggested Information to Present:</h6>
                                                                <ol class="fs15">
                                                                    <li>&nbsp;&nbsp;&nbsp;Information that is not present in the profile.</li>
                                                                    <li>&nbsp;&nbsp;&nbsp;Something unique or special about them.</li>
                                                                    <li>&nbsp;&nbsp;&nbsp;What they are looking for in their spouse, details could include their interest, location, hobby, short term or long term goals.</li>
                                                                    <li>&nbsp;&nbsp;&nbsp;Anything else that can stand out about your personality.</li>
                                                                </ol>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="pb15 mb15 border-bottom">
                                                <div class="card">
                                                    <div class="card-header fs18">
                                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                                            Formal Banquet
                                                        </a>
                                                    </div>
                                                    <div id="collapseThree" class="collapse" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <h6 class="text-danger mb-2">Formal Dinner -  Theme "   Dress to Impress "</h6>
                                                            <div class="fs16 text-violet mb-2">
                                                                <div>
                                                                <strong>Saturday July 2nd at 7 PM -  </strong> Banquet Night
                                                                </div>
                                                            </div>
                                                            <div class="fs16 text-violet font-weight-bold mb-2">
                                                                Restricted to Registered Participants
                                                            </div>
                                                            <div class="fs16 text-violet mb-3">
                                                                <div>
                                                                <strong>Dress Code : &nbsp;Western Attire : </strong> &nbsp;Suits / Gowns
                                                                </div>
                                                            </div>
                                                            <p>
                                                                This event is an entertaining event.    
                                                            </p>
                                                            <p>
                                                                We will have unique Ballroom / Bollywood instructors to take you through the evening and make this a fun, networking and memorable event where you can make close connections of friendship and companionship.   Be prepared to eat, party,  bring your cool spirits and dancing shoes for this event. 
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="pb15 mb15 border-bottom">
                                                <div class="card">
                                                    <div class="card-header fs18">
                                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                                                            Parents Meet 
                                                        </a>
                                                    </div>
                                                    <div id="collapseFour" class="collapse" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <h6 class="text-danger mb-2">Parents Meet - Only Parents </h6>
                                                            <div class="fs16 text-violet mb-2">
                                                                <div>
                                                                <strong>Sunday July 3rd</strong>
                                                                </div>
                                                            </div>
                                                            <div class="fs16 text-violet font-weight-bold mb-2">
                                                                Parents only ( Restricted to Registered Participant’s Parents)
                                                            </div>
                                                            <div class="fs16 text-violet font-weight-bold mb-2">
                                                                Panel Discussions  / Honorable Guest Speakers
                                                            </div>
                                                            <div class="fs16 text-violet font-weight-bold mb-2">
                                                                Talk about several topics like:
                                                            </div>
                                                            <ul class="list-unstyled fs15">
                                                                <li>" Reasons behind Traditions " </li>
                                                                <li>" How to motivate the youth to take the steps in the right direction "</li>
                                                                <li>
                                                                    " How do we educate them in finding peace and love in their married life " Many more and open to questions.
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="pb15 mb15 border-bottom">
                                                <div class="card">
                                                    <div class="card-header fs18">
                                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseFive">
                                                            Connect Over Coffee
                                                        </a>
                                                    </div>
                                                    <div id="collapseFive" class="collapse" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <h6 class="text-danger mb-2">Connect Over Coffee </h6>
                                                            <div class="fs16 text-violet mb-2">
                                                                <div>
                                                                <strong>Sunday July 3rd</strong>
                                                                </div>
                                                            </div>
                                                            <p class="mb10">
                                                                This program will be held for all participants,  we will have 2 sessions:
                                                            </p>
                                                            <ul class="fs15 mb20">
                                                                <li>Only  "USA born or raised" Participants.</li>
                                                                <li>
                                                                    All Global Participants - Mix of everyone.
                                                                </li>
                                                            </ul>
                                                            <p>
                                                                Come hangout with your peers to play games(board games) or chit chat while having Coffee, encouraging the participants to be open and network with others to  make good friends and meet the special person.
                                                            </p>
                                                            <p>
                                                                Many appropriate and fun games will be made available to interact with each other.
                                                            </p>
                                                            <p>
                                                                Please wear  your badge during all Matrimony events to be able to recognize each other.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="pb15 mb15 border-bottom">
                                                <div class="card">
                                                    <div class="card-header fs18">
                                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseSix">
                                                            One on One In Person Meetings
                                                        </a>
                                                    </div>
                                                    <div id="collapseSix" class="collapse" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <h6 class="text-danger mb-2">One on One Meetings - In-Person Private Meeting </h6>
                                                            <div class="fs16 text-violet font-weight-bold mb-2">
                                                                July 3rd and 4th at Breakout Rooms
                                                            </div>
                                                            <p>
                                                                Through this program you can meet with interested families.  Contact your assigned Relationship Manager and provide  your interested family details.  Due to logistics issues, we would take a maximum of 3 requests to meet. 
                                                            </p>
                                                            <p>
                                                                Although you are welcome to directly contact the other family and plan to meet anywhere.
                                                            </p>
                                                            <p>
                                                                These meetings are arranged based on mutual interests.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="pb15 mb15 border-bottom">
                                                <div class="card">
                                                    <div class="card-header fs18">
                                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseSeven">
                                                            Options to book accomodations
                                                        </a>
                                                    </div>
                                                    <div id="collapseSeven" class="collapse" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <div class="py-2">
                                                                <h6 class="text-danger mb-2">Hyatt Regency Schaumburg, Chicago</h6>
                                                                <p>
                                                                    <a href="https://www.hyatt.com/en-US/hotel/illinois/hyatt-regency-schaumburg-chicago/chirw?src=corp_lclb_gmb_seo_chirw" class="text-violet" target="_blank">https://www.hyatt.com/en-US/hotel/illinois/hyatt-regency-schaumburg-chicago/chirw?src=corp_lclb_gmb_seo_chirw</a>
                                                                </p>
                                                            </div>
                                                            <div class="py-2">
                                                                <h6 class="text-danger mb-2">SpringHill Suites Chicago Schaumburg/Woodfield Mall</h6>
                                                                <p>
                                                                    <a href="https://www.marriott.com/en-us/hotels/chisg-springhill-suites-chicago-schaumburg-woodfield-mall/overview/?scid=bb1a189a-fec3-4d19-a255-54ba596febe2&y_source=1_MTI1NjkwMS03MTUtbG9jYXRpb24ud2Vic2l0ZQ%3D%3D" class="text-violet" target="_blank">https://www.marriott.com/en-us/hotels/chisg-springhill-suites-chicago-schaumburg-woodfield-mall/overview/?scid=bb1a189a-fec3-4d19-a255-54ba596febe2&y_source=1_MTI1NjkwMS03MTUtbG9jYXRpb24ud2Vic2l0ZQ%3D%3D</a>
                                                                </p>
                                                            </div>
                                                            <div class="py-2">
                                                                <h6 class="text-danger mb-2">Wyndham Garden Schaumburg Chicago Northwest</h6>
                                                                <p>
                                                                    <a href="https://www.wyndhamhotels.com/wyndham-garden/schaumburg-illinois/wyndham-garden-schaumburg-chicago-northwest/overview?CID=LC:GN::GGL:RIO:National:49046&iata=00093796" class="text-violet" target="_blank">https://www.wyndhamhotels.com/wyndham-garden/schaumburg-illinois/wyndham-garden-schaumburg-chicago-northwest/overview?CID=LC:GN::GGL:RIO:National:49046&iata=00093796</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="pb15 mb15 border-bottom">
                                                <div class="card">
                                                    <div class="card-header fs18">
                                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseEight">
                                                            Matrimony Team Contact and Disclaimer
                                                        </a>
                                                    </div>
                                                    <div id="collapseEight" class="collapse" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <p>
                                                                Aspiring Brides and Grooms world-wide are encouraged to register  
                                                            </p>
                                                            <p>
                                                                We are thrilled to bring the matrimony service In-Person during this 6th Global NRIVA Convention at Chicago,   Aspiring Brides and Grooms world-wide are encouraged to register for this event and make plans to attend this In-Person event to find your soulmate. 
                                                            </p>
                                                            <p>
                                                                All Registrants will be assigned a “Relationship Manager (RM) ”  in June 2022, who will contact you to answer any question that you may have and assist you throughout this process until after the convention.
                                                            </p>
                                                            <p>
                                                                NRIVA has the right to capture any pictures and videos and use them for publishing.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title " style="float: left;">Registration</h4>
      </div>

      <div class="modal-body">
        <div class="alert alert-success">Please enter nriva password and procced to registration</div>
        <form class="" action="{{url('newAuthorize')}}" method="post">
         @csrf
         <input type="hidden" name="email" id="new_email">
         <div class="form-row">

            <div class="form-group col-12 col-md-6 col-lg-6">
                <label>Password:</label><span style="color:red;">*</span>
                <div>
                    <input type="password" name="password" id="password"  class="form-control" placeholder="Enter Password" required />
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>


</div>
<div class="modal-footer">
    <a href="{{url('/')}}" class="btn btn-danger" >Close</a>
</div>

</div>

</div>
</div> --}}

<div class="modal fade" id="t_and_c" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title " style="float: left;">Terms and Conditions</h4>
      </div>

      <div class="modal-body">
        <div class="">
            <ul>
                <li>Terms and Conditions</li>
                <li>Terms and Conditions</li>
                <li>Terms and Conditions</li>
                <li>Terms and Conditions</li>
            </ul>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>

</div>

</div>
</div>


@section('javascript')
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script src="{{ asset('js/vertical-carousel-slider.js') }}"></script>

<script src="{{ asset('js/slick.min.js') }}"></script>

<!-- Horizontal Slick Slider -->

<script type="text/javascript">
    $("#slick-carousel").slick({
        slidesToShow: 4,
        autoplay: true,
        autoplaySpeed: 0,
        speed: 2000,
        cssEase: "linear",
        infinite: true,
        focusOnSelect: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    arrows: false,
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    slidesToShow: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                },
            },
        ],
    });
</script>

<script>

 $.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
});
 $.validator.addMethod("numberss", function(value, element) {
    return this.optional(element) || value == value.match(/^[0-9) (-]+$/);
});


        // $("#form").validate({


        //     rules: {

        //         first_name: "required alpha",
        //         last_name: "required alpha",
        //         age: "required numberss",
        //         gothram: "required",
        //         rasi: "required",
        //         state: "required",
        //         country: "required",
        //         address: "required",
        //         email: "required",
        //         mobile :{
        //             maxlength: 14,
        //             numberss:true
        //         },
        //         'zip_code': {
        //                 digits: true
        //             },
        //     },
        //      messages: {

        //           "first_name": {
        //             alpha: "First name should not contain numbers.",
        //         },
        //          "last_name": {
        //             alpha: "Last name should not contain numbers.",
        //         },

        //          "mobile": {
        //             maxlength: "Max length is  14.",
        //             numberss: "Numbers only"
        //         }
        //     },
        // });

        @if(!Auth::user())
        $('#email').on('change',function(){
  //      checking();

});
        $('#profile_id').on('change',function(){
//        checking();

});
        function checking() {
         {{--  $('.email_valid_msg').text('')
         var email = $('#email').val();
         var profile_id = $('#profile_id').val();
         $.ajax({
            type: 'GET',
            url: "{{url('check_matrimony_email')}}?email="+email+"&profile_id="+profile_id,
            success:function(data){
              if(data!="failed"){
                $('#new_email').val(data);
                $('#myModal').modal({backdrop: 'static', keyboard: false});
                $('#myModal').modal('show');
            }else{
                window.location.href = "{{url('matrimony_registration')}}?error=not_registered";
            }


        }
    }); --}}
     }
     @endif
 </script>

 @endsection

 @endsection