@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12">
                <h5>Features</h5>
                <a href="{{route('myaccount')}}" data-toggle="tooltip" title="" class="float-right-back-btn btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
            <div class="col-12">
                <div class="card-body px-0">
                    <div class="table-responsive">
                        <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    <th>Sl NO.</th>
                                    <th>
                                        <div>Sponsor Category</div>
                                        <div>(Amount)</div>
                                    </th>
                                    <th style="min-width: 300px;">Benefits</th>
                                  {{--   <th>Assigned</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>
                                        <div>{{ $categorydetails->donortype->name }}</div>
                                        <div class="text-orange font-weight-bold pt-2">${{ $categorydetails->amount }}</div>
                                    </td>
                                    <td>
                                        <ul class="list-unstyled pl20 mb-0">
                                            @foreach ($categorydetails->benfits as $benfit)
                                                    <li class="py-1">{{ $loop->iteration}} .&nbsp;&nbsp;&nbsp;&nbsp;{{ $benfit->name  }} {{ $benfit->pivot->count!=null ?   '('.$benfit->pivot->count .')' : ""}} </li>

                                                    @if($benfit->has_image)
                                                    <a href="" style="display:none" id="anc_ben_img_{{ $benfit->id }}" target="blank"><img src="" id="ben_img_{{ $benfit->id }}"  width="50" height="50"></a>
                                                    @else
                                                    <li class="py-1  " id="benfit_{{$benfit->id}}"><b>Assigned:</b> </li>
                                                    @endif
                                                    @if($benfit->has_image)
                                                    <form action="{{url('update_banner_image')}}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                        <input type="file" id="benfit_Image_{{$benfit->id}}" name="benfitimage[{{ $benfit->id }}]" onchange="ValidateFile(this)"  accept="image/*"> 
                                                        <input type="hidden" name="benfit_id" value="{{$benfit->id}}" > 
                                                        <input type="submit" value="Update">  
                                                    </form>
                                                        @endif
                                            @endforeach
                                            </ul>
                                    </td>
                                   {{--  <td>
                                        <ul class="list-unstyled pl20 mb-0">
                                            @foreach ($categorydetails->benfits as $benfit)
                                            {{ $loop->iteration}}.&nbsp;&nbsp;&nbsp;&nbsp;
                                            @if($benfit->has_image)
                                             <a href="" id="anc_ben_img_{{ $benfit->id }}" target="blank"><img src="" id="ben_img_{{ $benfit->id }}"  width="50" height="50"></a>
                                            @else
                                            <li class="py-1  " id="benfit_{{$benfit->id}}"> .&nbsp;&nbsp;&nbsp;&nbsp;</li>
                                            @endif
                                            <br>
                                            @endforeach
                                        </ul>
                                    </td> --}}
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@section('javascript')
<script>
     var _validFileExtensions = [".jpg", ".jpeg",  ".png"];    
        function ValidateFile(oInput) {
           // $(oInput).parent().parent().parent().find('.file_name ').text(' ')
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            //filename=sFileName.split('\\')[(sFileName.split('\\').length)-1]
                            //alert(filename)
                           // $(oInput).parent().parent().parent().find('.file_name ').text(filename)
                            break;
                        }
                    }
                    
                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }

    benfits = JSON.parse('{!!json_encode($benfits) !!}')
    benfit_image_path= "{{asset(config('conventions.benfit_image_display'))}}"

    benfits.forEach(benfit => {
        $('#benfit_' + benfit.pivot.benfit_id).html('<b>Assigned:</b> ' + benfit.pivot.content)
    });
    bannerimages = JSON.parse('{!!json_encode($bannerimages) !!}')

    bannerimages.forEach(benfit => {
       $('#ben_img_'+ benfit.benfit_id).attr('src',benfit_image_path +'/' + benfit.image_url)
       $('#anc_ben_img_'+ benfit.benfit_id).attr('href',benfit_image_path +'/' + benfit.image_url)
       $('#anc_ben_img_'+ benfit.benfit_id).show();
    });

</script>
@endsection


@endsection
