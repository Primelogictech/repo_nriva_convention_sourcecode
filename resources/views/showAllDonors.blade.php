@extends('layouts.user.base')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    Our Donors
                </div>
            </div>
        </div>
    </div>
</div>
<section class="container-md">
    <div class="row">
        <div class="col-12 col-lg-12 py-0 py-lg-1">
            <div class="">
                <ul class="list-unstyled donors-tabs-ul d-lg-flex justify-content-center">
                   {{--  <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none donors_active_tab_btn" target-id="#event-sponsors">

                            <span>Grand&nbsp;Event&nbsp;Sponsors</span>
                            <h6 class="text-center">$50000+</h6>
                        </a>
                    </li> --}}
                    @foreach ($donor_users_arr as $key=>$donor)
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none  {{$loop->first?'donors_active_tab_btn1 donors_active_tab_btn':''}}" target-id="#{{$loop->iteration}}"  >
                            <span>{{explode('~',$key)[0]}}</span>
                           {{--  <h6 class="text-center">${{explode('~',$key)[1]}}+</h6> --}}
                        </a>
                    </li>
                    @endforeach
                    {{--<li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#sapphire">
                            <span>Sapphire</span>
                            <h6 class="text-center">$25000+</h6>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#diamond">
                            <span>Diamond</span>
                            <h6 class="text-center">$20000+</h6>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#platinum">
                            <span>Platinum</span>
                            <h6 class="text-center">$15000+</h6>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#gold">
                            <span>Gold</span>
                            <h6 class="text-center">$10000+</h6>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#silver">
                            <span>Silver</span>
                            <h6 class="text-center">$5000+</h6>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#bronze">
                            <span>Bronze</span>
                            <h6 class="text-center">$2500+</h6>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#patron">
                            <span>Patron</span>
                            <h6 class="text-center">$1500+</h6>
                        </a>
                    </li>--}}
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid px-3 px-md-5 px-lg-5">
    <div class="row px-3 px-lg-4">
        
        <div class="col-12 col-lg-12 " id="event-sponsors">
            <div class="row">
                
              {{--   @foreach ($grand_event_users as $key=>$donor)
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1" >
                    <div class="donors-block">
                        <div>
                            <img src="{{$donor['img_url']}}" alt="" />
                        </div>
                        <div class="donor-name">{{$donor['name']}}</div>
                    </div>
                </div>
                @endforeach --}}
                {{--<div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/sunitha_rachapalli.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mrs. Sunita Rachapalli</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/giri_kothamasu.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Giri Kothamasu</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/kiran_adimulam.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Kiran Adimulam</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/venu_mattey.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Venugopal Mattey</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/ravi_makam.jpg" alt="" />
                        </div>
                        <div class="donor-name">Dr. Ravi Makam</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/srinivas-s.jpg" alt="" />
                        </div>
                        <div class="donor-name">Dr. Srinivas Seela</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/Chittari_BOD.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Chittari Pabba</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/Bhanu_Illendra.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Bhanu Babu Ilindra</div>
                    </div>
                </div>--}}
            </div>
        </div>
        @foreach ($donor_users_arr as $key=>$donors)
        <div class="col-12 col-lg-12 " id="{{$loop->iteration}}"  style="display: none;" {{$loop->first?'':''}} >
            <div class="row">
                @foreach ($donors as $donor)
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="{{$donor['img_url']}}" alt="" />
                        </div>
                        <div class="donor-name">{{$donor['name']}}</div>
                    </div>
                </div>
                @endforeach
                {{--<div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>--}}
            </div>
        </div>
        @endforeach
        {{--<div class="col-12 col-lg-12" id="diamond" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="platinum" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-12" id="gold" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="silver" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="bronze" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="patron" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
            </div>
        </div>--}}
    </div>
</section>

<div class="donor-template" style="display:none">
     <div class="col-12 col-sm-6 col-md-6 col-lg-3 main">
        <div class="donors-block">
            <div>
                <img src="images/no-image1.jpg" class="donor-image" alt="" />
            </div>
            <div class="donor-name">Mr. Donor</div>
        </div>
    </div>
</div>


@section('javascript')

<script>

  $(document).ready(function() {
         $('.donors_active_tab_btn1').trigger('click')
        $('.first-date').addClass("donors_active_tab_btn");
        //$('.first-leadership-type').addClass("leadership_active_tab_btn");
    })

  $(".donors-nav-link").click(function() {
        ajaxCall($(this).data('url'), 'get', null, showDataInOurDonors)
        //active_tab
        $(".donors_active_tab_btn").removeClass("donors-nav-link");
        $(".donors_active_tab_btn").removeClass("donors_active_tab_btn");
        $(this).addClass("donors-nav-link");
        $(this).addClass("donors_active_tab_btn");
    });


    function showDataInOurDonors(data) {
        $('.donors-add-div').empty()
        if (data.length == 0) {
            $('.donors-add-div').append('<center> <p>No Donors</p> </center>');
        } else {
            for (let i = 0; i < data.length; i++) {
                 if(i>7){
                   // break
                }
                $(".donor-template").find('.main').first().clone()
                    .find(".donor-image").attr('src', "{{asset(config('conventions.member_display'))}}/" + data[i].member.image_url).end()
                    .find(".donor-name").text(data[i].member.name).end()
                    .appendTo($('.donors-add-div'));
            }
        }
    }


   $(".donors-nav-link").click(function () {
        //active_tab
        $(".donors_active_tab_btn").removeClass("donors-nav-link");
        $(".donors_active_tab_btn").removeClass("donors_active_tab_btn");
        $(this).addClass("donors-nav-link");
        $(this).addClass("donors_active_tab_btn");

        $(".donors_active_tab").hide();
        $(".donors_active_tab").removeClass("donors_active_tab");
        $($(this).attr("target-id")).addClass("donors_active_tab");
        $($(this).attr("target-id")).show();
    });

   
</script>

@endsection


@endsection
