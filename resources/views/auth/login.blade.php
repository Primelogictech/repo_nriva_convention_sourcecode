@extends('layouts.user.base')
@section('content')

<style type="text/css">
    .swal2-html-container {
        font-size: 15px;
        text-align:  left;
    }
</style>

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-8 offset-lg-2 py-2">

                @if ($errors->any())
                @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
                @endforeach
                @endif
               

                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif

                @yield('content')

                <div class="shadow-small border-radius-5 py-4 px-2 p-md-4">
                    <div class="text-center loggin-heading mb-3">Login</div>
                    <div class="text-orange loggin-heading px-3">* Please use your login details of nriva.org </div>

                    <form method="POST" action="{{ route('login') }}" class="col-12 pt-3 p-0">
                        @csrf
                        <input type="hidden" name="redirect_type" value="{{app('request')->input('type')??''}}">
                        <div class="col-12">
                            <label>Email:</label><span class="text-red">*</span>
                            <div>
                                <input type="text" id="email" type="email" name="email" value="{{app('request')->input('email')??''}}" required autofocus class="form-control" placeholder="Enter Your Email" />
                                <div class="email_valid_msg error fs20" > </div>
                                <div class="email_sucess sucess fs20" > </div>
                            </div>
                        </div>
                        {{-- <div class="col-12 my-2">
                            <label>Login with:</label><span class="text-red">*</span>
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-6">
                                    <input type="radio" class="login-radio-btn" id="with_otp" name="login_with" required value="with_otp" disabled="">
                                    <label for="with_password" class="pl-2" > Generate Verification Code</label>
                                </div>
                                <div class="col-12 col-md-6 col-lg-6">
                                    <input type="radio" class="login-radio-btn" id="with_password" name="login_with" required value="with_password">
                                    <label for="age2" class="pl-2">I Have Password</label>
                                </div>
                            </div>
                        </div> --}}

                        <div class="col-12 my-2" id="otp_div" style="display:none">
                            <div>
                                    <button id="get_opt_btn" type="button" class='login-get-otp-btn btn btn-violet w-100 border-radius-5'>Click Here to Get Verification Code to your email.</button>
                            </div>
                            <br>
                            <br>
                        <div id="otp_input_div" style="display:none">
                        
                            <label  >Email Verifiaction code Sent to your email. </label>
                            <label  >Please enter it below </label>
                            <span class="text-red"></span>
                            <div class="d-flex my-auto">
                                <div>
                                    <input type="text" type="text" data="login_otp"  name="login_otp" value=""  autofocus class="col-12  form-control login_otp" placeholder="Enter Your OTP" />
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="col-12 my-2" id="password_div" style="display:block">
                            <div class="form-group">
                                <label>Password</label><span class="text-red">*</span>
                                <div>
                                    <input type="password" type="password" name="password"  autocomplete="current-password" class="form-control password" placeholder="Password" />
                                </div>
                            </div>
                        </div>

                        <!-- <div class="col-12">
                            <div class="form-group">
                                <label>Password</label><span class="text-red">*</span>
                                <div>
                                    <input type="password" type="password" name="password" required autocomplete="current-password" class="form-control" placeholder="Password" />
                                </div>
                            </div>
                        </div> 
                        <div class="col-12">
                            <div>
                                <input type="checkbox" class="input-checkbox" name=""><span class="pl25 fs16">Keep me logged in</span>
                            </div>
                        </div>-->
                        <div class="col-12 pt-3">
                            <div>
                                <button class="btn btn-navyblue w-100 border-radius-5 text-white signin-btn"> {{ __('Login') }}</button>
                            </div>
                        </div>

                    </form>
                         <div class="col-12 pt-3">
                            <div>
                                <button  class="btn btn-orange w-100 border-radius-5 forgot_password text-white"> Forgot Password?</button>
                            {{-- <a target="_blank" href="https://nriva.org/reset">
                            </a> --}}
                            </div>
                        </div>

                </div>
                <!-- <div class="text-center pt-3 fs16">Don't have an account ? <a href="#" class="text-violet font-weight-bold">Sign Up</a></div>
            			<div class="fs16 text-center py-2">
            				<a href="#" class="text-violet font-weight-bold">Forgot Password ?</a>
            			</div> -->
            </div>
        </div>
    </div>
    </div>
    </div>
</section>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@section('javascript')
<script type="text/javascript">
    check_email();
    $('#email').on('change', function(e) {
        check_email();
    });
    function check_email() {
         var email = $('#email').val();
          $.ajax({
                type: 'GET'
                , url: "{{url('check_email')}}?email=" + email
                , success: function(data) {
                    if (data == "success") {
                        $('#with_otp').attr("checked",false);
                        $('#with_otp').attr("disabled",true);
                        $('#otp_div').hide();

                    }else{
                        $('#with_otp').attr("disabled",false);
                    }
                }
            });
        
    }
     $('.forgot_password').on('click', function(e) {


         Swal.fire({
                html:"You will be redirected to <b>https://nriva.org</b> to reset your password. After resetting the password, please come back and use your email and newly created password to login for convention. Click Continue to reset password",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continue'
        }).then((result) => {
             if(result.isConfirmed){
                   window.open('https://nriva.org/reset', '_blank');
                }
        })
            
     })


     $('#get_opt_btn').on('click', function(e) {
        e.preventDefault()
     
        $('.email_valid_msg').text('')
        $('.email_sucess').text('')
        var email = $('#email').val();
        var thiss = $(this);
        if (email != "") {
            // if (email.search('@') > 0) { }else {
            //     $('.email_valid_msg').text('Please enter a valid email')
            //     return 0;
            // }
             $('.email_valid_msg').text('Please wait ...')
            $.ajax({
                type: 'GET'
                , url: "{{url('send-otp-for-login')}}?email=" + email
                , success: function(data) {
                    if (data == "success") {
                         $('.email_valid_msg').text('')
                       // $('.email_sucess').text('Pleace check Your Mail For OTP')
                        $('#otp_input_div').show()
                      
                    } else {
                        $('.email_valid_msg').html('<p> We did not Find your details, please register <a href="{{url("bookticket")}}">here</a> </p>')
                        //window.location.href = "{{url('login')}}?error=exit&email=" + email;
                    }
                        $('#get_opt_btn').text('Click Here to Re-send Verification Code to your email.')

                }
            });
        } else {
            $('.email_valid_msg').text('Please enter email')
            //alert('Please enter email');
        }

    }); 
     $("input[name='login_with']").change(function(){
        if($(this).val()=='with_password') {
            $('#password_div').show()
            $('#otp_div').hide()
            $('#password').attr('required',true)
            $('.login_otp').attr('required',false)
            $('.password').val('');
        }else{
            $('#password_div').hide()
            $('#otp_div').show()
            $('.password').attr('required',false)
            $('.login_otp').attr('required',true)
            $('.password').val('NriVA@Testing@Password@NriVA');
        }
    });
</script>

@endsection


@endsection
