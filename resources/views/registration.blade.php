@extends('layouts.user.base')
@section('content')

<style>
.nav-link {
    color: #622595;
}
.fade:not(.show) {
    opacity: 1;
}
.sc-tabs .nav-link.active {
    border-bottom: none !important;
    background-color: #050a30;
    border-left: 0px;
    border-right: 0px;
    border-top: 0px;
    color: #ffffff;
}
.nav-tabs.sc-tabs .nav-link:focus,
.nav-tabs.sc-tabs .nav-link:hover {
    /*border-bottom: none !important;*/
    border: 0px;
}
.p-radio-btn {
    position: absolute;
    top: 6px;
}

 #captcha{
        font-size:25px;
        padding-left: 27px
    }

</style>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12 pb-3">
                        <div>
                            <img src="images/banners/convention-registration.jpg" class="img-fluid w-100" alt="">
                        </div>
                        <p class="text-center text-red pt25 mb-2">
                            Convention registration is closed, we reached our capacity. Thank you for your great support.
                        </p>
                        <p class="text-center text-red">
                            Please continue to watch for any further updates.
                        </p>
                    </div>
                </div>
               <!--  <div class="row px-4 px-lg-0">
                    <div class="col-12 col-lg-10 offset-lg-1 shadow-small px-sm-30 p40 p-md-4 mb-5"> -->
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                    <div class="row px-1 mx-0 mx-lg-5 px-lg-0">
                        <div class="col-12 col-lg-12 shadow-small px-sm-20 p40 p-md-4 mb-4">

                        <form id='form' action="{{url('bookticketstore')}}" method="post" enctype="multipart/form-data">
                            <div class="row">

                                @csrf
                                <!-- Personal Information -->

                                <div class="col-12">
                                   
                                    <h5 class="text-violet py-2">Personal Information</h5>
                                    @guest
                                   
                                   <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Email Id:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="email" required  name="email" id="email" value="" class="form-control" placeholder="Email Id" required="" />

                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Confirm Email Id:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="email" required name="confirm_email_id" id="confirm_email_id" value="" class="form-control" placeholder="Confirm Email Id" required="" />
                                            </div>
                                        </div>
                                    </div>

                                     <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Password:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="password" required  name="password" id="password" value="" class="form-control" placeholder="Password" required="" />

                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Confirm Password:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="password" required name="password_confirmation" id="password_confirmation"  class="form-control" placeholder="Confirm Password" required="" />
                                            </div>
                                        </div>
                                    </div>


                                    @endguest
                                @auth
                                    
                                
                                     <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Email Id:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="email" required {{ Auth::user()? 'readonly' : '' }}  name="email" id="email" value="{{  Auth::user()->email ?? '' }}" class="form-control" placeholder="Email Id" required="" />

                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Mobile:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" required name="mobile" id="mobile" value="{{  Auth::user()->mobile ?? '' }}" class="form-control" placeholder="Mobile" required="" />
                                            </div>
                                        </div>
                                    </div>
                                @endauth

                                  <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Mobile:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" required name="mobile" id="mobile" value="{{  Auth::user()->mobile ?? '' }}" class="form-control" placeholder="Mobile" required="" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6" id="nriva_email" style="display: none;">
                                            <label>Mail id Regestred with Nirva</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" id="nriva_email_field" class="form-control" placeholder="mail" />
                                            </div>

                                            <div>
                                                <button id="btn_get_data">Get data</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>First Name:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="first_name" id="first_name" value="{{  Auth::user()->first_name ?? '' }}" class="form-control" required placeholder="First Name" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Last Name:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" required name="last_name" id="last_name" value="{{  Auth::user()->last_name ?? '' }}" class="form-control" placeholder="Last Name" />

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Spouse First Name:</label>
                                            <div>
                                                <input type="text" name="spouse_first_name" id="spouse_first_name" class="form-control" placeholder="Spouse First Name"  value="{{  Auth::user()->spouse_first_name ?? '' }}"
                                                     @if (Auth::user())
                                                    {{  Auth::user()->spouse_first_name==null ? '' : 'readonly' }}
                                                @endif 
                                                />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Spouse Last Name:</label>
                                            <div>
                                                <input type="text" name="spouse_last_name" id="spouse_last_name" class="form-control" placeholder="Spouse Last Name"  value="{{  Auth::user()->spouse_last_name ?? '' }}" 
                                                 @if (Auth::user())
                                                 {{  Auth::user()->spouse_last_name==null ? '' : 'readonly' }}
                                                @endif
                                                 />
                                            </div>
                                        </div>
                                    </div>
                                    

                                    <div class="form-row">
                                        <div class="form-group col-7 col-sm-8 col-md-6 col-lg-6 mb-0">
                                            <label>Kid Name:</label>
                                        </div>
                                        <div class="form-group col-3 col-sm-3 col-md-5 col-lg-5 mb-0">
                                            <label>Age:</label>
                                        </div>
                                        <div class="form-group col-2 col-sm-1 col-md-1 col-lg-1">
                                        </div>
                                    </div>

                                <div class="child-template">
                                    <div class="form-row main">
                                        <div class="form-group col-7 col-sm-8 col-md-6 col-lg-6 chaild-of-main">
                                            <input type="text" name="Kid_name[]" class="form-control Kid_name" placeholder="Kid Name" />
                                        </div>

                                        <div class="form-group col-3 col-sm-3 col-md-5 col-lg-5">
                                            <input type="text" name="Kid_age[]" class="form-control Kid_age" placeholder="Age" />
                                        </div>

                                        <div class="form-group col-2 col-sm-1 col-md-1 col-lg-1">
                                            <div class="button-div">
                                                <span class="plus_n_minus_icons add-category-name" data-count="0">
                                                    <i class="fas fa-plus"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                    <div class="child-add-div"></div>
                                
                                    
                                <!-- End of Personal Information -->

                                <!-- Contact Information -->

                                <div class="col-12 px-0">
                                    <h5 class="py-2 text-violet">Contact Information</h5>
                                   
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Address line 1:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="address" value="{{ Auth::user()->address ?? "" }}"  class="form-control" placeholder="Address line 1" required="" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Address line 2:</label>
                                            <div>
                                                <input type="text" name="address2" value="{{ Auth::user()->address2 ?? ""  }}" id="zip_code" class="form-control" placeholder="Address line 2" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>City:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="city" value="{{ Auth::user()->city?? "" }}" id="city" class="form-control" placeholder="City" required="" />
                                            </div>
                                        </div>
                                        
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>State:</label><span class="text-red">*</span>
                                            <div>
                                                <select name="state" class="form-control" required="">
                                                    <option value="">Select State</option>
                                                    @foreach ($states as $state)
                                                    <option value="{{ $state->state }}" @if(Auth::user()) @if(Auth::user()->state==$state->state) {{'selected'}} @endif  @endif>{{ $state->state }}</option>
                                                    @endforeach
                                                 <option @if(Auth::user()) @if(Auth::user()->state=='Alberta') {{'selected'}} @endif  @endif value="Alberta">Alberta</option>
                                                 <option @if(Auth::user()) @if(Auth::user()->state=='British Columbia') {{'selected'}} @endif  @endif value="British Columbia">British Columbia</option>
                                                 <option @if(Auth::user()) @if(Auth::user()->state=='Manitoba') {{'selected'}} @endif  @endif value="Manitoba">Manitoba</option>
                                                 <option @if(Auth::user()) @if(Auth::user()->state=='New Brunswick') {{'selected'}} @endif  @endif value="New Brunswick">New Brunswick</option>
                                                 <option @if(Auth::user()) @if(Auth::user()->state=='Newfoundland and Labrador') {{'selected'}} @endif  @endif value="Newfoundland and Labrador">Newfoundland and Labrador</option>
                                                 <option @if(Auth::user()) @if(Auth::user()->state=='Northwest Territories') {{'selected'}} @endif  @endif value="Northwest Territories">Northwest Territories</option>
                                                 <option @if(Auth::user()) @if(Auth::user()->state=='Nova Scotia') {{'selected'}} @endif  @endif value="Nova Scotia">Nova Scotia</option>
                                                 <option @if(Auth::user()) @if(Auth::user()->state=='Nunavut') {{'selected'}} @endif  @endif value="Nunavut">Nunavut</option>
                                                 
                                                 <option @if(Auth::user()) @if(Auth::user()->state=='Ontario') {{'selected'}} @endif  @endif value="Ontario">Ontario</option>

                                                 <option @if(Auth::user()) @if(Auth::user()->state=='Prince Edward Island') {{'selected'}} @endif  @endif value="Prince Edward Island">Prince Edward Island</option>

                                                 <option @if(Auth::user()) @if(Auth::user()->state=='Quebec') {{'selected'}} @endif  @endif value="Quebec">Quebec</option>

                                                 <option @if(Auth::user()) @if(Auth::user()->state=='Saskatchewan') {{'selected'}} @endif  @endif value="Saskatchewan">Saskatchewan</option>

                                                 <option @if(Auth::user()) @if(Auth::user()->state=='Yukon') {{'selected'}} @endif  @endif value="Yukon">Yukon</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Zip Code:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="zip_code" value="{{ Auth::user()->zip_code ?? ""  }}" id="zip_code" class="form-control" placeholder="Zip Code" required="" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Country:</label><span class="text-red">*</span>
                                            <div>
                                                <select name="country" required class="form-control" required="">

                                                    <option value="" >Select Country</option>
                                                    <option selected>United States</option>
                                                    <option >Canada</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                      <div class="form-row">
                                        
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Chapter:</label>
                                            <div>
                                                
                                                 <select name="chapter" class="form-control" >
                                                    <option value="">Select Chapter</option>
                                                    @foreach ($chapters as $chapter)
                                                        <option 
                                                        @auth
                                                            {{ Auth::user()->chapter==$chapter->name? 'selected' : ''  }}
                                                        @endauth
                                                         value="{{$chapter->name}}" >{{$chapter->name}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                           
                                        </div>
                                    </div>

                                  
                                </div>

                            <!-- end of Contact Information -->

                            <hr class="dashed-hr" />

                            <div class="row">
                                <div class="col-12">
                                    <div class="queri-text">For any queries contact <a href="mailto: registration@nriva.org" target="_blank" class="text-orange">registration@nriva.org</a></div>
                                    <div class="pt-3">
                                        <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
                                            <li class="nav-item d-inline-flex">
                                                <a class="nav-link px-4 border active type_tab" href="#Sponsorship" role="tab" data-toggle="tab">Donor&nbsp;Registration</a>
                                            </li>
                                            <li class="nav-item d-inline-flex">
                                                <a class="nav-link px-4 border " href="#Registration" role="tab" data-toggle="tab">General&nbsp;Registration</a>
                                            </li>
                                            
                                        </ul>
                                    </div>
                                
                                    <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in "  id="Registration">
                                <div class="col-12 shadow-small py-3">
                                <div class="row" id='family-or-individual'>
                <div class="col-12">

                    <div class="table-responsive white-space-initial">
                        <table class="table table-bordered table-center mb-0">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Registration Category</th>
                                    
                                    <th style="width: 120px;">Price</th>
                                    <th style="width: 120px;">Count</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($Individuals as $Individual)
                                @if(!empty($Individual->price_change_date))
                                <tr class="individual_regis">
                                    <td>{{ $loop->iteration }}</td>
                                    <td class="individual_registration_name" id="name_{{ $Individual->id??'' }}">{{ $Individual->benefits[0]??''}}</td>
                                    {{--<td class="text-orange font-weight-bold">$ <span id="before_price_{{ $Individual->id??'' }}">{{ $Individual->amount_before??'' }}</span><br> ({{ $Individual->price_change_date->format('d-m-Y')??'' }})  </td>--}}
                                    <td class="text-orange font-weight-bold">$ <span id="after_price_{{ $Individual->id }}">{{ $Individual->amount_after??'' }}</span>{{-- <br> ({{ $Individual->price_change_date->format('d-m-Y')??'' }}) --}} </td>
                                    <td style="min-width: 120px;">

                                        <input type="text" class="form-control count regconditions" id="count_{{ $Individual->id??'' }}" name="count[{{$Individual->id}}]" />
                                    </td>
                                    <td> $ <span id="amount_{{ $Individual->id }}" class='Individual_registration_amount'> 0</span> </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-12 pt-3">
                    <div class="text-orange font-weight-bold fs15">* All registrations includes Breakfast, Lunch and Dinner</div>
                </div>
            </div>
        </div>
    </div>
        <div role="tabpanel" class="tab-pane fade in active" id="Sponsorship">
                                <div class="col-12 shadow-small py-3">
        
            <div class="row" id="donor">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-center mb-0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Sponsorship Level</th>

                                    <th style="min-width: 300px;">Benefits</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($donors as $donor)
                                <tr>
                                    <td>
                                        <div>
                                            <input type="radio" class="sponsor_category" id="sponsor_category_radio_{{$donor->id}}" name="sponsor_category" value="{{$donor->id}}" data-amount="{{$donor->amount  }}" data-cate_name="{{ $donor->donortype->name }}" />
                                        </div>
                                    </td>
                                    <td>{{ $donor->donortype->name }}</td>

                                    <td>
                                        <div class="row benfits-height" id="donation_{{$donor->donortype->id}}">
                                            <div class="col-12 col-md-12 col-lg-6">
                                                <ul class="flower-list fs16">
                                                    @foreach ($donor->benfits as $benefit )

                                                    <li>{{ $benefit->name }} {{ $benefit->pivot->count!=null ?   '('.$benefit->pivot->count .')' : ""}}
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <span class="show_more_btn text-orange pointer" id="ShowMoreDonation_{{$donor->donortype->id}}">Show More +</span>
                                    </td>
                                    <td class="text-orange font-weight-bold">$ <span id="sponsor_category_amount_{{$donor->id}}" class="sponsor_category_amount"> {{ number_format($donor->amount) }} </span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
         <div class="btn btn-sm btn-danger text-uppercase mt-3 mb-2 px-2 px-md-5 reset_sponsor_category fs-xs-9 fs-sm-11">Reset selected donation package</div> 
    </div>





                                    <!-- Family / Individual Table -->

                                  
                                    <!-- End of Family / Individual Table -->
                                    <!-- Donor's Table -->
                                  

                                    <!-- End of Donor's Table -->
                                
                                </div>
                            </div>
                            </div>

                            {{-- <div class="row">
                                <div class="col-12">
                                    <h5 class="text-violet py-2">Additional DONOR Benefits (Admission to)</h5>
                                    {!! $RegistrationContent->additional_donor_benefits !!}
                                </div>
                            </div> --}}
                            <div id="benfit_image_uploade"  class="row px-2" ></div>

                            {{-- <div class="row my-2 px-3">
                                <div class="col-12 shadow-small p-4">
                                    <div>
                                        <div class="text-orange fs16">Note:-</div>
                                        {!! $RegistrationContent->note !!}
                                    </div>
                                </div>
                            </div> --}}

                            <div class="row mt-2">
                                <div class="col-12">
                                    <h5 class="text-violet py-2">Payment Types</h5>
                                    <div class="row pb-2">
                                        @foreach ($RegistrationContent->patment_types as $patment_type)
                                        <div class="col-6 col-sm-3 col-md-3 col-lg-2">
                                            <div class="position-relative">
                                                <input type="radio" required class="p-radio-btn payment-button" data-name="{{$RegistrationContent->paymenttype( $patment_type)->name }}" value="{{$RegistrationContent->paymenttype( $patment_type)->id }}" name="payment_type" /><span class="fs16 pl-4 pl-md-4">{{ucfirst($RegistrationContent->paymenttype( $patment_type)->name )}}</span>
                                            </div>
                                        </div>

                                        {{-- <span class="position-relative pr-3"><input type="radio" class="p-radio-btn" name="payment_type" /><span class="fs16 pl-4 pl-md-4">Cheque/Cash</span></span> --}}
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <hr class="dashed-hr">
                            <div class="row">
                                <!-- paypal payment -->

                                <div class="col-12" id="paypal" style="display: none;">
                                    <h5 class="text-violet mb-3">Payment by using Paypal</h5>
                                    <div class="paypal-note">
                                    </div>
                                    <div class="form-row">
                                    </div>
                                </div>

                                <!-- paypal payment end -->

                                <div class="col-12" id="check_payment" style="display: none;">
                                    <h5 class="text-violet mb-3">Payment by using check</h5>
                                    <div class="check-note">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                                            <label class="mb3">Check Number: </label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="cheque_number" id="" class="form-control cheque_form_field" placeholder="Check Number">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                                            <label class="mb3">Bank Name:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="bank_name" id="" class="form-control cheque_form_field" placeholder="Bank Name">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                                            <label class="mb3">Check Date </label><span class="text-red">*</span>
                                            <div>
                                                <input type="date" name="cheque_date" id="" class="form-control cheque_form_field" placeholder="Check Date">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                                            <label class="mb3">Check Received By:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="check_received_by" id="" class="form-control cheque_form_field" placeholder="Check Received By">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- zelle -->
                                <div class="col-12" id="zelle" style="display: none;">
                                    <h5 class="text-violet mb-3">Payment by using Zelle</h5>
                                    <div class="zelle-note">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                                            <label class="mb3">Zelle Reference Number</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="Zelle_Reference_Number" id="" class="form-control zelle_form_field" placeholder="Zelle Reference Number">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- zelle end -->
                                <!-- othre Payments -->
                                <div class="col-12" id="other" style="display: none;">
                                    <h5 class="text-violet mb-3">Payment by using other payments</h5>
                                    <div class="other-note">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                                            <label class="mb3">On Behalf Of</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="payee_name" id="" class="form-control other_payment_form_field" placeholder="Payee Name">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                                            <label class="mb3">Company Name</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="company_name"  class="form-control other_payment_form_field" placeholder="Company Name">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                                            <label class="mb3">Payment Date </label><span class="text-red">*</span>
                                            <div>
                                                <input type="date" name="transaction_date" id="" class="form-control other_payment_form_field" placeholder="Transaction Date">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                                            <label class="mb3">Payment Reference Number </label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="transaction_id" id="" class="form-control other_payment_form_field" placeholder="Payment Reference Number">
                                            </div>
                                        </div>

                                          <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                                            <label>Document </label><span class="text-red">*</span>
                                            <div>
                                                <input type="file" name="other_document"  class="form-control other_payment_form_field" style="height: 44px;">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- othre Payments end -->
                            </div>
                            <hr class="dashed-hr">
                            <div class="row my-2">
                                        <div class="col-12">
                                            <div class="row pt-1">
                                                <div class="col-7 col-md-9 my-auto">
                                                    <label class="mb-0"><b>Selected Registration Packages</b> </label>
                                                </div>
                                                <div class="col-5 col-md-3 my-auto">
                                                   <div class="text-right">
                                                    <label class="mb-0"><b>Amount</b></label>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="col-12" id="append_reg_data_div">
                                            <div class="row">
                                                <div class="col-7 col-md-9 my-auto">
                                                    <label class="mb-0"><i>No Packages Selected </i></label>
                                                </div>
                                                <div class="col-5 col-md-3 my-auto">
                                                   <div class="text-right">
                                                    <label class="mb-0"> $ 0</label>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="row my-2">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-12 col-md-8 my-auto">
                                                    <label class="mb-0"><b>Selected Donation Packages</b> </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 donation_amount_text pb-2">
                                            <div class="row">
                                                <div class="col-7 col-md-9 my-auto">
                                                    <label class="mb-0"><i>No Packages Selected </i></label> <b class="text-orange"><span></span></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            
                            <hr class="dashed-hr">
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-6 pt-2">
                                    <!--  <h5>Security Code</h5> -->
                                    <div class="row">
                                        <div class="col-11">
                                              <div class="registration-captcha-block">
                                                <div class="row">
                                                    <div class="col-6 px-0 my-auto">
                                                       <input type="text" id="captcha_number" class="border-0 form-control bg-transparent" placeholder="Enter the Characters you see" />
                                                    </div>
                                                    <div class="col-4 px-0 my-auto ">
                                                        <span class="fs-20" id="captcha">8500817</span>
                                                    </div>
                                                </div>
                                            </div>
 

                                        </div>
                                        <div class="col-1 px-2 my-auto">
                                            <div>
                                                <!--   <button type="button" class="btn btn-danger" class="reload" id="reload">
                                                    &#x21bb;
                                                </button> -->
                                                <!--  <img src="images/refresh.png" class="img-fluid mx-auto d-block" width="15" height="16" alt=""> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 offset-md-1 col-md-5 pt-2">
                                    <div class="row my-1" id="paid_amount_div" style="display: none;">
                                        <div class="col-12 px-3">
                                            <div class="row">
                                                <div class="col-7 col-md-9 my-auto">
                                                    <label class="mb-0">Paid Amount</label>
                                                </div>
                                                <div class="col-5 col-md-3 my-auto">
                                                    <div class="payment fs16"> 
                                                        <span>:</span>
                                                        <span class="mb-0 float-right">$ 
                                                            <label class="previous_paid_amount">0</label>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-1 my-auto">
                                                    <label class="mb-0">:</label>
                                                </div>
                                                <div class="col-3 my-auto">
                                                    <div class="payment text-right"> <label class="previous_paid_amount">$ 0</label></div>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row my-1" style="display:none">
                                        <div class="col-12 px-3 px-md-0">
                                            <div class="row">
                                                <div class="col-7 col-md-9 my-auto">
                                                    <label class="mb-0">Registration Amount</label>
                                                </div>
                                                <div class="col-5 col-md-3 my-auto">
                                                    <div class="payment fs16"> 
                                                        <span>:</span>
                                                        <span class="mb-0 float-right">$ 
                                                            <label class="registration_amount">0</label>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-1 my-auto">
                                                    <label class="mb-0">:</label>
                                                </div>
                                                <div class="col-3 my-auto">
                                                    <div class="payment ">$ <span class="registration_amount">0</span></div>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="row my-1" style="display:none">
                                        <div class="col-12 px-3 px-md-0 border-bottom">
                                            <div class="row mb-2">
                                                <div class="col-7 col-md-8 my-auto">
                                                    <label class="mb-0">Donation Amount</label>
                                                </div>
                                                <div class="col-5 col-md-3 my-auto">
                                                    <div class="payment fs16"> 
                                                        <span>:</span>
                                                        <span class="mb-0 float-right">$
                                                            <label class="donotion_amount">{{ Auth::user()->donor_amount??0}}>0</label>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-1 my-auto">
                                                    <label class="mb-0">:</label>
                                                </div>
                                                <div class="col-3 my-auto">
                                                    <div class="payment">$ <span class="donotion_amount">{{ Auth::user()->donor_amount??0}}</span></div>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row my-1">
                                        <div class="col-12 px-3">
                                            <div class="row mb-2">
                                                <div class="col-7 col-md-9 my-auto">
                                                    <label class="mb-0">Total Amount</label>
                                                </div>
                                                
                                                <div class="col-5 col-md-3 my-auto">
                                                    <div class="payment fs16"><span>:</span><span class="mb-0 float-right" >$ <label id="total_amount_to_be_paid"> 0</label></span> </div>
                                                </div>
                                            </div>
                                            <div  class="error-if-amount-is-zero error" > </div>
                                            <div  class="partial-amount-error error" ></div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <input type="hidden" id="donation_amount_hidden" name="donation_amount_hidden" value="{{Auth::user()->donor_amount??'0'}}">
                                            <input type="hidden" id="registration_amount_hidden" name="registration_amount_hidden" value="0">
                                            <input type="hidden" id="total_amount_hidden" name="total_amount_hidden">
                                            <input type="hidden" id="is_upgrade" name="is_upgrade">
                                            <input type="checkbox" class="input-checkbox" id="pay_partial_amount" name="pay_partial_amount"><span class="pl25 fs16">Pay Partial Amount</span>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6" id="paying_amount" style="display: none;">
                                            <div>
                                                <input type="text" name="paying_amount" value="" class="form-control paying_amount_inputfield" placeholder="Paying Amount" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="dashed-hr">
                            <div class="row py-2">
                                <div class="col-12 col-sm-12 col-md-12 my-auto">
                                    <div class="pb-4 pb-md-2">
                                        <input type="checkbox" class="input-checkbox agree-checkbox" required name="agrement">
                                        
                                   <div class="pl40 fs15 pointer" >I DECLARE that I have read and understand the package information and <span class="fs15 pointer text-orange font-weight-bold"  data-toggle="modal" data-target="#t_and_c" >I accept Terms and Conditions</span>, I AGREE all donations are final and no refunds / Exchanges / Cancellations will be accepted.
I authorize the above charge and No refunds will be provided for any type of Registrations. I abide by NRI Vasavi Association, Inc. (NRIVA) standards, rules and regulations.</div>
                                        
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 pt-2 my-2 mt-lg-3 mb-lg-2" >
                                    <div class="text-center text-sm-right">
                                        
                                        
                                    {{--    @if(Auth::user())
                                        @else
                                        @endif --}}
                                       {{--  <input type="submit" class="btn btn-lg btn-danger text-uppercase px-5 submit-button" value="Check out" name="" @if(!Auth::user()) disabled @endif> --}}
                                        <input type="submit" disabled class="btn btn-lg btn-danger text-uppercase px-5 submit-button1" value="Check out" name=""  >
                                        
                                        <!-- <div class="btn btn-lg btn-danger text-uppercase px-5">ASDFAD</div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="row pt-3 pb-2">
                                <div class="col-12 fs16">
                                    <!--  <span class="text-uppercase font-weight-bold">MAKE CHECKS PAYABLE TO :</span> -->
                                    {!! $RegistrationContent->check_payable_to !!}

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!--  -->

<div style="display:none" class="image_Uploade_template" >
<div class="col-12 col-sm-10 col-md-6 col-lg-4  my-2 px-2 main">
    <div class="shadow-small py-4">
        <h6 class="text-center benfit_name">Upload Photo</h6>
        <div class="browse-button py-2">
            <input type="file"  class="form-control browse benfit_input" onchange="ValidateFile(this)" name="image">
        </div>
        <div class="file_name text-center"></div>

        </div>
        <div class="pt-3 text-center fs15">(Allowed file types: JPG, JPEG, PNG. File Max limit 5MB)</div>
    </div>
</div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Registration</h4>
        </div>

        <div class="modal-body">
            <div class="alert alert-success px-3 py-2" style="border-radius: 5px !important;">Please enter the email verification code received in email. There might be delay up to 30 seconds to receive email and please check spam/junk folder also</div>
            <form class="pt-3" action="{{url('new_registertion')}}" method="post">
                 @csrf
                <input type="hidden" name="email" id="new_email">
             <div class="form-row">
                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Code :</label><span class="text-red">*</span>
                        <div>
                            <input type="number" required name="otp" id="otp" value="" class="form-control" placeholder="Verification Code"  />

                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Create Password (Optional) :</label>
                        <div>
                            <input type="password" name="password" id="password"  class="form-control" placeholder="Password"  />
                        </div>
                    </div>
                </div>
                 <button type="submit" class="btn btn-primary">Submit</button>
        <div style="float:right">
        <a href="{{url('/')}}" class="btn btn-danger" >Close</a>
      </div>
            </form>
            
          
        </div>
         
        
      </div>
      
    </div>
  </div>


  <div class="modal fade" id="t_and_c" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Terms and Conditions</h4>
        </div>

        <div class="modal-body">
            <div class="">
                <ul>
                    <li>All the fields marked with *, in this form are mandatory and must be filled for processing an application.</li>
                    <li>The tickets for Youth and children will be issued based on the information provided by the donors</li>
                    <li>Please mention Zelle Reference Number, Check Details and Other payment details clearly where applicable to speed up the processing and avoid cancellation of registration.</li>
                    <li>NRIVA is not responsible if checks bounce and the Registrant / Donor is liable to pay the fee for all such failed checks.</li>
                    <li>NRIVA will contact you in the case where credit cards are declined or checks bounce.</li>
                    <li>After the process of application is completed, an email will be sent with a confirmation Number to the Registrant / Donor.</li>
                    <li>All Registrations and Donations are final and for any upgrades please contact the Registrations committee.</li>
                    <li>No Refunds / Exchanges / Cancellations are accepted for any type of Registrations or Donations.</li>
                    <li>All the details of the program and information regarding the convention can be found in www.convention.nriva.org</li>
                </ul>
            </div>
        </div>
         <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
        
      </div>
      
    </div>
  </div>

@section('javascript')
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
<script>

       var _validFileExtensions = [".jpg", ".jpeg",  ".png"];    


        function ValidateFile(oInput) {
            $(oInput).parent().parent().parent().find('.file_name ').text(' ')
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            filename=sFileName.split('\\')[(sFileName.split('\\').length)-1]
                            $(oInput).parent().parent().parent().find('.file_name ').text(filename)
                            break;
                        }
                    }
                    
                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }

          captcha();
    $('.captcha_refresh').on('click',function(){
        captcha();

    });
    function captcha() {
        var number = Math.floor(100000 + Math.random() * 900000);
        $('#captcha').text(number);
    }
    $(document).ready(function() {
    $('.type_tab').click(function(e){
         if(! $(".count").valid()){
            e.stopPropagation()
             swal("","Please enter required information to continue", "warning");
         }
    })
      

        registrationtype = JSON.parse('{!!json_encode(1) !!}')
       // Individuals = JSON.parse('{!!json_encode($Individuals) !!}')
        Individuals =({!!html_entity_decode($Individuals) !!}) 
        paymenttypes = ({!!html_entity_decode($paymenttype) !!})
        donors = ({!!html_entity_decode($donors) !!})
        user = JSON.parse('{!!json_encode($user) !!}')
        @if($registrationtype)
        $('#paid_amount_div').show();
        @endif

        if(user.amount_paid==null){
            user.amount_paid=0
        }
        is_upgrade_url="{{Route::is('upgrade')}}" 
        if(is_upgrade_url){

            $('#is_upgrade').val('upgrade')
            $('.sponsor_category_amount').each(function() {
            sponsor_category_amount=parseInt($(this).text().replace(",", ""))

            if (sponsor_category_amount < parseInt(user.donor_amount)) {
                id = $(this).attr('id').split("_")
                $('#sponsor_category_radio_' + id[3]).attr('disabled', 'disabled')
            }
            if (sponsor_category_amount == parseInt(user.donor_amount)) {
                id = $(this).attr('id').split("_")
                $('#sponsor_category_radio_' + id[3]).attr('checked', 'checked');

            }
        })
        }

        if (registrationtype) {
            $('.previous_paid_amount').text(user.amount_paid)
            $('[data-name="' + registrationtype.name + '"]').trigger("click");
            changeRegType($('[data-name="' + registrationtype.name + '"]'))
          
             $.each(user.individual_registration, function (key,data) {
                Individuals.forEach(Individual => {
                    if(Individual.id==key){ 
                        $('#count_'+key).val(data);
                        var split_id = 'count_'+key;
                        id = split_id.split("_");
                        countkeyup(id,data)
                    }
                })
            })
            $.each(user.kids_details, function (key,data) {
                if(key==0){
                    $('.Kid_name').val(data.name)
                    $('.Kid_age').val(data.age)
                }else{
                    $('.add-category-name').attr('data-count', parseInt($('.add-category-name').attr('data-count')) + 1)
                    $(".child-template").find('.main').first().clone()
                    .find(".chaild-of-main").attr('id', 'delete_individual_' + $('.add-category-name').attr('data-count')).end()
                    .find(".button-div").empty().end()
                    .find(".Kid_name").val(data.name).end()
                    .find(".Kid_age").val(data.age).end()
                    .find(".button-div").append('<span class="plus_n_minus_icons delete-item" id="btn_delete_individual_' + $('.add-category-name').attr('data-count') + '" ><i class="fas fa-minus"></i>').end()
                    .appendTo($('.child-add-div'));
                }
            })
                
        }

         
         $(".invitees-nav-link").click(function () {
        //active_tab
        $(".invitees_active_tab_btn").removeClass("invitees-nav-link");
        $(".invitees_active_tab_btn").removeClass("invitees_active_tab_btn");
        $(this).addClass("invitees-nav-link");
        $(this).addClass("invitees_active_tab_btn");

        $(".invitees_active_tab").hide();
        $(".invitees_active_tab").removeClass("invitees_active_tab");
        $($(this).attr("target-id")).addClass("invitees_active_tab");
        $($(this).attr("target-id")).show();
    });
         $.validator.addMethod('filesize', function (value, element, arg) {
                if(element.files[0]!=undefined){
                    if(element.files[0].size<=arg){ 
                        return true; 
                    }else { 
                        return false; 
                    } 
                }else{
                    return true; 
                }
         }); 

          $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        });
          $.validator.addMethod("numberss", function(value, element) {
            return this.optional(element) || value == value.match(/^[0-9) (-]+$/);
        });

          $.validator.addMethod("regconditions", function(value, element) {
            return this.optional(element) || generalRegExtraConditions(element);
        },"Is not satisfying registration conditions");


        jQuery.validator.addClassRules('count', {
            digits: true,
            min: 1
         });

        jQuery.validator.addClassRules('Kid_age', {
            digits: true,
            min: 1
        });

        $("#form").validate({
           
            ignore: [],
            rules: {
                'benfitimage[]': { 
                    filesize : 5242880,
                },
                 password : {
                    minlength : 5
                },
                password_confirmation : {
                    minlength : 5,
                    equalTo : "#password"
                },
                
                confirm_email_id : {
                    equalTo : "#email"
                },
                first_name: "alpha",
                last_name: "alpha",
                state: "required",
                country: "required",
                address: "required",
                paying_amount: {
                        digits: true
                },
                mobile :{
                    maxlength: 14,
                    numberss:true
                },
            },
             messages: {
                "benfitimage[]": {
                    required: "Please upload file.",
                    filesize: "Max file size allowed is 5MB",
                },
                  "first_name": {
                    alpha: "First name should not contain numbers.",
                },
                 "last_name": {
                    alpha: "Last name should not contain numbers.",
                },
                 "spouse_full_name": {
                    alpha: "Spouse name should not contain numbers.",
                },
                 "mobile": {
                    maxlength: "Max length is  14.",
                    numberss: "Numbers only"
                },
                 password_confirmation : {
                    equalTo : "Password does not match"
                },
                 confirm_email_id : {
                    equalTo : "email id does not match"
                },
            },
        });

        $('.submit-button').click(function(e){

            var enter_captha = $('#captcha_number').val();
            var captcha =$('#captcha').text();
            if(enter_captha != captcha){
            e.preventDefault();
            e.stopPropagation();
            alert("Captcha Invalid.");
            }

            if($('#total_amount_to_be_paid').text()==0){
                $('.error-if-amount-is-zero').text('Please select any of the above packages')
                if ($('.reg-type').prop("checked")) {
                    if($('.reg-type').val()=='Family / Individual'){
                        $('.error-if-amount-is-zero').text('Please Enter Count')
                       }
                } 
                e.preventDefault()
            }else{
                $('.error-if-amount-is-zero').empty()
            }

             if ($('#pay_partial_amount').prop("checked")) {

                 Total=parseInt($('#total_amount_to_be_paid').text().replace(/,/g , ''));

                    payment=$('.paying_amount_inputfield').val();

                   

                if(payment==''){
                        $('.partial-amount-error').text("Please Enter Partial Amount")
                        e.preventDefault()
                }else if(payment>=Total){
                        $('.partial-amount-error').text("Partial Amount Should be less then total amount")
                        e.preventDefault()
                }else{
                        $('.partial-amount-error').empty()
                    }
            }
             if((!e.isDefaultPrevented()) && $('#form').valid() ){
                 $('.submit-button').prop('disabled', true);
                 $("#form").submit();
             }
        })
        $('.reset_sponsor_category').click(function () {
            var registration_amount = parseInt($('.registration_amount').text().replace(/,/g , '')) ;
            $('.donotion_amount').text('');
            $('#donation_amount_hidden').val('0');
            
            $('.donation_amount_text').html('<div class="row"><div class="col-7 col-md-9 my-auto"><label class="mb-0"><i>No Packages Selected</i></label></div><div class="col-5 col-md-3 my-auto"><div class="text-right"><label class="mb-0"></label></div></div></div>');
            var total_amounts = parseInt(registration_amount) - parseInt(user.amount_paid);
            $('#total_amount_to_be_paid').text(total_amounts);
            $('#total_amount_hidden').text(parseInt(registration_amount) - parseInt(user.amount_paid));
            $('#benfit_image_uploade').empty()
             $("input:radio[name=sponsor_category]:checked")[0].checked = false;
             $('#count_18').val("");
              countkeyup($('#count_18').attr('id').split("_"),0);

            if(parseInt($(this).data('amount'))+parseInt(registration_amount) <= parseInt(user.amount_paid)){
                    $('.submit-button').prop('disabled', true);
                }else{
                    $('.submit-button').prop('disabled', false);
                }

          
        })

        function displayImageUploadeField (benfit,doner_id){
                 $(".image_Uploade_template").find('.main').first().clone()
                 .find(".benfit_name").text(benfit.name).end()
                 .find(".benfit_input").attr('name', 'benfitimage['+doner_id+'_'+benfit.id+']').end()
                    .appendTo($('#benfit_image_uploade'));
        }

        function showImageUploadeFieldsIfexist(doner_id){
            $('#benfit_image_uploade').empty()
            donors.forEach(donor => {
                if(doner_id==donor.id){
                    donor.benfits.forEach(benfit => {
                        if(benfit.has_image==1){
                            displayImageUploadeField(benfit,doner_id)
                        }
                    });
                }
            });
        }

        var checkedsponsor_category =$('.sponsor_category:checked').val();
        if(checkedsponsor_category){
                 $('.donation_amount_text').html('<div class="row"><div class="col-7 col-md-9 my-auto"><label class="mb-0"><i>1. '+$('.sponsor_category:checked').data('cate_name')+'</i></label></div><div class="col-5 col-md-3 my-auto"><div class="text-right"><label class="mb-0"> $ '+$('.sponsor_category:checked').data('amount').toLocaleString()+'</label></div></div></div>');
        }
    

       
        $(".sponsor_category").click(function(e) {
            var registration_amount = parseInt($('.registration_amount').text().replace(/,/g , ''));
            $('.donotion_amount').text(parseInt($(this).data('amount')));
            $('#donation_amount_hidden').val(parseInt($(this).data('amount')));
            var paid_amounts= parseInt($(this).data('amount'))+parseInt(registration_amount) - parseInt(user.amount_paid);
            $('#total_amount_to_be_paid').text(paid_amounts.toLocaleString());
            $('#total_amount_hidden').text(parseInt($(this).data('amount'))+parseInt(registration_amount) - parseInt(user.amount_paid))
            if(parseInt($(this).data('amount'))+parseInt(registration_amount) <= parseInt(user.amount_paid)){
                    $('.submit-button').prop('disabled', true);
                }else{
                    $('.submit-button').prop('disabled', false);
                }
                $('.donation_amount_text').html('<div class="row"><div class="col-7 col-md-9 my-auto"><label class="mb-0"><i>1. '+$(this).data('cate_name')+'</i></label></div><div class="col-5 col-md-3 my-auto"><div class="text-right"><label class="mb-0"> $ '+$(this).data('amount').toLocaleString()+'</label></div></div></div>');
            showImageUploadeFieldsIfexist($(this).val())
        });

        $('#reload').click(function() {
            $.ajax({
                type: 'GET',
                url: 'reload-captcha',
                success: function(data) {
                    $(".captcha span").html(data.captcha);
                }
            });
        });

        $('#pay_partial_amount').click(function() {
            if ($(this).prop("checked")) {
                $('#paying_amount').show()
            } else {
                $('#paying_amount').hide()
            }
        })

        $(".payment-button").click(function(e) {
            if ($(this).prop("checked")) {
                if ($(this).data('name') == "{{ config('conventions.paypal_name_db') }}") {
                    $('#paypal').show();
                    $('#check_payment').hide();
                    $('#zelle').hide();
                    $('#other').hide();
                    paymenttypes.forEach(data => {
                        if (data.name == "{{ config('conventions.paypal_name_db') }}") {
                            $('.paypal-note').html(data.note)
                        }
                    });
                    $('.cheque_form_field').prop("required", false);
                    $('.zelle_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", false);
                }

                if ($(this).data('name') == "{{ config('conventions.check_name_db') }}") {
                    $('#check_payment').show();
                    $('#paypal').hide();
                    $('#zelle').hide();
                    $('#other').hide();

                    paymenttypes.forEach(data => {
                        if (data.name == "Check") {
                            $('.check-note').html(data.note)
                        }
                    });
                    $('.cheque_form_field').prop("required", true);
                    $('.zelle_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", false);
                }

                if ($(this).data('name') ==  "{{ config('conventions.zelle_name_db') }}") {
                    $('#zelle').show();
                    $('#paypal').hide();
                    $('#check_payment').hide();
                    $('#other').hide();
                    paymenttypes.forEach(data => {
                        if (data.name ==  "{{ config('conventions.zelle_name_db') }}") {
                            $('.zelle-note').html(data.note)
                        }

                    });
                    $('.zelle_form_field').prop("required", true);
                    $('.cheque_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", false);

                }
                if ($(this).data('name') == "{{ config('conventions.other_name_db') }}") {

                    $('#other').show();
                    $('#zelle').hide();
                    $('#paypal').hide();
                    $('#check_payment').hide();
                    paymenttypes.forEach(data => {
                        if (data.name == "{{ config('conventions.other_name_db') }}") {
                            $('.other-note').html(data.note)
                        }
                    });

                    $('.zelle_form_field').prop("required", false);
                    $('.cheque_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", true);

                }
                //zelle
            }
        });

        $("#btn_get_data").click(function(e) {
           // e.preventDefault();
            ajaxCall('{{ env("APP_URL") }}/api/get-details?email=' + $('#nriva_email_field').val(), 'get', null, poplateInpFields)
        });

        function poplateInpFields(data) {
            const obj = JSON.parse(data);
            if (obj.length == 0) {
                alert('no data fount')
            } else {
                $('#first_name').val(obj.first_name)
                $('#last_name').val(obj.last_name)

                $('#email').val(obj.email)
                $('#mobile').val(obj.mobile)

                $('#city').val(obj.member.city)
                $('#zip_code').val(obj.member.zipcode)

            }
        }


          function isGreaterThenTodatDate(date) {
            today = new Date("{{date('m/d/Y')}}")
            date = new Date(date)
            if (today >= date) {
                return false
            } else {
                return true
            }
        }
     

        $(".count").keyup(function(e) {
            var donotion_amount = $('.donotion_amount').text();
            sum =0;
            id = $(this).attr('id').split("_");
            count = $(this).val();
            var total_count = 0;
             $(".count").each(function() {
                var len = $(this).val().length;     
                if (len > 0 ) {
                    total_count++;
                    }
            });
            for (let i = 0; i < Individuals.length; i++) { 
            if(Individuals[i].id==18 && total_count <=1 && $('#count_18').val() > 0){
                var donotion_package= parseInt($('#donation_amount_hidden').val());
               if(donotion_package ==0 ){
                    $('#count_18').val("");
                     e.preventDefault();
                    swal("","Please select either any one General Registration (Serial Number of the categories are 1 or 3 or 4 or 5) or One Donor package to register for this package", "warning");
                    
                    if(parseInt(count.length) == 0){
                         countkeyup(id,count);
                         countkeyup($('#count_18').attr('id').split("_"),0);
                    }else{
                        return false;
                    }
                   
                    }
                }
            }
           countkeyup(id,count);

            //generalRegExtraConditions(this)
        })

        function generalRegExtraConditions(this_val){
            benfit_list=['General Registration (2 Adults and 2 Kids) -- Includes all Events Except Banquet','Additional Members for General Registration (Requires General Registration, and No Banquet)','Single Adult Registration (Includes all Events Except Banquet)','Single Day Registration for Single Adult (Includes all Activities in A Single Day Such As Day with Sadguru On July 4th, Except Saturday Banquet On July 2nd)','General Registration for Student ( Proof of ID is required) (No Banquet)']
            id=$(this_val).attr('id').split('_')[1]
            name=$('#name_'+id).text()
               
           if( ($.inArray( name,benfit_list ) ) > -1 ){
            count=0
            confition_list=['General Registration (2 Adults and 2 Kids) -- Includes all Events Except Banquet','Single Adult Registration (Includes all Events Except Banquet)','Single Day Registration for Single Adult (Includes all Activities in A Single Day Such As Day with Sadguru On July 4th, Except Saturday Banquet On July 2nd)','General Registration for Student ( Proof of ID is required) (No Banquet)']
            $('.individual_registration_name').each(function() {
                 if( ($.inArray( $(this).text() ,confition_list ) ) > -1   ){
                        id=$(this).attr('id').split('_')[1]
                        //val=
                        if($('#count_'+id).val()==''){
                            val=0
                        }else{
                            val=$('#count_'+id).val()
                        }
                       count=count+ parseInt(val) 
                }
            })
            if($('.sponsor_category:checked').val()!=undefined){
                count=count+1
            }
              if(count<1){
                  swal("","Please select either any one General Registration (Serial Number of the categories are 1 or 3 or 4 or 5) or One Donor package to register for this package", "warning");
                    return false
                 // $('.submit-button').attr('disabled',true)
              }
           }else{
                    return true
                  //$('.submit-button').attr('disabled',false)
           }
           return true
        }
        function countkeyup(id,count) { 
            if($('.donotion_amount').text()==""){
                var donotion_amount=0
            }else{
                var donotion_amount = $('.donotion_amount').text();
            }
            sum =0;

            for (let i = 0; i < Individuals.length; i++) { 
                if (Individuals[i].id == id[1]) {
                    
                    if (isGreaterThenTodatDate(Individuals[i].price_change_date)) {
                            amount=parseInt($('#before_price_' + id[1]).text() * count)
                            $('#amount_' + id[1]).text(amount.toLocaleString())
                    } else {
                            amount=parseInt($('#after_price_' + id[1]).text() * count)
                            $('#amount_' + id[1]).text(amount.toLocaleString())
                    }
                }
            }


            var registrationtype_text='';
            var j=1;
            $('.Individual_registration_amount').each(function() { 
                if( parseInt($(this).text().replace(/,/g , '')) > 0){
                    registrationtype_text+='<div class="row"><div class="col-7 col-md-9 my-auto"><label class="mb-0"><i>'+j+'. '+$(this).parents('.individual_regis').find('.individual_registration_name').text()+'- count('+$(this).parents('.individual_regis').find('.count').val()+')</i></label></div><div class="col-5 col-md-3 my-auto"><div class="text-right"><label class="mb-0"> $ '+($(this).text())+'</label></div></div></div>';
                    j++;
                };
                
               // if(registrationtype_text != ""){
               //  registrationtype_text+='<div class="row"><div class="col-7 col-md-9 my-auto"><label class="mb-0"><i>No Packages Selected </i></label></div><div class="col-4 col-md-3 my-auto"><div class=""><label class="mb-0"> $ 0</label></div></div></div>';
                    
               // }
               $('#append_reg_data_div').html(registrationtype_text);
                sum = parseInt(sum) + parseInt($(this).text().replace(/,/g , ''))
                $('.registration_amount').text(sum);
                $('#registration_amount_hidden').val(parseInt(sum));
            });
               var total_amounts= parseInt(sum)+parseInt(donotion_amount)- user.amount_paid;
                $('#total_amount_to_be_paid').text(total_amounts.toLocaleString());
                $('#total_amount_hidden').text(parseInt(sum)+parseInt(donotion_amount)- user.amount_paid);
                if(parseInt(sum)+parseInt(donotion_amount) <= user.amount_paid){
                    $('.submit-button').prop('disabled', true);
                }else{
                    $('.submit-button').prop('disabled', false);
                }
        }

        $('.reg-type').click(function() {
            changeRegType(this)
        })

        function changeRegType(thisVal) {
            if ($(thisVal).prop("checked")) {
                if ($(thisVal).data('name') == 'Donor') {
                    $('#donor').show();
                    $('#family-or-individual').hide();
                    $('.sponsor_category').attr('required', 'required')
                    $('.count').val(0);
                    $( ".count" ).trigger( "keyup" );
                }

                if ($(thisVal).data('name') == 'Family / Individual') {
                    $('#donor').hide();
                    $('#total_amount_to_be_paid').text('0');
                    $('.sponsor_category').prop('checked', false)
                    $('.donotion_amount').text('0')
                    $('#family-or-individual').show();
                    $('.sponsor_category').removeAttr('required', 'required')
                }
            }
        }
    });
 @if(!Auth::user())
$('#email').on('change',function(){
        $('.email_valid_msg').text('')
        var email = $('#email').val();
        var thiss =$(this);
          if(email !=""){
            if(email.search('@') >0 ){
            }else{
                $('.email_valid_msg').text('Please enter a valid email')
            }
        $.ajax({
            type: 'GET',
            url: "{{url('email_verification')}}?email="+email,
            success:function(data){
              if(data=="success"){
                $('#new_email').val(email);
               // $('#myModal').modal({backdrop: 'static', keyboard: false});
                //$('#myModal').modal('show');
              }else{
                window.location.href="{{url('login')}}?error=exit&type=conventions&email="+email;
              }
             
            }
        });
      }else{
          $('.email_valid_msg').text('Please enter email')
        //alert('Please enter email');
      }

    });
@endif
</script>

@endsection


@endsection
