@extends('layouts.user.base')
<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
@section('content')

<section class="container-fluid my-5">
    @if($page_content!=null)
    {!! $page_content !!}
    @else
    <h4 class="my-5 text-center text-skyblue">
        Coming Soon ...
    </h4>
    @endif

</section>



@section('javascript')


@endsection


@endsection
