@extends('layouts.user.base')
@section('content')

<style>
.radio {
    margin-top:5px;
    margin:5px;
}
.yes-lable{
    margin-left:5px;
}
</style>

<section class="container-fluid my-3 my-lg-5">
    
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">

         <div class="text-right">
              @if(Auth::user()->total_amount==0)
             <a href="{{url('bookticket')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Convention Registration</a>
             @else

           {{--   @foreach($MrAndMissTypes as $type)

              <a href="{{url('mr_mis_registration')}}?type={{$type->title}}" class="btn btn-sm btn-success text-white my-1 mx-1">{{$type->name}}</a>

             @endforeach --}}

             @endif
            {{--  <a href="{{url('exhibits-reservation-list')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Exhibit Registration List</a> --}}
            <!--  <a href="{{url('youthActivities-regestration')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Youth Activities Registration</a> -->
            
           

                 
        </div>
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
               <button type="button" class="close" data-dismiss="alert">×</button>
               <strong>{{ $message }}</strong>
            </div>
            @endif

                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif

        <div class="row">
            <div class="col-12">
                <h5>My Dashboard</h5>
            </div>
            <div class="col-12">
                <div class="card-body px-0">
                    <div class="table-responsive">
                        @if(Auth::user()->total_amount!=0)
                        <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    <th>Member ID</th>
                                    <th>Registration ID</th>
                                    <th>Member Details</th>
                                    <th width="150px" >Other Details</th>
                                    
                                   
                                    <th>Payment Details</th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ Auth::user()->member_id }}</td>
                                    <td>{{ Auth::user()->registration_id }}</td>
                                    <td>
                                        <div class="py-1">
                                            <span>Name:</span><span> {{ ucfirst(Auth::user()->first_name) }} {{ ucfirst(Auth::user()->last_name) }}</span>
                                        </div>
                                        <div class="py-1">
                                            <span>Email:</span><span> {{ Auth::user()->email }}</span>
                                        </div>
                                        <div class="py-1">
                                            <span>Mobile:</span><span> {{ Auth::user()->phone_code }} {{ Auth::user()->mobile }} </span>
                                        </div>
                                    </td>
                                    <td> 
                                     <b>Chapter</b>: {{ Auth::user()->chapter }}
                                    <br>
                                    <b>Spouse First Name</b>: {{ Auth::user()->spouse_first_name }}
                                    <br>
                                    <b>Spouse Last Name</b>: {{ Auth::user()->spouse_last_name }}
                                    <br>
                                    @if(Auth::user()->kids_details)
                                    <br>
                                        @foreach (Auth::user()->kids_details as $kid)
                                    <b>kid Name</b>: {{ $kid['name'] }}<br>
                                    <b>Age</b>: {{ $kid['age'] }}<br><br>
                                        @endforeach
                                    @endif

                                    </td>
                                   
                                    
                                    <td>
                                        <div>Paid Amount : ${{number_format(Auth::user()->amount_paid) }}</div>
                                        <div>Due :
                                            @php
                                                $due=Auth::user()->total_amount -Auth::user()->amount_paid;
                                            @endphp    
                                        $ {{   number_format($due)  }}</div>
                                        <div>Payment status: {{ucfirst(Auth::user()->payment_status) }}</div>
                                         @if($due>0)
                                        <div>
                                            <a href="{{route('pay-pending-amount')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Pay Pending Amount</a>
                                        </div>
                                        @endif
                                          <div>
                                            <a href="{{url('upgrade')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Upgrade</a>
                                        </div> 
                                        <div>
                                            <a href="{{url('paymenthistory')}}" class="btn btn-sm btn-success text-white my-1 mx-1">View Payment History</a>
                                        </div>
                                        <div>
                                            <a href="{{url('youth_banquet_registration')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Youth Banquet Registration</a>
                                        </div> 
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        @if(Auth::user()->categorydetails)
                        <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    
                                    
                                    <th>Sponsorship Type</th>
                                    <th>Sponsorship Category</th>
                                    <th>Action</th>
                                    <th>Sponsorship Benefits</th>
                                     @if($exhibit_registrations_count>0)
                                    <th>Status</th>
                                    @endif
                                  
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Donor</td>
                                    
                                   
                                    <td>
                                         {{  Auth::user()->categorydetails->donortype->name ?? ""}} ($ {{number_format(Auth::user()->donor_amount) }})</td>
                                    <td>
                                        <div>
                                            <a href="{{route('viewfeatures')}}" class="btn btn-sm btn-success text-white my-1 mx-1">View Features</a>
                                        </div>
                                         
                                       
                                        
                                        
                                    </td>
                                    <td>
                                            
                                           
                                         @if(Auth::user()->categorydetails)
                                        @foreach(Auth::user()->categorydetails->benfits as $benfits)
                                            @if($benfits->name=='Shathamanam Bhavathi Tickets')
                                            <div>
                                                <a href="{{url('shathamanam-bhavathi-registration')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Shathamanam Bhavathi Registration</a>
                                                <div style="display:inline-flex;">
                                                <p>Required </p>
                                                <p class="yes-lable" >Yes</p>
                                                @php
                                                    if(Auth::user()->is_shathamanam_bhavathi_required!= null){
                                                        if(Auth::user()->is_shathamanam_bhavathi_required)
                                                        $is_shathamanam_bhavathi_required=Auth::user()->is_shathamanam_bhavathi_required;

                                                    }else{
                                                        $is_shathamanam_bhavathi_required='';
                                                    }
                                                @endphp

                                                <input type="radio" 
                                                @if(Auth::user()->is_shathamanam_bhavathi_required!= null  &&  Auth::user()->is_shathamanam_bhavathi_required==1)
                                                    checked
                                                @endif
                                                class="radio" name="shathamanam_bhavathi" value="Yes" > 
                                                <p>No</p>
                                                <input
                                                @if( Auth::user()->is_shathamanam_bhavathi_required==0)
                                                    checked
                                                @endif
                                                 type="radio"  class="radio" name="shathamanam_bhavathi" value="No" >
                                                </div>
                                            </div>
                                            @endif
                                            @if($benfits->name=='Vendor booth spaces at the event' && Auth::user()->free_exbhit_registrations > 0)
                                            <div>
                                                <a href="{{url('exhibits-reservation')}}?type=free" class="btn btn-sm btn-success text-white my-1 mx-1">Exhibit Registration</a>

                                                <div style="display:inline-flex;">
                                                <p>Required </p>
                                                <p class="yes-lable" >Yes</p>
                                                <input type="radio" 
                                                    @if( Auth::user()->is_exbhit_required==1)
                                                    checked
                                                @endif
                                                 class="radio" name="Exbhit" value="Yes" > 
                                                <p>No</p>
                                                <input 
                                                @if( Auth::user()->is_exbhit_required==0)
                                                    checked
                                                @endif
                                                type="radio"  class="radio" name="Exbhit" value="No" >
                                                </div>
                                            </div>
                                            @endif
                                        @endforeach
                                    @endif
                                    </td>
                                    @if($exhibit_registrations_count>0)
                                    <td>
                                     <a href="{{url('exhibits-reservation-list')}}" class="btn btn-sm btn-success text-white my-1 mx-1">My Exhibit Registration List</a>
                                    </td>
                                    @endif
                                    
                                </tr>
                            </tbody>
                        </table>
                        @endif
                        <br>
                        @if(Auth::user()->registration_amount)

                         <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    
                                    
                                    <th>Sponsorship Type</th>
                                    <th>Sponsorship Category</th>
                                   
                                  
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Family / Individual</td>
                                    
                                   
                                    <td>
                                        @foreach(Auth::user()->individual_registration as $key => $value)
                                        @if($value > 0)
                                        <?php $Individual = \App\Models\Admin\SponsorCategory::where('status', 1)->where('id',$key)->first();
                                        if($Individual){
                                            echo $Individual->benefits[0]." - ".$value;
                                        }
                                        
                                        echo "</br>";

                                         ?>
                                         @endif
                                        @endforeach
                                       Total Amount : $ {{number_format(Auth::user()->registration_amount) }}</td>
                                    <td>
                                        
                                       
                                        
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                            @endif
                        @else
                        <h5> You have not Registered for the event Please click <a href="bookticket">here<a /> to register</h5>
                        @endif

                @if(count($CorporateSponsorReg)>0)
               <h6>Corporate Sponsors registration Details</h6>
                         <table class="table-bordered table table-hover table-center mb-0 mt-2">
                            <thead>
                                <tr>
                                    <th>Registration Id</th>
                                    <th>Name</th>
                                    <th>Corporate Sponsors Type</th>
                                    <th>Registration Details</th>
                                    <th>Payment Details</th>
                                    <th>Logo</th>
                                    <th>Image</th>
                                    <th>video link</th>
                                    <th>Discription</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($CorporateSponsorReg as $CorporateSponsor)
                                <tr>
                                    <td>{{$CorporateSponsor->registration_id}}</td>
                                    <td>{{$CorporateSponsor->full_name}}</td>
                                    <td>{{$CorporateSponsor->CorporateSponsortype->name??""}}</td>
                                    <td>
                                        phone:{{ $CorporateSponsor->phone_no }}<br>
                                        email:{{ $CorporateSponsor->email }}<br>
                                        website url:{{ $CorporateSponsor->website_url }}<br>
                                        address:{{ $CorporateSponsor->mailing_address}}
                                    </td>
                                    <td>
                                    @if($CorporateSponsor->paymentdata) 
                                        Amount: ${{ $CorporateSponsor->paymentdata->payment_amount??'' }}<br>
                                     
                                        Payment Type: 
                                                @if($CorporateSponsor->paymentdata->payment_methord==1)
                                                Paypal
                                                @elseif($CorporateSponsor->paymentdata->payment_methord==2)
                                                Check
                                                @elseif($CorporateSponsor->paymentdata->payment_methord==3)
                                                Zelle
                                                @elseif($CorporateSponsor->paymentdata->payment_methord==4)
                                                Other
                                                @endif
                                                <br>
                                        Payment Id :{{ $CorporateSponsor->paymentdata->unique_id_for_payment??'' }}
                                    @endif
                                </td>
                                <td>
                                     @if($CorporateSponsor->logo_url)
                                    <img src="{{asset(config('conventions.corporate_sponsors_image_display').$CorporateSponsor->logo_url)}}" width="250px" class="img-fluid">
                                @endif
                                </td>

                                <td>
                                     @if($CorporateSponsor->image_url)
                                    <img src="{{asset(config('conventions.corporate_sponsors_image_display').$CorporateSponsor->image_url)}}" width="250px" class="img-fluid">
                                @endif
                                </td>
                                <td>
                                {{$CorporateSponsor->video_link}}
                                </td>
                                <td>
                                 {{$CorporateSponsor->extra_discriptioon}}
                                </td>
                                    
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                @endif
                
                @if(count($shathamanamBhavathiRegistrations)>0)
                <br>
                        <h6>Shathamanam bhavathi registration Details</h6>
                         <table class="table-bordered table table-hover table-center mb-0 mt-2">
                            <thead>
                                <tr>
                                    <th>Registration Id</th>
                                    <th>Name</th>
                                    <th>No of Participants</th>
                                    <th>Registration Details</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($shathamanamBhavathiRegistrations as $shathamanamBhavathiRegistration)
                                <tr>
                                    <td>{{$shathamanamBhavathiRegistration->registration_id}}</td>
                                    <td>{{$shathamanamBhavathiRegistration->full_name}}</td>
                                    <td>{{$shathamanamBhavathiRegistration->extra_data['number_of_participants']??""}}</td>
                                    <td>
                                            @php
                                                $count=1;
                                            @endphp
                                    @while (array_key_exists('ticket_'.$count,$shathamanamBhavathiRegistration->extra_data))
                                    <h6><b> Details for ticket {{$count}} </b> </h6> 
                                        <b>Father (In-Law) Details  : </b><br>
                                        <div>
                                            <b>Name:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['father_or_in_laws_name']??""}}<br>
                                            <b>DOB:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['father_In_law_dob']??""}}<br>
                                            <b>Naksthram:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['father_In_law_naksthram']??""}}<br>
                                            <b>Rasi:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['father_In_law_rasi']??""}}<br>
                                            <b>Gothram:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['father_In_law_gothram']??""}}<br>
                                        </div>
                                        <b>Mother (In-Law) Details  : </b><br>
                                            <div>
                                            <b>Name:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['mother_or_in_laws_name']??""}}<br>
                                            <b>DOB:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['mother_In_law_dob']??""}}<br>
                                            <b>Naksthram:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['mother_In_law_naksthram']??""}}<br>
                                            <b>Rasi:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['mother_In_law_rasi']??""}}<br>
                                            <b>Gothram:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['mother_In_law_gothram']??""}}<br>
                                        </div>
                                        @php
                                        $count=$count+1;
                                        @endphp
                                    @endwhile
                                    </td>
                                    
                                    
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif


                        @if(count($matrimony_registrations)>0)
                        <br>
                        <h6><b>Matrimony registration Details :</b></h6>
                         <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                            
                            <th>Profile Id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email Id</th>
                            <th>Participate In</th>
                            <th>Number of Participant</th>
                            <th>Registered Date</th>
                            
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($matrimony_registrations as $matrimony_registration)
                            <tr>
                                <td>{{ $matrimony_registration->member_id }}</td>
                                <td>{{ $matrimony_registration->first_name }}</td>
                                <td>{{ $matrimony_registration->last_name }}</td>
                                <td>{{ $matrimony_registration->email }}</td>
                                <td>
                                    @foreach($matrimony_registration->participate_in as $part)
                                        {{$part}} <br>
                                    @endforeach
                                </td>
                                <td>{{ $matrimony_registration->no_of_people_attending }}</td>
                                <td>
                                For Convention: {{ $matrimony_registration->created_at }} <br>
                                For Nriva: {{ $matrimony_registration->nriva_created_at }}
                                
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                           
                        </table>
                        @endif
                         @if(!empty($mr_miss))
                       {{--   <br>
                        <h6><b>Mr, Mrs, Miss and Master NRIVA Registration Details :</b></h6>
                         <table class="table-bordered table table-hover table-center mb-0 mt-2">
                            <thead>
                                <tr>
                            
                            <th>Type</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email Id</th>
                            <th>Mobile No</th>
                            <th>Chapter</th>
                            <th>DOB</th>
                            <th>City</th>
                            <th>About</th>
                        
                            <th>Registered Date</th>
                            <th>Action</th>
                            
                        </tr>
                        </thead>
                        <tbody>
                           
                           @foreach($mr_miss as $mrmiss)
                            <tr>
                              
                                <td>{{ $mrmiss->title }}</td>
                                <td>{{ $mrmiss->first_name }}</td>
                                <td>{{ $mrmiss->last_name }}</td>
                                <td>{{ $mrmiss->email }}</td>
                                <td>{{ $mrmiss->phone}}</td>
                                <td>{{ $mrmiss->chapter }}</td>
                                <td>{{ $mrmiss->dob }}</td>
                                <td>{{ $mrmiss->city }}</td>
                                <td>{{ $mrmiss->about }}</td>
                                
                                <td>{{ $mrmiss->created_at }}</td>
                                <td>
                                     <a href="{{url('mr_mis_registration_edit')}}?type={{$mrmiss->title}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                </td>
                                
                            </tr>
                            @endforeach
                           
                        </tbody>
                           
                        </table> --}}
                        @endif

                          @if(count($SouvenirRegistrations)>0)
                          <br>
                        <h6><b>Souvenir Registration Details :</b></h6>
                         <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                            
                            <th>Registration Id</th>
                            <th>Full Name</th>
                            <th>Company Name</th>
                            <th>Mobile No</th>
                            <th>Email</th>
                            <th>Website</th>
                            <th>Referred By</th>
                            <th>Payment Details</th>
                            <th>Action</th>
                            
                        </tr>
                        </thead>
                        <tbody>
                           
                           @foreach($SouvenirRegistrations as $SouvenirRegistration)
                            <tr>    
                              @php
                                 $payment = $SouvenirRegistration->paymentdata
                              @endphp
                                <td>{{ $SouvenirRegistration->registration_id }}</td>
                                <td>{{ $SouvenirRegistration->full_name }}</td>
                                <td>{{ $SouvenirRegistration->company_name }}</td>
                                <td>{{ $SouvenirRegistration->phone_no }}</td>
                                <td>{{ $SouvenirRegistration->email }}</td>
                                <td>{{ $SouvenirRegistration->website_url }}</td>
                                <td>{{ $SouvenirRegistration->extra_data['referred_By']??'' }}</td>
                                <td>
                                <b>Date</b>
                                    {{paymentDate($payment) }}
                                    <br>
                                    <b>Transaction Id </b>
                                    {{ $payment->unique_id_for_payment??'' }}
                                    @if($payment->paymentmethord->name == config('conventions.other_name_db'))
                                         <br>
                                        <b>On Behalf Of</b>
                                        {{ $payment->more_info['Payment_made_through']??""  }}
                                        <br>
                                    <b>Company Name</b>
                                        {{ $payment->more_info['company_name'] ??""  }}
                                    @endif

                                    @if($payment->paymentmethord->name == config('conventions.check_name_db'))
                                         <br>
                                        <b>Bank Name</b>
                                        {{ $payment->more_info['bank_name'] ??""  }}
                                        <br>
                                    <b>Check Received By</b>
                                        {{ $payment->more_info['check_received_by']??""   }}
                                    @endif
                                </td>
                                <td>
                                      <a href="{{url('souvenir-registration')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a> 
                                </td>
                                
                            </tr>
                            @endforeach
                           
                        </tbody>
                           
                        </table>
                        @endif


                         @if(count($youth_banquet_reg)>0)
                          <br>
                        <h6><b>Youth Banquet Registration :</b></h6>
                         <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                            
                            <th>Sl NO.</th>
                            <th>Convention Registration</th>
                            <th>Whatsapp</th>
                            <th>Parent/Guardian Phone</th>
                            <th>Parent Guardian email</th>
                            <th>Kids Details</th>
                            
                            <th>Registered Date</th>
                            
                        </tr>
                        </thead>
                        <tbody>
                           
                           @foreach($youth_banquet_reg as $registration)
                           <tr>
                                <td>{{ $loop->iteration }} </td>
                                <td>{{ $registration->convention_id }}</td>
                               
                                <td>{{ $registration->whatsapp }}</td>
                                <td>{{ $registration->parent_phone }}</td>
                                <td>{{ $registration->parent_email }}</td>


                                <td>
                                    @foreach(explode('||',$registration->FirstName) as $key=>$value)

                                    FirstName: {{explode('||',$registration->FirstName)[$key]??'' }}<br>
                                    LastName: {{explode('||',$registration->LastName)[$key]??'' }}
                                    <br>
                                    Age: {{explode('||',$registration->age)[$key]??'' }}
                                     <br>
                                    Email: {{explode('||',$registration->email)[$key]??'' }}
                                     <br>
                                    Phone: {{explode('||',$registration->phone)[$key]??'' }}
                                    <hr>

                                     @endforeach
                                </td>
                               
                                <td>{{ $registration->created_at }}</td>
                                
                            </tr>
                            @endforeach
                           
                        </tbody>
                           
                        </table>
                        @endif







                    </div>
                    <div class="fs15 mt-3 font-weight-bold">Comments from Registration Committee: {!! Auth::user()->registration_note !!} </div>
                </div>
            </div>
        </div>
    </div>
</section>



@section('javascript')

<script>
$(document).ready(function() {
    
    $('.radio').click(function(){
         data = {
            "_token": $('meta[name=csrf-token]').attr('content'),
            "user_id": {{Auth::user()->id}},
        }
        ajaxCall('{{ env("APP_URL") }}/api/update-is-required-fields?type=' + $(this).attr('name') +'&&val='+$(this).val() , 'put', data, afterupdate)
    })

    function afterupdate(data){
        
        swal({
                title: "success!",
                text: "Status Updated!",
                icon: "success",
            });
    }
    
    
    @if( strtotime('06/15/2022')<strtotime(Carbon\Carbon::now()) )
            $('.radio').attr('disabled','disabled')
    @endif
    
})
</script>



@endsection


@endsection
