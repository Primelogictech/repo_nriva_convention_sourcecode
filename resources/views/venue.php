<style type="text/css">
    p {
        font-size: 15px;
    }
    .nav-pills .nav-link.active,
    .nav-pills .show > .nav-link {
        color: #fff;
        background-color: #ff9900;
    }
    .nav-link {
        color: #fff;
        background: #050a30;
        margin: 5px;
        font-size: 16px;
    }
    .nav-link:focus,
    .nav-link:hover {
        color: #fff;
        background: #050a30;
    }
    .show-neighbors {
        overflow: hidden;
    }
    .show-neighbors .item__third:first-child,
    .show-neighbors .item__third:last-child {
        display: none;
    }
    .carousel-control-prev {
        left: 0px !important;
    }
    .carousel-control-next {
        right: 0px !important;
    }
    .fs50 {
        font-size: 55px;
    }
    .carousel-control-prev-icon {
        background-image: none;
    }
    .carousel-control-next-icon {
        background-image: none;
    }
    .carousel-control-next,
    .carousel-control-prev {
        position: absolute;
        top: -25%;
        width: auto;
        opacity: 1;
    }

    /*FAQ's*/

    .card {
        background-color: transparent;
        border: 0px solid;
    }
    .card-header {
        padding: 0px;
    }
    .card-header a {
        background: #784d98;
        color: #fff;
        padding: 0.75rem 1.25rem;
        width: 100%;
        display: block;
    }
    .p-radio-btn {
        position: absolute;
        top: 3px;
    }
    .card-header a::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/plus.png);
    }
    .card-header a[aria-expanded="true"]::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/minus.png);
    }

    @media (min-width: 992px) {
        .carousel-control-next,
        .carousel-control-prev {
            position: absolute;
            top: -35% !important;
        }

        .carousel-control-next,
        .carousel-control-prev {
            font-size: 55px;
        }
    }

    @media (max-width: 768px) {
        .carousel-control-next,
        .carousel-control-prev {
            font-size: 37px;
        }
        .item__third h6 {
            font-size: 14px !important;
        }
    }
    @media (min-width: 768px) {
        .carousel-control-next,
        .carousel-control-prev {
            position: absolute;
            top: -55%;
        }
        .show-neighbors .carousel-indicators {
            margin-right: 25%;
            margin-left: 25%;
        }
        .show-neighbors .carousel-control-prev,
        .show-neighbors .carousel-control-next {
            background: rgba(255, 255, 255, 0.2);
            width: 25%;
            z-index: 11;
            /* .carousel-caption has z-index 10 */
        }
        .show-neighbors .carousel-inner {
            width: 150%;
            left: -25%;
        }
        .show-neighbors .carousel-item-next:not(.carousel-item-left),
        .show-neighbors .carousel-item-right.active {
            -webkit-transform: translate3d(33%, 0, 0);
            transform: translate3d(33%, 0, 0);
        }
        .show-neighbors .carousel-item-prev:not(.carousel-item-right),
        .show-neighbors .carousel-item-left.active {
            -webkit-transform: translate3d(-33%, 0, 0);
            transform: translate3d(-33%, 0, 0);
        }
        .show-neighbors .item__third {
            display: block !important;
            float: left;
            position: relative;
            padding-left: 20px;
            padding-right: 20px;
            width: 33.33333333%;
            border-right: 2px solid #050a30;
        }
    }

    @media (max-width: 767px) {
        .carousel-control-next,
        .carousel-control-prev {
            font-size: 40px;
        }
        .show-neighbors .carousel-indicators {
            margin-right: 25%;
            margin-left: 25%;
        }
        .show-neighbors .carousel-control-prev,
        .show-neighbors .carousel-control-next {
            background: rgba(255, 255, 255, 0.2);
            width: 25%;
            z-index: 11;
            /* .carousel-caption has z-index 10 */
        }
        .show-neighbors .carousel-inner {
            width: 150%;
            left: -25%;
        }
        .show-neighbors .carousel-item-next:not(.carousel-item-left),
        .show-neighbors .carousel-item-right.active {
            -webkit-transform: translate3d(33%, 0, 0);
            transform: translate3d(33%, 0, 0);
        }
        .show-neighbors .carousel-item-prev:not(.carousel-item-right),
        .show-neighbors .carousel-item-left.active {
            -webkit-transform: translate3d(-33%, 0, 0);
            transform: translate3d(-33%, 0, 0);
        }
        .show-neighbors .item__third {
            display: block !important;
            float: left;
            position: relative;
            padding-left: 10px;
            padding-right: 10px;
            width: 33.33333333%;
            border-right: 1px solid #050a30;
        }
        .item__third h5 {
            font-size: 8px !important;
        }
        .carousel-control-next,
        .carousel-control-prev {
            top: -37% !important;
        }
    }
    @media (max-width: 640px) {
        .item__third h6 {
            font-size: 8px !important;
        }
    }
    @media (max-width: 576px) {
        .carousel-control-next,
        .carousel-control-prev {
            font-size: 13px;
        }
        .item__third h6 {
            font-size: 7px !important;
        }
    }
</style>

<div class="container-fluid py-4">
    <div class="row mx-0 mx-sm-3">
        <div class="col-12 shadow-small py-0 pt-1">
            <div class="row">
                <div class="col-12 px-1">
                    <div>
                        <img src="images/banners/venue.jpg" class="img-fluid w-100" alt="" />
                    </div>
                </div>
            </div>
            <div class="row my-4">
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="shadow-small p-3">
                        <ul class="nav nav-pills flex-column" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="venue-tab" data-toggle="tab" href="#venue" role="tab" aria-controls="venue" aria-selected="true">Venue</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="hotels-tab" data-toggle="tab" href="#hotels" role="tab" aria-controls="hotels" aria-selected="false">Hotels</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="preferred-hotels-tab" data-toggle="tab" href="#preferred-hotels" role="tab" aria-controls="preferred-hotels" aria-selected="false">Preferred Hotels</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="transportation-tab" data-toggle="tab" href="#transportation" role="tab" aria-controls="transportation" aria-selected="false">Transportation</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-8 col-lg-9 py-3 py-md-0 pl-3 pl-md-0">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="venue" role="tabpanel" aria-labelledby="venue-tab">
                            <div class="col-12 shadow-small py-3">
                                <div class="row">
                                    <div class="col-12">
                                        <h4 class="text-orange fs-sm-20">Schaumburg Convention Center</h4>
                                        <h5 class="text-violet fs-sm-18">The powerhouse meeting facility in the Midwest with abundant free parking</h5>
                                        <p class="pt-2 mb-0">
                                            The sophisticated 100,000 square foot state-of-the-art Convention Center features pillar-free space, 30-foot ceilings, 120 feet of covered dock space and 750 MB Wi-Fi access throughout. Seamlessly
                                            connected is an additional 48,000 square feet of meeting space including a 27,500 square-foot ballroom at the 500 guest room Renaissance Schaumburg Convention Center Hotel. It’s the perfect
                                            combination of high-tech performance and total travel convenience.
                                        </p>
                                    </div>

                                    <div class="col-12 py-5">
                                        <div class="bd-example">
                                            <div id="carouselExampleCaptions" class="carousel slide show-neighbors" data-ride="carousel" data-interval="100000">
                                                <!-- <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                                            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                                            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                                        </ol> -->

                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <div class="item__third">
                                                            <img src="images/venue/image1.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Schaumburg Convention Center</h6>
                                                        </div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="item__third">
                                                            <img src="images/venue/image2.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Schaumburg Convention Center Entrances</h6>
                                                        </div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="item__third">
                                                            <img src="images/venue/image3.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Renaissance Schaumburg Convention Center Hotel</h6>
                                                        </div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="item__third">
                                                            <img src="images/venue/image4.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Renaissance Schaumburg Convention Center Hotel with 500 guest rooms</h6>
                                                        </div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="item__third">
                                                            <img src="images/venue/image5.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Schaumburg Convention Center Trade Show Floor</h6>
                                                        </div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="item__third">
                                                            <img src="images/venue/image6.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Schaumburg Convention Center - Aquatics Show</h6>
                                                        </div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="item__third">
                                                            <img src="images/venue/image7.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Schaumburg Convention Center - Gymnastics and Tumbling Event</h6>
                                                        </div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="item__third">
                                                            <img src="images/venue/image8.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Reimagined Trade Show Space</h6>
                                                        </div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="item__third">
                                                            <img src="images/venue/image9.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Schaumburg Ballroom Pre-function space</h6>
                                                        </div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="item__third">
                                                            <img src="images/venue/image10.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Perfection Boardroom</h6>
                                                        </div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="item__third">
                                                            <img src="images/venue/image11.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Musical entertainment seasonally on The Terrace</h6>
                                                        </div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="item__third">
                                                            <img src="images/venue/image12.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Warmth</h6>
                                                        </div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="item__third">
                                                            <img src="images/venue/image13.jpg" class="d-block w-100" alt="" />
                                                            <h6 class="py-2 text-center">Plenty of room to spread out in the lobby</h6>
                                                        </div>
                                                    </div>
                                                </div>

                                                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true">
                                                        <i class="fas fa-long-arrow-alt-left text-white"></i>
                                                    </span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true">
                                                        <i class="fas fa-long-arrow-alt-right text-white"></i>
                                                    </span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 pb-4">
                                        <h5 class="text-violet fs-sm-18"><strong>Hotel 1 : Renaissance Schaumburg Convention Center Hotel</strong></h5>

                                        <p class="mb-0">
                                            is seamlessly connected to the Convention Center. This visually arresting escape features a clean, bold architectural design. Here, you’ll find a lifestyle hotel experience that inspires and
                                            provokes guests to discover something wonderfully new and unique each and every time they stay. Your guest room is the ultimate personal space, an imaginatively reinvented urban retreat where a
                                            new generation of travelers can discover unmatched urbane comfort and stylish design. Concierge Level rooms feature upgraded amenities and exclusive use of our VIP lounge offering complimentary
                                            breakfast and evening hors d’oeuvres.
                                        </p>
                                    </div>

                                    <div class="col-12 col-lg-8 offset-lg-2 pb-5">
                                        <div class="text-center">
                                            <iframe
                                                width="100%"
                                                height="315"
                                                src="https://www.youtube.com/embed/3j7rbPoJeBI"
                                                title="YouTube video player"
                                                frameborder="0"
                                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen=""
                                            ></iframe>
                                        </div>
                                    </div>

                                    <div class="col-12 pb-2">
                                        <h5 class="text-violet fs-sm-18"><strong>Hotel 2 : Hyatt Regency Schaumburg, Chicago</strong></h5>
                                    </div>
                                    <div class="col-12 pb-4">
                                        <div class="row px-3">
                                            <div class="col-12 shadow-small py-3">
                                                <div class="row">
                                                    <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                        <div>
                                                            <img src="images/hotels/hotel-2.jpg" class="img-fluid mx-auto d-block w-100" />
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                        <div class="fs16 pb-3">
                                                            <div>1800 East Golf Road</div>
                                                            <div>Schaumburg, Illinois 60173 United States</div>
                                                        </div>
                                                        <div class="fs16">
                                                            <div class="pb-3">
                                                                <div class="font-weight-bold text-violet">Phone</div>
                                                                <div data-locator="hotel-phone">+1 847 605 1234</div>
                                                            </div>
                                                            <div class="pb-3">
                                                                <div class="font-weight-bold text-violet">Email</div>
                                                                <div><a href="mailto:chirw.rfp@hyatt.com" target="_blank">chirw.rfp@hyatt.com</a></div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-sm-3 col-md-4 col-lg-4 pr-3 pr-sm-0">
                                                                <div class="py-1">
                                                                    <a href="https://www.hyatt.com/en-US/group-booking/CHIRW/G-NVIB/OPEN" target="_blank" class="btn btn-violet px-4">Book Now</a>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6 col-md-7 col-lg-8">
                                                                <div class="py-1">
                                                                    <a href="https://www.hyatt.com/en-US/hotel/illinois/hyatt-regency-schaumburg-chicago/chirw" target="_blank" class="btn btn-violet px-4">View Hotel Website</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="row mx-0">
                                            <div class="col-12 mb-5 shadow-small py-3">
                                                <h5 class="text-violet fs-sm-18"><strong>Disclaimer :</strong></h5>

                                                <p class="mb-0">
                                                    Renaissance Schaumburg Convention Center Hotel and Hyatt Regency are Convention accommodation hotels and will have breakfast and transportation services. Due to the limited availability of
                                                    different configurations of rooms all the requests will be handled based on the availability.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 pb-5">
                                        <h5 class="text-violet"><strong>NEARBY ATTRACTIONS</strong></h5>
                                        <ul class="fs15 mb-0">
                                            <li>Go on a shopping spree at Woodfield Mall, Streets of Woodfield, the Arboretum of South Barrington, and Ikea</li>
                                            <li>Go back in time at Medieval Times and watch the top knights in the kingdom battle to determine one victor to protect the throne. You can feast and raise a goblet to the Queen.</li>
                                            <li>
                                                Spring Valley Nature Sanctuary is a refuge of135 acres of fields, forests, and marshes. Features over three miles of handicapped-accessible hiking trails, a nature center, and an 1880s living
                                                history farm
                                            </li>
                                            <li>Watch the Boomers swing for the fences at Wintrust Field, our minor league baseball team in town</li>
                                            <li>Discover the contemporary art at The Chicago Athenaeum Sculpture Park</li>
                                            <li>Practice your swing at Top Golf Schaumburg</li>
                                            <li>
                                                Enjoy Busse Woods one of the largest and mostdiverse forest preserves at 3,558-acres. The site includes one of the largestfishing and boating waters in Cook County, nearly 13 miles of paved
                                                trails, anelk pasture and much more.
                                            </li>
                                            <li>Make your bets at the Rivers Casino DesPlaines</li>
                                            <li>Main Event is a dining and entertainmentdestination that offers bowling, laser tag, billiards, full arcade, and avirtual reality experience!</li>
                                        </ul>
                                    </div>

                                    <div class="col-12 pb-5">
                                        <h5 class="text-violet"><strong>Contact</strong></h5>
                                        <div class="fs15">
                                            <div>Bhargava Venishetty</div>
                                            <div class="py-1 font-weight-bold">Hotels & Accomodations Committee - Chair</div>
                                            <div>
                                                <a href="#">hotels@nriva.org</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg-8 offset-lg-2 pb-5">
                                        <div class="text-center">
                                            <a
                                                href="https://assets.simpleviewinc.com/simpleview/image/upload/v1/clients/chicagonorthwest/Meet_Chicago_Northwest_Visitors_Guide_Final_small_d9a4b2fc-880c-49a9-a4e5-1e892ac62f53.pdf"
                                                target="_black"
                                            >
                                                <img src="images/venue/visitors-guide-image.jpg" class="img-fluid" alt="" />
                                            </a>
                                        </div>
                                    </div>
                                    <!-- <div class="col-12 pb-5">
                                <div class="img-thumbnail pb-0">
                                    <iframe
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2962.1839619261896!2d-88.04307988455315!3d42.06067607920813!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880faf87fa197467%3A0xd71fbedf03ecdc5!2sRenaissance%20Schaumburg%20Convention%20Center%20Hotel!5e0!3m2!1sen!2sin!4v1634983917473!5m2!1sen!2sin"
                                        width="100%"
                                        height="350"
                                        class=""
                                        style="border: 0;"
                                        allowfullscreen=""
                                        loading="lazy"
                                    ></iframe>
                                </div>
                            </div> -->
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="hotels" role="tabpanel" aria-labelledby="hotels-tab">
                            <div class="col-12 shadow-small py-3">
                                <div class="row px-3">
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/hotels/hotel-1.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <h6 class="text-violet fs18">Hotel 1 : Renaissance Schaumburg Convention Center Hotel</h6>
                                                <div class="fs16 pb-3">
                                                    <div>1551 N. THOREAU DR.,</div>
                                                    <div>Schaumburg, Illinois 60173, United States</div>
                                                </div>
                                                <div class="fs16">
                                                    <div class="pb-3">
                                                        <div class="font-weight-bold text-violet">Phone</div>
                                                        <div data-locator="hotel-phone">+1 312-795-3473</div>
                                                    </div>
                                                    <!-- <div class="pb-3">
                                                        <div class="font-weight-bold text-violet">Email</div>
                                                        <div><a href="#" target="_blank"></a></div>
                                                    </div> -->
                                                </div>
                                                <div class="row">
                                                    <!-- <div class="col-12 col-sm-3 col-md-4 col-lg-4 pr-3 pr-sm-0">
                                                        <div class="py-1">
                                                            <a class="btn btn-violet px-4">Book Now</a>
                                                        </div>
                                                    </div> -->
                                                    <div class="col-12 col-sm-6 col-md-7 col-lg-8">
                                                        <div class="py-1">
                                                            <a href="https://www.marriott.com/en-us/hotels/chirs-renaissance-schaumburg-convention-center-hotel/overview/" target="_blank" class="btn btn-violet px-4">View Hotel Website</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/hotels/hotel-2.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <h6 class="text-violet fs18">Hotel 2 : Hyatt Regency Schaumburg, Chicago</h6>
                                                <div class="fs16 pb-3">
                                                    <div>1800 East Golf Road</div>
                                                    <div>Schaumburg, Illinois 60173 United States</div>
                                                </div>
                                                <div class="fs16">
                                                    <div class="pb-3">
                                                        <div class="font-weight-bold text-violet">Phone</div>
                                                        <div data-locator="hotel-phone">+1 847 605 1234</div>
                                                    </div>
                                                    <div class="pb-3">
                                                        <div class="font-weight-bold text-violet">Email</div>
                                                        <div><a href="mailto:chirw.rfp@hyatt.com" target="_blank">chirw.rfp@hyatt.com</a></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-3 col-md-4 col-lg-4 pr-3 pr-sm-0">
                                                        <div class="py-1">
                                                            <a href="https://www.hyatt.com/en-US/group-booking/CHIRW/G-NVIB/OPEN" target="_blank" class="btn btn-violet px-4">Book Now</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-md-7 col-lg-8">
                                                        <div class="py-1">
                                                            <a href="https://www.hyatt.com/en-US/hotel/illinois/hyatt-regency-schaumburg-chicago/chirw" target="_blank" class="btn btn-violet px-4">View Hotel Website</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <h5 class="text-violet fs-sm-18"><strong>Disclaimer :</strong></h5>

                                        <p class="mb-0">
                                            Renaissance Schaumburg Convention Center Hotel and Hyatt Regency are Convention accommodation hotels and will have breakfast and transportation services. Due to the limited availability of
                                            different configurations of rooms all the requests will be handled based on the availability.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="preferred-hotels" role="tabpanel" aria-labelledby="preferred-hotels-tab">
                            <div class="col-12 shadow-small py-3">
                                <div class="row px-3">
                                    <div class="col-12 shadow-small py-3">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/hotels/pre-hotel-1.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <h6 class="text-violet fs18">Hotel 1 : Sheraton Suites Chicago Elk Grove</h6>
                                                <div class="fs16 pb-3">
                                                    <div>121 Northwest Point Boulevard Elk Grove Village,</div>
                                                    <div>Illinois 60007, United States</div>
                                                </div>
                                                <div class="fs16">
                                                    <div class="pb-3">
                                                        <div class="font-weight-bold text-violet">Phone</div>
                                                        <div data-locator="hotel-phone">+1 847-290-1600</div>
                                                    </div>
                                                    <!-- <div class="pb-3">
                                                        <div class="font-weight-bold text-violet">Email</div>
                                                        <div><a href="#" target="_blank"></a></div>
                                                    </div> -->
                                                </div>
                                                <div class="row">
                                                    <!-- <div class="col-12 col-sm-3 col-md-4 col-lg-4 pr-3 pr-sm-0">
                                                        <div class="py-1">
                                                            <a class="btn btn-violet px-4">Book Now</a>
                                                        </div>
                                                    </div> -->
                                                    <div class="col-12 col-sm-6 col-md-7 col-lg-8">
                                                        <div class="py-1">
                                                            <a href=" https://www.marriott.com/events/start.mi?id=1652738690922&key=GRP" target="_blank" class="btn btn-violet px-4">View Hotel Website</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="transportation" role="tabpanel" aria-labelledby="transportation-tab">
                            <div class="col-12 shadow-small py-3">
                                <div class="row">
                                    <div class="col-12">
                                        <h5 class="text-violet"><strong>Airport</strong></h5>
                                        <p>
                                            Chicago Northwest is a unique location to visit with two airports nearby: O’Hare International Airport (17 miles) and Midway Airport (32 miles). Both airports allow you the opportunity to choose
                                            from over 1,000 daily direct flights to our region. Southwest Airlines now offers direct flights from select cities to O’Hare and Midway. O’Hare International Airport is a dual-hub: American and
                                            United.
                                        </p>
                                        <h5 class="text-violet"><strong>Rental Cars</strong></h5>
                                        <p>O’Hare’s Multi-Modal Facility is the hotspot for one-stop rental car pick-up and drop-off. Take the Airport Transit System (ATS) to easily and conveniently get your ride and go.</p>
                                        <p>
                                            Midway shuttle service is available at Lower Level door number 2. Operation is available 24/7 and every 15 minutes.
                                        </p>
                                        <h5 class="text-violet"><strong>Ride-hailing Services</strong></h5>
                                        <p>At O’Hare, passenger pick-up is available at Terminal 2 - Upper Level (between Door 2A and Door 2D) and Terminal 5 - Lower Level (Door 5B).</p>
                                        <p>
                                            At Midway, passenger pick-up is available on the Lower Level/Baggage Claim (Door 4).
                                        </p>
                                        <h5 class="text-violet"><strong>Suburban Taxi Cabs</strong></h5>
                                        <p>
                                            All suburban cabs are operated as prearranged rides or dispatched rides. Suburban cabs are called to the terminals for specific passengers when you get to O’Hare’s Baggage Claim (lower level).
                                            Instructions will be given on where to meet your taxi and a taxi number will be given to identify the correct vehicle. You will meet your taxi in the center lane.
                                        </p>
                                        <p>
                                            Be sure you know which terminal you are in when you call in your order. Your taxi can usually be at the curb in 10 minutes from the time of your call, unless you are told otherwise. During peak
                                            travel times, you may encounter a delay as a result of the high volume traffic entering the terminals. Due to security procedures in place at this time, your assigned vehicle cannot wait at the
                                            curb for you, by law.
                                        </p>
                                        <p>
                                            Utilizing a suburban taxi company will save you money. Courtesy phones are available in baggage claim. The taxi stand line will result in double the cost to you. These are City of Chicago taxis.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>