@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1 col-lg-6 offset-lg-3 py-2">

                @if ($errors->any())
                @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
                @endforeach
                @endif
               

                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif

                @yield('content')

                <div class="shadow-small border-radius-5 py-4 px-2 p-md-4">
                    <h4 class="text-center">Create/Change Password</h4>
                     <form method="POST" action="{{ url('change_password') }}" class="col-12 pt-3 p-0">
                        @csrf
                        <div class="col-12">
                            <label>New Password:</label><span class="text-red">*</span>
                            <div>
                                <input type="password" id="password" name="password" required autofocus class="form-control" placeholder="Enter Your New Password" />
                            </div>
                        </div>
                         <div class="col-12">
                            <div class="form-group">
                                <label>Confirm Password</label><span class="text-red">*</span>
                                <div>
                                    <input type="password" name="confirmpassword"  class="form-control" placeholder="Enter Confirm Password" required />
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-12 pt-3">
                            <div>
                                <button class="btn btn-violet w-100 border-radius-5 signin-btn"> {{ __('Submit') }}</button>
                            </div>
                        </div>
                    </form>

                </div>
                
            </div>
        </div>
    </div>
    </div>
    </div>
</section>


@section('javascript')
<script type="text/javascript">
   
</script>

@endsection


@endsection
