@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Corporate Sponsor Type</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('corporatesponsors.update', $CorporateSponsortype->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Donortype Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$CorporateSponsortype->name}}" name="name" placeholder="donortype Name">
                        </div>
                    </div>

                    
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Benfits<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="benfits">{{$CorporateSponsortype->benfits}}</textarea>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Amount</label>
                        <div class="col-md-8 my-auto">
                            <input type="number" class="form-control" value="{{$CorporateSponsortype->amount}}" name="amount" placeholder="Display Order">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Display Order</label>
                        <div class="col-md-8 my-auto">
                            <input type="number" class="form-control" value="{{$CorporateSponsortype->display_order}}" name="display_order" placeholder="Display Order">
                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')

<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>


<script>
    CKEDITOR.replace('benfits');
</script>
@endsection

@endsection
