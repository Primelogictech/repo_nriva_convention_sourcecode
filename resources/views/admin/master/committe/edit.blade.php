@extends('layouts.admin.base')
@section('content')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit Committee Details</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('committe.index')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('committe.update', $committe->id )}}" method="post" enctype="multipart/form-data" id="form">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Committe Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$committe->name}}" name="name" placeholder="committe Name">
                        </div>
                    </div>

                     <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Committe Slug<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$committe->slug}}" name="slug" placeholder="committe Slug">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Display Order</label>
                        <div class="col-md-8 my-auto">
                            <input type="number" class="form-control" value="{{$committe->display_order}}" name="display_order" placeholder="Display Order">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Committe Mail<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="email" class="form-control" value="{{$committe->committe_email}}" name="committe_email" placeholder="Committe Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Registartion Form</label>
                        <div class="col-md-8 my-auto">
                       <select class="form-control" name="registration_type">
                        <option value="0">Select Registartion Form</option>
                       @foreach($registartionforms as $forms)

                       <option value="{{$forms->id}}" @if($committe->registration_type==$forms->id) selected @endif>{{$forms->name}}</option>

                       @endforeach
                           
                       </select>
                   </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Upload Committee Icon <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="file" class="form-control" name="image">
                            <b>Image should be in 1:1 ration eg: 200X200</b>
                        </div>
                        <div>
                            <img src="{{asset(config('conventions.committe_display').$committe->image_url)}}" alt="{{$committe->name}}" width="200px" height="200px" class="img-fluid">
                        </div>
                    </div>

                    

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Banner Upload Image <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="file" class="form-control" name="banner_image">
                            <b>Image should be in 1:1 ration eg: 960X250</b>
                        </div>
                        <div>
                            <img src="{{asset(config('conventions.committe_display').$committe->banner_image)}}" alt="{{$committe->name}}" width="200px" height="200px" class="img-fluid">
                        </div>
                    </div>

                     <input type="checkbox" class="program_checkbox" name="is_program" value="1" @if($committe->is_program > 0) checked @endif> Create Program
                    <div class="program_list" style="display: @if($committe->is_program > 0) block;    @else none; @endif">

                    <!-- <div>
                        <h5 class="mb-3">Contact Details :</h5>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Chair Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" name="chair_name" value="{{ $committe->programList->chair_name??'' }}" placeholder="Chair Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Mobile Number<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="number" class="form-control" name="mobile_number" value="{{ $committe->programList->mobile_number??'' }}" placeholder="Mobile Number">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Co-Chair Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" name="co_chair_name" value="{{ $committe->programList->co_chair_name??'' }}" placeholder="Co-Chair Name">
                        </div>
                    </div> -->
                    <div>
                        <h5 class="mb-3">Program Details :</h5>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Program Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{ $committe->programList->program_name??'' }}" name="program_name" placeholder="Program Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Date<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="date" class="form-control" value="{{ $committe->programList->date??'' }}" name="date" min="<?php echo date("Y-m-d"); ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Time<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" id="programtime" class="form-control" value="{{ $committe->programList->programtime??'' }}" name="programtime">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Location<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" name="Location" value="{{ $committe->programList->Location??'' }}" placeholder="Location">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Description</label>
                        <div class="col-md-8 my-auto">
                            <textarea name="page_content">{{ $committe->programList->page_content??'' }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Upload Event Image <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="file" class="form-control" name="programimage">
                            <b>Image should be in 4:3 ration eg: 375X281</b>
                        </div>
                    </div>
                    @if($committe->programList)
                     <img src="{{asset(config('conventions.program_display').$committe->programList->image_url??'')}}" alt="event" width=100 class="img-fluid">
                     @endif
                </div>





                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@section('javascript')
<script>
    $('.program_checkbox').change(function(){ 
        if($(this).is(':checked')){ 
            $('.program_list').show();
        }else{
          
            $('.program_list').hide();
        }
    })
    $(".count-checkbox").change(function() {
        if (IsCheckBoxChecked(this)) {
            $('.count-field').show()
        } else {
            $('.count-field').hide()
        }
    })
</script>


<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script>
    CKEDITOR.replace('page_content');
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#programtime').timepicker({});
});
    $(document).ready(function(){
    $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        });
          $.validator.addMethod("numberss", function(value, element) {
            return this.optional(element) || value == value.match(/^[0-9) (-]+$/);
        });

          $.validator.addMethod("checking", function(value, element) {
            if($("input[name='is_program']").is(":checked")== true && value == ""){  
                        return false;
             }else{
                return true;
             }

             
        });

        $("#form").validate({
            rules: {               
                name: "required alpha" ,
                committe_email:"required",                                     
                program_name:"checking",  
                date:"checking",              
                programtime:"checking",              
                Location:"checking",              
                                         
            },
             messages: {
                "name": {
                    required: "Committe Name is Required",
                    alpha: "First name should not contain numbers.",
                },
                "committe_email": {
                    required: "Committe Email is Required"
                 },
                 "chair_name": {
                    required: "Committe Chair Name is Required",
                    checking:"Committe Chair Name is Required"
                 },
                 "mobile_number": {
                    required: "Mobile Number is Required",
                    checking:"Mobile Number is Required"
                 },
                 "co_chair_name": {
                    required: "Committe Co Chair Name is Required",
                    checking:"Committe Co Chair Name is Required"
                 },
                 "program_name": {
                    required: "Program Name is Required",
                    checking:"Program Name is Required"
                 },
                  "programtime": {
                    required: "Program Time is Required",
                    checking:"Program Time is Required"
                 },
                 "date": {
                    required: "Date is Required",
                    checking:"Date is Required"
                 },
                 "Location": {
                    required: "Location is Required",
                    checking:"Location is Required"
                 },
                 "page_content": {
                    required: "Description is Required",
                    checking:"Description is Required"
                 }

            },
        });

    });

</script>

@endsection


@endsection