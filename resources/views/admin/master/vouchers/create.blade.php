@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Add Vouchers</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('vouchers.index')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('vouchers.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Vouchers Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{old('name')}}" name="name" placeholder="Vouchers Name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Discount Type<span class="mandatory">*</span></label>
                        <div class="col-md-4 my-auto">
                            <input type="radio" value="1" name="discount_type" placeholder="Discount"> Discount in Percentage(%)
                        </div>
                         <div class="col-md-4 my-auto">
                            <input type="radio"  value="2" name="discount_type" placeholder="Discount"> Discount in Dollar($)
                        </div>
                    </div>
                     <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Discount(%)<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="number" class="form-control" value="{{old('discount')}}" name="discount" placeholder="Discount">
                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')


@endsection


@endsection
