@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit Mr And Miss Type</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('mrandmisstype.update', $MrAndMissTypes->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}
                    <input type="hidden" name="id" value="{{$MrAndMissTypes->id??''}}">
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">MrAndMissTypes Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$MrAndMissTypes->name}}" name="name" placeholder="MrAndMissTypes Name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Title<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" name="title" placeholder="Title" required="" value="{{$MrAndMissTypes->title}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Min Age<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" name="min_age" placeholder="Min Age" required="" value="{{$MrAndMissTypes->min_age}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Max Age<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" name="max_age" placeholder="Max Age" required="" value="{{$MrAndMissTypes->max_age}}">
                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')


@endsection

@endsection
