@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit Menu</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('menu.index')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>
<!-- /Page Header -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{url('admin/update-admin-menu')}}" method="post">
                    @csrf
                    
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Parent Menu</label>
                        <div class="col-md-8 my-auto">
                            <input type="hidden" value="{{$menu->id}}" name="menu_id">
                            <input type="text" class="form-control" value="{{$main_menu_name[$menu->parent_id]}}" name="parent_name" readonly="true">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Menu Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$menu->menu_name}}" name="menu_name" placeholder="Menu Name">
                        </div>
                    </div>

                    
                    <div class="">
                        <div class="slug-field form-group row" style="display:none">
                            <label class="col-form-label col-md-4 my-auto ">Slug Name<span class="mandatory">*</span></label>
                            <div class="col-md-8 my-auto my-auto">
                                <input type="text" class="form-control" name="slug" value="{{$menu->slug}}" placeholder="Slug" readonly="true">
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script>
    $(document).ready(function() {
        if ('{{$menu->slug}}' != "") {
            $('.slug-field').show()
        } else {
            $('.slug-field').hide()
        }

        $(".slug-checkbox").change(function() {
            if (IsCheckBoxChecked(this)) {
                $('.slug-field').show()
            } else {
                $('.slug-field').hide()
            }
        })
    })
</script>

@endsection

@endsection
