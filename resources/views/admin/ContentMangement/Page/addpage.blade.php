@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Add Pages</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('page.index')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('page.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Select Menu<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <select class="form-control dropdown-element" name="parent_id" data-url='{{env('APP_URL')}}/admin/get-submenu-with-menu'>
                                <option selected disabled>-- Select --</option>
                                @foreach($menus as $menu)
                                <option value="{{$menu->id}}">{{$menu->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Select Submenu<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <select class="form-control dropdown-target" name="id">
                                <option selected disabled>-- Select --</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Add Page Details<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="page_content"></textarea>
                        </div>
                    </div>


                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')

<script type="text/javascript" src="{{asset('public/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('public/plugins/ckfinder/ckfinder.js')}}"></script>
<script>
    CKEDITOR.replace('page_content', {
        contentsCss: "{{ asset('css/editor.css')}}",
        filebrowserBrowseUrl: "{{ asset('public/plugins/ckfinder/ckfinder.html') }}",
        filebrowserImageBrowseUrl: "{{ asset('public/plugins/ckfinder/ckfinder.html?type=Images') }}",
        filebrowserUploadUrl: "{{ asset('public/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')}}",
        filebrowserImageUploadUrl: "{{ asset('public/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')}}",
    });
</script>


@endsection


@endsection
