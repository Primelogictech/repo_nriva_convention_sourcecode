<table class="datatable table table-hover table-center mb-0">
    <thead>
        <tr>
            <th>Sl NO.</th>
            <th>Convention Registration</th>
            <th>Whatsapp</th>
            <th>Parent/Guardian Phone</th>
            <th>Parent Guardian email</th>
            <th>Kids Details</th>

            <th>Registered Date</th>

        </tr>
    </thead>
    <tbody>
        @foreach ($registrations as $registration)
        <tr>
            <td>{{ $loop->iteration }} </td>
            <td>{{ $registration->convention_id }}</td>

            <td>{{ $registration->whatsapp }}</td>
            <td>{{ $registration->parent_phone }}</td>
            <td>{{ $registration->parent_email }}</td>


            <td>
                @foreach(explode('||',$registration->FirstName) as $key=>$value)

                FirstName: {{explode('||',$registration->FirstName)[$key]??'' }}<br>
                LastName: {{explode('||',$registration->LastName)[$key]??'' }}
                <br>
                Age: {{explode('||',$registration->age)[$key]??'' }}
                <br>
                Email: {{explode('||',$registration->email)[$key]??'' }}
                <br>
                Phone: {{explode('||',$registration->phone)[$key]??'' }}
                <br>
                ------------------
                <br>
                @endforeach
            </td>

            <td>{{ $registration->created_at }}</td>

        </tr>
        @endforeach
    </tbody>


</table>
