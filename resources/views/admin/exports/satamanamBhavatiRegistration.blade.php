<table class="datatable table table-hover table-center mb-0">
    <thead>
        <tr>
            <th>Sl NO.</th>
            <th>Registration Id</th>
            <th>Full Name and Email</th>
            <th>No of Participants:</th>
            <th>Instructions To Sponsors:</th>
            <th>Registration Details:</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $registration)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $registration->registration_id }}</td>
            <td>
                {{ $registration->full_name }}<br>
                {{ $registration->email }}
            </td>
            <td>{{ $registration->extra_data['number_of_participants']??""}}</td>
            <td>{{ $registration->extra_data['instructions_to_sponsors']??""}}</td>
            <td>
                @php
                $count=1;
                @endphp
                @while (array_key_exists('ticket_'.$count,$registration->extra_data))
                <h6>Details for ticket {{$count}} </h6>
                <b>Father (In-Law) Details : </b><br>
                <div>
                    <b>Name:</b> {{$registration->extra_data['ticket_'.$count]['father_or_in_laws_name']??""}}<br>
                    <b>DOB:</b> {{$registration->extra_data['ticket_'.$count]['father_In_law_dob']??""}}<br>
                    <b>Naksthram:</b> {{$registration->extra_data['ticket_'.$count]['father_In_law_naksthram']??""}}<br>
                    <b>Rasi:</b> {{$registration->extra_data['ticket_'.$count]['father_In_law_rasi']??""}}<br>
                    <b>Gothram:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['father_In_law_gothram']??""}}<br>
                </div>
                <b>Mother (In-Law) Details : </b><br>
                <div>
                    <b>Name:</b> {{$registration->extra_data['ticket_'.$count]['mother_or_in_laws_name']??""}}<br>
                    <b>DOB:</b> {{$registration->extra_data['ticket_'.$count]['mother_In_law_dob']??""}}<br>
                    <b>Naksthram:</b> {{$registration->extra_data['ticket_'.$count]['mother_In_law_naksthram']??""}}<br>
                    <b>Rasi:</b> {{$registration->extra_data['ticket_'.$count]['mother_In_law_rasi']??""}}<br>
                    <b>Gothram:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['mother_In_law_gothram']??""}}<br>
                </div>
                @php
                $count=$count+1;
                @endphp
                @endwhile
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
