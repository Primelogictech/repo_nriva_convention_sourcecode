 <table class="datatable table table-hover table-center mb-0">
     <thead>
         <tr>
             <th>Sl NO.</th>
             <th>Profile Id</th>
             <th>First Name</th>
             <th>Last Name</th>
             <th>Gender</th>
             <th>Age</th>
             <th>Email Id</th>
             <th>Phone</th>
             <th>Height</th>
             <th>Gothram</th>
             <th>Maternal Gothram</th>
             <th>Education</th>
             <th>Type Of Visa</th>
             <th>Citizenship</th>
             <th>Working At</th>
             <th>Parent Name</th>
             <th>Parent Phone</th>
             <th>Parent Email</th>
             <th>Participate In</th>
             <th>Convention Registration id</th>
             <th>Number of Participant</th>
             <th>Registered Date</th>
             <th>Sequence</th>
         </tr>
     </thead>
     <tbody>
         @foreach ($registrations as $registration)
         <tr>
             <td>{{ $loop->iteration }} </td>
             <td>{{ $registration->member_id }}</td>
             <td>{{ $registration->first_name }}</td>
             <td>{{ $registration->last_name }}</td>
             <td>{{ $registration->gender }}</td>
             <td>
                 @php
                    $age=0;
                 @endphp
                 @if(checkForValadateDateFormat($registration->dob, $format = 'm-d-Y'))
                    @php
                        $d= DateTime::createFromFormat('m-d-Y', $registration->dob);
                        $age=$d->format('Y')
                    @endphp
                 @endif
                 @if(checkForValadateDateFormat($registration->dob, $format = 'Y-m-d'))
                    @php
                        $d= DateTime::createFromFormat('Y-m-d', $registration->dob);
                        $age=$d->format('Y')
                    @endphp
                 @endif
                 @if(isset($registration->dob))
                    {{date("Y")-$age}}
                 @else
                     0 (no data)
                 @endif

             </td>
             <td>{{ $registration->email }}</td>
             <td>{{ $registration->phone_code }} {{ $registration->mobile }}</td>
             <td>{{$registration->feet}}' {{$registration->inches}}"</td>
             <td>{{$registration->gothram}}</td>
             <td>{{$registration->maternal_gothram}}</td>
             <td>
                 @if($registration->education_level=='Other')
                    {{ removeBraces($registration->education_level_description) }}
                @else
                    {{ removeBraces($registration->education_level) }}
                @endif
             </td>
             <td>
                @if ($registration->legal_status=='Other')
                    {{$registration->legal_status_description??""}}
                @else
                    {{$registration->legal_status??""}}
                @endif
             </td>
             <td>  
             @if ($registration->citizen_of=='Other')
                {{$registration->citizen_of_description??""}}
            @else
                {{$registration->citizen_of??""}}
            @endif
            </td>
             <td>{{$registration->designation??""}} @if($registration->employer) ({{$registration->employer??""}})  @endif </td>
             <td>{{$registration->father_first_name}} {{$registration->father_last_name}}</td>
             <td>{{$registration->parent_phone_code}} {{$registration->parent_number}}</td>
             <td>{{$registration->parent_email}}</td>
             <td>
                 @foreach($registration->participate_in as $part)
                 {{$part}} <br>
                 @endforeach
             </td>
             <td>{{ $registration->convention_id }}</td>
             <td>{{ $registration->no_of_people_attending }}</td>
             <td>{{ $registration->created_at }}</td>
             <td>@if(isset($registration->sequence)){{ $registration->sequence }} @endif</td>
         </tr>
         @endforeach
     </tbody>
 </table>
