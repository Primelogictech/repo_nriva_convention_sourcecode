 <table class="table table-hover table-center mb-0">
    <thead>
                        <tr>
                            <th>Sl NO.</th>
                            <th>Full Name</th>
                            <th>Email Id</th>
                            
                            <th>Category</th>
                            <th>Tax_ID</th>
                            <th>Mailing Address</th>
                            <th>Phone No</th>
                            <th>Fax</th>
                            <th>Name of your Website</th>
                            <th>Exhibitor Type</th>
                            <th>Amount</th>
                            <th>Payment Method</th>
                            <th>Transaction Id</th>
                            <th>Payment Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $registration)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $registration->full_name }}</td>
                                <td>{{ $registration->email }}</td>
                                
                                <td>{{ $registration->category }}</td>
                                <td>{{ $registration->tax_ID }}</td>
                                <td>{{ $registration->mailing_address }}</td>
                                <td>{{ $registration->phone_no }}</td>
                                <td>{{ $registration->fax }}</td>
                                <td>{{ $registration->website_url }}</td>
                                <td><?php 
                                $booths= json_decode($registration->booth_type);
                                ?>
                                @if(is_object($booths))
                                    @foreach(json_decode($registration->booth_type) as $key => $value)
                                        @if($value > 0)
                                        <?php
                                         $Individual =  \App\Models\Admin\ExhibitorType::where("id",$key)->first();

                                         ?>
                                         {{$Individual->name}} ({{$Individual->size_price[0]['size']}}) - {{$value}}<br>
                                         @endif
                                        @endforeach
                                        @endif
                                    </td>
                                 <td> @if($registration->paymentdata) 
                                    Total Amount : $ {{ number_format($registration->paymentdata->payment_amount)??'' }} <br>
                                    Discount Amount : $ {{ number_format($registration->paymentdata->discount_amount)??'' }}  <br>
                                    Discount Code :  {{ $registration->paymentdata->discount_code??'' }}  <br>
                                    Paid Amount : $ {{ number_format($registration->paymentdata->paid_amount)??'' }}  <br>
                                @endif</td>
                                <td>
                                    @if($registration->paymentdata)
                                        @if($registration->paymentdata->payment_methord==1)
                                        Paypal
                                        @elseif($registration->paymentdata->payment_methord==2)
                                        Check
                                        @elseif($registration->paymentdata->payment_methord==3)
                                        Zelle
                                        @elseif($registration->paymentdata->payment_methord==4)
                                        Other
                                        @endif
                                    @endif


                                </td>
                                 <td>{{ $registration->paymentdata->unique_id_for_payment??'' }}
                                 </td>  
                                     <td>
                                    @if(@$registration->paymentdata)
                                        @foreach(paymentStatus() as $payment_status_name)
                                        {{ ucfirst($registration->paymentdata->payment_status)==$payment_status_name? $payment_status_name :"" }} 
                                        @endforeach
                                    @endif
                                </td> 
                            </tr>
                            @endforeach
                       </tbody>
</table>