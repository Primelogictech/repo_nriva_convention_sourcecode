 <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>Sl NO.</th>
                                <th>Member ID</th>
                                <th>Registration ID</th>
                                <th>Member Details</th>
                                <th>Family Details</th>
                                <th>Sponsorship Type</th>
                                <th>Sponsorship Category</th>
                                <th>Amount Paid</th>
                                <th>Excel Import</th>
                                <th>Payment Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($users as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td> {{ $user->member_id }}</td>
                                <td> {{ $user->registration_id }}</td>
                                <td>
                                    <div class="py-1">
                                        <span>Name:</span><span> {{ $user->first_name }} {{ $user->last_name }}</span><br>
                                    </div>
                                    <div class="py-1">
                                        <span>Email:</span><span>{{ $user->email }}</span><br>
                                    </div>
                                    <div class="py-1">
                                        <span>Mobile:</span><span>{{ $user->phone_code }} {{ $user->mobile }}</span><br>
                                    </div>
                                    <div class="py-1">
                                        <span>Chapter:</span><span>{{ $user->chapter }}</span><br>
                                    </div>
                                      <div class="py-1">
                                        <span>Shathamanam Bhavathi Required: </span><span>{{ $user->is_shathamanam_bhavathi_required?'Yes':'No' }}</span>
                                    </div><br>

                                      <div class="py-1">
                                        <span>Exbhit Required: </span><span>{{  $user->is_exbhit_required?'Yes':'No'  }}</span>
                                    </div>
                                </td>
                                <td>
                                           <br>
                                    <b>Spouse First Name</b>: {{ $user->spouse_first_name }}
                                    <br>
                                    <b>Spouse Last Name</b>: {{ $user->spouse_last_name }}
                                    <br>

                                        @if($user->kids_details)
                                        <br>
                                        @foreach ($user->kids_details as $kid)
                                        <b>kid Name</b>: {{ $kid['name'] }}<br>
                                        <b>Age</b>: {{ $kid['age'] }}<br><br>
                                        @endforeach
                                        @endif 
                                </td>
                                <td>
                                    @if(!empty($user->sponsorship_category_id)){{  "Donor" }}
                                    @endif
                                    <br>
                                    @if(!empty($user->registration_amount)){{  "Family / Individual" }}
                                    @endif
                                </td>
                                <td> {{ $user->categorydetails->donortype->name ?? ""}} {{ isset($user->categorydetails->start_amount) ? ( '($'. $user->categorydetails->start_amount . '- $'. $user->categorydetails->end_amount . ")")  : ""  }}
                                    <br>
                                    @if(!empty($user->sponsorship_category_id))
                                    Donation Amount : $ {{number_format($user->donor_amount) }}
                                    @endif
                                    <br>
                                        @if(is_array($user->individual_registration))
                                        @foreach($user->individual_registration as $key => $value)
                                        @if($value > 0)
                                        <?php 
                                        $Individual = \App\Models\Admin\SponsorCategory::where('status', 1)->where('id',$key)->first();
                                         ?>
                                         @if($Individual)
                                               {{$Individual->benefits[0]}} - {{$value}};
                                         @endif
                                       <br>
                                         @endif
                                        @endforeach
                                       Registration Amount : $ {{number_format($user->registration_amount) }}
                                       @endif

                                 </td>
                                <td> {{ isset($user->amount_paid) ? '$'. number_format($user->amount_paid)  : ""  }}</td>
                                 <td>{{($user->is_imported_record==1)?'Yes':'No'}}</td>
                                <td>{{ ucfirst($user->payment_status) }}</td> 
                            </tr>
                            @empty

                            @endforelse
                        </tbody>
                    </table>