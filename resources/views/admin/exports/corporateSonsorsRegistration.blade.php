 <table class="datatable table table-hover table-center mb-0">
     <thead>
         <tr>
             <th>Sl NO.</th>
             <th>Email Id</th>
             <th>Phone No</th>
             <th>Full Name</th>
             <th>Name of your Website</th>
             <th>Address</th>
             <th>Sponsor Type</th>
             <th>video link</th>
             <th>Discription</th>
             <th>Total amount</th>
             <th>Payment Type</th>
             <th>Payment Id</th>
             <th>Payment status</th>
         </tr>
     </thead>
     <tbody>
         @foreach ($registrations as $registration)
         <tr>
             <td>{{ $loop->iteration }} </td>
             <td>{{ $registration->email }}</td>
             <td>{{ $registration->phone_no }}</td>
             <td>{{ $registration->full_name }}</td>
             <td>{{ $registration->website_url }}</td>
             <td>{{ $registration->mailing_address}}</td>
             <td>{{$registration->CorporateSponsortype->name??""}}</td>
             <td>{{$registration->video_link}}</td>
             <td>{{$registration->extra_discriptioon}}</td>
             <td>@if($registration->paymentdata) ${{ $registration->paymentdata->payment_amount??'' }} @endif</td>
             <td> @if($registration->paymentdata)
                 @if($registration->paymentdata->payment_methord==1)
                 Paypal
                 @elseif($registration->paymentdata->payment_methord==2)
                 Check
                 @elseif($registration->paymentdata->payment_methord==3)
                 Zelle
                 @elseif($registration->paymentdata->payment_methord==4)
                 Other
                 @endif
                 @endif
             </td>
             <td>{{ $registration->paymentdata->unique_id_for_payment??'' }}</td>
             <td>
                 @if($registration->paymentdata)
                    {{ucfirst($registration->paymentdata->payment_status)}}
                 @endif
             </td>
         </tr>
         @endforeach
     </tbody>
 </table>
