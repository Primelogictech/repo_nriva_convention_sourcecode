@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Miss and Mr. NRIVA Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">

        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <tr>
                            <th>Sl NO.</th>
                            <th>Title</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email Id</th>
                            <th>Phone No</th>
                            <th>About</th>
                            
                            <th>Registered Date</th>
                            
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($registrations as $registration)
                            <tr>
                                <td>{{ $loop->iteration }} </td>
                                <td>{{ $registration->title }}</td>
                                <td>{{ $registration->first_name }}</td>
                                <td>{{ $registration->last_name }}</td>
                                <td>{{ $registration->email }}</td>
                                <td>{{ $registration->phone }}</td>
                                <td>{{ $registration->about }}</td>
                               
                                <td>{{ $registration->created_at }}</td>
                                
                            </tr>
                            @endforeach
                        </tbody>

                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
