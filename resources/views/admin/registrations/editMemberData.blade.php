@extends('layouts.admin.base')
@section('content')


<style type="text/css">
    input.form-control {
        height: 30px;
        width: 300px;
    }
</style>

<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Note</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{url('admin\registrations')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('registration.user.edit')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">First Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input class="form-control" required name="first_name" value={{ $user->first_name }}>
                        </div>

                          <label class="col-form-label col-md-4 my-auto">Last Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input class="form-control" required name="last_name" value={{ $user->last_name }}>
                        </div>

                        <label class="col-form-label col-md-4 my-auto">Spouse First  Name</label>
                        <div class="col-md-8 my-auto">
                            <input class="form-control" name="spouse_first_name" value={{ $user->spouse_first_name }}>
                        </div>
                        
                        <label class="col-form-label col-md-4 my-auto">Spouse Last Name</label>
                        <div class="col-md-8 my-auto">
                            <input class="form-control" name="spouse_last_name" value={{ $user->spouse_last_name }}>
                        </div>


                         <label class="col-form-label col-md-4 my-auto">Mobile <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input class="form-control" required name="mobile" value={{ $user->mobile }}>
                        </div>

                    </div>


                    <div class="form-group row">

                        
                         <label class="col-form-label col-md-4 my-auto">Address line 1 <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input class="form-control" required name="address" value={{ $user->address }}>
                        </div>

                          <label class="col-form-label col-md-4 my-auto">Address line 2 </label>
                        <div class="col-md-8 my-auto">
                            <input class="form-control"  name="address2" value={{ $user->address2 }}>
                        </div>

                             <label class="col-form-label col-md-4 my-auto">City <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input class="form-control" required name="city" value={{ $user->city }}>
                        </div>
                        
                        <label class="col-form-label col-md-4 my-auto">State <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                              <select name="state" class="form-control" required="">
                                <option value="">Select State</option>
                                @foreach ($states as $state)
                                         <option 
                                          {{ $user->state==$state->state? 'selected' : ''  }}
                                         value="{{ $state->state }}" >{{ $state->state }}</option>
                                @endforeach
                            </select>
                        </div>

                         <label class="col-form-label col-md-4 my-auto">Zip Code <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input class="form-control" required name="zip_code" value={{ $user->zip_code }}>
                        </div>
                          <label class="col-form-label col-md-4 my-auto">Country <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                             <select name="country" required class="form-control" required="">
                                <option value="" >Select Country</option>
                                <option {{ $user->country=='United States'? 'selected' : ''  }} >United States</option>
                                <option {{ $user->country=='Canada'? 'selected' : ''  }}>Canada</option>
                            </select>
                        </div>

                        <label class="col-form-label col-md-4 my-auto">Chapter</label>
                        <div class="col-md-8 my-auto">
                            <select name="chapter" class="form-control" >
                            <option value="">Select Chapter</option>
                            @foreach ($chapters as $chapter)
                                <option 
                                    {{ $user->chapter==$chapter->name? 'selected' : ''  }}
                                    value="{{$chapter->name}}"   >{{$chapter->name}}</option>
                            @endforeach
                        </select>
                        </div>

                        <label class="col-form-label col-md-4 my-auto">Sponsorship Type</label>
                        <div class="col-md-8 my-auto">
                            <select name="sponsorship_category_id" class="form-control" >
                            <option value="">Select Sponsorship</option>
                            @foreach ($donors as $donor)
                                <option 
                                    {{ $user->sponsorship_category_id==$donor->id? 'selected' : ''  }}
                                    value="{{$donor->id}}" 
                                    >{{$donor->donortype->name}} - {{$donor->amount}}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')

@endsection


@endsection
