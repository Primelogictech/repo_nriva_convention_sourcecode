@extends('layouts.admin.base')
@section('content')

<style type="text/css">
    #pageloader
{
  background: rgba( 255, 255, 255, 0.8 );
  display: none;
  height: 100%;
  position: fixed;
  width: 100%;
  z-index: 9999;
}

#pageloader img
{
  left: 30%;
  margin-left: -32px;
  margin-top: -32px;
  position: absolute;
  top: 30%;
}
.ancer{
    color: white;
}
</style>
<div id="pageloader">
   <img src="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" alt="processing..." />
</div>
<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Registrations List</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">

               <!--  <a href="{{url('convention-registration-export')}}" title="" class="add-new-btn btn" data-original-title="Export">Export</a> -->
            </div>
        </div>
    </div>
</div>
<form method="post" enctype="multipart/form-data" id="donorImport" action="{{ url('admin/ImportRegistrations') }}">
    {{ csrf_field() }}
    <div class="form-group">
     <table class="table">
      <tr>
       <td width="40%" align="right"><label>Select File for Import</label></td>
       <td width="30">
        <input type="file" name="select_file" />
       </td>
       <td width="30%" align="left">
        <input type="submit" name="upload" class="btn btn-primary" value="Import">
       </td>
      </tr>
      <tr>
       <td width="40%" align="right"></td>
       <td width="30"><span class="text-muted">.xls, .xslx</span></td>
       <td width="30%" align="left"></td>
      </tr>
     </table>
    </div>
   </form>
   <form action="{{url('admin/registrationsList')}}" method="post">
    @csrf
    <div class="col-12 col-sm-12 col-md-12 my-auto">
        <h5 class="page-title mb-0">Filters</h5>
        <div class="row">
            <div class="col-3">
                <label class="col-form-label">CON REG ID</label>
                <div class="ui-widget">
                    <input type="text" class="form-control" id="con_id" name="con_id" placeholder="CON REG ID" value="{{$request->con_id??''}}">
                    
                </div>
            </div>
            <div class="col-3">
                <label class="col-form-label">Email</label>
                <div class="ui-widget">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{$request->email??''}}">
                    
                </div>
            </div>
            <div class="col-3">
                <label class="col-form-label">Full Name</label>
                <div class="ui-widget">
                    <input type="text" class="form-control" id="first_name" name="full_name" placeholder="Full Name" value="{{$request->first_name??''}}">
                    
                </div>
            </div>
            <div class="col-3 ">
                <label class="col-form-label">Search by keyword</label>
                <div>
                    <input type="text" class="form-control"  name="keyword" placeholder="Search by keyword" value="{{$request->keyword??''}}">
                </div>
            </div>
            <br>

           
         <div class="col-6 col-sm-6 col-md-6 my-auto">
             <div class="row">
                 <div class="col-3 my-auto">
                     <button class="add-new-btn btn">Search</button>
                 </div>
                 <div class="col-3 my-auto">
                    {{--  <a href="{{url('admin/registrationsList')}}"><button class="add-new-btn btn">Reset</button></a> --}}
                     
                     <button class="add-new-btn btn"><a class="ancer" href="{{url('admin/registrationsList')}}?reset=true">Reset</a></button>
                 </div>
             </div>
         </div>




    </div>
</div>
</form>
<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-0" id="table_registarion">
                        <thead>
                            <tr>
                                
                                <th>Convention ID</th>
                                <th>Member Details</th>
                               
                                <!-- <th>Hotel Details</th> -->
                                <th>Tickets Details</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                       
                        <tbody>
                            @foreach($registrationsList as $reg)
                            <tr>
                                <td>{{$reg['convention_id']??''}}</td>
                                <td>
                                    <b>Full Name :</b> {{$reg['first_name']??''}}</br>
                                    <b>Email :</b> {{$reg['email']??''}}</br>
                                   <b> Mobile :</b> {{$reg['mobile']??''}}</br>
                                    <b>Chapter :</b> {{$reg['chapter']??''}}</br>
                                    <b>Package Name :</b> {{$reg['package_name']??''}}</br>
                                </td>
                               
                               <!--  <td>
                                    <b>Room No :</b>{{$reg['roomnum']??''}}</br>
                                    <b>Hotel name :</b>{{$reg['hotel_name']??''}}</br>
                                    <b>Hotel Count :</b>{{$reg['hotel_count']??''}}</br>
                                    
                                </td> -->
                                <td>
                                    <b>Banquet Tickets Count :</b>{{$reg['banquet_tickets_count']??''}}</br>
                                    <b>Main Event Tickets Count :</b>{{$reg['main_event_tickets_count']??''}}</br>
                                    
                                    <b>Youth Banquet Count :</b>{{$reg['youth_banquet_count']??''}}</br>

                                    <b>VIP Banquet Tickets Count :</b>{{$reg['vip_banquet_tickets_count']??''}}</br>
                                    <b>VIP Admissions Count :</b>{{$reg['vip_admissions_count']??''}}</br>
                                    <b>VVIP Banquet Count :</b>{{$reg['vvip_banquet_count']??''}}</br>
                                    <b>VVIP Admissions Count :</b>{{$reg['vvip_admissions_count']??''}}</br>
                                    
                                </td>
                                <td class="actions">
                                    <textarea id="comments" data-id="{{$reg['id']}}" class="notes" placeholder="Comments">{{$reg['comments']??''}}</textarea><br>
                                    <input type="checkbox" data-id="{{$reg['id']}}" data-roomnum="{{$reg['roomnum']}}"  class="ticket_issue" value="1" id="checkedinStatus"     @if($reg["check_in"]==1) checked @endif>  Registration CheckIn 
                                    
                                </td>
                            </tr>
                            @endforeach
                          
                        </tbody>
                        
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         
          <h4 class="modal-title">Update Hotel Details</h4>
        </div>
        <div class="modal-body">
            <form method="post" enctype="multipart/form-data" action="{{ url('admin/checkedinRoom') }}">
                {{ csrf_field() }}
                
                <input type="text" name="roomnum" id="roomnum" class="form-control" placeholder="Enter Room Number" required>
                <br>
                <select name="hotel_name" class="form-control" id="hotel_name" required>
                    <option value="">Select Hotel</option>
                    <option value="Hotel 1">Hotel 1</option>
                    <option value="Hotel 2">Hotel 2</option>
                    <option value="Hotel 3">Hotel 3</option>
                    <option value="Hotel 4">Hotel 4</option>
                    <option value="Hotel 5">Hotel 5</option>
                    <option value="Hotel 6">Hotel 6</option>
                </select>
                <br>

                <input type="hidden" name="user_id" id="user_id" required>

                <button type="submit">Submit</button>
                
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

  <button type="button" id="model_button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="display: none;">Open Modal</button>



@section('javascript')
<script>
 $(document).ready(function() {
   
      $("#donorImport").on("submit", function(){
        $("#pageloader").fadeIn();
      });

        $('.notes').change(function(){
            var user_id = $(this).data('id');
            var notes = $(this).val();
                $.ajax({
                type: 'GET',
                url: "{{url('admin/update_notes')}}?user_id="+user_id+"&notes="+notes,
                success:function(data){
                }
            });
        });

        $('.ticket_issue').change(function(){
            var user_id = $(this).data('id');
            var notes = $(this).parents('.actions').find('.notes').val();
            
            if($(this).is(':checked')){
                if(notes){
                var status =1;
                // var roomnum = $(this).data('roomnum');
                // $('#roomnum').val(roomnum);
                // $('#user_id').val(user_id);
                // $('#model_button').click();
                        $.ajax({
                    type: 'GET',
                    url: "{{url('admin/update_tickets')}}?user_id="+user_id+"&status="+status,
                    success:function(data){
                          
                        }
                    });
                }else{
                    $(this).prop('checked',false);
                 alert('Please fill the comments');   
                }
            }else{
                var status =0;
                $.ajax({
            type: 'GET',
            url: "{{url('admin/update_tickets')}}?user_id="+user_id+"&status="+status,
            success:function(data){
                  
                }
            });
            
            }            
    });

    //  $('.fulfilment_status').change(function () {
    //      var existstatus=$(this).data('existstatus');
    //      data = {
    //              "_token": $('meta[name=csrf-token]').attr('content'),
    //             id: $(this).data('id'),
    //             status: $(this).val()
    //         }
    //     if (confirm("Are you sure to change status!") == true) {
    //       ajaxCall('{{ url('admin/update-fulfilment-status') }}/'+$(this).data('id'), 'put', data , afterStatusUpdate)
    //     } else {
            
    //       $(this).val(existstatus); 
    //       return true;
    //     }
       
    // })

    // function afterStatusUpdate(data){
    //     if(data==1){
    //             swal({
    //                     title: "success!",
    //                     text: "Fulfilment Stauts updated!",
    //                     icon: "success",
    //                 });
    //     }else{
    //          swal({
    //                 title: "Oops...!",
    //                 text: "Something went wrong!!",
    //                 icon: "error",
    //             });
    //     }
    // }   

    // // Setup - add a text input to each footer cell
    // $('#table_registarion tfoot th').each( function (i) {
    //     var title = $('#table_registarion tfoot th').eq( $(this).index() ).text();
    //     $(this).html( '<input type="text" placeholder="'+title+'" data-index="'+i+'" />' );
    // } );
  
    // DataTable
    var table = $('#table_registarion').DataTable( {
        //scrollY:        "500px",
        //scrollX:        true,
        scrollCollapse: true,
        paging:         true,
        fixedColumns:   true,
        "order": [[ 0, "desc" ]]
    } );
 
    // // Filter event handler
    $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
        table
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
    } );
  });
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
  <script>
  $( function() {
    var availableTags = ['<?php echo implode("','",array_column($registrationsList, 'convention_id'));?>'];
    $( "#con_id" ).autocomplete({
      source: availableTags
    });
    var availableTags1 = ['<?php echo implode("','",array_column($registrationsList, 'email'));?>'];
    $( "#email" ).autocomplete({
      source: availableTags1
    });
    var availableTags2 = ['<?php echo implode("','",array_column($registrationsList, 'first_name'));?>'];
    $( "#first_name" ).autocomplete({
      source: availableTags2
    });
  } );
  </script>
@endsection

@endsection
