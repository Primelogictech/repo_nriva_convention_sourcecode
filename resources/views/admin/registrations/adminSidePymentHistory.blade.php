@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url('admin/registrations') }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>S No</th>
                                <th>Transaction details</th>
                                <th>Payment Methord</th>
                                <th width=300px>Amount</th>
                               
                                <th>Payment Status</th>
                                <th>Account Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($payments as $payment)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                     <b>Date</b>
                                    {{paymentDate($payment) }}
                                    <br>
                                    <b>Transaction Id </b>
                                    {{ $payment->unique_id_for_payment }} 
                                    @if($payment->paymentmethord && $payment->paymentmethord->name == config('conventions.other_name_db'))
                                         <br>
                                        <b>Payee Name</b>
                                        {{ $payment->more_info['payee_name']??""  }}
                                        <br>
                                    <b>Company Name</b>

                                        {{ $payment->more_info['company_name']??""  }}
                                        <br>
                                        @if(isset($payment->more_info['document']))
                                            @if(!empty($payment->more_info['document']))
                                            <a href="{{asset('public/storage/user/'.$payment->more_info['document'])}}" download>Download Document</a>
                                            @endif
                                        @endif
                                    @endif

                                    @if($payment->paymentmethord && $payment->paymentmethord->name == config('conventions.check_name_db'))
                                         <br>
                                        <b>Bank Name</b>
                                        {{ $payment->more_info['bank_name']??""  }}
                                        <br>
                                    <b>Check Received By</b>
                                        {{ $payment->more_info['check_received_by']??""  }}
                                    @endif
                                  <!--   <b>Transaction Id </b>{{ $payment->unique_id_for_payment }} -->
                                </td>
                                <td>{{ ucfirst($payment->paymentmethord->name??"")  }}</td>
                               
                                <td> 
                                
                                $ {{ $payment->payment_amount }} 

                                <div class="d-flex">
                                    <span class="my-1 mr-1">
                                        <input type="text" style="width:125px" value={{$payment->payment_amount}} id="input_{{$payment->id}}" class="form-control"></span>
                                    <button data-url="{{ env("APP_URL") }}/admin/update-payment-amount"  data-id="{{$payment->id}}" class="save-btn btn btn-sm btn-success my-auto mx-1">Update</button>
                                </div>
                                </td>
                               
                               
                                <td> 
                                @if(canUpdatePaymentStatus(Auth::user()) || (in_array(Auth::user()->email,Config::get('conventions.admin_emails'))) )
                                <select class="form-control payment_status" name="payment_status" data-id="{{ $payment->id }}">
                                    @foreach(paymentStatus() as $payment_status_name)
                                    <option value="{{$payment_status_name}}" 
                                    {{ ucfirst($payment->payment_status)==$payment_status_name? "selected" :"" }} >{{$payment_status_name}}</option>
                                    @endforeach
                                </select>
                                @else
                                    {{ucfirst($payment->payment_status)}}
                                @endif
                                </td>
                                <td> {{ ucfirst($payment->account_status) }} </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script>
 $(document).ready(function() {

     
    $('.payment_status').change(function () {
        
         data = {
                 "_token": $('meta[name=csrf-token]').attr('content'),
                id: $(this).data('id'),
                status: $(this).val()
            }
       ajaxCall('{{ url('admin/update-payment-status') }}/'+$(this).data('id'), 'put', data , afterStatusUpdate)
    })

    function afterStatusUpdate(data){
        if(data==1){
                swal({
                        title: "success!",
                        text: "Stauts updated!",
                        icon: "success",
                    });
        }else{
             swal({
                    title: "Oops...!",
                    text: "Something went wrong!!",
                    icon: "error",
                });
        }
    }

  })

$('.save-btn').click(function() {
    id = $(this).attr('data-id')
   

    amount = $('#input_' + id).val()
    
    data = {
        "_token": $('meta[name=csrf-token]').attr('content'),
        id: $(this).attr('data-id'),
        amount: amount
    }
    console.log(data)
    ajaxCall($(this).data('url'), 'put', data, aftersave)
})

function aftersave(data) {
    location.reload();
}




</script>
@endsection


@endsection
