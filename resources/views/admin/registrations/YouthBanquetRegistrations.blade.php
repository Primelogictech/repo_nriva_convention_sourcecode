@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Youth Banquet Registration</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
             <div class="float-right">
                <a href="{{url('youth-banquet-registration-export')}}" title="" class="add-new-btn btn" data-original-title="Export">Export</a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <tr>
                            <th>Sl NO.</th>
                            <th>Convention Registration</th>
                            <th>Whatsapp</th>
                            <th>Parent/Guardian Phone</th>
                            <th>Parent Guardian email</th>
                            <th>Kids Details</th>
                            
                            <th>Registered Date</th>
                            
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($registrations as $registration)
                            <tr>
                                <td>{{ $loop->iteration }} </td>
                                <td>{{ $registration->convention_id }}</td>
                               
                                <td>{{ $registration->whatsapp }}</td>
                                <td>{{ $registration->parent_phone }}</td>
                                <td>{{ $registration->parent_email }}</td>


                                <td>
                                    @foreach(explode('||',$registration->FirstName) as $key=>$value)

                                    FirstName: {{explode('||',$registration->FirstName)[$key]??'' }}<br>
                                    LastName: {{explode('||',$registration->LastName)[$key]??'' }}
                                    <br>
                                    Age: {{explode('||',$registration->age)[$key]??'' }}
                                     <br>
                                    Email: {{explode('||',$registration->email)[$key]??'' }}
                                     <br>
                                    Phone: {{explode('||',$registration->phone)[$key]??'' }}
                                    <hr>

                                     @endforeach
                                </td>
                               
                                <td>
                                    <a href="{{ url('admin/SendMailYouthBanquet') }}?email={{$registration->email}}" class="btn btn-sm btn-success my-1 mx-1">ReSend Mail</a>
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>

                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
