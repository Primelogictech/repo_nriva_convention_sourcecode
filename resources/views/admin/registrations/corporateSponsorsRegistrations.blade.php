@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Corporate Sonsors</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{url('corporate-sonsors-registration-export')}}" title="" class="add-new-btn btn" data-original-title="Export">Export</a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <tr>
                            <th>Sl NO.</th>
                            <th>Email Id</th>
                            <th>Phone No</th>
                            <th>Full Name</th>
                            <th>Name of your Website</th>
                            <th>Address</th>
                            <th>Sponsor Type</th>
                            <th>Logo</th>
                            <th>Image</th>
                            <th>video link</th>
                            <th>Discription</th>
                            <th>Total amount</th>
                            <th>Payment Type</th>
                            <th>Payment Id</th>
                            <th>Payment status</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($registrations as $registration)
                            <tr>
                                <td>{{ $loop->iteration }} </td>
                                <td>{{ $registration->email }}</td>
                                <td>{{ $registration->phone_no }}</td>
                                <td>{{ $registration->full_name }}</td>
                                <td>{{ $registration->website_url }}</td>
                                <td>{{ $registration->mailing_address}}</td>
                                <td>{{$registration->CorporateSponsortype->name??""}}</td>
                                <td>
                                @if($registration->logo_url)
                                    <img src="{{asset(config('conventions.corporate_sponsors_image_display').$registration->logo_url)}}" width="150px" class="img-fluid">
                                @endif
                                </td>
                                <td>
                                @if($registration->image_url)
                                    <img src="{{asset(config('conventions.corporate_sponsors_image_display').$registration->image_url)}}"  width="150px" class="img-fluid">
                                @endif
                                </td>
                                <td>{{$registration->video_link}}</td>
                                <td>{{$registration->extra_discriptioon}}</td>
                                <td>@if($registration->paymentdata) ${{ $registration->paymentdata->payment_amount??'' }} @endif</td>
                                <td> @if($registration->paymentdata)
                                        @if($registration->paymentdata->payment_methord==1)
                                        Paypal
                                        @elseif($registration->paymentdata->payment_methord==2)
                                        Check
                                        @elseif($registration->paymentdata->payment_methord==3)
                                        Zelle
                                        @elseif($registration->paymentdata->payment_methord==4)
                                        Other
                                        @endif
                                    @endif
                                </td>
                                <td>{{ $registration->paymentdata->unique_id_for_payment??'' }}</td>
                                  <td>
                                        @if($registration->paymentdata)
                                <select class="form-control payment_status" name="payment_status" data-id="{{ $registration->paymentdata->id??'' }}">
                                    @foreach(paymentStatus() as $payment_status_name)
                                    <option value="{{$payment_status_name}}" 
                                    {{ ucfirst($registration->paymentdata->payment_status)==$payment_status_name? "selected" :"" }} >{{$payment_status_name}}</option>
                                    @endforeach
                                </select>
                                @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script>
 $(document).ready(function() {
    $('.payment_status').change(function () {
        
         data = {
                 "_token": $('meta[name=csrf-token]').attr('content'),
                id: $(this).data('id'),
                status: $(this).val()
            }
       ajaxCall('{{ url('admin/update-payment-status') }}/'+$(this).data('id'), 'put', data , afterStatusUpdate)
    })

    function afterStatusUpdate(data){
        if(data==1){
                swal({
                        title: "success!",
                        text: "Stauts updated!",
                        icon: "success",
                    });
        }else{
             swal({
                    title: "Oops...!",
                    text: "Something went wrong!!",
                    icon: "error",
                });
        }
    }

  })
</script>
@endsection


@endsection
