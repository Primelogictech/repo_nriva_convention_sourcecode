@extends('layouts.admin.base')
@section('content')
<style type="text/css">
    #pageloader
{
  background: rgba( 255, 255, 255, 0.8 );
  display: none;
  height: 100%;
  position: fixed;
  width: 100%;
  z-index: 9999;
}

#pageloader img
{
  left: 30%;
  margin-left: -32px;
  margin-top: -32px;
  position: absolute;
  top: 30%;
}
</style>
<div id="pageloader">
   <img src="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" alt="processing..." />
</div>
<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">

                <a href="{{url('convention-registration-export')}}" title="" class="add-new-btn btn" data-original-title="Export">Export</a>
            </div>
        </div>
    </div>
</div>
<form method="post" enctype="multipart/form-data" id="donorImport" action="{{ url('admin/donorImport') }}">
    {{ csrf_field() }}
    <div class="form-group">
     <table class="table">
      <tr>
       <td width="40%" align="right"><label>Select File for Import</label></td>
       <td width="30">
        <input type="file" name="select_file" />
       </td>
       <td width="30%" align="left">
        <input type="submit" name="upload" class="btn btn-primary" value="Import">
       </td>
      </tr>
      <tr>
       <td width="40%" align="right"></td>
       <td width="30"><span class="text-muted">.xls, .xslx</span></td>
       <td width="30%" align="left"></td>
      </tr>
     </table>
    </div>
   </form>
<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-0" id="table_registarion">
                        <thead>
                            <tr>
                                
                                <th>Member ID</th>
                                <th>Registration ID</th>
                                <th>Member Details</th>
                                <th>Family Details </th>
                                <th>Sponsorship Type</th>
                                <th>Sponsorship Category</th>
                                <th>Amount Paid</th>
                                <th>Excel Import</th>
                                <th>Payment Status</th>
                                @if($isForTreasury==1)
                                <th>Fulfilment Status</th>
                                @endif
                                
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot style="display: table-header-group;">
                            <tr>
                                
                                <th>Member ID</th>
                                <th>Registration ID</th>
                                <th>Member Details</th>
                                <th>Family Details</th>
                                <th>Sponsorship Type</th>
                                <th>Sponsorship Category</th>
                                <th>Amount Paid</th>
                                <th>Excel Import</th>
                                <th>Payment Status</th>
                                @if($isForTreasury==1)
                                <th>Fulfilment Status</th>
                                @endif
                                
                            </tr>
                    </tfoot>
                        <tbody>
                            @forelse ($users as $user)


                            <tr>
                                
                                <td> {{ $user->member_id }}</td>
                                <td> {{ $user->registration_id }}</td>
                                <td>
                                    <div class="py-1">
                                        <span>Name:</span><span> {{ $user->first_name }} {{ $user->last_name }}</span>
                                    </div>
                                    <div class="py-1">
                                        <span>Email:</span><span>{{ $user->email }}</span>
                                    </div>
                                    <div class="py-1">
                                        <span>Mobile:</span><span>{{ $user->phone_code }} {{ $user->mobile }}</span>
                                    </div>
                                     <div class="py-1">
                                        <span>Chapter:</span><span>{{ $user->chapter }}</span>
                                    </div>

                                      <div class="py-1">
                                        <span>Shathamanam Bhavathi Required: </span><span>{{ $user->is_shathamanam_bhavathi_required?'Yes':'No' }}</span>
                                    </div>

                                      <div class="py-1">
                                        <span>Exbhit Required: </span><span>{{  $user->is_exbhit_required?'Yes':'No'  }}</span>
                                    </div>
                                    
                                </td>
                                <td>
                                <br>
                                    <b>Spouse First Name</b>: {{ $user->spouse_first_name }}
                                    <br>
                                    <b>Spouse Last Name</b>: {{ $user->spouse_last_name }}
                                    <br>

                                 @if($user->kids_details)
                                 <br>
                                 @foreach ($user->kids_details as $kid)
                                 <b>kid Name</b>: {{ $kid['name'] }}<br>
                                 <b>Age</b>: {{ $kid['age'] }}<br><br>
                                 @endforeach
                                 @endif

                                </td>
                                <td>@if(!empty($user->sponsorship_category_id)){{  "Donor" }}
                                    @endif
                                    <br>
                                    @if(!empty($user->registration_amount)){{  "Family / Individual" }}
                                    @endif
                                </td>
                                <td> {{ $user->categorydetails->donortype->name ?? ""}} {{ isset($user->categorydetails->start_amount) ? ( '($'. $user->categorydetails->start_amount . '- $'. $user->categorydetails->end_amount . ")")  : ""  }}
                                    <br>
                                    @if(!empty($user->sponsorship_category_id))
                                    Donation Amount : $ {{number_format($user->donor_amount) }}
                                    @endif
                                
                                    <br>


                                        @if(is_array($user->individual_registration))
                                        @foreach($user->individual_registration as $key => $value)

                                        @if($value > 0)
                                        <?php $Individual = \App\Models\Admin\SponsorCategory::where('status', 1)->where('id',$key)->first();

                                        if(isset($Individual->benefits)){
                                        
                                            echo $Individual->benefits[0]." - ".$value;
                                            echo "</br>";
                                            }

                                         ?>
                                         @endif

                                        @endforeach
                                       Registration Amount : $ {{number_format($user->registration_amount) }}

                                       @endif

                                 </td>
                                <td> {{ isset($user->amount_paid) ? '$'. number_format($user->amount_paid)  : ""  }}</td>
                                <td>{{($user->is_imported_record==1)?'Yes':'No'}}</td>
                                <td> {{ ucfirst($user->payment_status)}}</td>

                                @if($isForTreasury==1)
                                <td>
                                    <select class="form-control fulfilment_status" data-existstatus="{{$user->fulfilment_status}}" name="fulfilment_status" style="width: 110px;" data-id="{{ $user->id }}">
                                        <option value="">-- Select --</option>
                                        @foreach($fulfilment_statuses as $status)
                                        <option {{$status ==$user->fulfilment_status? "selected"  : ""}} value="{{$status}}">{{$status}}</option>
                                        @endforeach
                                    </select>
                                    
                                </td>
                                @endif
                                <td>
                                   <!--  <a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a> -->
                                    
                                   @if($isForTreasury==0)
                                   @if($user->total_amount>0)
                                        <a href="{{ url('admin/ResendMail') }}?email={{$user->email}}" class="btn btn-sm btn-success my-1 mx-1">ReSend Mail</a>
                                    @endif
                                        @if($user->sponsorship_category_id!=0)
                                        <div class="my-1">
                                        <a href="{{ url('admin/assigne-features',$user->id) }}" class="btn btn-sm btn-success my-1 mx-1">Assign Features</a>
                                        </div>
                                        @endif
                                        @if(!empty($user->sponsorship_category_id) || !empty($user->registration_amount))
                                                <div class="my-1">
                                                <a href="{{ url('admin/add-note',$user->id) }}" class="btn btn-sm btn-success my-1 mx-1">Add Note</a>
                                                </div>
                                                <div class="my-1">
                                                <a href="{{ url('admin/payment-history',$user->id) }}" class="btn btn-sm btn-success my-1 mx-1">Payment History</a>
                                                </div>
                                        @endif
                                        <div class="my-1">
                                        <a href="{{ url('admin/delete-registration',$user->id) }}" class="btn btn-sm btn-success my-1 mx-1" onclick="return confirm('Are you sure?')" data-id="{{ $user->id }}">Delete</a>
                                        </div>
                                    @else
                                    
                                    <!--<div class="my-1">
                                        <a href="#" class="btn btn-sm btn-success width-180">Activate</a>
                                    </div>
                                    <div class="my-1">
                                        <a href="{{ url('admin/add-note',$user->id) }}" class="btn btn-sm btn-success width-180" data-toggle="modal" data-target="#myModal">Add Notes</a>
                                    </div>
                                    <div class="my-1">
                                        <button class="btn btn-sm btn-success width-180" data-toggle="modal" data-target="#myModal">Send email to member</button>
                                    </div>-->
                                    <div class="my-1">
                                    <a href="{{ url('admin/payment-history',$user->id) }}" class="btn btn-sm btn-success width-180">Payment History</a>
                                    </div>
                                    @endif
                                  <div class="my-1">
                                    <a href="{{ url('admin/user/edit',$user->id) }}" class="btn btn-sm btn-success my-1 mx-1">Edit</a>
                                </div>
                                    <button type="button" class="user_upload btn btn-sm btn-success width-180" data-toggle="modal" data-id="{{$user->id}}" data-target="#myModal">Upload Profile Pic</button>
                                </td>

                            </tr>
                            @empty

                            @endforelse


                            <!--  <tr>
                                <td>2</td>
                                <td>23456</td>
                                <td>
                                    <div class="py-1">
                                        <span>Name:</span><span> Dinesh</span>
                                    </div>
                                    <div class="py-1">
                                        <span>Email:</span><span> dinesh@gmail.com</span>
                                    </div>
                                    <div class="py-1">
                                        <span>Mobile:</span><span> 9230447709</span>
                                    </div>
                                </td>
                                <td>Donor</td>
                                <td>Diamond ($5000)</td>
                                <td>$5000</td>
                                <td>Full amount paid</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <a href="assign_features.php" class="btn btn-sm btn-success my-1 mx-1">Assign Features</a>
                                </td>
                            </tr> -->
                        </tbody>
                        
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         
          <h4 class="modal-title">Upload Profile Pic</h4>
        </div>
        <div class="modal-body">
            <form method="post" enctype="multipart/form-data" action="{{ url('admin/conventionUserImages') }}">
                {{ csrf_field() }}
                <input type="file" name="image" accept="image/x-png, image/gif, image/jpeg" required>

                <input type="hidden" name="id" id="user_id" required>

                <button type="submit">Submit</button>
                <br>
                 <b>Image should be in 1:1 ration eg: 200X200</b>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
@section('javascript')
<script>
 $(document).ready(function() {
    $('.user_upload').click(function(){
        $('#user_id').val($(this).data('id'));
    })
      $("#donorImport").on("submit", function(){
        $("#pageloader").fadeIn();
      });

     $('.fulfilment_status').change(function () {
         var existstatus=$(this).data('existstatus');
         data = {
                 "_token": $('meta[name=csrf-token]').attr('content'),
                id: $(this).data('id'),
                status: $(this).val()
            }
        if (confirm("Are you sure to change status!") == true) {
          ajaxCall('{{ url('admin/update-fulfilment-status') }}/'+$(this).data('id'), 'put', data , afterStatusUpdate)
        } else {
            
          $(this).val(existstatus); 
          return true;
        }
       
    })

    function afterStatusUpdate(data){
        if(data==1){
                swal({
                        title: "success!",
                        text: "Fulfilment Stauts updated!",
                        icon: "success",
                    });
        }else{
             swal({
                    title: "Oops...!",
                    text: "Something went wrong!!",
                    icon: "error",
                });
        }
    }   

    // Setup - add a text input to each footer cell
    $('#table_registarion tfoot th').each( function (i) {
        var title = $('#table_registarion tfoot th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="'+title+'" data-index="'+i+'" />' );
    } );
  
    // DataTable
    var table = $('#table_registarion').DataTable( {
        //scrollY:        "500px",
        //scrollX:        true,
        scrollCollapse: true,
        paging:         true,
        fixedColumns:   true,
        "order": [[ 0, "desc" ]]
    } );
 
    // Filter event handler
    $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
        table
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
    } );
     

    

  });
</script>
@endsection

@endsection
