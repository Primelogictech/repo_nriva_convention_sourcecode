@extends('layouts.admin.base')
@section('content')
<style>
.add-new-btn{
    margin-top: 30px;
}
.age_range{
    display: inline-flex;
}
</style>
<div class="page-header">
    <div class="row">
        <div class="col-4 col-sm-4 my-auto">
            <h5 class="page-title mb-0">{{$name}} Batch Users</h5>
        </div>
        
        <div class="col-2 col-sm-2 mb15">
        <form action="{{url('admin/removebatchusers')}}" method="post">
             @csrf
              <input type="hidden" placeholder="Enter Batch Name" name="batch_name" required class="form-control" value="{{$name}}">
              <input type="hidden" name="selectedIds" id="selectedIds" required>
              <br>
             <button class="btn btn-primary batch_button" type="submit" disabled>Remove User</button>
          </form>
      </div>
       <div class="col-1 col-sm-1 col-md-1 my-auto">
              <div class="text-center">
                <a href="{{url('admin/batchusers',$name)}}" title="" class="add-new-btn mt-0 btn" >Sort</a>
            </div>
        </div> 
        <div class="col-1 col-sm-1 col-md-1 my-auto">
              <div class="text-center">
                <a href="{{url('matrimony-registration-export')}}?name={{$name}}" title="" class="add-new-btn mt-0 btn" data-original-title="Export">Export</a>
            </div>
        </div> 
        <div class="col-1 col-sm-1 col-md-1 my-auto">
              <div class="text-center">
                <a href="{{url('admin/matrimony_profile_downloade',$name)}}" title="" class="add-new-btn mt-0 btn" data-original-title="Export">Download</a>
            </div>
        </div>
        <div class="col-2 col-sm-2 col-md-2 my-auto">
              <div class="float-right">
               <a href="{{url('admin/matrimonyRegistrations')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
              
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <tr>
                             <th><input type="checkbox" class="checkboxx" id='selectall'>
                            <th>Sl NO.</th>
                            <th>Sequence</th>
                            <th>Details</th>
                            <th>Participate In</th>
                            <th>Legal Status</th>
                            <th>Registered Date</th>
                            <th>Download PDF</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($registrations as $registration)
                            <tr>
                               <!--  <td><input type="checkbox" class="selectRow" value="{{ $registration->id }}" checked></td> -->
                                <td><input type="checkbox" class="selectRow" value="{{ $registration->id }}"></td>
                                <td>{{ $loop->iteration }} </td>
                                <td><input type="number" data-branchname="{{$name}}" data-id="{{$registration->id}}" class="orderby" value="{{$registration->orderby}}" min="0" style="width: 100px;" ></td>
                                <td>
                                 <b>Profile Id</b> {{ $registration->member_id }}<br> 
                                    <b>Full Name</b> 
                                {{ $registration->first_name }}  {{ $registration->last_name }}<br>
                                       @php
                                    $age=0;
                                  @endphp
                                  @if(checkForValadateDateFormat($registration->dob, $format = 'm-d-Y'))
                                  @php
                                    $d= DateTime::createFromFormat('m-d-Y', $registration->dob);
                                    $age=$d->format('Y')
                                  @endphp
                                  @endif
                                  @if(checkForValadateDateFormat($registration->dob, $format = 'Y-m-d'))
                                    @php
                                        $d= DateTime::createFromFormat('Y-m-d', $registration->dob);
                                        $age=$d->format('Y')
                                    @endphp
                                  @endif
                                  <b>Age</b>:
                                  @if(isset($registration->dob))
                                    {{date("Y")-$age}} 
                                  @else
                                    0  (no data)
                                  @endif <br>
                                  <b>Gender</b>: {{$registration->gender}}<br>
                                   <b>Email</b>: {{$registration->email}}<br>
                                   <b>Convention Reg id	</b>: {{$registration->convention_id}}<br>
                                   <b>Participants	</b>: {{$registration->no_of_people_attending}}<br>
                                </td>
                                <td>
                                    @foreach($registration->participate_in as $part)
                                        {{$part}} <br>
                                    @endforeach
                                </td>
                                 <td>
                                    <b>Visa status</b> :{{$registration->legal_status=="Other"? $registration->legal_status_description :$registration->legal_status }} <br>
                                     <b>Citizenship</b> :{{$registration->citizen_of=="Other"? $registration->citizen_of_description :$registration->citizen_of }}
                                     
                                </td>
                                <td>
                                    <b>Convention</b>: {{ $registration->created_at }}
                                    <br>
                                    <b>Nriva</b> : {{ $registration->nriva_created_at }}
                                
                                </td>
                                <td>
                                <a href="{{url('admin/matrimony_profile_downloade',$registration->id)}}" >Download  </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script type="text/javascript">
    $('.orderby').keyup(function(){

         data = {
                 "_token": $('meta[name=csrf-token]').attr('content'),
                id: $(this).data('id'),
                branchname: $(this).data('branchname'),
                order_no: $(this).val()
            }
       ajaxCall('{{ url('admin/update_branch_user_order') }}', 'put', data);

    });
    $('#selectall').change(function(){
        if($(this).is(":checked")){ 
            $('.selectRow').prop('checked',true);
        }else{ 
            $('.selectRow').prop('checked',false);
        }
        var selectedIds = [];
        $('.selectRow').each(function(){
            if($(this).is(":checked")){
                selectedIds.push($(this).val());
            }
        });
        $('#selectedIds').val(selectedIds);
        
        if(selectedIds.length !=0){
            $('.batch_button').prop("disabled",false);
        }else{
            $('.batch_button').prop("disabled",true);
        }

    });
     $('.selectRow').change(function(){
        var selectedIds = [];
        $('.selectRow').each(function(){
            if($(this).is(":checked")){
                selectedIds.push($(this).val());
            }
        });
        $('#selectedIds').val(selectedIds);

        if($(this).is(':not(:checked)')){ 
             $('#selectall').prop('checked',false);
        }else{
            $('.selectRow').each(function(){

            });
        }
        if(selectedIds.length !=0){
            $('.batch_button').prop("disabled",false);
        }else{
            $('.batch_button').prop("disabled",true);
        }
     });
</script>


@endsection
