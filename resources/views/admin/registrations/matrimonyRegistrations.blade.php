@extends('layouts.admin.base')
@section('content')
<style>
 
    .add-new-btn{
        margin-top: 30px;
    }
    .age_range{
        display: inline-flex;
    }
    .checkboxx{
        position: relative;
        top: 2px;
        margin-right: 10px;
    }
</style>
<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Matrimony Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
              <div class="float-right">
                <a href="{{url('matrimony-registration-export')}}" title="" class="add-new-btn btn" data-original-title="Export">Export</a>
            </div>
        </div>
<form action="{{url('admin/matrimonyRegistrations')}}" method="post">
    @csrf
    <div class="col-12 col-sm-12 col-md-12 my-auto">
        <h5 class="page-title mb-0">Filters</h5>
        <div class="row">
            <div class="col-3">
                <label class="col-form-label">Age Range</label>
                <div class=" age_range">
                    <input type="number" class="form-control col-5 mr-1 pr-0" value="{{$request->min_age??''}}" name="min_age" placeholder="min Age">
                    <input type="number" class="form-control col-5 ml-1 pr-0" value="{{$request->max_age??''}}" name="max_age" placeholder="max Age">
                </div>
            </div>

            <div class="col-3">
                <label class="col-form-label">Citizenship</label>
                <select class="form-control" name="citizenship">
                    <option selected="" value="">-- Select --</option>
                    @foreach ($citizen_of as $lst)
                    <option {{$request->citizenship==$lst?'selected' :''}} value='{{$lst}}'>{{$lst}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-3">
                <label class="col-form-label">Gender</label>
                <select class="form-control" name="Gender">
                    <option selected="" value="">-- Select --</option>
                    <option {{$request->Gender=='Male'?'selected' :''}} value="Male">Male</option>
                    <option {{$request->Gender=='Female'?'selected' :''}} value="Female">Female</option>
                </select>
            </div>
            <div class="col-3">
                <label class="col-form-label  my-auto">Visa Status</label>
                <select class="form-control" name="visa_status">
                    <option selected="" value="">-- Select --</option>
                    @foreach ($legal_status as $lst)
                    <option {{$request->visa_status==$lst?'selected' :''}} value='{{$lst}}'>{{$lst}}</option>
                    @endforeach
                </select>
            </div>


        </div>
        <div class="row">
            <div class="col-3">
                <label class="col-form-label my-auto">Sort By Age</label>
                <select class="form-control" name="sort_age">
                    <option selected="" value="">-- Select --</option>
                    <option {{$request->sort_age=='desc'?'selected' :''}} value="desc">Ascending</option>
                    <option {{$request->sort_age=='asc'?'selected' :''}} value="asc">Descending</option>
                </select>
            </div>
            <div class="col-3">
                <button class="add-new-btn btn" data-original-title="Export">Search</button>
            </div>
            <div class="col-3">
            <button type="button" class="add-new-btn btn batch_button" data-toggle="modal" data-target="#myModal" disabled>Create Batch</button>
        </div>

  <!-- Modal -->

  
    </div>




    </div>
    </div>
</form>
</div>
  <div class="modal fade" id="myModal" role="dialog" dropback="static">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Create Batch</h4>
        </div>
        <div class="modal-body">
          <form action="{{url('admin/createbatch')}}" method="post">
             @csrf
              <input type="text" placeholder="Enter Batch Name" name="batch_name" required class="form-control">
              <input type="hidden" name="selectedIds" id="selectedIds" required>
              <br>
             <button class="btn btn-primary" type="submit">Submit</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

             

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
               
                    <a href="{{url('admin/viewbatch')}}" class="btn btn-primary mb-2">View Batch</a>
                
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <tr>
                            <th><input type="checkbox" class="checkboxx" id='selectall'>Select All</th>
                            <th>S No</th>
                            <th>Details</th>
                            <th>Participate In</th>
                            <th>Legal Status</th>
                            <th>Registered Date</th>
                            <th>Download PDF</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($registrations as $registration)
                            <tr>
                            
                                <td><input type="checkbox" class="selectRow" value="{{ $registration->id }}"></td>
                                <td>{{$loop->iteration}}</td>
                                <td>
                                <b>Profile Id</b> {{ $registration->member_id }}<br> 
                                    <b>Full Name</b>
                                {{ $registration->first_name }}  {{ $registration->last_name }} <br>
                                       @php
                                    $age=0;
                                  @endphp
                                  @if(checkForValadateDateFormat($registration->dob, $format = 'm-d-Y'))
                                  @php
                                    $d= DateTime::createFromFormat('m-d-Y', $registration->dob);
                                    $age=$d->format('Y')
                                  @endphp
                                  @endif
                                  @if(checkForValadateDateFormat($registration->dob, $format = 'Y-m-d'))
                                    @php
                                        $d= DateTime::createFromFormat('Y-m-d', $registration->dob);
                                        $age=$d->format('Y')
                                    @endphp
                                  @endif
                                  <b>Age</b>:
                                  @if(isset($registration->dob))
                                    {{date("Y")-$age}} 
                                  @else
                                    0  (no data)
                                  @endif <br>

                                   <b>Gender</b>: {{$registration->gender}}<br>
                                   <b>Email</b>: {{$registration->email}}<br>
                                   <b>Convention Reg id</b>: {{$registration->convention_id}}<br>
                                   <b>Participants</b>: {{$registration->no_of_people_attending}}<br>

                                </td>
                                <td>
                                    @foreach($registration->participate_in as $part)
                                        {{$part}} <br>
                                    @endforeach
                                </td>
                                <td>
                                    <b>Visa status </b>:{{$registration->legal_status=="Other"? $registration->legal_status_description :$registration->legal_status }} <br>
                                     <b>Citizenship</b> :{{$registration->citizen_of=="Other"? $registration->citizen_of_description :$registration->citizen_of }}
                                     
                                </td>
                                 <td>
                                    <b>Convention</b>: {{ $registration->created_at }}
                                    <br>
                                    <b>Nriva</b> : {{ $registration->nriva_created_at }}
                                
                                </td>
                                <td>
                                <a href="{{url('admin/matrimony_profile_downloade',$registration->id)}}" >Download  </a>
                                </tr>
                            </tr>
                            @endforeach
                        </tbody>

                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script type="text/javascript">
    $('#selectall').change(function(){
        if($(this).is(":checked")){ 
            $('.selectRow').prop('checked',true);
        }else{ 
            $('.selectRow').prop('checked',false);
        }
        var selectedIds = [];
        $('.selectRow').each(function(){
            if($(this).is(":checked")){
                selectedIds.push($(this).val());
            }
        });
        $('#selectedIds').val(selectedIds);
        
        if(selectedIds.length !=0){
            $('.batch_button').prop("disabled",false);
        }else{
            $('.batch_button').prop("disabled",true);
        }

    });
     $('.selectRow').change(function(){
        var selectedIds = [];
        $('.selectRow').each(function(){
            if($(this).is(":checked")){
                selectedIds.push($(this).val());
            }
        });
        $('#selectedIds').val(selectedIds);

        if($(this).is(':not(:checked)')){ 
             $('#selectall').prop('checked',false);
        }else{
            $('.selectRow').each(function(){

            });
        }
        if(selectedIds.length !=0){
            $('.batch_button').prop("disabled",false);
        }else{
            $('.batch_button').prop("disabled",true);
        }
     });
</script>


@endsection
