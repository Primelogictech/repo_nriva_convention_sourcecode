@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Exhibit Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{url('satamanam-bhavati-registration-export')}}" title="" class="add-new-btn btn" data-original-title="Export">Export</a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                    <thead>
                        <tr>
                            <th>Sl NO.</th>
                            <th>Registration Id</th>
                            <th>Full Name & Email</th>
                            <th>No of Participants:</th>
                            <th>Instructions To Sponsors:</th>
                            <th>Registration Details:</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($registrations as $registration)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $registration->registration_id }}</td>
                                <td>
                                {{ $registration->full_name }}<br>
                                {{ $registration->email }}
                                </td>
                                <td>{{ $registration->extra_data['number_of_participants']??""}}</td>
                                <td>{{ $registration->extra_data['instructions_to_sponsors']??""}}</td>
                                 
                               <td>
                                    @php
                                        $count=1;
                                    @endphp
                                    @while (array_key_exists('ticket_'.$count,$registration->extra_data))
                                    <h6>Details for ticket {{$count}} </h6> 
                                         <b>Father (In-Law) Details  : </b><br>
                                        <div>
                                            <b>Name:</b> {{$registration->extra_data['ticket_'.$count]['father_or_in_laws_name']??""}}<br>
                                            <b>DOB:</b> {{$registration->extra_data['ticket_'.$count]['father_In_law_dob']??""}}<br>
                                            <b>Naksthram:</b> {{$registration->extra_data['ticket_'.$count]['father_In_law_naksthram']??""}}<br>
                                            <b>Rasi:</b> {{$registration->extra_data['ticket_'.$count]['father_In_law_rasi']??""}}<br>
                                            <b>Gothram:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['father_In_law_gothram']??""}}<br>
                                        </div>
                                        <b>Mother (In-Law) Details  : </b><br>
                                            <div>
                                            <b>Name:</b> {{$registration->extra_data['ticket_'.$count]['mother_or_in_laws_name']??""}}<br>
                                            <b>DOB:</b> {{$registration->extra_data['ticket_'.$count]['mother_In_law_dob']??""}}<br>
                                            <b>Naksthram:</b> {{$registration->extra_data['ticket_'.$count]['mother_In_law_naksthram']??""}}<br>
                                            <b>Rasi:</b> {{$registration->extra_data['ticket_'.$count]['mother_In_law_rasi']??""}}<br>
                                            <b>Gothram:</b> {{$shathamanamBhavathiRegistration->extra_data['ticket_'.$count]['mother_In_law_gothram']??""}}<br>
                                        </div>
                                        @php
                                        $count=$count+1;
                                        @endphp
                                    @endwhile
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                       
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script>
 $(document).ready(function() {

     
    $('.payment_status').change(function () {
        
         data = {
                 "_token": $('meta[name=csrf-token]').attr('content'),
                id: $(this).data('id'),
                status: $(this).val()
            }
       ajaxCall('{{ url('admin/update-payment-status') }}/'+$(this).data('id'), 'put', data , afterStatusUpdate)
    })

    function afterStatusUpdate(data){
        if(data==1){
                swal({
                        title: "success!",
                        text: "Stauts updated!",
                        icon: "success",
                    });
        }else{
             swal({
                    title: "Oops...!",
                    text: "Something went wrong!!",
                    icon: "error",
                });
        }
    }

  })
</script>
@endsection


@endsection

