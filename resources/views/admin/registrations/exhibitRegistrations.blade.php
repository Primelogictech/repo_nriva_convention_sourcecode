@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Exhibit Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{url('exhibits-registration-export')}}" title="" class="add-new-btn btn" data-original-title="Export">Export</a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-center mb-0" id="table_registarion">
                    <thead>
                        <tr>
                            <th>Sl NO.</th>
                            <th>Full Name</th>
                            <th>Email Id</th>
                            
                            <th>Category</th>
                            <th>Tax_ID</th>
                            <th>Mailing Address</th>
                            <th>Phone No</th>
                            <th>Fax</th>
                            <th>Name of your Website</th>
                            <th>Exhibitor Type</th>
                            <th>Amount</th>
                            <th>Payment Method</th>
                            <th>Transaction Id</th>
                            <th>Payment Date</th>
                            <th>Payment Status</th>
                            <th>Update Payment Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot style="display: table-header-group;">
                            <tr>
                                <th>Sl NO.</th>
                                <th>Full Name</th>
                                <th>Email Id</th>
                                <th>Category</th>
                                <th>Tax_ID</th>
                                <th>Mailing Address</th>
                                <th>Phone No</th>
                                <th>Fax</th>
                                <th>Name of your Website</th>
                                <th>Exhibitor Type</th>
                                <th>Amount</th>
                                <th>Payment Method</th>
                                <th>Transaction Id</th>
                                <th>Payment Date</th>
                                <th>Payment Status</th>
                                <th>Update Payment Status</th>
                                <th>Action</th>
                            </tr>
                    </tfoot>
                        <tbody>
                        @foreach ($registrations as $registration)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $registration->full_name }}</td>
                                <td>{{ $registration->email }}</td>
                                
                                <td>{{ $registration->category }}</td>
                                <td>{{ $registration->tax_ID }}</td>
                                <td>{{ $registration->mailing_address }}</td>
                                <td>{{ $registration->phone_no }}</td>
                                <td>{{ $registration->fax }}</td>
                                <td>{{ $registration->website_url }}</td>
                                <td><?php 
                                $booths= json_decode($registration->booth_type);
                                
                                ?>
                                @if(is_object($booths))
                                    @foreach(json_decode($registration->booth_type) as $key => $value)
                                        @if($value > 0)
                                        <?php $Individual = 
                                        \App\Models\Admin\ExhibitorType::where("id",$key)->first();
                                        
                                        echo $Individual->name.' ('.$Individual->size_price[0]['size'].')'." - ".$value;
                                        echo ",</br>";

                                         ?>
                                         @endif
                                        @endforeach
                                        @endif
                                        
                                    </td>
                               
                                 <td> 
                                    @if($registration->paymentdata) 
                                    Total Amount : $ {{ number_format($registration->paymentdata->payment_amount)??'' }} <br>
                                    Discount Amount : $ {{ number_format($registration->paymentdata->discount_amount)??'' }}  <br>
                                    Discount Code :  {{ $registration->paymentdata->discount_code??'' }}  <br>
                                    Paid Amount : $ {{ number_format($registration->paymentdata->paid_amount)??'' }}  <br>
                                @endif
                            </td>
                                <td>
                                    @if($registration->paymentdata)
                                        @if($registration->paymentdata->payment_methord==1)
                                        Paypal
                                        @elseif($registration->paymentdata->payment_methord==2)
                                        Check
                                        @elseif($registration->paymentdata->payment_methord==3)
                                        Zelle
                                        @elseif($registration->paymentdata->payment_methord==4)
                                        Other
                                        @endif
                                    @endif


                                </td>
                                 <td>{{ $registration->paymentdata->unique_id_for_payment??'' }}


                                     @if(isset($registration->paymentdata->more_info['document']))
                                            @if(!empty($registration->paymentdata->more_info['document']))
                                            <br>
                                            <a href="{{asset('public/storage/user/'.$registration->paymentdata->more_info['document'])}}" download>Download Document</a>
                                            @endif
                                        @endif

                                 </td>
                                 <td>{{ paymentDate($registration->paymentdata) }}</td>
                                 <td>{{$registration->paymentdata->payment_status??""}} </td> 
                                     <td>
                                        @if(@$registration->paymentdata)
                                 @if(canUpdatePaymentStatus(Auth::user()))
                                <select class="form-control payment_status" name="payment_status" data-id="{{ $registration->paymentdata->id??'' }}">
                                    @foreach(paymentStatus() as $payment_status_name)
                                    <option value="{{$payment_status_name}}" 
                                    {{ ucfirst($registration->paymentdata->payment_status)==$payment_status_name? "selected" :"" }} >{{$payment_status_name}}</option>
                                    @endforeach
                                </select>
                                @else
                                     {{ucfirst($registration->paymentdata->payment_status)}}
                                @endif

                                @endif
                                </td>
                                <td>
                                <a href="{{ url('admin/ResendExhibitRegMail') }}?id={{$registration->id}}" class="btn btn-sm btn-success my-1 mx-1">ReSend Mail</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script>
 $(document).ready(function() {
    $('.payment_status').change(function () {
         data = {
                 "_token": $('meta[name=csrf-token]').attr('content'),
                id: $(this).data('id'),
                status: $(this).val()
            }
       ajaxCall('{{ url('admin/update-payment-status') }}/'+$(this).data('id'), 'put', data , afterStatusUpdate)
    })

    function afterStatusUpdate(data){
        if(data==1){
                swal({
                        title: "success!",
                        text: "Stauts updated!",
                        icon: "success",
                    });
        }else{
             swal({
                    title: "Oops...!",
                    text: "Something went wrong!!",
                    icon: "error",
                });
        }
    }

     $('#table_registarion tfoot th').each( function (i) {
        var title = $('#table_registarion tfoot th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="'+title+'" data-index="'+i+'" />' );
    });

      // DataTable
    var table = $('#table_registarion').DataTable( {
        //scrollY:        "500px",
        //scrollX:        true,
        scrollCollapse: true,
        paging:         true,
        fixedColumns:   true,
        "order": [[ 0, "desc" ]]
    } );
 
    // Filter event handler
    $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
        table
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
    } );

  })
</script>
@endsection


@endsection

