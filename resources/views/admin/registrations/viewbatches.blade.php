@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">View Batches</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">

        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <tr>
                            <th>Sl NO.</th>
                            <th>Batches</th>
                            <th>View Action</th>
                           
                            
                        </tr>
                        </thead>
                        <tbody>
                             @foreach($batches as $batch)
                 
                                <tr>
                                        <td>{{ $loop->iteration }} </td>
                                        <td>{{$batch->batch_name}}</td>
                                        <td> <a href="{{url('admin/batchusers/'.$batch->batch_name)}}" class="btn btn-primary mb-2">View</a></td>
                                      
                                        
                                    </tr>
                                 @endforeach
                           
                        </tbody>

                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
