@extends('layouts.admin.base')
@section('content')


<style type="text/css">
    .plus-iconn {
        /*border: 1px solid;
    padding: 7px;
    border-radius: 20px;*/
        padding: 3px 8px;
    }
</style>

<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Assign Permissions to {{$member->name}}</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('member.index')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{url('admin/store-assigne-permissions')}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <div class="col-12 pt-2">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <h6 class="mb-3 font-weight-bold">Permission Type</h6>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <h6 class="mb-3 font-weight-bold">Permissions</h6>
                                </div>
                            </div>
                                <input type="hidden" value="{{$member->member_id}}" name="member_id">

                            <div class="row my-2">
                                
                                <div class="col-12 col-sm-6 col-md-6 my-1 my-sm-auto">
                                    <span class="pl25">Admin Menu Permissions</span>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 my-1 my-sm-auto">
                                    <div>
                                        <select name="menu_page_ids[]" id='pageMenu' class="form-control abc" multiple="multiple">
                                            @foreach($menu_array as $key=>$menus)
                                            <optgroup label="{{$main_menu_name[$key]}}">
                                                @foreach($menus as $menu)
                                                <option value="{{$menu['id']}}">{{$menu['name']}}</option>
                                                @endforeach
                                            </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>

                             <div class="row my-2">
                                
                                <div class="col-12 col-sm-6 col-md-6 my-1 my-sm-auto">
                                    <span class="pl25">Permissions</span>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 my-1 my-sm-auto">
                                    <div>
                                        <select name="permission_ids[]" id='pageMenu1' class="form-control abc" multiple="multiple">
                                                @foreach($PermitionsTypes as $PermitionsType)
                                                <option value="{{$PermitionsType->id}}">{{$PermitionsType->display_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>



                            


                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<!-- end -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function() {
        record = {!! $record!!}
        $('#pageMenu').multiselect({
                        maxHeight: 200
                    })
        $('#pageMenu').val(record.menu_id.split(','));
        $("#pageMenu").multiselect("refresh");

        $('#pageMenu1').multiselect({
                        maxHeight: 200
                    })
        $('#pageMenu1').val(record.permission_type_id.split(','));
        $("#pageMenu1").multiselect("refresh");
    });

    





</script>


@endsection
