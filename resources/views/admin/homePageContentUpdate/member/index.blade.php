@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Add Members</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('member.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <th>S. No.</th>
                            <th>member Name</th>
                            <th>Discription</th>
                            <th style="width: 300px;">Action</th>
                        </thead>
                        <tbody>
                            @forelse($members as $member)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$member->name}} </td>
                                <td>{!!$member->description!!}</td>
                                <td>
                                    <a href="{{route('member.edit', $member->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <button  data-id={{$member->id}} data-url='{{ env("APP_URL") }}/admin/member/{{$member->id}}' class="btn btn-sm btn-success my-1 mx-1  delete-btn" {{($member->roles_count)>0 ? 'disabled' : ' ' }}>Delete</button>
                                    <button data-url="{{env("APP_URL") }}/admin/member-status-update" class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" data-id={{$member->id}}>{{ $member->status=='1' ?  'Active': 'InActive' }}</button>
                                    <a href="{{url('admin/assigne-roles',$member->id)}}" class="btn btn-sm btn-success my-1 mx-1">Assign Roles</a>
                                    <a href="{{url('admin/assigne-permissions',$member->member_id)}}" class="btn btn-sm btn-success my-1 mx-1">Assign Permissions</a>
                                </td>
                            </tr>
                            @empty
                            <p>No Members to Show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
