@extends('layouts.admin.base')
@section('content')

<style type="text/css">
     #country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:200px;height: 118px;overflow-y: auto;position: absolute;z-index: 999;}
#country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
#country-list li:hover{background:#ece3d2;cursor: pointer;}
#membername{padding: 10px;border: #a8d4b1 1px solid;border-radius:4px;}
</style>


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit Messages</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('member.update', $member->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}


                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Member Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$member->name}}" name="name" placeholder="Member Name" id="membername">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Member Email<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="email" class="form-control" value="{{$member->email}}" name="email" placeholder="Member Email">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Member Contact Number<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="number" class="form-control" value="{{$member->mobile_number}}" name="mobile_number" placeholder="Member Contact Number">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Description<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="description">{{$member->description}}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Upload Photo<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="file" class="form-control" name="image">
                            <b>Image should be in 1:1 ration eg: 250X250</b>
                        </div>
                        <img src="{{asset(config('conventions.member_display').$member->image_url)}}" alt="{{$member->name}}" width=250px class="img-fluid">
                    </div>
                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
    CKEDITOR.replace('description');

$(document).ready(function(){
    $("#membername").keyup(function(){
        
        $.ajax({
        type: "POST",
        url: "{{url('admin/members_search')}}",
        data:'keyword='+$(this).val(),
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(data);
        }
        });
    });
    $('body').on('click','.text_name',function(){
        var val=$(this).text();
        $("#membername").val(val);
        $("#suggesstion-box").hide();
    });
});
//To select country name
function selectCountry(val) {
//$("#search-box").val(val);
//$("#suggesstion-box").hide();
}
</script>

@endsection