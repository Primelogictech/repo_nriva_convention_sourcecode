@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->
<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Upload Banners</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>
<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('banner.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Upload Banner <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="file" class="form-control" name="banner">
                            <b>Image should be in 5:2 ration  eg: 4000X1600, 2000X800</b>
                        </div>
                    </div>
                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit"> </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection

