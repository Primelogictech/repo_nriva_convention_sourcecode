@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit Program</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('program.update', $program->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}

                    <div>
                        <h5 class="mb-3">Contact Details :</h5>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Chair Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" name="chair_name" value="{{$program->chair_name}}" placeholder="Chair Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Mobile Number<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="number" class="form-control" value="{{$program->mobile_number}}" name="mobile_number" placeholder="Mobile Number">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Co-Chair Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$program->co_chair_name}}" name="co_chair_name" placeholder="Co-Chair Name">
                        </div>
                    </div>
                    <div>
                        <h5 class="mb-3">Program Details :</h5>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Program Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$program->program_name}}" name="program_name" placeholder="Program Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Date<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="date" class="form-control" value="{{ \Carbon\Carbon::parse($program->date)->isoFormat('Y-MM-DD')}}" name="date">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Location<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$program->Location}}" name="Location" placeholder="Location">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Create Schedule<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="page_content">{{$program->page_content}}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Upload Event Image <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="file" class="form-control" name="image">
                            <b>Image should be in 4:3 ration eg: 375X281</b>
                        </div>
                    </div>
                    <img src="{{asset(config('conventions.program_display').$program->image_url)}}" alt="event" width=100 class="img-fluid">
                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace('page_content');
</script>


@endsection

@endsection
