@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Programs</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('program.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>Sl NO.</th>
                                <th>Chair Name</th>
                                <th>Mobile Number</th>
                                <th>Co-Chair Name</th>
                                <th>Program Name</th>
                                <th>Date</th>
                                <th>Location</th>
                                <th style="width: 300px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($programs as $program)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$program->chair_name}}</td>
                                <td>{{$program->mobile_number}}</td>
                                <td>{{$program->co_chair_name}}</td>
                                <td>{{$program->program_name}}</td>
                                <td> {{ \Carbon\Carbon::parse($program->date)->isoFormat('MMM Do YYYY')}}</td>
                                <td>{{$program->Location}}</td>
                                <td>
                                    <a href="{{route('program.edit', $program->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <button href="#" data-id={{$program->id}} data-url='{{ env("APP_URL") }}/admin/program/{{$program->id}}' class="btn btn-sm btn-success my-1 mx-1  delete-btn">Delete</button>
                                    <button href="#" data-url="{{ env("APP_URL") }}/admin/program-status-update" class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" data-id={{$program->id}}>{{ $program->status=='1' ?  'Active': 'InActive' }}</button>
                                </td>
                            </tr>
                            @empty
                            <p>No donortypes to Show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
