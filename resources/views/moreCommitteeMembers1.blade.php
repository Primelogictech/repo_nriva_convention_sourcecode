@extends('layouts.user.base')
@section('content')
<!-- convention-leaders-bg   class for chair  -->
<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small">
         <div class="row">
                    <div class="col-12 pb-5">
                        <div>
                           
                            <img src="{{asset(config('conventions.committe_display').$committe->banner_image)}}" class="img-fluid w-100" alt="" style="height: 300px;">
                        </div>
                    </div>
                </div>
        <div class="row">
            <div class="col-12 px-0">
                <div class="leadership-heading-bg p-3">
                    <a href="{{url('/')}}" data-toggle="tooltip" title="" class="float-right-back-btn btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
                    <h4 class="mb-0">{{ $committe->name }} Committee</h4>
                </div>
                @if($committe->registrationForms)

                    <div class="text-md-right">
                      <a href="{{url($committe->registrationForms->url??'')}}" class="btn btn-danger btn-lg text-uppercase mr-2 my-1 fs-xs-15">{{$committe->registrationForms->name??''}}</a>
                    </div>
                    @endif
                           


            </div>
            <div class="col-12 border-top mt2 px-sm-20 py-4 p-md-4 p40">
                <div class="row">
                    <div class="col-12">
                       {{--  <div>
                            <img src="{{asset('images/banners/av.jpg')}}" class="img-fluid w-100" alt="" />
                        </div> --}}
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-12 py-4">
                        <div class="text-center">
                            <h5><span class="text-violet">Committee Email:</span><span class="text-danger">{{ $committe->committe_email }}</span></h5>
                        </div>
                    </div> -->

                    

                </div>

                <div class="row p-2">
                    @forelse($Committe_members as $member)
                    <div class="col-12 col-md-6 col-lg-4 my-1 p-3 border-violet-dashed-1">
                        <div class="">
                            <div class="row">
                                <div class="col-4">
                                    <div>
                                        <img src="{{asset(config('conventions.member_display'))}}/{{$member->member->image_url}}" class="img-fluid rounded border-radius-5 w-100" alt="">
                                    </div>
                                </div>
                                <div class="col-8">
                                    <h6 class="mb-0"> {{ $member->member->name }}</h6>
                                    <div class="pt-1">{{ $member->designation->name }}</div>
                                    <!-- <div>
                                        <a href="#" class="text-danger text-decoration-none">{{ hideContent($member->member->email) }}</a>
                                    </div>
                                    <h6 class="mb-0 pt-1 text-violet font-weight-bold">{{ hideContent($member->member->mobile_number)  }}</h6> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    @empty
                    <h5>No Members</h5> @endforelse </div>

                </div>
            </div>
        </div>
</section>
@endsection
