<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        body{
            margin: 0px;
            overflow-x: hidden;
        }
        .img-fluid{
            max-width: 100%;
            height: auto;
        }
        .px-0{
            padding-left: 0px !important;
            padding-right: 0px !important;
        }
        .px15{
            padding-left: 15px;
            padding-right: 15px;
        }
        .mx-0{
            margin-left: 0px !important;
            margin-right: 0px !important;
        }
        .container,
        .container-fluid,
        .container-lg,
        .container-md,
        .container-sm,
        .container-xl {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }
        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            border-collapse: collapse;
        }
        .table-responsive {
            display: block;
            width: 100%;
            overflow-x: auto;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
            padding: 10px;
        }

        @media (min-width: 576px) {
            .container,
            .container-sm {
                max-width: 540px !important;
            }
        }
        @media (min-width: 768px) {
            .container,
            .container-md,
            .container-sm {
                max-width: 720px !important;
            }
        }
        @media (min-width: 992px) {
            .container,
            .container-lg,
            .container-md,
            .container-sm {
                max-width: 960px !important;
            }
        }
        @media (min-width: 1200px) {
            .container,
            .container-lg,
            .container-md,
            .container-sm,
            .container-xl {
                max-width: 1140px !important;
            }
        }
        .d-none {
            display: none !important;
        }
        .d-block {
            display: block !important;
        }
        @media (min-width: 992px) and (max-width: 1600px) {
            .d-lg-none {
                display: none !important;
            }
            .d-lg-block {
                display: block !important;
            }
        }
    </style>
</head>
<body>
 @include('mails.partials.header')
 <div class="container px-0">
    <div class="px15">
<p>Auto generated email, pls do not reply to this.</p>
<h1>Hi {{ucfirst($user->name)}},</h1>
<h3> you have successfully paid due amount of $ {{ $payment->payment_amount }}.</h3>


<h2>Payment Details:</h2>

    <table   class="table table-responsive" >
        <tr>
            <td><b>Mode of Payment:</b></td>
            <td>{{ $PaymentType->name }}</td>
        </tr>

        <tr>
            <td><b>Total Amount:</b></td>
            <td>$ {{ $user->total_amount }}</td>
        </tr>
        <tr>
            <td><b>Paid Amount:</b></td>
            <td>$ {{ $payment->payment_amount }}</td>
        </tr>
        <tr>
            <td><b>Due Amount:</b></td>
            <td>$ 0</td>
        </tr>
         <tr>
            <td><b>Payment Details: </b></td>
            <td> 
            <b>Transaction Id : </b>  {{$payment->unique_id_for_payment}}
            @if ($PaymentType->name=='Other')
            <br>
                <b>On Behalf Of : </b>{{$payment->more_info['Payment_made_through']}}
            <br>
                <b>Company Name : </b>{{$payment->more_info['company_name']}}
            <br>
                <b>Transaction Date : </b>{{$payment->more_info['transaction_date']}}
            @endif
                
            @if( $PaymentType->name=='Check')
                <br>
                    <b>Check Date : </b>{{$payment->more_info['cheque_date']??""}}
            @endif
            </td>
        </tr>
    </table>

            
  <p>Please reach out to us if you have any questions.</p>
  Regards,
<address>
Registration Committee<br> 

Telangana American Telugu Association - Mega Convention<br> 

registrations@telanganaus.org (vendorsexhibits@telanganaus.org)<br> 

For online registration and Convention programs & updates, please visit www.ttaconvention.org<br> 

Date and Venue: May 27th to 29th, 2022. <br> 

New Jersey Convention And Exposition Center<br> 

97 Sunfield Ave, Edison, NJ 08837, United States<br> 
</address>
</div>
</div>
</body>
</html>