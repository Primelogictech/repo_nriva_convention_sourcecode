<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        body{
            margin: 0px;
            overflow-x: hidden;
        }
        .img-fluid{
            max-width: 100%;
            height: auto;
        }
        .px-0{
            padding-left: 0px !important;
            padding-right: 0px !important;
        }
        .px15{
            padding-left: 15px;
            padding-right: 15px;
        }
        .mx-0{
            margin-left: 0px !important;
            margin-right: 0px !important;
        }
        .container,
        .container-fluid,
        .container-lg,
        .container-md,
        .container-sm,
        .container-xl {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }
        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            border-collapse: collapse;
        }
        .table-responsive {
            display: block;
            width: 100%;
            overflow-x: auto;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
            padding: 10px;
        }

        @media (min-width: 576px) {
            .container,
            .container-sm {
                max-width: 540px !important;
            }
        }
        @media (min-width: 768px) {
            .container,
            .container-md,
            .container-sm {
                max-width: 720px !important;
            }
        }
        @media (min-width: 992px) {
            .container,
            .container-lg,
            .container-md,
            .container-sm {
                max-width: 960px !important;
            }
        }
        @media (min-width: 1200px) {
            .container,
            .container-lg,
            .container-md,
            .container-sm,
            .container-xl {
                max-width: 1140px !important;
            }
        }
        .d-none {
            display: none !important;
        }
        .d-block {
            display: block !important;
        }
        @media (min-width: 992px) and (max-width: 1600px) {
            .d-lg-none {
                display: none !important;
            }
            .d-lg-block {
                display: block !important;
            }
        }
    </style>
</head>
<body >

 @include('mails.partials.header')
 <div class="container px-0">
    <div class="px15">
<p>Auto generated email, pls do not reply to this.</p>
<h1>Hi</h1>
<p>Thank you for registering for NRI VASAVI ASSOCIATION, USA 6TH GLOBAL CONVENTION. We have received your registration and payment. Here is the summary of your registration. </p>


<h2>Registration Details:</h2>

    <table   class="table table-responsive" >
        <tr>
        <td><b>Convention Registration :<b> </td>
        <td>{{ucfirst($userdetails->convention_id??'') }}</td>
        </tr>
       
        <tr>
        <td><b>Whatsapp :<b> </td>
        <td>{{ucfirst($userdetails->whatsapp??'') }}</td>
        </tr>
         <tr>
        <td><b>Parent/Guardian Phone :<b> </td>
        <td>{{ucfirst($userdetails->parent_phone??'') }}</td>
        </tr>
         <tr>
        <td><b>Parent Guardian email :<b> </td>
        <td>{{ucfirst($userdetails->parent_email??'') }}</td>
        </tr>
       
        @foreach(explode('||',$userdetails->FirstName) as $key=>$value)

        <tr>
        <td><b>Kid FirstName :<b> </td>
        <td>{{explode('||',$userdetails->FirstName)[$key]??'' }}</td>
        </tr>
        <tr>
        <td><b>Kid LastName :<b> </td>
        <td>{{explode('||',$userdetails->LastName)[$key]??'' }}</td>
        </tr>
        <tr>
        <td><b>Kid Age :<b> </td>
        <td>{{explode('||',$userdetails->age)[$key]??'' }}</td>
        </tr>
        <tr>
        <td><b>Kid Email :<b> </td>
        <td>{{explode('||',$userdetails->email)[$key]??'' }}</td>
        </tr>
        <tr>
        <td><b>Kid Phone :<b> </td>
        <td>{{explode('||',$userdetails->phone)[$key]??'' }}</td>
        </tr>

        @endforeach

         
    </table>
<br>
<br>

 Download the Youth Banquet 
                          <a class="btn btn-sm btn-success" href="https://conventionuat.nriva.org/DraftWaiverforYouthBanquetParticipation.docx" download>Youth Banquet Waiver Form</a>


  <br>

  <p>Please reach out to us if you have any questions.</p>

  Regards,
 <address>
      
        
NRI VASAVI ASSOCIATION (NRIVA)  - 6TH GLOBAL CONVENTION<br>
For online registration and Convention programs & updates, please visit <br>www.convention.nriva.org<br>
Renaissance Schaumburg Convention Center Hotel 1551 Thoreau Dr N, Schaumburg, Chicago, IL 60173, United States<br>
    </address>
</div>
</div>
</body>
</html>