<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        body{
            margin: 0px;
            overflow-x: hidden;
        }
        .img-fluid{
            max-width: 100%;
            height: auto;
        }
        .px-0{
            padding-left: 0px !important;
            padding-right: 0px !important;
        }
        .mx-0{
            margin-left: 0px !important;
            margin-right: 0px !important;
        }
        .px15{
            padding-left: 15px;
            padding-right: 15px;
        }
        .container,
        .container-fluid,
        .container-lg,
        .container-md,
        .container-sm,
        .container-xl {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }
        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            border-collapse: collapse;
        }
        .table-responsive {
            display: block;
            width: 100%;
            overflow-x: auto;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
            padding: 10px;
        }

        @media (min-width: 576px) {
            .container,
            .container-sm {
                max-width: 540px !important;
            }
        }
        @media (min-width: 768px) {
            .container,
            .container-md,
            .container-sm {
                max-width: 720px !important;
            }
        }
        @media (min-width: 992px) {
            .container,
            .container-lg,
            .container-md,
            .container-sm {
                max-width: 960px !important;
            }
        }
        @media (min-width: 1200px) {
            .container,
            .container-lg,
            .container-md,
            .container-sm,
            .container-xl {
                max-width: 1140px !important;
            }
        }
        .d-none {
            display: none !important;
        }
        .d-block {
            display: block !important;
        }
        @media (min-width: 992px) and (max-width: 1600px) {
            .d-lg-none {
                display: none !important;
            }
            .d-lg-block {
                display: block !important;
            }
        }
    </style>
</head>
<body>

    @include('mails.partials.header')
<div class="container px-0">
    <div class="px15">
<p>Auto generated email, pls do not reply to this.</p>
<h1>Dear {{ucfirst($user->full_name) }},</h1>
<p>Thank you for registering for NRI VASAVI ASSOCIATION, USA 6TH GLOBAL CONVENTION. We have received your registration and payment. Here is the summary of your registration. </p>


<h2>Registration Details:</h2>

    <table class="table table-responsive" >
        <tr>
        <td><b>Name :<b> </td>
        <td>{{ucfirst($user->full_name) }}</td>
        </tr>

        

        <tr>
            <td><b>Mode of Payment:</b></td>
            <td>{{ $Paymenttype->name??'Free' }}</td>
        </tr>

        <tr>
            <td><b>Total Amount:</b></td>
            <td>$ {{ $payment->payment_amount??'0' }}</td>
        </tr>
        <tr>
            <td><b>Discount Amount:</b></td>
            <td>$ {{ $payment->discount_amount??'0' }}</td>
        </tr>
        <tr>
            <td><b>Discount Code:</b></td>
            <td>{{ $payment->discount_code??'' }}</td>
        </tr>
        <tr>
            <td><b>Paid Amount:</b></td>
            <td>$ {{ $payment->paid_amount??'0' }}</td>
        </tr>
         <tr>
           

            <td> 
            <b>Transaction Id : </b>  {{$payment->unique_id_for_payment??''}}
            @if($Paymenttype)
            @if ($Paymenttype->name=='Other')
            <br>
                <b>On Behalf Of : </b>{{$payment->more_info['Payment_made_through']??''}}
            <br>
                <b>Company Name : </b>{{$payment->more_info['company_name']??''}}
            <br>
                <b>Transaction Date : </b>{{$payment->more_info['transaction_date']??''}}
            @endif
                
            @if( $Paymenttype->name=='Check')
                <br>
                    <b>Check Date : </b>{{$payment->more_info['cheque_date']??''}}
            @endif
           
            @endif
            </td>
        </tr>
    </table>
    <br>
<br>
<br>

    <table class="table" border="1">
    <tr>
        <th>S.No</th>
        <th>Exhibit Type</th>
        <th>Number Booths</th>
        <th>Amount</th>
    </tr>
    
        <?php $i=1 ?>
             @foreach(json_decode($user->booth_type) as $key => $value)
                @if($value > 0)
        <tr class="text-center" style="text-align: center;">
                                   
            <td>{{$i}}</td>
                <?php $exbhit = \App\Models\Admin\ExhibitorType::where("id",$key)->first();
                $i++;
                 ?>

            <td>{{$exbhit->name  }}  {{($exbhit->size_price[0]['size'] )}}</td>
            <td>{{$value}}</td>
            <td>$
               
             @if(strtotime($exbhit->size_price[0]['till_date']) < time() ){{$exbhit->size_price[0]['price_after']*$value}} @else
                {{$exbhit->size_price[0]['price_before']*$value}}
            @endif
            </td>
        </tr>
                @endif
            @endforeach
        
   
    </table>

            
<br>

  <p>Please reach out to us if you have any questions.</p>

  Regards,
 <address>
        Exhibit Committee<br>
        
NRI VASAVI ASSOCIATION (NRIVA)  - 6TH GLOBAL CONVENTION<br>
exhibits@nriva.org<br>
For online registration and Convention programs & updates, please visit <br>www.convention.nriva.org<br>
Renaissance Schaumburg Convention Center Hotel 1551 Thoreau Dr N, Schaumburg, Chicago, IL 60173, United States<br>
    </address>
</div>
</div>
</body>
</html>