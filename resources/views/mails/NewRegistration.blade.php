<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        body{
            margin: 0px;
            overflow-x: hidden;
        }
        .img-fluid{
            max-width: 100%;
            height: auto;
        }
        .px-0{
            padding-left: 0px !important;
            padding-right: 0px !important;
        }
        .mx-0{
            margin-left: 0px !important;
            margin-right: 0px !important;
        }
        .px15{
            padding-left: 15px;
            padding-right: 15px;
        }
        .container,
        .container-fluid,
        .container-lg,
        .container-md,
        .container-sm,
        .container-xl {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }
        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            border-collapse: collapse;
        }
        .table-responsive {
            display: block;
            width: 100%;
            overflow-x: auto;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
            padding: 10px;
        }

        @media (min-width: 576px) {
            .container,
            .container-sm {
                max-width: 540px !important;
            }
        }
        @media (min-width: 768px) {
            .container,
            .container-md,
            .container-sm {
                max-width: 720px !important;
            }
        }
        @media (min-width: 992px) {
            .container,
            .container-lg,
            .container-md,
            .container-sm {
                max-width: 960px !important;
            }
        }
        @media (min-width: 1200px) {
            .container,
            .container-lg,
            .container-md,
            .container-sm,
            .container-xl {
                max-width: 1140px !important;
            }
        }
        .d-none {
            display: none !important;
        }
        .d-block {
            display: block !important;
        }
        @media (min-width: 992px) and (max-width: 1600px) {
            .d-lg-none {
                display: none !important;
            }
            .d-lg-block {
                display: block !important;
            }
        }
    </style>
</head>
<body>
    @include('mails.partials.header')
    <div class="container px-0">
        <div class="px15">
            <div>Auto generated email, pls do not reply to this.</div>
            <h1 class="py-3">Dear {{ ucfirst($user->first_name) }} {{ucfirst($user->last_name)}},</h1>
            <div>Thank you for registering for NRI VASAVI ASSOCIATION, USA 6TH GLOBAL CONVENTION. We have received your registration and payment. Here is the summary of your registration.</div>

            <h2>Registration Details:</h2>

            <table>

                <tr>
                    <td><b>Name : </b> </td>
                    <td>{{ ucfirst($user->first_name)}} {{ ucfirst($user->last_name)}}</td>
                </tr>

                <tr>
                    <td><b>Total Amount : </b> </td>
                    <td>$ {{number_format($user->total_amount)}}</td>
                </tr>
                
                <tr>
                    <td><b>Amount Paid : </b> </td>
                    <td>$ {{number_format($user->amount_paid)}}</td>
                </tr>

                 <tr>
                    <td><b>Due Amount : </b> </td>
                    <td>$ {{number_format($user->total_amount - $user->amount_paid)}}</td>
                </tr>


                <tr>
                    <td><b>Mode of Payment : </b></td>
                    <td>{{$PaymentType->name??""}}</td>
                </tr>



                <tr>
                    <td><b>Payment Details: </b></td>
                    <td> 
                    <b>Transaction Id : </b>  {{$payment->unique_id_for_payment??''}}
                    @if ($PaymentType->name=='Other')
                   <br>
                        <b>On Behalf Of : </b>{{$payment->more_info['Payment_made_through']??''}}
                    <br>
                        <b>Company Name : </b>{{$payment->more_info['company_name']??''}}
                    <br>
                        <b>Transaction Date : </b>{{$payment->more_info['transaction_date']??''}}
                    @endif
                        
                    @if( $PaymentType->name=='Check')
                     <br>
                         <b>Check Date : </b>{{$payment->more_info['cheque_date']??''}}
                    @endif
                    </td>
                </tr>

                


                <tr>
                    <td><b>Registration Id : </b></td>
                    <td>{{$user->registration_id??''}}</td>
                </tr>
            </table>



        </br>
        </br>


@if($user->registration_amount)
    <div class="table-responsive pt15">
        <table class="table-bordered table table-hover table-center mb-0">
            <tbody>
                <tr>
                    <th>Registration Package</th>
                    <th>Amount</th>
                    <th>Count</th>
                    <th>Total</th>
                </tr>
                <!-- <tr>
                    <td>1 </td>
                    <td>
                        One Person (Age 7 Years and Above) (May 27 &amp; May 29, 2022) * - 2 Days
                    </td>
                    <td>11</td>
                </tr> -->

                
              @foreach($user->individual_registration as $key => $value)
                 @if($value==null)   
                    @continue
                 @endif
                  <tr>
                      <td>
                          <?php $Individual = \App\Models\Admin\SponsorCategory::where('status', 1)->where('id',$key)->first();
                          echo $Individual->benefits[0];
                              ?>

                      </td>
                       <td>
                       @if($Individual->price_change_date>Carbon\Carbon::now())
                            $ {{number_format($Individual->amount_before)}}
                       @else
                            $ {{number_format($Individual->amount_after)}}
                       @endif

                      </td>
                      <td>
                      {{$value??0}}
                      </td>
                      <td>
                       @if($Individual->price_change_date>Carbon\Carbon::now())
                            $ {{number_format($Individual->amount_before*$value) }}
                       @else
                            $ {{number_format($Individual->amount_after*$value) }}
                       @endif
                      </td>
                  </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <br>
@endif

  
@if($user->categorydetails)
    <div class="table-responsive">
        <table class="table-bordered table table-hover table-center mb-0" >
            <tbody>
                <tr>
                    <th width="160px">Donation Package</th>
                    <th>Benefits</th>
                    <th width="120px" >Amount</th>
                </tr>

                <tr>
                    <td>{{  $user->categorydetails->donortype->name ?? ""}}</td>
                    <td>
                        @if(!empty($user->categorydetails->benfits))
                         @foreach ($user->categorydetails->benfits as $benfit)
                            {{ $benfit->name  }} {{ ($benfit->pivot->count) }}
                            <br>
                        @endforeach
                        @endif
                    </td>
                    <td>$ {{number_format($user->donor_amount??'') }}</td>
                </tr>
            </tbody>
        </table>
    </div>
@endif
    <br>
    <p>Please reach out to us if you have any questions.</p>

    Regards,
    <address class="mb-3">
        Registration Committee<br>
        
NRI VASAVI ASSOCIATION (NRIVA)  - 6TH GLOBAL CONVENTION<br>
registration@nriva.org <br>
For online registration and Convention programs & updates, please visit <br>www.convention.nriva.org<br>
Renaissance Schaumburg Convention Center Hotel 1551 Thoreau Dr N, Schaumburg, Chicago, IL 60173, United States<br>
    </address>
</div>
</div>
</body>
</html>
