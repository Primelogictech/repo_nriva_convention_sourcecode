<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        body{
            margin: 0px;
            overflow-x: hidden;
        }
        .img-fluid{
            max-width: 100%;
            height: auto;
        }
        .px-0{
            padding-left: 0px !important;
            padding-right: 0px !important;
        }
        .mx-0{
            margin-left: 0px !important;
            margin-right: 0px !important;
        }
        .px15{
            padding-left: 15px;
            padding-right: 15px;
        }
        .container,
        .container-fluid,
        .container-lg,
        .container-md,
        .container-sm,
        .container-xl {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }
        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            border-collapse: collapse;
        }
        .table-responsive {
            display: block;
            width: 100%;
            overflow-x: auto;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
            padding: 10px;
        }

        @media (min-width: 576px) {
            .container,
            .container-sm {
                max-width: 540px !important;
            }
        }
        @media (min-width: 768px) {
            .container,
            .container-md,
            .container-sm {
                max-width: 720px !important;
            }
        }
        @media (min-width: 992px) {
            .container,
            .container-lg,
            .container-md,
            .container-sm {
                max-width: 960px !important;
            }
        }
        @media (min-width: 1200px) {
            .container,
            .container-lg,
            .container-md,
            .container-sm,
            .container-xl {
                max-width: 1140px !important;
            }
        }
        .d-none {
            display: none !important;
        }
        .d-block {
            display: block !important;
        }
        @media (min-width: 992px) and (max-width: 1600px) {
            .d-lg-none {
                display: none !important;
            }
            .d-lg-block {
                display: block !important;
            }
        }
    </style>
</head>
<body>

    @include('mails.partials.header')
<div class="container px-0">
    <div class="px15">
<p>Auto generated email, pls do not reply to this.</p>
<h1>Dear</h1>
    <p>Name:{{$user->first_name}} {{$user->last_name}}</p>
    <p>Email:{{$user->email}}</p>
    @foreach ($images as $img)
    <p>For {{$img->benfit->name}}<p>
    <a  href="{{env('APP_URL')}}/{{config('conventions.benfit_image_display')}}/{{$img->image_url}}" download="{{$img->benfit->name}}" ><img src="{{env('APP_URL')}}/{{config('conventions.benfit_image_display')}}/{{$img->image_url}}" width="500" height="600"></a>
    @endforeach
        

  <p>Please find the accachements.</p>


  <p>Please reach out to us if you have any questions.</p>

  Regards,
 <address>
        Exhibit Committee<br>
        
NRI VASAVI ASSOCIATION (NRIVA)  - 6TH GLOBAL CONVENTION<br>
exhibits@nriva.org<br>
For online registration and Convention programs & updates, please visit <br>www.convention.nriva.org<br>
Renaissance Schaumburg Convention Center Hotel 1551 Thoreau Dr N, Schaumburg, Chicago, IL 60173, United States<br>
    </address>
</div>
</div>
</body>
</html>