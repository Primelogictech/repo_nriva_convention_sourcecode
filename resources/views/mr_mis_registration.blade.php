@extends('layouts.user.base')
@section('content')
<section class="container-fluid my-3 my-lg-5">
   <div class="container">
      <div class="row">
         <div class="col-12 shadow-small py-0 pt-1 px-1">
             <div class="row">
                    <div class="col-12 pb-5" style="text-align:center;margin-top: 30px;">
                        <div>
                            <h4><b>Mr, Mrs, Miss and Master NRIVA Registration</b></h4>
                        </div>
                    </div>
                </div>
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
               <button type="button" class="close" data-dismiss="alert">×</button>
               <strong>{{ $message }}</strong>
            </div>
            @endif
            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
               <button type="button" class="close" data-dismiss="alert">×</button>
               <strong>{{ $message }}</strong>
            </div>
            @endif
             
            <div class="row px-1 mx-0 mx-lg-5 px-lg-0">
               
               <div class="col-12 col-lg-12 shadow-small px-sm-20 p40 p-md-4 mb-4">
                  <form id='form' action="{{url('mr_mis_registration')}}" method="post" enctype="multipart/form-data">
                     <div class="row">
                        @csrf
                        <!-- Personal Information -->
                        <div class="col-12">
                           <h5 class="text-violet py-2">Personal Information</h5>
                           <div class="form-row">
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Email Id of the Participant:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="email"  name="email" id="email"  class="form-control" placeholder="Email Id of the Participant" value="{{  $user->email ?? '' }}" />
                                    <input type="hidden" name="id" value="{{  $user->id ?? '' }}">
                                 </div>
                              </div>
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Mobile Number:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" name="phone" id="phone" value="{{  $user->phone ?? '' }}" class="form-control" placeholder="Mobile Number"   />
                                 </div>
                              </div>
                              
                           </div>
                           
                           <div class="form-row">
                              <div class="form-group col-12 col-md-2 col-lg-2">
                                 <label>Title:</label><span class="text-red">*</span>
                                 <div>
                                    
                                    <input type="text" name="title" value="{{Request::get('type')}}" class="form-control" readonly="">
                                 </div>
                              </div>
                              <div class="form-group col-12 col-md-5 col-lg-5">
                                 <label>First Name:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" name="first_name" id="first_name" value="{{  $user->first_name ?? '' }}" class="form-control"  placeholder="First Name"  />
                                 </div>
                              </div>
                              <div class="form-group col-12 col-md-5 col-lg-5">
                                 <label>Last Name:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text"  name="last_name" id="last_name" value="{{  $user->last_name ?? '' }}" class="form-control" placeholder="Last Name" />
                                 </div>
                              </div>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-12 col-md-3 col-lg-3">
                                 <label>DOB:</label><span class="text-red">*</span>
                                 <div>
                                    
                                    <input type="date" id="dob" name="dob" value="{{$user->dob??''}}" class="form-control" max="{{date('Y-m-d')}}">
                                 </div>
                              </div>
                              <div class="form-group col-12 col-md-4 col-lg-4">
                                 <label>Chapter:</label><span class="text-red">*</span>
                                 <div>
                                  <select class="form-control" name="chapter">
                                    <option value="">Select Chapter</option>
                                    @foreach($chapters as $chapter)
                                    <option value="{{$chapter->name}}" @if($user) @if($chapter->name==$user->chapter) selected @endif @endif>{{$chapter->name}}</option>
                                    @endforeach
                                  </select>
                                    
                                 </div>
                              </div>
                              <div class="form-group col-12 col-md-5 col-lg-5">
                                 <label>City:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text"  name="city" id="city" value="{{  $user->city ?? '' }}" class="form-control" placeholder="City" />
                                 </div>
                              </div>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-12 col-md-12 col-lg-12">
                                 <label>About Your Self:</label>
                                 <div>
                                    <textarea name="about" class="form-control" placeholder="About Your Self" maxlength="500">{{  $user->about ?? '' }}</textarea>
                              </div>
                              
                           </div>
                           
                            <div class="form-row">
                            <div class="col-12 col-sm-6 col-md-12 my-2 my-md-auto">
                                    <div class="text-center text-sm-right">
                                      @if($user)
                                      <button type="submit" class="btn btn-lg btn-danger text-uppercase px-5 submit">Submit</button>
                                      @else
                                        <button type="submit" class="btn btn-lg btn-danger text-uppercase px-5 submit" name="" disabled="">Submit</button>
                                        @endif
                                        
                                    </div>
                                </div>
                            </div>

                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Registration</h4>
        </div>

        <div class="modal-body">
            <div class="alert alert-success">Please enter Email Verification Code received in email. There might be delay up to 30 seconds to receive email and check spam/junk folder</div>
            <form class="" action="{{url('new_registertion')}}" method="post">
                 @csrf
                <input type="hidden" name="email" id="new_email">
                <input type="hidden" name="register_type" value="mr_miss">
             <div class="form-row">
                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Code :</label><span class="text-red">*</span>
                        <div>
                            <input type="number" required name="otp" id="otp" value="" class="form-control" placeholder="Verification Code"  />

                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Create Password (Optional) :</label>
                        <div>
                            <input type="password" name="password" id="password"  class="form-control" placeholder="Password"  />
                        </div>
                    </div>
                </div>
                 <button type="submit" class="btn btn-primary" >Submit</button>
            </form>

          
        </div>
         <div class="modal-footer">
        <a href="{{url('/')}}" class="btn btn-danger" >Close</a>
      </div>
        
      </div>
      
    </div>
  </div>


@section('javascript')
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
<script>
    $(document).ready(function() {

      $('#dob').change(function(){ 
        var dob =$(this).val();

        var min = "{{$mr_type->min_age}}";
        var max = "{{$mr_type->max_age}}";

        dob = new Date(dob);
var today = new Date('2022-07-01');
var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
 console.log(parseInt(min),parseInt(age),parseInt(max));
if(parseInt(min) <= parseInt(age)){ 
   if(parseInt(age) <= parseInt(max)){
      $('.submit').prop('disabled',false);
   }else{
         $('.submit').prop('disabled',true);
   }
}else{
 
  $('.submit').prop('disabled',true);
}



      });

     $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        });
          $.validator.addMethod("numberss", function(value, element) {
            return this.optional(element) || value == value.match(/^[0-9) (-]+$/);
        });


        $("#form").validate({
           
            
            rules: {
                
                first_name: "required alpha",
                last_name: "required alpha",
                
                email: "required",
                title: "required",
                city: "required",
                chapter: "required",
                dob: "required",
                phone :{
                    maxlength: 14,
                    numberss:true,
                    required:true
                }
            },
             messages: {
               
                  "first_name": {
                    alpha: "First name should not contain numbers.",
                },
                 "last_name": {
                    alpha: "Last name should not contain numbers.",
                },
                 
                 "phone": {
                    maxlength: "Max length is  14.",
                    numberss: "Numbers only"
                }
            },
        });
   });

 @if(!Auth::user())
$('#email').on('change',function(){
        $('.email_valid_msg').text('')
        var email = $('#email').val();
        var thiss =$(this);
          if(email !=""){
            if(email.search('@') >0 ){
            }else{
                $('.email_valid_msg').text('Please enter a valid email')
            }
        $.ajax({
            type: 'GET',
            url: "{{url('email_verification')}}?email="+email,
            success:function(data){
              if(data=="success"){
                $('#new_email').val(email);
                $('#myModal').modal({backdrop: 'static', keyboard: false});
                $('#myModal').modal('show');
              }else{
                window.location.href="{{url('login')}}?error=exit&type=mr_miss&email="+email;
              }
             
            }
        });
      }else{
          $('.email_valid_msg').text('Please enter email')
        //alert('Please enter email');
      }

    });
@endif
</script>

@endsection

@endsection