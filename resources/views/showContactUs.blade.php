@extends('layouts.user.base') @section('content')

<style type="text/css">
    body {
        background-color: #fff !important;
        background-image: none;
    }
</style>

<section class="container px-4 mt-3 mb-5 px-lg-5">
    <div class="row">
        <div class="col-12 col-lg-12 mb-3">
            <div class="">
                <h2 class="text-violet text-center mt-3">Contact Us</h2>
            </div>
        </div>
    </div>
    <!-- <div class="row">
        <div class="col-12">
            <div class="contact-box">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="address-box">
                            <div class="address-icon">
                                <i class="fa fa-home"></i>
                            </div>
                            <div class="address-text">
                                <span class="label">For Sponsorship:</span>
                                <div>
                                    <a href="#" class="text-navyblue text-break">conventioncorecommittee@nriva.org</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="address-box">
                    <div class="address-icon">
                       <i class="fa fa-phone"></i>
                    </div>
                    <div class="address-text">
                       <span class="label">Phone:</span>
                       <a href="#" class="text-navyblue">1-855-WE NRIVA</a>
                    </div>
               </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-5">
                        <div class="address-box">
                    <div class="address-icon">
                       <i class="fas fa-map-marker-alt"></i>
                    </div>
                    <div class="address-text">
                       <span class="label">Address:</span>
                       <div class="text-navyblue">Renaissance Schaumburg Convention Center, Hotel 1551 Thoreau Dr N,Schaumburg, Chicago, IL 60173, US</div>
                    </div>
               </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="row">
        <div class="col-12 col-lg-10 offset-lg-1">
            @foreach ($errors->all() as $error)
            <div class="error fs20">{{ $error }}</div>

            @endforeach
            <div class="contact-section">
                <div class="mb20 text-center mb-4">
                   <span class="clr-blue mb15">Any questions or remarks? Just write us a message</span>
                   <h5 class="py-2 fs-sm-15">Please fill the form below and our team will get back to you</h5>
                </div>
                <form method="post" action="" id="contact_form">
                    @csrf
                    <fieldset>
                        <div class="row">
                            <div class="col-12 col-md-6 col-sm-6 col-lg-6 mb20">
                                <div><input class="form-control" type="text" id="name" name="name" placeholder="Your Name" required="" /></div>
                            </div>

                            <div class="col-12 col-md-6 col-sm-6 col-lg-6 mb20">
                                <div>
                                    <input type="email" name="email" value="" required autofocus class="form-control" placeholder="Your Email" />
                                </div>
                            </div>

                            <div class="col-12 mb20">
                                <select class="form-control" required name="committee_mail">
                                    <option value="">Select Committee</option>
                                    @foreach ($committes as $committe)
                                    <option value="{{$committe->committe_email}}">{{$committe->name}} Committee</option>
                                    @endforeach
                                
                                </select>
                            </div>
                            <div class="col-12 mb20">
                                <div>
                                    <input type="text" name="Subject" value="" required autofocus class="form-control" placeholder="Subject" />
                                </div>
                            </div>

                            <div class="col-12 mb20">
                                <div>
                                    <textarea maxlength="1000" class="form-control" id="message" name="message" placeholder="Your message Here" required=""></textarea>
                                    <div style="color: #adadad;">Maximum Limit : 1000 Characters</div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-sm-6 col-lg-6 mb20 my-auto">
                                <div>
                                    <input type="text" id="captcha_number" name="Security Code" value="" required autofocus class="form-control" placeholder="Security Code" />
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-sm-6 col-lg-6 mb20 my-auto">
                                <div class="text-center text-sm-left mt-3 mt-sm-0">
                                    <span id="captcha">8500817</span>
                                    <img src="images/refresh.png" class="img-fluid ml-3 captcha_refresh" width="19" height="20" alt="" />
                                </div>
                            </div>
                            <div class="col-12 mt20">
                                <div>
                                    <input type="submit" class="contact-submit" type="submit" value="Submit" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</section>

@section('javascript')

<script type="text/javascript">
    $("#contact_form").on("submit", function (event) {
        var enter_captha = $("#captcha_number").val();
        var captcha = $("#captcha").text();
        if (enter_captha != captcha) {
            event.preventDefault();
            event.stopPropagation();
            alert("Captcha Invalid.");
        }
    });
    captcha();
    $(".captcha_refresh").on("click", function () {
        captcha();
    });
    function captcha() {
        var number = Math.floor(100000 + Math.random() * 900000);
        $("#captcha").text(number);
    }
</script>

@endsection @endsection
