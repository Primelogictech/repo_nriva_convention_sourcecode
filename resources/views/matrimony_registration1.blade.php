<?php include 'header.php';?> 

<style type="text/css">
    .card {
        background-color: transparent;
        border: 0px solid;
    }
    .card-header {
        padding: 0px;
    }
    .card-header a {
        background: #784d98;
        color: #fff;
        padding: 0.75rem 1.25rem;
        width: 100%;
        display: block;
    }
    .p-radio-btn {
        position: absolute;
        top: 3px;
    }
    .card-header a::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/plus.png);
    }
    .card-header a[aria-expanded="true"]::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/minus.png);
    }
</style>
<section class="container-fluid my-3 my-lg-5">
   <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12 pb-5">
                        <div>
                            <img src="images/banners/matrimony.jpg" class="img-fluid w-100" alt="">
                        </div>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                   <button type="button" class="close" data-dismiss="alert">×</button>
                   <strong>{{ $message }}</strong>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                   <button type="button" class="close" data-dismiss="alert">×</button>
                   <strong>{{ $message }}</strong>
                </div>
                @endif
                 @if ($message = Request::get('error'))
                <div class="alert alert-danger alert-block">
                   <button type="button" class="close" data-dismiss="alert">×</button>
                   <strong>This member not registred with nriva eedujodu. Please register in Nriva.org with eedujodu membership.</strong>
                </div>
                @endif
                <div class="row px-1 mx-0 mx-lg-5 px-lg-0">
                    <div class="col-12 mb-5">
                        <h5 class="text-violet text-center mb-0">Find your perfect Match at NRIVA Matrimony!</h5>
                    </div>
                    <div class="col-12 col-lg-12 shadow-small px-sm-20 p-md-3 mb-4">
                        <form id='form' action="{{url('matrimony_registration')}}" method="post" enctype="multipart/form-data">
                            <div class="row">
                            @csrf
                            <!-- Personal Information -->
                                <!-- Register Page -->
                                
                               <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 col-sm-5 col-md-4 col-lg-3">
                                            <div>
                                                <img 
                                                    @if(isset($user->profile_image))
                                                  src="{{config('conventions.NRIVA_URL')}}/uploads/documents/Eedu-Jodu/{{$user->user_id}}/{{$user->profile_image}}"
                                                    @else
                                                          src="images/no-image1.jpg"
                                                    @endif
                                                    class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-8 col-lg-9">
                                            <h5>Partcipant's Information from Eedu-Jodu profile</h5>
                                            <div class="table-responsive  white-space-nowrap pt15">
                                                <table class="table-bordered table table-hover table-center mb-0">
                                                    <tbody>
                                                        <tr>
                                                            <td><strong>Profile Name : </strong> {{$user->first_name??""}} {{$user->last_name??""}}</td>
                                                            <td><strong>Current Address : </strong> {{$user->address??""}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Current City : </strong>Hyderabad</td>
                                                            <td><strong>Current Job : </strong>{{$user->job_pos??""}} {{$user->curr_job??""}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Phone Number : </strong> {{$user->phone_code??""}} {{$user->mobile??""}}</td>
                                                            <td><strong>Current State : </strong>Telangana</td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Email ID : </strong>{{$user->email??""}}</td>
                                                            <td><strong>Current Citizenship Status : </strong>{{$user->legal_status??""}}</td>
                                                        </tr>
                                                        <tr>
                                                            <!-- <td><strong>Age : </strong> 
                                                            @if($user->dob)
                                                                {{ now()->year - getDate(strtotime($user->dob))['year'] }} Years
                                                            @endif
                                                             </td> -->
                                                             <td><strong>Parents Phone Number : </strong> 908765432</td>
                                                            <td><strong>Parents Email Address : </strong> email@gmail.com</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 py-3">
                                    <p><strong>Note : </strong>If there are any changes, please go to <a href="https://matrimony.nriva.org/">https://matrimony.nriva.org/</a> to make changes.</p>
                                </div>
                                <div class="col-12 pb-3">
                                    <h6 class="text-violet">Check the segments you would like to participate</h6>
                                    <div class="py-1">
                                        <input type="hidden" name="nriva_user_id" value={{$user->id??""}} >
                                        <input type="hidden" name="email" value="{{$user->email??''}}" >
                                        <input type="hidden" name="profile_id" value="{{$user->member_id??''}}" >
                                        <input type="checkbox" name="participate_in[Social_Hangout]" class="sp-checkbox" name="">
                                        <span class="pl20 fs16"><strong>Ashirvachanam - </strong>Participants only - Saturday July 2nd 9 AM</span>
                                    </div>
                                    <div>
                                        <input type="checkbox" name="participate_in[Speed_Dating]" class="sp-checkbox" name="">
                                        <span class="pl20 fs16"><strong>Profile Introductions - </strong>Parents and Participants are required - Saturday July 2nd 11 AM - 5 PM</span><span class="text-danger pl20">* Required</span>
                                    </div>
                                    <div class="py-1">
                                        <input type="checkbox" name="participate_in[Parents_Meet]" class="sp-checkbox" name="">
                                        <span class="pl20 fs16">
                                            <strong>Formal Banquet - </strong> "Dress to Impress" - Saturday July 2nd at 7 PM
                                        </span><span class="text-danger pl20">* Required</span>
                                    </div>
                                    <div>
                                        <input type="checkbox" name="participate_in[Profile_Introductions]" class="sp-checkbox" name="">
                                        <span class="pl20 fs16">
                                            <strong>Parents Meet - </strong>Only Parents - Saturday July 3rd - TBD
                                        </span>
                                    </div> 
                                    <div>
                                        <input type="checkbox" name="participate_in[Profile_Introductions]" class="sp-checkbox" name="">
                                        <span class="pl20 fs16">
                                            <strong>Connect Over Coffee - </strong>Only US Raised - Saturday July 3rd - TBD
                                        </span>
                                    </div>          
                                </div>
                                <div class="col-12 py-3">
                                    <div class="shadow-small py-3 border-radius-5 p15">
                                        <p class="mb-3">Please enter number of people attending including Participant</p>
                                        <div>
                                            <textarea name="note" placeholder="Short answer text" class="w-100 border-0" style="border-bottom: 1px solid #ccc !important;"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 py-3">
                                    <div class="">
                                        <input type="checkbox" required class="sp-checkbox" name="">
                                        <span class="pl20 fs16">I have understood, how each segment is conducted</span><span class="text-danger pl20">* Required</span>
                                    </div>
                                    <div class="">
                                        <input type="checkbox" required class="sp-checkbox" name="">
                                        <span class="pl20 fs16">I have understood, I am responsible to find my own accomodation to stay</span><span class="text-danger pl20">* Required</span>
                                    </div>
                                    <div class="form-row my-3">
                                        <div class="col-12 col-sm-6 col-md-12 my-2 my-md-auto">
                                            <div class="text-center text-sm-right">
                                                <input type="submit" class="btn btn-lg btn-danger text-uppercase px-5" value="Submit" name="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- End of Register Page -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Registration</h4>
        </div>

        <div class="modal-body">
            <div class="alert alert-success">Please enter nriva password and procced to registration</div>
            <form class="" action="{{url('newAuthorize')}}" method="post">
                 @csrf
                <input type="hidden" name="email" id="new_email">
             <div class="form-row">
                    
                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Password:</label><span style="color:red;">*</span>
                        <div>
                            <input type="password" name="password" id="password"  class="form-control" placeholder="Enter Password" required />
                        </div>
                    </div>
                </div>
                 <button type="submit" class="btn btn-primary">Submit</button>
            </form>

          
        </div>
         <div class="modal-footer">
        <a href="{{url('/')}}" class="btn btn-danger" >Close</a>
      </div>
        
      </div>
      
    </div>
  </div> --}}

  <div class="modal fade" id="t_and_c" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Terms and Conditions</h4>
        </div>

        <div class="modal-body">
            <div class="">
                <ul>
                    <li>Terms and Conditions</li>
                    <li>Terms and Conditions</li>
                    <li>Terms and Conditions</li>
                    <li>Terms and Conditions</li>
                </ul>
            </div>
        </div>
         <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
        
      </div>
      
    </div>
  </div>


@section('javascript')
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
<script>

     $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        });
          $.validator.addMethod("numberss", function(value, element) {
            return this.optional(element) || value == value.match(/^[0-9) (-]+$/);
        });


        // $("#form").validate({
           
            
        //     rules: {
                
        //         first_name: "required alpha",
        //         last_name: "required alpha",
        //         age: "required numberss",
        //         gothram: "required",
        //         rasi: "required",
        //         state: "required",
        //         country: "required",
        //         address: "required",
        //         email: "required",
        //         mobile :{
        //             maxlength: 14,
        //             numberss:true
        //         },
        //         'zip_code': {
        //                 digits: true
        //             },
        //     },
        //      messages: {
               
        //           "first_name": {
        //             alpha: "First name should not contain numbers.",
        //         },
        //          "last_name": {
        //             alpha: "Last name should not contain numbers.",
        //         },
                 
        //          "mobile": {
        //             maxlength: "Max length is  14.",
        //             numberss: "Numbers only"
        //         }
        //     },
        // });

 @if(!Auth::user())
$('#email').on('change',function(){
  //      checking();

    });
$('#profile_id').on('change',function(){
//        checking();

    });
function checking() {
 {{--  $('.email_valid_msg').text('')
        var email = $('#email').val();
        var profile_id = $('#profile_id').val();
        $.ajax({
            type: 'GET',
            url: "{{url('check_matrimony_email')}}?email="+email+"&profile_id="+profile_id,
            success:function(data){
              if(data!="failed"){
                $('#new_email').val(data);
                $('#myModal').modal({backdrop: 'static', keyboard: false});
                $('#myModal').modal('show');
              }else{
                window.location.href = "{{url('matrimony_registration')}}?error=not_registered";
              }
             
           
        }
      }); --}}
}
@endif
</script>

<?php include 'footer.php';?>