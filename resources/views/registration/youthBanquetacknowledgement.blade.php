@extends('layouts.user.base')
@section('content')
<style>
#agrement-error{
    top: 22px;
    left: 22px;
}
</style>
<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/youth-banquet-registration.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                <br>


                @endif
                @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif

                <div class="row px-1 mx-0 mx-lg-5 px-lg-0">

                    <div class="col-12 col-lg-12 shadow-small px-sm-20 p40 p-md-4 mb-4">
                            <div class="row">
                                <!-- Personal Information -->
                                <h4 class="text-orange text-center mb-4">Youth Banquet Consent Form</h4>
                                <div class="col-12">
                            @if(!isset($user))
                                  <form id='form' action="{{url('youth-banquet-consent')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Parent Email</label><span class="text-red">*</span>
                                            <div>
                                                <input type="email" name="parent_email"  class="form-control"  placeholder="Parent Email"   />
                                            </div>
                                            <p>Note: Please use email ID used in youth banquet registration</p>
                                        </div>

                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label></label>
                                            <button style="margin-top:22px" type="submit" class="btn btn-lg btn-danger text-uppercase px-5 submit">Search</button>
                                        </div>
                                    </div>
                          </form>
                               
                @else

                                
                        <form id='form' action="{{url('youth-banquet-consent-store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                 <input type="hidden" value="{{$user->parent_email}}" name="parent_email"  class="form-control"  placeholder="Parent Email"   />
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Parent Email</label><span class="text-red">*</span>
                                            <div>
                                                <p>{{$user->parent_email}}</p>
                                            </div>
                                        </div>
                                    </div>

                        <hr>
                        <h6>Kids Details</h6>
                      
                       

            @foreach(explode('||',$user->FirstName) as $key=>$value)

            {{--     FirstName: <br>
                LastName: 
                <br>
                Age: {{explode('||',$user->age)[$key]??'' }}
                    <br>
                Email: {{explode('||',$user->email)[$key]??'' }}
                    <br>
                Phone: {{explode('||',$user->phone)[$key]??'' }}
                <hr> --}}

                 <div class="form-row">
                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>First Name</label>
                        <div>
                            <p>{{explode('||',$user->FirstName)[$key]??'' }}</p>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Last Name</label>
                         <div>
                            <p>{{explode('||',$user->LastName)[$key]??'' }}</p>
                        </div>
                    </div>

                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Phone:</label>
                        <div>
                            <p>{{explode('||',$user->phone)[$key]??'' }}</p>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Email</label>
                         <div>
                            <p>{{explode('||',$user->email)[$key]??'' }}</p>
                        </div>
                    </div>
                      <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Age</label>
                         <div>
                            <p>{{explode('||',$user->age)[$key]??'' }}</p>
                        </div>
                    </div>
                </div>
         @endforeach
    
    
                                
                                    <div class="form-row">
                                        <div class="col-12 px-0">
                                            <div class="pb-4 pb-md-2">
                                                <input type="checkbox" class="input-checkbox" required="" name="agrement">
                                                <span class="pl25 fs16 pointer text-orange font-weight-bold" data-toggle="modal" data-target="#t_and_c">I accept Terms and Conditions</span>
                                            </div>
                                        <br>
                                            <ul class="mb-4 pl-4 text-violet">
                                                <li>I hereby agree to and abide by all the rules and guidelines. I understand that the
                                                    Organizers are not responsible for any loss or physical injury that may occur to me on
                                                    account of my participation in the event. I am participating at my own risk and can have no
                                                    claim against NRIVA in any regard.</li>
                                                <li>I hereby grant the NRIVA permission to use my photographs, video, or other digital media
                                                    (“Media”) in any and all of its publications, including web-based publications, without
                                                    payment or other consideration.</li>
                                                <li>I hereby hold harmless, release, and forever discharge the NRIVA from all claims, demands,
                                                    and causes of action which I, my heirs, representatives, executors, administrators, or any
                                                    other persons acting on my behalf or on behalf of my estate have or may have by reason of
                                                    this authorization.</li>
                                                <li>I understand and agree that all Media will become the property of the NRIVA and will not
                                                    be returned.</li>
                                                <li>I hereby irrevocably authorize the NRIVA to edit, alter, copy, exhibit, publish, or distribute
                                                    these MEDIA for any purpose. In addition, I waive any right to inspect or approve the
                                                    finished product wherein my likeness appears. Additionally, I waive any right to royalties
                                                    or other compensation arising or related to the use of the photo.</li>
                                            </ul>

                                        </div>

                                    </div>
                                </div>
                            </div>

                             <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                           
                                        </div>

                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label></label>
                                            <button style="margin-top:22px" type="submit" class="btn btn-lg btn-danger text-uppercase px-5 submit">Accept</button>
                                        </div>
                                    </div>
                    </form>
                   @endif
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@section('javascript')
<script src="https://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
<script>
    $(document).ready(function() {


        $("#form").validate({

            rules: {

            }
            , messages: {

            }
        , });
    });

   

</script>

@endsection

@endsection
