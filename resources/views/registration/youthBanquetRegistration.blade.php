@extends('layouts.user.base')
@section('content')
<style>
#agrement-error{
   top: 22px;
    left: 22px;
}
</style>
<section class="container-fluid my-3 my-lg-5">
   <div class="container">
      <div class="row">
         <div class="col-12 shadow-small py-0 pt-1 px-1">
             <div class="row">
                <div class="col-12">
                    <div>
                        <img src="images/banners/youth-banquet-registration.jpg" class="img-fluid w-100" alt="" />
                    </div>
                    <p class="text-center text-red pt25 mb-2">
                        Convention registration is closed, we reached our capacity. Thank you for your great support.
                    </p>
                    <p class="text-center text-red">
                        Please continue to watch for any further updates.
                    </p>
                </div>
            </div>
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
               <button type="button" class="close" data-dismiss="alert">×</button>
               <strong>{{ $message }}</strong>
            </div>
         <br>
            <div class='px-1'>
            Download the Youth Banquet
            <a class="btn btn-sm btn-success" href="https://conventionuat.nriva.org/DraftWaiverforYouthBanquetParticipation.docx" download>Waiver Form</a>. (You can download waiver form from the email which is been sent to your parent email address)
            </div>

            @endif
            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
               <button type="button" class="close" data-dismiss="alert">×</button>
               <strong>{{ $message }}</strong>
            </div>
            @endif
             
            <div class="row px-1 mx-0 mx-lg-5 px-lg-0">
            @guest
               <div class="col-12 px-0">
                   <h5 class="text-danger my-3 font-weight-bold">* Please login with the convention registered email ID to register for Youth Banquet.</h5>
                   <h6>Note:</h6>
                   <ul class="mb-4 pl-4 text-violet">
                       <li>Ages 14+ will be attending Youth Banquet at Hyatt Regency(shuttle services will be available from convention center to Hyatt Regency and back) meeting time 5 pm on 7/2(Saturday), meeting location by the main registration desk.
                        </li>
                        <li>Ages between 7 and 13 will be attending the Kids Banquet at the convention center.</li>
                   </ul>
               </div>
            @endguest
            
               <div class="col-12 col-lg-12 shadow-small px-sm-20 p40 p-md-4 mb-4">
                  <form id='form' action="{{url('youth_banquet_registration')}}" method="post" enctype="multipart/form-data">
                     <div class="row">
                        @csrf
                        <!-- Personal Information -->
                        <div class="col-12">
                         
                          <div class="form-row">
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Convention Registration ID (Parent/Individual):</label><span class="text-red">*</span>
                                 <div>
                                   <input type="text"  name="convention_id" id="convention_id"  class="form-control" value="{{Auth::user()->registration_id??''}}" placeholder="Convention Registration" @if(!Auth::user()) readonly @endif/>
                                    
                                 </div>
                              </div>
                              
                               <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Whatsapp Number:</label>
                                 <div>
                                    <input type="text"  name="whatsapp" id="whatsapp" class="form-control" placeholder="Whatsapp" />
                                </div>
                                <div class="text-danger fs14">Phone Number Format : xxx-xxx-xxxx (or) xxxxxxxxxx</div>
                            </div>
                           </div>
                          
                           <div class="form-row">
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Parent/Guardian Phone:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text"  name="parent_phone" id="parent_phone" class="form-control" placeholder="Parent/Guardian Phone" />
                              </div>
                              <div class="text-danger fs14">Phone Number Format : xxx-xxx-xxxx (or) xxxxxxxxxx</div>
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Parent Guardian email:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="email"  name="parent_email" id="parent_email" class="form-control" placeholder="Parent Guardian email" />
                                 </div>
                              </div>
                              
                           </div>
                             <h5 class="text-violet py-2">Kids Information <a id="add_kid"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></h5>
                           <div id="kids_list">
                           <div class="form-row">
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>First Name:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text"  name="FirstName[]" id="FirstName"  class="form-control FirstName" placeholder="First Name" required/>
                                   
                                 </div>
                              </div>
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Last Name:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" name="LastName[]" id="LastName"  class="form-control LastName" placeholder="Last Name"   required/>
                                 </div>
                              </div>
                              
                           </div>
                            <div class="form-row">
                             <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Phone:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text"  name="phone[]" id="phone" class="form-control" placeholder="Phone" />
                                 </div>
                                 <div class="text-danger fs14">Phone Number Format : xxx-xxx-xxxx (or) xxxxxxxxxx</div>
                              </div>
                            <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Email:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="email"  name="email[]" id="email" class="form-control" placeholder="Email" />
                                 </div>
                              </div>
                              
                           </div>
                           
                               <div class="form-row">
                                  <div class="form-group col-12 col-md-6 col-lg-6">
                                     <label>Age:</label><span class="text-red">*</span>
                                     <div>
                                        
                                        <input type="number" name="age[]" class="form-control age" min="1" required>
                                     </div>
                                  </div>
                              </div>
                          </div>
                            <div class="form-row">

               <div class="col-12 px-0">
                   <div class="pb-4 pb-md-2">
                       <input type="checkbox" class="input-checkbox" required="" name="agrement">
                       <span class="pl25 fs16 pointer text-orange font-weight-bold" data-toggle="modal" data-target="#t_and_c">I accept Terms and Conditions</span>
                   </div>
            <br>
                   <ul class="mb-4 pl-4 text-violet">
                       <li>I hereby agree to and abide by all the rules and guidelines. I understand that the
                           Organizers are not responsible for any loss or physical injury that may occur to me on
                           account of my participation in the event. I am participating at my own risk and can have no
                           claim against NRIVA in any regard.</li>
                       <li>I hereby grant the NRIVA permission to use my photographs, video, or other digital media
                           (“Media”) in any and all of its publications, including web-based publications, without
                           payment or other consideration.</li>
                       <li>I hereby hold harmless, release, and forever discharge the NRIVA from all claims, demands,
                           and causes of action which I, my heirs, representatives, executors, administrators, or any
                           other persons acting on my behalf or on behalf of my estate have or may have by reason of
                           this authorization.</li>
                       <li>I understand and agree that all Media will become the property of the NRIVA and will not
                           be returned.</li>
                       <li>I hereby irrevocably authorize the NRIVA to edit, alter, copy, exhibit, publish, or distribute
                           these MEDIA for any purpose. In addition, I waive any right to inspect or approve the
                           finished product wherein my likeness appears. Additionally, I waive any right to royalties
                           or other compensation arising or related to the use of the photo.</li>
                   </ul>

               </div>


                                <div class="col-12 col-sm-6 col-md-12 my-2 my-md-auto">
                                    <div class="text-center text-sm-right">
                                        <button type="submit" disabled class="btn btn-lg btn-danger text-uppercase px-5 submit1">Submit</button>
                                    </div>
                                </div>
                            </div>

                           
                            
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>




@section('javascript')
<script src="https://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
<script>
    $(document).ready(function() {

        $('#add_kid').click(function(){
            var kids='<div class="append_kid"><div class="form-row"><div class="form-group col-12 col-md-6 col-lg-6"><label>FirstName:</label><span class="text-red">*</span><div><input type="text"  name="FirstName[]"   class="form-control FirstName" placeholder="FirstName" required/></div></div><div class="form-group col-12 col-md-6 col-lg-6"><label>LastName:</label><span class="text-red">*</span><div><input type="text" name="LastName[]"   class="form-control LastName" placeholder="LastName"   required/></div></div></div> <div class="form-row"><div class="form-group col-12 col-md-6 col-lg-6"><label>Phone:</label><span class="text-red">*</span><div><input type="text"  name="phone[]" id="phone" class="form-control" placeholder="Phone" /></div></div><div class="form-group col-12 col-md-6 col-lg-6"><label>Email:</label><span class="text-red">*</span><div><input type="email"  name="email[]" id="email" class="form-control" placeholder="Email" /></div></div></div><div class="form-row"><div class="form-group col-12 col-md-6 col-lg-6"><label>Age:</label><span class="text-red">*</span><div><input type="number"  name="age[]" class="form-control age" min="1" required></div></div><div class="form-group col-12 col-md-6 col-lg-6"><label></label><div><button class="remove_append_child btn btn-sm btn-danger">Remove</button></div></div></div></div>';
            $('#kids_list').append(kids);

        });
        $('body').on('click','.remove_append_child',function(){

            $(this).parents('.append_kid').remove();

        });

     $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        });
          $.validator.addMethod("numberss", function(value, element) {
            return this.optional(element) || value == value.match(/^[0-9) (-]+$/);
        });


        $("#form").validate({
           
            
            rules: {
                
                "FirstName[]": "required alpha",
                "LastName[]": "required alpha",
                
                "age[]": "required",
                convention_id: "required",
                parent_phone: {
                    required:true,
                    numberss:true
                },
                whatsapp: {
                    numberss:true
                },
                parent_email: "required",
                "email[]": "required",
                "phone[]" :{
                    maxlength: 14,
                    numberss:true,
                    required:true
                }
            },
             messages: {
               
                  "first_name[]": {
                    alpha: "First name should not contain numbers.",
                },
                 "last_name[]": {
                    alpha: "Last name should not contain numbers.",
                },
                 
                 "phone[]": {
                    maxlength: "Max length is  14.",
                    numberss: "Numbers only"
                },
                 "parent_phone": {
                    numberss: "Numbers only"
                },
                  "whatsapp": {
                    numberss: "Numbers only"
                }
            },
        });
   });

 
</script>

@endsection

@endsection