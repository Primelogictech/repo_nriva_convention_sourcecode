@extends('layouts.user.base')
@section('content')


<style type="text/css">
    .card {
        background-color: transparent;
        border: 0px solid;
    }
    .card-header {
        padding: 0px;
    }
    .card-header a {
        background: #784d98;
        color: #fff;
        padding: 0.75rem 1.25rem;
        width: 100%;
        display: block;
    }
    .p-radio-btn {
        position: absolute;
        top: 6px;
    }
     .error {
         color:red
    }
    #captcha{
        font-size:25px;
        padding-left: 27px
    }
    .dob-input {
        width: -webkit-fill-available !important;
        height: 36px;
         padding: 0.375rem 0.75rem; 
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
    }
    .card-header a::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/plus.png);
    }
    .card-header a[aria-expanded="true"]::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/minus.png);
    }
    .flower-list li {
        background: url(images/flower-icon1.png) no-repeat scroll left 3px;
    }
</style>

                @php
                    $count=0; 
                    $having_Shathamanam_Bhavathi_in_reg=true
                @endphp  
                               
                @if(Auth::user())
                    @if (Auth::user()->categorydetails)
                        @foreach(Auth::user()->categorydetails->benfits as $benfits)
                            @if($benfits->name=='Shathamanam Bhavathi Tickets')
                                @php
                                $count= explode(",",$benfits->pivot->count)[0];
                                $count=((int)$count[0]);
                                $count=$count/2
                                @endphp
                            @endif
                        @endforeach
                    @endif

                    @if(Auth::user()->registration_amount>0 && $count==0)
                        @php
                             $count=1;
                        @endphp
                        
                    @endif
                 
                    @if ($count==0)
                        @php
                            $having_Shathamanam_Bhavathi_in_reg=false
                        @endphp
                    @endif
                @else
                     @php
                        $count=3;
                     @endphp
                @endif

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/shathamanam-bhavathi.jpg" class="img-fluid w-100" alt="" />
                        </div>
                        <p class="text-center text-red pt25 mb-2">
                            Convention registration is closed, we reached our capacity. Thank you for your great support.
                        </p>
                        <p class="text-center text-red mb-0">
                            Please continue to watch for any further updates.
                        </p>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-12 py-5 my-5">
                        <h4 class="text-center text-skyblue mb-3">Coming Soon ...</h4>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <!-- <div class="col-12 col-md-5 col-lg-4">
                                <h5 class="text-violet">Contact For More Details</h5>
                                <h6 class="">Sreeram Kolisetty</h6>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">(617)-XXX-XXXX</span></div>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i><a href="#" class="icon-positions text-black text-decoration-none fs16 lh16">shathamanam2022@nriva.org</a></div>
                            </div>
                            <div class="col-12 col-md-7 col-lg-8"> -->
                            <div class="col-12">
                                <div class="text-center text-md-right">
                                    <a href="#shathamanam-bhavathi-registration" class="btn btn-danger mr-2 my-1 fs-xss-15"> SHATHAMANAM BHAVATHI REG.</a>
                                </div>
                            </div>
                        </div>
                        

                        <div class="row mt-3">
                            <div class="col-12" id="shathamanam-bhavathi-registration">
                                <h4 class="text-violet text-center mb-3">Shathamanam Bhavathi Registration</h4>
                                <h6 class="font-weight-bold text-center mb-3">Convention Registration is a must for any of the Event Registration.</h6>
                                <h6 class="font-weight-bold text-center ">Please make sure that you have registered for the Convention before you register for this Event.</h6>
                                @auth()
                                @if (!$having_Shathamanam_Bhavathi_in_reg)
                                    <h6 class="font-weight-bold text-center error">You Don't have Shathamanam Bhavathi included in your Registration.</h6>
                                @endif
                                @endauth

                                <div class="row">
                                <form action="" method="post" id="form">
                                @csrf
                                    <div class="col-12 col-md-12 col-lg-10 offset-lg-1">
                                        <div class="shadow-small px-sm-20 p30">
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Name:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="full_name" required class="form-control" placeholder="Name" value="{{  Auth::user()->first_name ?? '' }}" {{  Auth::user()!=null? 'readonly': ''}}/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Email:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="email" required name="email"  class="form-control" placeholder="Email"  value="{{  Auth::user()->email?? '' }}" {{  Auth::user()!=null? 'readonly': ''}}  />
                                                        </div>
                                                        Note: Please use your convention Registred email
                                                    </div>
                                                </div>

                                            

                      

                               @for ($i=1; $i<=$count; $i++)
                                   <hr class="dashed-hr" />
                                   <h6 class="text-violet my-3">Family {{$i}} Details</h6>
                                   <div class="form-row">
                                       <div class="form-group col-12 col-md-2 col-lg-2">
                                           <label>Title:</label>
                                           <div>
                                               <input type="text" name="" value="MR" readonly class="form-control" placeholder="Full Name" />
                                           </div>
                                       </div>

                                       <div class="form-group col-12 col-md-2 col-lg-2">
                                           <label>Full Name:</label>{!! $i==1? '</label><span class="text-red">*</span>' :"" !!}
                                           <div>
                                               <input type="text" {{ $i==1? 'required' :""}} name="father_or_in_laws_name[]" class="form-control" placeholder="Full Name" />
                                           </div>
                                       </div>
                                       <div class="form-group col-12 col-md-2 col-lg-2">
                                           <label>DOB:</label>{!! $i==1? '</label><span class="text-red">*</span>' :"" !!}
                                           <div class="border border-radius-5">
                                               <input type="text" {{ $i==1? 'required' :""}} name="father_In_law_dob[]" class="datepicker dob-input border-0" placeholder="DOB" />
                                           </div>
                                       </div>
                                       <div class="form-group col-12 col-md-2 col-lg-2">
                                           <label>Gothram:</label>{!! $i==1? '</label><span class="text-red">*</span>' :"" !!}
                                           <div>
                                               <input type="text" {{ $i==1? 'required' :""}} name="father_In_law_gothram[]" class="form-control" placeholder="Gothram" />
                                           </div>
                                       </div>
                                       <div class="form-group col-12 col-md-2 col-lg-2">
                                           <label>Naksthram:</label>{!! $i==1? '</label><span class="text-red">*</span>' :"" !!}
                                           <div>
                                               <input type="text" {{ $i==1? 'required' :""}} name="father_In_law_naksthram[]" class="form-control" placeholder="Naksthram" />
                                           </div>
                                       </div>
                                       <div class="form-group col-12 col-md-2 col-lg-2">
                                           <label>Rasi:</label>{!! $i==1? '</label><span class="text-red">*</span>' :"" !!}
                                           <div>
                                               <input type="text" {{ $i==1? 'required' :""}} name="father_In_law_rasi[]" class="form-control" placeholder="Rasi" />
                                           </div>
                                       </div>
                                   </div>

                                   <div class="row">

                                       <div class="col-12 mt-2">
                                           <div class="form-row">

                                               <div class="form-group col-12 col-md-2 col-lg-2">
                                                   <label>Title:</label>
                                                   <div>
                                                       <input type="text" name="" value="Mrs" readonly class="form-control" placeholder="Full Name" />
                                                   </div>
                                               </div>
                                               <div class="form-group col-12 col-md-2 col-lg-2">
                                                   <label>Full Name:</label>{!! $i==1? '</label><span class="text-red">*</span>' :"" !!}
                                                   <div>
                                                       <input type="text" {{ $i==1? 'required' :""}} name="mother_or_in_laws_name[]" class="form-control" placeholder="Mothers (In-Laws) Name" />
                                                   </div>
                                               </div>
                                               <div class="form-group col-12 col-md-2 col-lg-2">
                                                   <label>DOB:</label>{!! $i==1? '</label><span class="text-red">*</span>' :"" !!}
                                                   <div class="border border-radius-5">
                                                       <input type="text" {{ $i==1? 'required' :""}} name="mother_In_law_dob[]" class="datepicker dob-input border-0" placeholder="DOB" />
                                                   </div>
                                               </div>

                                               <div class="form-group col-12 col-md-2 col-lg-2">
                                                   <label>Gothram:</label>{!! $i==1? '</label><span class="text-red">*</span>' :"" !!}
                                                   <div>
                                                       <input type="text" {{ $i==1? 'required' :""}} name="mother_In_law_gothram[]" class="form-control" placeholder="Gothram" />
                                                   </div>
                                               </div>

                                               <div class="form-group col-12 col-md-2 col-lg-2">
                                                   <label>Naksthram:</label>{!! $i==1? '</label><span class="text-red">*</span>' :"" !!}
                                                   <div>
                                                       <input type="text" {{ $i==1? 'required' :""}} name="mother_In_law_naksthram[]" class="form-control" placeholder="Naksthram" />
                                                   </div>
                                               </div>
                                               <div class="form-group col-12 col-md-2 col-lg-2">
                                                   <label>Rasi:</label>{!! $i==1? '</label><span class="text-red">*</span>' :"" !!}
                                                   <div>
                                                       <input type="text" {{ $i==1? 'required' :""}} name="mother_In_law_rasi[]" class="form-control" placeholder="Rasi" />
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                              @endfor
                                 
                                   <hr class="dashed-hr" />
                                            <div class="row pt-3">
                                                <div class="col-12 col-md-7 col-lg-6 my-1 my-md-auto">
                                                    <h5 class="text-violet">Number of Participants from your family:</h5>
                                                    
                                                </div>
                                                <div class="col-12 col-md-5 col-lg-6 my-1 my-md-auto">
                                                    <div>
                                                        <input type="number" max="4" class="form-control" name="number_of_participants" />
                                                    </div>
                                                <div class="text-violet">Maximum Number of Participants allowed are 4</div>
                                                </div>
                                            </div>

                                            

                                            <div class="row mt-3">
                                                <div class="col-12">
                                                    <div class="border_violet-4 px-3 pb30 pt30 my-3 border-radius-5">
                                                        <div class="shathamanam-bhavathi-reg-terms-n-conditions">
                                                            <h5 class="text-violet mb-3">Terms & Conditions:</h5>
                                                            <p>
                                                                <strong>1.</strong> Maximum of 102 couples are allowed for the event and purely on first come first serve basis. Anything above 102 couples is at the discretion of event
                                                                organizers.
                                                            </p>
                                                            <p><strong>2.</strong> Maximum of 4 people are allowed to sit around the participating couples (excludes kids of age less than 10).</p>
                                                            <p><strong>3.</strong> Participants are expected to be physically able to perform the rituals without assistance from another person.</p>
                                                            <p>
                                                                <strong>4.</strong> Photographs and Videos taken during the program may be shared in social and electronic media and by accepting these terms and conditions you are providing
                                                                consent to share the photos and videos.
                                                            </p>
                                                            <p><strong>5.</strong> Participants are advised to keep any medication and water as required for their needs.</p>
                                                            <p><strong>6.</strong> These terms and conditions are subject to change as required to support the smooth functioning of the ceremony.</p>
                                                            <p><strong>7.</strong> Please speak to the convention committee and chairs of the program for any questions that you may have.</p>
                                                        </div>
                                                    </div>
                                                    <div class="py-1 position-relative">
                                                        <input type="checkbox" class="" required="" />
                                                        <span class="pl15 fs16">
                                                            By clicking this check box, you and participants are agreeing to the above terms and conditions. It is responsibility of the registering person to update about the terms and
                                                            conditions to their parents/in-laws or participants.
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mt-3">
                                                <div class="col-12 col-md-8 col-lg-7 pt-2">
                                                    <h5>Security Code</h5>
                                                     <div class="row">
                                                        <div class="col-11">
                                                            <div class="registration-captcha-block pr-3">
                                                                <div class="row">
                                                                    <div class="col-8 px-0 my-auto">
                                                                        <input type="text" id="captcha_number" class="border-0 form-control bg-transparent"  placeholder="Enter the Characters you see" />
                                                                    </div>
                                                                    <div class="col-4 px-0 my-auto pr5 ">
                                                                        {{-- <img src="images/ShowCaptchaImage.png" class="captcha-img float-md-right" alt="" /> --}}
                                                                        <span class="fs-20" id="captcha">8500817</span>
                                                                
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-1 px-1 my-auto">
                                                            <div class>
                                                            {{-- <span id="captcha">8500817</span> --}}
                                                                <img src="images/refresh.png" class="img-fluid ml-3 captcha_refresh" width="19" height="20" alt="">
                                                                {{-- <img src="images/refresh.png" class="img-fluid mx-auto d-block" width="15" height="16" alt="" /> --}}
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-4 col-lg-5 mt40">
                                                    <div class="text-center text-sm-right">
                                        
                                                        <button disabled class="btn btn-lg btn-danger text-uppercase px-5 submit_btn1">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 shadow-small py-0 pt-1 px-1 mt-4">
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="text-violet text-center mb-4">Shathamanam Bhavathi FAQ's</h5>
                                <div id="accordion" class="o-accordion">
                                    <ul class="list-unstyled">
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header">
                                                    <a class="card-link fs18" data-toggle="collapse" href="#sbcollapseOne">
                                                        What is the event ?
                                                    </a>
                                                </div>
                                                <div id="sbcollapseOne" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                           Shastabdhya Purti.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseTwo">
                                                        Venue of the event? 
                                                    </a>
                                                </div>
                                                <div id="sbcollapseTwo" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                           Schaumburg, IL, Convention Centre BallRoom-B. 
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseThree">
                                                        What is the  event ?
                                                    </a>
                                                </div>
                                                <div id="sbcollapseThree" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                          Date: 07/03/2022 Sunday.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseFour">
                                                        What time the event starts and how long it is going to be?          
                                                    </a>
                                                </div>
                                                <div id="sbcollapseFour" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                           The event is going to start at 12 o'clock noon and will go till 4pm approx. 
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseFive">
                                                        What age group is eligible for this event? 
                                                    </a>
                                                </div>
                                                <div id="sbcollapseFive" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Anyone who is 60+ & above, 70+, 80+ .
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseSix">
                                                       Who is eligible for the Sowbhagyam packet?
                                                    </a>
                                                </div>
                                                <div id="sbcollapseSix" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Only the parents who are registered for the Shatamanam Bhavathi will receive the sowbhagyam packet(Pasupu /Kumkum gift bag).
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseSeven">
                                                        Do we need to bring anything? 
                                                    </a>
                                                </div>
                                                <div id="sbcollapseSeven" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            You should dress up nicely as it's your special day and also come with your kids and family. We want you to take tons &  tons of happiness with you.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseEight">
                                                        Is there any particular clothing attire we should follow? 
                                                    </a>
                                                </div>
                                                <div id="sbcollapseEight" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Dress up traditionally, if possible wear Pattu saree and Panchi-vani (wear comfortable clothing ).
                                                        </p>
                                                        <h6 class="fs18">
                                                            Whom to communicate with?   
                                                        </h6>
                                                        <p>
                                                            Convention team will communicate the event timings and which room to come and what time exactly the program starts Keep watching for the whatsapp messages
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseNine">
                                                        Are we having any meetings prior to the event? 
                                                    </a>
                                                </div>
                                                <div id="sbcollapseNine" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Zoom meetings are going to be conducted prior to the event for the easy flow.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseTen">
                                                        Shathamanam Bhavathi timings and the events: 
                                                    </a>
                                                </div>
                                                <div id="sbcollapseTen" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <h6 class="mb-3">SHATHAMAANAM BHAVATHI </h6>
                                                        <ul class="flower-list fs16">
                                                            <li>12 noon : procession with parents </li>
                                                            <li>12:15 deeparadhana</li>
                                                            <li>12:30 anugna </li>
                                                            <li>12:45 vigneshwara puja</li>
                                                            <li>1:00 Sankalpam</li>
                                                            <li>1:15 punyahavaachanam </li>
                                                            <li>1:30 108 Kalasha stapanam </li>
                                                            <li>2:00 Sri Rudra japam</li>
                                                            <li>Ayushya japam</li>
                                                            <li>Maha mruthynjaya japam</li>
                                                            <li>Sahasra chandra japam</li>
                                                            <li>Sudarshana japam & puja Archana aarathi.</li>
                                                            <li>3:00 paada puja</li>
                                                            <li>3:15 mala dharana & Kumkuma dharana</li>
                                                            <li>3:30 aarathi</li>
                                                            <li>3:45 prasada viniyog </li>
                                                            <li>4:00 shanti mantra.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>



@section('javascript')
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>

<script>
    $(document).ready(function() {
 
         $(".datepicker").datepicker();
    });

            @auth()
                @if (!$having_Shathamanam_Bhavathi_in_reg)
                    $('.submit_btn').attr('disabled',true)
                @endif
            @endauth
     
    
    
        $('#form').on('submit', function(event){

        var enter_captha = $('#captcha_number').val();
        var captcha =$('#captcha').text();
        if(enter_captha != captcha){
           event.preventDefault();
           event.stopPropagation();
           alert("Captcha Invalid.");
        }
    });
    captcha();
    $('.captcha_refresh').on('click',function(){
        captcha();

    });
    function captcha() {
        var number = Math.floor(100000 + Math.random() * 900000);
        $('#captcha').text(number);
    }

</script>

@endsection


@endsection
