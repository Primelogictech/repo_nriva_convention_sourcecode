@extends('layouts.user.base')
@section('content')

<style type="text/css">
    th {
        border-color: #dadada !important;
    }
    
    .p-radio-btn {
        position: absolute;
        top: 6px !important;
    }
    input#discount_code{
        width: 91% !important;
    }
    #discount_msg{
        padding-top: 10px;
    }
    @media (max-width: 768px){
        input#discount_code{
            width: 100% !important;
        }
    }
</style>

@php
    $is_free=false;
   if(app('request')->input('type')=='free'){
       if(Auth::check()){
           if(Auth::user()->free_exbhit_registrations){
               $is_free=True;
           }
       }
   }

@endphp


<section class="container-fluid my-3">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/exhibits-registration.jpg" class="img-fluid w-100" alt="" />
                        </div>
                        <p class="text-center text-red pt25 mb-2">
                            Convention registration is closed, we reached our capacity. Thank you for your great support.
                        </p>
                        <p class="text-center text-red mb-0">
                            Please continue to watch for any further updates.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 col-md-5 col-lg-4">
                            </div>
                            <div class="col-12 col-md-7 col-lg-8 my-2 my-md-0">
                                <div class="text-md-right">
                                    <a href="#exhibit-registration" class="btn btn-danger btn-lg text-uppercase mr-2 my-1 fs-xs-15">Exhibit Registration</a>
                                    <a href="documents/2022-Chicago-Convention-Exhibit_Booths_Package.pdf" target="_blank" class="btn btn-danger btn-lg text-uppercase my-1 fs-xs-15">Exhibits Package Details</a>
                                </div>
                            </div>
                            
                            
                            <div class="col-12 mt-5" id="exhibit-registration">
                                <h4 class="text-violet text-center mb-3">Exhibits Registration</h4>
                                @auth
                                 @if((app('request')->input('type')!='free') && (Auth::user()->free_exbhit_registrations > 0)  )
                                 <div>
                                    <p style="color:red">
                                    <b>Note: You have free vendor booth registration included in your donor package. Please click <a href="{{url('exhibits-reservation')}}?type=free" > here </a>
                                    to avail it </b></p>
                                 </div>
                                 @endif
                                    
                                @endauth
                                <div class="row px-3 px-lg-0">
                                    <div class="col-12 col-md-12 col-lg-12 shadow-small p-3 py-md-5 px-md-4 my-3">
                                        <form action="{{url('exhibits-reservation')}}" method="post" id="form" enctype="multipart/form-data">
                                                @csrf
                          
                           
                                            <div class="form-row is_free_field">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Full Name:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="full_name" 
                                                        @auth
                                                           value="{{Auth::user()->first_name??''}} {{Auth::user()->late_name??''}}" 
                                                        @endauth
                                                          class="form-control " placeholder="Full Name" />

                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Company Name:</label>
                                                    <div>
                                                        <input type="text" name="company_name"  class="form-control" placeholder="Company Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row is_free_field">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Category:</label>
                                                    <div>
                                                        <input type="text" name="category" class="form-control" placeholder="Category" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Tax ID:</label>
                                                    <div>
                                                        <input type="text" name="tax_ID" class="form-control" placeholder="Tax ID" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row is_free_field">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Mailing Address:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="mailing_address"  class="form-control" placeholder="Mailing Address" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Phone No:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="phone_no"  class="form-control numberss" placeholder="Phone No" value="{{  Auth::user()->mobile ?? '' }}"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row is_free_field">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Email Id:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="email" name="email" id="email" class="form-control" placeholder="Email Id" value="{{  Auth::user()->email ?? '' }}" @if(Auth::user()) readonly @endif/>

                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Fax: </label>
                                                    <div>
                                                        <input type="text" name="fax"  class="form-control" placeholder="Fax" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row is_free_field">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Name of your Website:</label>
                                                    <div>
                                                        <input type="text" name="website_url" id="website_url" class="form-control" placeholder="Name of your Website" />

                                                    </div>
                                                </div>
                                               
                                            </div>
                                             
                                             <div class="col-12 px-0 mt-4">
                                                <h5 class="text-violet text-center">Exhibit Booths Pricing</h5>
                               
                                        <div class="table-responsive">
                                        <table class="table-bordered table table-bordered table table-hover table-center mb-0">
                                            <tr>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center">S.No</th>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center"><strong>Exhibitor Type</strong></th>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center"><strong>Size</strong></th>
                                                <th align="center" class="bg-violet text-white text-center"><strong> Price Before</strong></th>
                                                <th align="center" class="bg-violet text-white text-center"><strong>Price After</strong></th>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center"><strong>Count</strong></th>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center"><strong>Amount</strong></th>
                                            </tr>
                                             <tr>
                                                <th align="center" class="bg-violet text-white text-center">
                                                    <strong>03/20/2022</strong>
                                                </th>
                                                <th align="center" class="bg-violet text-white text-center">
                                                    <strong>03/20/2022</strong>
                                                </th>
                                            </tr>
                                            

                                            <tbody>

                                                @foreach ($exhibitsTypes as $exhibitsType )


                                                @foreach($exhibitsType->size_price as $size_price)
                                                <tr>
                                                    <td align="center">{{ $loop->parent->iteration }}</td>
                                                    <td 
                                                    >{{ $exhibitsType->name }}</td>
                                                    <td  >{{ $size_price['size'] }}</td>
                                                    <td  >
                                                         $ {{number_format($size_price['price_before'])}}<span style="display:none" id="before_price_{{ $exhibitsType->id??'' }}">{{ $size_price['price_before']}}</span>  
                                                    </td>
                                                    <td>
                                                    <span class="price_lable_{{$size_price['size']=='10’ x 10’'? '10X10':'10X20' }}"  >$ {{ number_format($size_price['price_after'])}}</span>
                                                    <span style="display:none"  class="price_input_{{$size_price['size']=='10’ x 10’'? '10X10':'10X20' }}" id="after_price_{{ $exhibitsType->id??'' }}">{{ $size_price['price_after'] }}</span> 
                                                    </td>
                                                    <td style="min-width: 120px;">
                                                            
                                                                <input type="text" class="form-control count"  id="count_{{ $exhibitsType->id??'' }}" name="count[{{$exhibitsType->id}}]"  
                                                                @if($is_free)
                                                                    {{ ($size_price['size']=='10’ x 10’')? '':"readonly"  }}  
                                                                @endif
                                                                
                                                                 />
                                                            </td>
                                                            <td> $ <span id="toshow_amount_{{ $exhibitsType->id }}">0</span> <span style="display:none" id="amount_{{ $exhibitsType->id }}" class='exhibitsType_registration_amount'>0</span> </td>
                                                </tr>

                                                @endforeach

                                                @endforeach

                                                <tr>
                                                    <td colspan="5" class="text-right"> <b> Amount :</b></td>
                                                    <td colspan="2"> $<span id="total_amount">0</span></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                      
                                         <div  class="error-if-amount-is-zero error" > </div>
                                         <div  class="fs-25 error-if-vonder-booth-space-limit error" > </div>
                                    </div>
                                   
                                
                            </div>
                              <div class="row pb-2 pt-3">
                                        <div class="col-12 px-3 px-md-0">
                                            <div class="row">
                                                <div class="form-group col-6 col-md-8 col-lg-8 my-auto mt-sm-2">
                                                    <div class="text-left text-md-right font-weight-bold fs15">
                                                Discount Code :
                                            </div>
                                        </div>
                                                <div class="form-group col-6 col-md-4 col-lg-4 px11 my-auto mb-0" >
                                                 <div>
                                                    <input type="text" class="form-control" id="discount_code" name="discount_code">
                                                 <div id="discount_msg" ></div>
                                                    <input type="hidden" class="input-checkbox" id="discount_amount" name="discount_amount">
                                                    <input type="hidden" class="input-checkbox" id="discount_type" name="discount_type">
                                                    <input type="hidden" class="input-checkbox" id="discount_cal_amount" name="discount_cal_amount">
                                                 </div>
                                                 </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="row py-2">
                                        <div class="col-12 px-3 px-md-0">
                                            <div class="row">
                                                <div class="form-group col-6 col-md-8 my-auto">
                                                    <div class="text-left text-md-right font-weight-bold fs15">
                                                Discount Amount :
                                            </div>
                                        </div>
                                                <div class="form-group col-6 col-md-3 my-auto" >
                                                 $<span class="discount_value">0</div>
                                                 </span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="row py-2">
                                        <div class="col-12 px-3 px-md-0">
                                            <div class="row">
                                                <div class="form-group col-6 col-md-8 my-auto">
                                                    <div class="text-left text-md-right font-weight-bold fs15">
                                                Total Amount :
                                            </div>
                                        </div>
                                                <div class="form-group col-6 col-md-3 my-auto" >
                                                 $<span class="total_amount_with_discount">0</div>
                                                 </span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                            @include('partalles.paymentForm')

                                    <div class="col-12 col-sm-6 col-md-6 my-auto">
                                    <div class="pb-4 pb-md-2">
                                        <input type="checkbox" class="input-checkbox " required name="agrement">
                                        <span class="pl25 fs16 pointer text-orange font-weight-bold"  data-toggle="modal" data-target="#t_and_c" >I accept Terms and Conditions</span>
                                    </div>
                                </div>

                                           
                                            <div class="col-12">
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6 my-auto" style="display: none;">
                                                        <div>
                                                            <label>Amount </label><span class="text-red">*</span>
                                                        </div>
                                                        <div class="d-flex">
                                                            <span class="pt8">$&nbsp;&nbsp;</span>
                                                            <input type="number" readonly name="amount" id="amount" class="form-control" placeholder="Amount" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-12 col-lg-12 my-auto">
                                                        <div class="mt20 text-right">
                                                            <input type="submit" class="btn btn-lg btn-danger text-uppercase px-5 submit-button1" disabled id="submit_btn1" value="Check out">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </form>
                                    </div>
                                </div>
                            </div>
                           {{--  <div class="col-12 mt-3">
                                <div>
                                    <img src="images/Exhibits-Flyer.jpeg" class="img-fluid mx-auto d-block" alt="" />
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


  <div class="modal fade" id="t_and_c" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Terms and Conditions</h4>
        </div>

        <div class="modal-body">
            <div class="">
                <ul>
                    <li>Each exhibitor will get two admissions for the conference, does not include accommodation.</li>
                    <li>Each additional admission per person is $50.00.</li>
                    <li>Allocation is on first come first serve basis. Booths will be allotted upon payment only. Space is not guaranteed until payment is received</li>
                    <li>Exhibitors committee has right to refuse booth to anybody</li>
                    <li>Any form of Food should not be sold within the vendor booths</li>
                    <li>Neither NRIVA nor the convention center shall be responsible for loss or damage occurring to the exhibits from any cause. No Insurance will be provided by NRIVA or Conference Center. If insurance is desired the exhibitor must obtain it.</li>
                    <li>The NRIVA 2022 organizers and other participating organization officers and staff members disclaim all liability for damages or losses caused to any exhibitor by an act of god, or by fire, water, flood, windstorm, utility failures, and rodents, acts of vandalism, insurrection, civil disorder strikes, criminal act or theft. NRIVA 2022 organizers will not be responsible for any failure for electric or other services.</li>
                    <li>The NRIVA 2022 organizers and other participating organization officers and staff members
are not responsible for any immigration or customs issues</li>
                    <li>All matters related to disputes will be settled through negotiation. If there are any legal disputes, the jurisdiction of the court will be in Schaumburg, IL, USA.
</li>
                    <li>No nails or screws may be driven into the floor. No damage of any nature may be done to the booth structure or to any part of the exhibit hall. Exhibitor warrants against structural damages, shall be held responsible for damage to individual exhibit area, and agrees to indemnify Conference organizers for any such damage.</li>
                    <li>No open flames are allowed. All exhibitors must adhere to the local fire department regulations.</li>
                    <li>All wiring must meet appropriate specifications. Each exhibitor is responsible for the knowledge and compliance of all laws, ordinances and regulations pertaining to health, fire prevention and public safety while participating in the convention.
</li>
                    <li>Exhibitor is responsible for obtaining the necessary business license and collecting local and state sales taxes during the convention.</li>
                    <li>Each Exhibitor should provide a copy of business license during registration of exhibit booth</li>
                    <li>Exhibitor signing this agreement will be bound by all the terms and conditions mentioned in this agreement</li>
                    <li>Exhibits committee has right on final allocations of booths and changing the layout at any point of time.</li>
                    <li>Every Booth is provided with few standard items for use. Please refer to ‘What is included’ section on Exhibit Booths page of our Website.</li>
                    <li>All Optional Accessories mentioned on our Website available for ADDITIONAL COST.</li>
                </ul>
            </div>
        </div>
         <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
        
      </div>
      
    </div>
  </div>


@section('javascript')
<script>
        $('.free_registration').hide()
        @if($is_free)
        $('.payment_types_div').hide()
        $('.free_registration').show()
        $('#payment_Paypal').attr('disabled',true)
        $('#payment_Check').attr('disabled',true)
        $('#payment_Zelle').attr('disabled',true)
        $('#payment_Other').attr('disabled',true)
        $('#payment_free').trigger("click");
        $('.is_free_field').hide();
        @endif
    $(document).ready(function() {


        $('.submit-button').click(function(e){
            if($('#total_amount').text()==0){
                $('.error-if-amount-is-zero').text('Please select any of the above packages')
                
                e.preventDefault()
            }else{
                
                $('.error-if-amount-is-zero').empty();
                $s('.error-if-amount-is-zero').text(' ');
                return true;
            }
        });
        exhibitsTypes = JSON.parse('{!! $exhibitsTypes !!}')
        var count;
        var sum;
        function isGreaterThenTodatDate(date) {
            today = new Date("{{date('m/d/Y')}}")
            date = new Date(date)
            if (today >= date) {
                return false
            } else {
                return true
            }
        }
         $(".count").keyup(function() { 
            
            sum =0;
            id = $(this).attr('id').split("_")

            count = $(this).val()
            countkeyup(id,count)
        });
        function countkeyup(id,count) { 
            sum =0; 
            for (let i = 0; i < exhibitsTypes.length; i++) { 
                if (exhibitsTypes[i].id == id[1]) {
                    if (isGreaterThenTodatDate(exhibitsTypes[i].size_price[0].till_date)) {
                            amount=$('#before_price_' + id[1]).text() * count
                            $('#amount_' + id[1]).text(amount)
                            $('#toshow_amount_' + id[1]).text(( amount ).toLocaleString())
                    } else {
                            amount=$('#after_price_' + id[1]).text() * count
                            $('#amount_' + id[1]).text(amount)
                            $('#toshow_amount_' + id[1]).text((amount).toLocaleString())
                    }
                }
            }

            $('.exhibitsType_registration_amount').each(function() { 
                        
                sum = parseInt(sum) + parseInt($(this).text())
                //console.log(sum.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'))
            });
            $('#submit_btn').prop('disabled', true);
          
               var total_amounts= parseInt(sum);
                $('#amount').val(total_amounts);
                $('#total_amount').text(total_amounts.toLocaleString());
                $('.submit-button').prop('disabled', false);

                var items_count = 0;
           $('.count').each(function() {
               if($(this).val()==''){
                   val=0
               }else{
                   val=$(this).val()
               }
                items_count=items_count+parseInt(val);
             });


            @if($is_free)
                    $('.error-if-vonder-booth-space-limit').text('')
                vendor_booth_space_count=parseInt('{{Auth::user()->vendor_booth_space_count}}')
                if(vendor_booth_space_count<items_count){
                    $('#submit_btn').prop('disabled', true);
                    $('.error-if-vonder-booth-space-limit').text('You have Only ' +vendor_booth_space_count +' vendor booth spaces included in your Sponsorship benefits')
                }
            @endif
            discount_calculate();
        }

        function isGreaterThenTodatDate(date) {
            today = new Date("{{date('m/d/Y')}}")
            date = new Date(date)
            if (today >= date) {
                return false
            } else {
                return true
            }
        }

        $('#booth_type').change(function() {
            booth_type = $('#booth_type').val()
            price = 0;
            for (let i = 0; i < exhibitsTypes.length; i++) {
                if (exhibitsTypes[i].id == booth_type) {
                    data = exhibitsTypes[i].size_price
                    data.forEach(element => {
                        if (isGreaterThenTodatDate(element.till_date)) {
                            price = price + parseInt(element.price_before)
                        } else {
                            price = price + parseInt(element.price_after)
                        }
                    });
                }
            }
            $('#amount').val(price)
        })

           $.validator.addMethod("numberss", function(value, element) {
            return this.optional(element) || value == value.match(/^[0-9) (-]+$/);
        });

          jQuery.validator.addClassRules('count', {
              digits: true,
              min: 1
         });

        $("#form").validate({
            rules: {
                full_name: "required",
                mailing_address: "required",
                email: "required",
                amount: "required",
                payment_type: {
                    required: true
                },
                 phone_no :{
                    maxlength: 14,
                    numberss:true,
                    required:true,
                },
            },
             messages: {
                 "phone_no": {
                    maxlength: "Max length is  14.",
                    numberss: "Numbers only"
                }
            },
        });
    });


function forDiscountApply(){
        change_discount_amount=false
        $('.price_lable_10X20').text('$ 1,000')
        $('.price_lable_10X10').text('$ 500')
        $('.price_input_10X20').text('1000')
        $('.price_input_10X10').text('500')
        $('#discount_code').attr('readonly','readonly')
        $('#discount_msg').html('<span style="color:green;">Voucher Applied <i class="fab fa-check-circle-o" aria-hidden="true"></i></span><p class="mb-0">click <a href="/exhibits-reservation" >here</a> to remove Discount code</p>');
}
var change_discount_amount=true
$('body').on('keyup','#discount_code',function(){
    if($(this).val() !=""){
            if($(this).val()=='NRIVA-2022'){
                forDiscountApply()
                $(".count").keyup()
            }else{
                checkDiscountCode(this)
            }
    }else{
        $('#discount_cal_amount').val('0');
        $('#discount_amount').val(0);
        $('#discount_type').val(0);
        $('#total_amount_with_discount').text(parseInt($('#total_amount').val()));
        $('#discount_msg').html('');
    }
});

function checkDiscountCode(this_val){
    $.ajax({
        type: 'GET',
        url: "{{url('voucher_verification')}}?name="+$(this_val).val(),
        dataType: 'json',
        success:function(data){
        if(data !="failed"){
            console.log(data.discount);
            $('#discount_amount').val(data.discount);
            $('#discount_type').val(data.discount_type);
            discount_calculate();
            $('#discount_msg').html('<span style="color:green;">Voucher Applied <i class="fab fa-check-circle-o" aria-hidden="true"></i></span>');
            
            }else{
                $('#discount_amount').val(0);
                $('#discount_type').val(0);
                 discount_calculate();
                 if(change_discount_amount){
                    $('#discount_msg').html('<span style="color:red">Invalid Voucher <i class="fab fa-times-circle-o" aria-hidden="true"></i></span>');
                 }

                
            }
        }
    }); 
}

function discount_calculate(){
    var discount_per = $('#discount_amount').val();

    
    var regi_amount = parseInt($('#total_amount').text().replace(',',''));
    if(parseInt($('#discount_type').val())==1){
    var dis_amount = (parseInt(regi_amount)/100)*parseInt(discount_per);
    } else if(parseInt($('#discount_type').val())==2){
        var dis_amount =discount_per;
    }else{
        var dis_amount =0;
    }
    
    $('.discount_value').text(dis_amount.toLocaleString());
    $('#discount_cal_amount').val(dis_amount);
    $('.total_amount_with_discount').text((regi_amount-dis_amount).toLocaleString());
    
}
</script>

@endsection
@endsection
