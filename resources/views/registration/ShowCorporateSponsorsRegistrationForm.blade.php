@extends('layouts.user.base')
@section('content')

<style type="text/css">
    th {
        border-color: #dadada !important;
    }
    
    .p-radio-btn {
        position: absolute;
        top: 6px !important;
    }
</style>

@php
    $is_free=false;
   if(app('request')->input('type')=='free'){
       if(Auth::check()){
           if(Auth::user()->free_exbhit_registrations){
               $is_free=True;
           }
       }
   }

@endphp


<section class="container-fluid my-3">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/corporate-sponsors-registration.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 col-md-5 col-lg-4">
                            </div>
                            <div class="col-12 col-md-12 col-lg-12 my-2 my-md-0">
                                <div class="text-md-right">
 {{--                                      <a href="documents/Omni Express Corporate Sponsorship.pdf" target="_blank" class="btn btn-danger btn-lg text-uppercase my-1 fs-xs-15">Corporate Sponsors Details</a>
                                    <a href="#exhibit-registration" class="btn btn-danger btn-lg text-uppercase mr-2 my-1 fs-xs-15">Corporate Sponsors Registration</a>
 --}}
                                  {{--   <a href="#Corporate-Sponsors-Registration" class="btn btn-danger btn-lg text-uppercase mr-2 my-1 fs-xs-15">Corporate Sponsors Registration</a> --}}
                                    <a href="documents/Omni Express Corporate Sponsorship.pdf" target="_blank" class="btn btn-danger btn-lg text-uppercase my-1 fs-xs-15">Corporate Sponsorship Package Details</a>
                                </div>
                            </div>
                            
                            
                            <div class="col-12 mt-5" id="Corporate-Sponsors-Registration">
                                <h4 class="text-violet text-center mb-3">Corporate Sponsors Registration</h4>
                              
                                <div class="row px-3 px-lg-0">
                                    <div class="col-12 col-md-12 col-lg-12 shadow-small p-3 py-md-5 px-md-4 my-3">
                                        <form action="{{url('corporate_sponsors_registration')}}" method="post" id="form" enctype="multipart/form-data">
                                                @csrf
                                            <div class="form-row is_free_field">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Full Name:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="full_name" 
                                                        @auth
                                                           value="{{Auth::user()->first_name??''}} {{Auth::user()->late_name??''}}" 
                                                        @endauth
                                                          class="form-control " placeholder="Full Name" />

                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Company Name:</label>
                                                    <div>
                                                        <input type="text" name="company_name"  class="form-control" placeholder="Company Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- <div class="form-row is_free_field">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Category:</label>
                                                    <div>
                                                        <input type="text" name="category" class="form-control" placeholder="Category" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Tax ID:</label>
                                                    <div>
                                                        <input type="text" name="tax_ID" class="form-control" placeholder="Tax ID" />
                                                    </div>
                                                </div>
                                            </div> --}}
                                            <div class="form-row is_free_field">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Mailing Address:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="mailing_address"  class="form-control" placeholder="Mailing Address" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Phone No:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="phone_no"  class="form-control numberss" placeholder="Phone No" value="{{  Auth::user()->mobile ?? '' }}"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row is_free_field">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Email Id:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="email" name="email" id="email" class="form-control" placeholder="Email Id" value="{{  Auth::user()->email ?? '' }}" @if(Auth::user()) readonly @endif/>

                                                    </div>
                                                </div>
                                                {{-- <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Fax: </label>
                                                    <div>
                                                        <input type="text" name="fax"  class="form-control" placeholder="Fax" />
                                                    </div>
                                                </div> --}}
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Name of your Website:</label>
                                                    <div>
                                                        <input type="text" name="website_url" id="website_url" class="form-control" placeholder="Name of your Website" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row is_free_field">

                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Image:</label>
                                                    <div>
                                                        <input type="file" name="image_url" onchange="ValidateFile(this)"  class="form-control" placeholder="Name of your Website" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Logo:</label>
                                                    <div>
                                                        <input type="file" name="logo_url" onchange="ValidateFile(this)" class="form-control" placeholder="Name of your Website" />
                                                    </div>
                                                </div>
                                            </div>

                                             <div class="form-row is_free_field">
                                               
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Video link:</label>
                                                    <div>
                                                        <input type="text" name="video_link"  class="form-control" placeholder="Video link" />
                                                    </div>
                                                </div>

                                                 <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Discription:</label>
                                                    <div>
                                                    <textarea  name="extra_discriptioon"  class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                             

                                             <div class="col-12 px-0 mt-4">
                                                <h5 class="text-violet text-center">Corporate Sponsors Type</h5>
                               
                                        <div class="table-responsive">
                                        <table class="table-bordered table table-bordered table table-hover table-center mb-0">
                                            <tr>
                                                <th align="center"  class="bg-violet text-white text-center">S.No</th>
                                                <th align="center"  class="bg-violet text-white text-center"><strong> Sponsors Type</strong></th>
                                                <th align="center"  class="bg-violet text-white text-center"><strong>Benefits</strong></th>
                                                <th align="center"  class="bg-violet text-white text-center"><strong>Amount</strong></th>
                                            </tr>
                                            

                                            <tbody>

                                                @foreach ($CorporateSponsortypes as $CorporateSponsortype )

                                                <tr>
                                                    <td>
                                                        <input type="radio" class="corporate_sponsor_type" name="corporate_sponsor_type"  data-amount={{$CorporateSponsortype->amount}}  value={{$CorporateSponsortype->id}}>
                                                    </td>
                                                    <td>{{$CorporateSponsortype->name}}</td>
                                                    <td>{!!$CorporateSponsortype->benfits!!}</td>
                                                    <td>$ {{$CorporateSponsortype->amount}}</td>
                                                </tr>




                                                @endforeach
                                                <tr>
                                                <td colspan=3>
                                                    Amount To Be paid            
                                                </td>
                                                <td>
                                                  <span id="total_amount"></span>
                                                </td>

                                                </tr>

                                            </tbody>
                                        </table>
                                         <div  class="error-if-amount-is-zero error" > </div>
                                         <div  class="fs-25 error-if-vonder-booth-space-limit error" > </div>
                                    </div>
                                  
                            </div>
                                            @include('partalles.paymentForm')

                                    <div class="col-12 col-sm-6 col-md-6 my-auto">
                                    <div class="pb-4 pb-md-2">
                                        <input type="checkbox" class="input-checkbox" required name="agrement">
                                        <span class="pl25 fs16 pointer text-orange font-weight-bold"  data-toggle="modal" data-target="#t_and_c" >I accept Terms and Conditions</span>
                                    </div>
                                </div>

                                           
                                            <div class="col-12">
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6 my-auto" style="display: none;">
                                                        <div>
                                                            <label>Amount </label><span class="text-red">*</span>
                                                        </div>
                                                        <div class="d-flex">
                                                            <span class="pt8">$&nbsp;&nbsp;</span>
                                                            <input type="number" readonly name="amount" id="amount" class="form-control" placeholder="Amount" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-12 col-lg-12 my-auto">
                                                        <div class="mt20 text-right">
                                                            <input type="submit" disabled class="btn btn-lg btn-danger text-uppercase px-5 submit-button1" id="submit_btn1" value="Check out">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </form>
                                    </div>
                                </div>
                            </div>
                           {{--  <div class="col-12 mt-3">
                                <div>
                                    <img src="images/Exhibits-Flyer.jpeg" class="img-fluid mx-auto d-block" alt="" />
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


  <div class="modal fade" id="t_and_c" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Terms and Conditions</h4>
        </div>

        <div class="modal-body">
            <div class="">
                <ul>
                    <li>Each exhibitor will get two admissions for the conference, does not include accommodation.</li>
                    <li>All Optional Accessories mentioned on our Website available for ADDITIONAL COST.</li>
                </ul>
            </div>
        </div>
         <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
        
      </div>
      
    </div>
  </div>


@section('javascript')
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
<script>
    $('#payment_Paypal').trigger("click");
    
         var _validFileExtensions = [".jpg", ".jpeg",".png"]; 

        function ValidateFile(oInput) {
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }
                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }
    
    $(document).ready(function() {
        $('.submit-button').click(function(e){  
            if($('#total_amount').text()==0){
                $('.error-if-amount-is-zero').text('Please select any of the above packages')
                e.preventDefault()
            }else{
                $('.error-if-amount-is-zero').empty();
                $('.error-if-amount-is-zero').text(' ');
            }
        });
        
        $('.corporate_sponsor_type').click(function(){
            $('#total_amount').text('$'+ $(this).data('amount'))
        })
           $.validator.addMethod("numberss", function(value, element) {
            return this.optional(element) || value == value.match(/^[0-9) (-]+$/);
        });

         $.validator.addMethod('filesize', function (value, element, arg) {
                if(element.files[0]!=undefined){
                    if(element.files[0].size<=arg){ 
                        return true; 
                    }else { 
                        return false; 
                    } 
                }else{
                    return true; 
                }
         }); 

        $("#form").validate({
            rules: {
                'image_url': { 
                    filesize : 5242880,
                },
                 'logo_url': { 
                    filesize : 5242880,
                },
                full_name: "required",
                mailing_address: "required",
                email: "required",
                amount: "required",
                payment_type: {
                    required: true
                },
                 phone_no :{
                    maxlength: 14,
                    numberss:true,
                    required:true,
                },
            },
             messages: {
                 "phone_no": {
                    maxlength: "Max length is  14.",
                    numberss: "Numbers only"
                },
                 "logo_url": {
                    filesize: "max allowed Size is 5mb",
                },
                 "image_url": {
                    filesize: "max allowed Size is 5mb",
                }
            },
        });
    });
</script>

@endsection


@endsection
