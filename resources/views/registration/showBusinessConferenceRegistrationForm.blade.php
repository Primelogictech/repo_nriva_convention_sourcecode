@extends('layouts.user.base')
@section('content')

<style type="text/css">
    .card {
        background-color: transparent;
        border: 0px solid;
    }
    .card-header {
        padding: 0px;
    }
    .card-header a {
        background: #784d98;
        color: #fff;
        padding: 0.75rem 1.25rem;
        width: 100%;
        display: block;
    }
    .p-radio-btn {
        position: absolute;
        top: 6px;
    }
     .error {
         color:red
    }
    #captcha{
        font-size:25px;
        padding-left: 27px
    }
</style>


<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/souvenir-ads.jpg" class="img-fluid w-100" alt="">
                        </div>
                        <p class="text-center text-red pt25 mb-2">
                            Convention registration is closed, we reached our capacity. Thank you for your great support.
                        </p>
                        <p class="text-center text-red mb-0">
                            Please continue to watch for any further updates.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                
                                <div class="row">
                                    <div class="col-12 col-lg-10 offset-lg-1">

                                        @foreach ($errors->all() as $error)
                                    <div class="error">{{ $error }}</div>

                                    @endforeach
                                    @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @endif
                                       
                                <div class="shadow-small py-5 px-3 px-md-4">
                                    
                                        <iframe src="https://docs.google.com/forms/d/1-JlP9PWdJJKC0SvA2uoa4ULwDz65BARh4yl4OiotXo4/viewform?embedded=true" width="850" height="1322" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe> 

                                </div>
                                           
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@section('javascript')


@endsection


@endsection
