@extends('layouts.user.base')
@section('content')


<section class="container-fluid my-3 my-lg-5">
    
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">

       

        <div class="row">
            <div class="col-12">
                <h5>My Exhibit Registrations</h5>

                <div class="text-md-right">
                      <a href="{{url('myaccount')}}" class="btn btn-danger btn-lg text-uppercase mr-2 my-1 fs-xs-15">Back To Dashboard</a>
                    </div>
            </div>
            <div class="col-12">
                <div class="card-body px-0">
                    <div class="table-responsive">
                       
                        <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                            <th>Sl NO.</th>
                            <th>Full Name</th>
                            <th>Email Id</th>
                            
                            <th>Category</th>
                            <th>Tax_ID</th>
                            <th>Mailing Address</th>
                            <th>Phone No</th>
                            <th>Fax</th>
                            <th>Name of your Website</th>
                            <th>Exhibitor Type</th>
                            <th>Amount</th>
                            <th>Payment Method</th>
                            <th>Transaction Id</th>
                            <th>Payment Status</th>
                            
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($list as $registration)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $registration->full_name }}</td>
                                <td>{{ $registration->email }}</td>
                                
                                <td>{{ $registration->category }}</td>
                                <td>{{ $registration->tax_ID }}</td>
                                <td>{{ $registration->mailing_address }}</td>
                                <td>{{ $registration->phone_no }}</td>
                                <td>{{ $registration->fax }}</td>
                                <td>{{ $registration->website_url }}</td>
                                 <td><?php 
                                $booths= json_decode($registration->booth_type);
                                
                                ?>
                                @if(is_object($booths))
                                    @foreach(json_decode($registration->booth_type) as $key => $value)
                                        @if($value > 0)
                                        <?php $Individual = 
                                        \App\Models\Admin\ExhibitorType::where("id",$key)->first();
                                        echo $Individual->name." - ".$value;
                                        echo ",</br>";

                                         ?>
                                         @endif
                                        @endforeach
                                        @endif
                                        
                                    </td> 
                                    <td>
                                    @if($registration->paymentdata) 
                                    Total Amount : $ {{ number_format($registration->paymentdata->payment_amount)??'' }} <br>
                                    Discount Amount : $ {{ number_format($registration->paymentdata->discount_amount)??'' }}  <br>
                                    Discount Code :  {{$registration->paymentdata->discount_code??'' }}  <br>
                                    Paid Amount : $ {{ number_format($registration->paymentdata->paid_amount)??'' }}  <br>
                                @endif</td>
                                <td>
                                    @if($registration->paymentdata)
                                        @if($registration->paymentdata->payment_methord==1)
                                        Paypal
                                        @elseif($registration->paymentdata->payment_methord==2)
                                        Check
                                        @elseif($registration->paymentdata->payment_methord==3)
                                        Zelle
                                        @elseif($registration->paymentdata->payment_methord==4)
                                        Other
                                        @endif
                                    @endif


                                </td>
                                 <td>{{ $registration->paymentdata->unique_id_for_payment??'' }}

                                     @if(isset($registration->paymentdata->more_info['document']))
                                            @if(!empty($registration->paymentdata->more_info['document']))
                                            <br>
                                            <a href="{{asset('public/storage/user/'.$registration->paymentdata->more_info['document'])}}" download>Download Document</a>
                                            @endif
                                        @endif
                                 </td>  
                                     <td>
                               {{$registration->paymentdata->payment_status??''}}
                                </td>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                        
                </div>
            </div>
        </div>
    </div>
</section>



@section('javascript')



@endsection


@endsection
