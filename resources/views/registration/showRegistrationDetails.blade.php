@extends('layouts.user.base')
@section('content')


<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12">
                <h5>{{ $Registration->registration_type_name }} Registration Details</h5>
                <a href="{{route('myaccount')}}" data-toggle="tooltip" title="" class="float-right-back-btn btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
            <div class="col-12">
                <div class="card-body px-0">
                    <div class="table-responsive">
                        @if($Registration->registration_type_name=='Exhibit')
                                <table class="datatable table-bordered table table-hover table-center mb-0">
                                    <thead>
                                        <tr>
                                            <th>Company Name</th>
                                            <th>Category</th>
                                            <th>Tax ID</th>
                                            <th>Phone Number</th>
                                            <th>Website Url</th>
                                            <th>Booth Type</th>
                                            <th>Booth Details</th>
                                            <!-- <th>Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>


                                            <td>{{ ucfirst($Registration->company_name)  }}</td>
                                            <td> {{ ucfirst($Registration->category)}} </td>
                                            <td> {{ ($Registration->tax_ID) }} </td>
                                            <td> {{ ($Registration->phone_no) }} </td>
                                            <td> {{ ($Registration->website_url) }} </td>
                                            <td> {{ $Registration->exhibitorTypes->name }} </td>
                                            <td>
                                                @forelse($Registration->exhibitorTypes->size_price as $size_price)
                                                    {{ $size_price['size'] }}
                                                    <br>
                                                @empty

                                                @endforelse
                                            </td>


                                        </tr>
                                    </tbody>
                                </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>





@section('javascript')
<script>

</script>

@endsection


@endsection
