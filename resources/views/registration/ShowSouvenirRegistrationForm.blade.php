@extends('layouts.user.base')
@section('content')


<style type="text/css">
    .card {
        background-color: transparent;
        border: 0px solid;
    }
    .card-header {
        padding: 0px;
    }
    .card-header a {
        background: #784d98;
        color: #fff;
        padding: 0.75rem 1.25rem;
        width: 100%;
        display: block;
    }
    .p-radio-btn {
        position: absolute;
        top: 6px;
    }
     .error {
         color:red
    }
    #captcha{
        font-size:25px;
        padding-left: 27px
    }
</style>


<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/souvenir-ads.jpg" class="img-fluid w-100" alt="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                
                                <div class="row">
                                    <div class="col-12 col-lg-10 offset-lg-1 mt-3">

                                        @foreach ($errors->all() as $error)
                                    <div class="error">{{ $error }}</div>

                                    @endforeach
                                    @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @endif
                                       
                                        <div class="shadow-small py-5 px-3 px-md-4">
                                            <form action="{{url('souvenir-registration')}}" method="post" id="form" enctype="multipart/form-data">
                                              @csrf
                                              <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Email:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" id="email" name="email"  value="{{  Auth::user()->email ?? '' }}" class="form-control" placeholder="Email" @if(Auth::user()) readonly="" @endif>
                                                            <div class="email_valid_msg error"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Phone No:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="phone_no" value="{{$Registration->phone_no??""}}"  class="form-control" placeholder="Phone No">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Full Name:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="full_name"  value="{{$Registration->full_name??""}}" class="form-control" placeholder="Full Name">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Company Name:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="company_name1"  value="{{$Registration->company_name??""}}" class="form-control" placeholder="Company Name">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Name of your Website:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="website_url" value="{{$Registration->website_url??""}}"   class="form-control" placeholder="Name of your Website">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Address:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="address" value="{{$Registration->extra_data['address']??""}}"  class="form-control" placeholder="Address">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Referred By:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="referred_By" value="{{$Registration->extra_data['referred_By']??""}}"  class="form-control" placeholder="Referred By">
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                            <h5 class="mt-2 mb-3 text-violet">Sponsor Type:</h5>
                                            
                                            <div class="table-responsive">
                                        <table class="table-bordered table">
                                            <tbody>
                                            <tr>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center">S.No</th>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center"><strong>Name</strong></th>
                                                <th align="center" class="bg-violet text-white text-center"><strong> Price</strong></th>
                                                <th align="center" class="bg-violet text-white text-center"><strong> Count</strong></th>
                                                <th align="center" class="bg-violet text-white text-center"><strong> Total</strong></th>
                                            </tr>
                                            </tbody>
                                            <tbody>
                                            @foreach ($SouvenirSponsorTypes as $SouvenirSponsorType)
                                                <tr>
                                                    <td align="center">{{$loop->iteration}}</td>
                                                    <td>{{$SouvenirSponsorType->name}}</td>
                                                    <td>$  <span id="SouvenirSponsorType_{{$SouvenirSponsorType->id}}">{{$SouvenirSponsorType->amount}}</span> </td>
                                                    <td> <input type="text" class="form-control count" id="count_{{$SouvenirSponsorType->id}}" name="count[{{$SouvenirSponsorType->id}}]" {{$Registration==null? "" : "disabled"}}   ></td>
                                                    <td>$ <span class="subtotal" id="subtotal_{{$SouvenirSponsorType->id}}">0</span></td>
                                                </tr>
                                            @endforeach
                                               
                                                <tr>
                                                    <td colspan="4" class="text-right"> <b>Total Amount :</b></td>
                                                    <td colspan="2"> $<span id="total_amount">0</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                         <div class="error-if-amount-is-zero error"></div>
                                    <input type="hidden" name="payment_amount" id="amount">
                                    </div>
                                            <div class="row" id="benfit_image_uploade">

                                                {{-- <div class="col-12 col-sm-10 col-md-6 col-lg-5 shadow-small py-4">
                                                    <h6 class="text-center">Upload Photo</h6>
                                                    <div class="browse-button py-2">
                                                        <input type="file" class="form-control browse" name="">
                                                    </div>
                                                    <div class="pt-3 text-center fs15">( File Size could not be more than 5MB )</div>
                                                </div> --}}
                                                
                                           

                                                
                                            </div>
                                            <!-- <button id="add_btn">Add new image</button> -->
                                            @if (!$Registration)
                                            @include('partalles.paymentForm')
                                            @endif

                                            <div class="col-12 col-sm-6 col-md-6 my-auto">
                                                    <div class="pb-4 pb-md-2">
                                                    <input type="checkbox" class="input-checkbox " required name="agrement">
                                                    <span class="pl25 fs16 pointer text-orange font-weight-bold" data-toggle="modal" data-target="#t_and_c">I accept Terms and Conditions</span>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-12 col-lg-12 my-auto">
                                                        <div class="mt20 text-right">
                                                            <input type="submit" class="btn btn-lg btn-danger text-uppercase px-5 submit-button payment-button" @if(!Auth::user()) disabled="" @endif value="Check out">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div style="display:none" class="image_Uploade_template">
    <div class="col-12 col-sm-10 col-md-6 col-lg-4  my-2 px-3 main">
     <div class="display_image"></div>
        <span class="remove_btn btn btn-sm btn-danger border-0">Remove</span>
            <input type="hidden" class="souvenir_image_id" name="souvenir_image_id[]" value="" >
        <div class="my-2">
            <select name="souvenir_sponsor_type_id[]"   class="form-control sponsor_type_id" required="">
                <option value="" selected disabled>Sponsor Type</option>
                @foreach ($SouvenirSponsorTypes as $SouvenirSponsorType)
                    <option value="{{$SouvenirSponsorType->id}}" >{{$SouvenirSponsorType->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="shadow-small py-4">
            <h6 class="text-center benfit_name">Upload Photo</h6>
            <div class="browse-button py-2">
                <input type="file" required class="form-control browse benfit_input" onchange="ValidateFile(this)" name="image[]">
            </div>
            <div class="file_name text-center"></div>
        </div>
        <div class="pt-3 text-center fs15">(Allowed file types: JPG, JPEG, PNG. File Max limit 5MB)</div>
       
    </div>

</div>
</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title " style="float: left;">Registration</h4>
            </div>

            <div class="modal-body">
                <div class="alert alert-success">Please enter OTP received in email. There might be delay up to 30 seconds to receive email and check spam/junk folder</div>
                <form class="" action="{{url('souvenir_new_registertion')}}" method="post">
                    @csrf
                    <input type="hidden" name="email" id="new_email">
                    <div class="form-row">
                        <div class="form-group col-12 col-md-6 col-lg-6">
                            <label>Code :</label><span class="text-red">*</span>
                            <div>
                                <input type="number" required name="otp" id="otp" value="" class="form-control" placeholder="Verification Code" />

                            </div>
                        </div>
                        <div class="form-group col-12 col-md-6 col-lg-6">
                            <label>Create Password (Optional) :</label>
                            <div>
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" />
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>


            </div>
            <div class="modal-footer">
                <a href="{{url('/')}}" class="btn btn-danger">Close</a>
            </div>

        </div>

    </div>
</div>


  <div class="modal fade" id="t_and_c" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Terms and Conditions</h4>
        </div>

        <div class="modal-body">
            <div class="">
                <ul>
                    <li>Terms and Conditions</li>
                    <li>Terms and Conditions</li>
                    <li>Terms and Conditions</li>
                    <li>Terms and Conditions</li>
                </ul>
            </div>
        </div>
         <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
        
      </div>
      
    </div>
  </div>


<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>



@section('javascript')
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>

<script>
         let count=1
        Registration = JSON.parse('{!!json_encode($Registration) !!}')
        if(Registration){
             (Registration.souvenir_details).forEach(souvenir_detail => {
                    
                    $('#count_'+souvenir_detail.souvenir_sponsor_type_id).val(souvenir_detail.count)
                    calculateSubTotal('#count_'+souvenir_detail.souvenir_sponsor_type_id);
                })
                if((Registration.souvenir_images).length>0){
                    (Registration.souvenir_images).forEach(souvenir_images => {
                        displayImageUploadeField(count,souvenir_images)
                    })
                }

        }else{

        }


         $('.submit-button').click(function(e){
            
            if($('#amount').val()==0){
                $('.error-if-amount-is-zero').text('Please select any of the above packages')
                e.preventDefault()
            }else{
                $('.error-if-amount-is-zero').empty();
                $('.error-if-amount-is-zero').text(' ');
                return true;
            }
        });
    
       var _validFileExtensions = [".jpg", ".jpeg",  ".png"];    


        function ValidateFile(oInput) {
            $(oInput).parent().parent().parent().find('.file_name ').text(' ')
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            filename=sFileName.split('\\')[(sFileName.split('\\').length)-1]
                            $(oInput).parent().parent().parent().find('.file_name ').text(filename)
                            break;
                        }
                    }
                    
                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }
       
  
    $(document).ready(function() {
        
        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        });
    $.validator.addClassRules('count', {
            digits: true, 
            min: 1, 
        });
      


        $("#form").validate({
            rules: {
                'phone_no':{
                    digits:true,
                    required:true
                },
                 'full_name':{
                    alpha:true,
                    required:true
                },
                 'company_name1':{
                    required:true
                },
                 'website_url':{
                    required:true
                },
                 'address':{
                    required:true
                },
                 'referred_By':{
                    required:true
                },
                'payment_type': {
                    required: true
                },


            }, 
            messages: {
                 "full_name": {
                    alpha: "Full name should not contain numbers.",
                    alpha: "Full name should not contain numbers.",
                },
            }
        , });
    });
    $('#add_btn').click(function(e){
        e.preventDefault()
        displayImageUploadeField (count,null)
    })

   
    

      $(document).on("click",".remove_btn",function(e) {
        e.preventDefault()
          swal({
        title: "Are you sure ?",
        text: "Do you want to delete ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
          $(this).parent().remove()
            if(willDelete){
                data = {
                    "_token": $('meta[name=csrf-token]').attr('content'),
                    id: $(this).attr('data-imgid').split('_'),
                }
               // ajaxCall( "{{url('souvenir-registration-image-delete')}}/"+$(this).attr('data-imgid').split('_') , 'delete', data, afterdelete) 
               
            }
      });
    });

    

    function afterdelete(){
        alert('deleted')
    }

     function displayImageUploadeField (count,souvenir_image,sponsor_type_id1=null){
        
        if(souvenir_image!=null){
            imgurl='{{asset( config('conventions.souvenir_image_display'))}}/'+souvenir_image.image_url
            image_src=  '<img src=" '+imgurl + ' " alt="Girl in a jacket" width="100" height="100">' 
            remove_btn_sov_id =souvenir_image.id
            sponsor_type_id=souvenir_image.souvenir_sponsor_type_id;
        }else{
            image_src="";
            remove_btn_sov_id = '';
            sponsor_type_id='';
        }

        if(sponsor_type_id1!=null){
            sponsor_type_id=sponsor_type_id1
        }

            $(".image_Uploade_template").find('.main').first().clone()
            .find(".remove_btn").attr('id','remove_btn_'+count).end()
            .find(".remove_btn").attr('data-imgid','remove_btn_'+remove_btn_sov_id ).end()
            .find(".sponsor_type_id").val(sponsor_type_id).end()
            .find(".benfit_name").text('Image '+ count).end()
            .find(".display_image").append(image_src).end()
            .find(".souvenir_image_id").val((souvenir_image != null) ? souvenir_image.id : ''  ).end()
            .appendTo($('#benfit_image_uploade'));
             count=count+1
     }

        



     $('#email').on('change', function() {
        $('.email_valid_msg').text('')
        var email = $('#email').val();
        var thiss = $(this);
        if (email != "") {
            if (email.search('@') > 0) {} else {
                $('.email_valid_msg').text('Please enter a valid email')
            }
            $.ajax({
                type: 'GET'
                , url: "{{url('souvenir-email-verification')}}?email=" + email
                , success: function(data) {
                    if (data == "success") {
                        $('#new_email').val(email);
                        $('#myModal').modal({
                            backdrop: 'static'
                            , keyboard: false
                        });
                        $('#myModal').modal('show');
                    } else {
                        window.location.href = "{{url('login')}}?error=exit&email=" + email;
                    }

                }
            });
        } else {
            $('.email_valid_msg').text('Please enter email')
            //alert('Please enter email');
        }
    });


    $(".count").keyup(function(event) { 
            selected_id=$(this).attr('id').split('_')[1]
            calculateSubTotal(this);
            if((($(this).val()).length==1) && (!(event.keyCode==8 || event.keyCode==46)) ){
                displayImageUploadeField (count,null,selected_id)
            }
            if( ($(this).val()).length==0 ){
                $('.sponsor_type_id').each(function(){
                    if($(this).val()==selected_id){
                    $(this).parent().parent().remove()
                    }
                })
            }
    })



    function calculateSubTotal(element){
         sum =0;
            id = $(element).attr('id').split("_")
            count = $(element).val()
            amount = parseInt($('#SouvenirSponsorType_' + id[1]).text())
            $('#subtotal_' + id[1]).text(amount * count)
             countkeyup(id,count)
    } 

     function countkeyup(id,count) { 
            sum =0; 
             

           var items_count = 0;

            $('.subtotal').each(function() { 
                sum = parseInt(sum) + parseInt($(this).text())
                
            });
            @if(\Auth::user())
                show_submit_btn=true
                $('.count').each(function() { 
                    if($(this).val()!=''){
                        show_submit_btn=false
                    }
                });
                    $('.submit-button').prop('disabled', show_submit_btn);
            @endif

            var total_amounts= parseInt(sum);
            $('#amount').val(total_amounts);
            $('#total_amount').text(total_amounts.toLocaleString());
            
        }

     
    
    
    {{--     $('#form').on('submit', function(event){

        var enter_captha = $('#captcha_number').val();
        var captcha =$('#captcha').text();
        if(enter_captha != captcha){
           event.preventDefault();
           event.stopPropagation();
           alert("Captcha Invalid.");
        }
    });
    captcha();
    $('.captcha_refresh').on('click',function(){
        captcha();

    });
    function captcha() {
        var number = Math.floor(100000 + Math.random() * 900000);
        $('#captcha').text(number);
    } --}}

</script>

@endsection


@endsection
