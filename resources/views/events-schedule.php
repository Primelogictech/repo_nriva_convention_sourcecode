<style type="text/css">
    button.close:focus {
        outline: 0px auto -webkit-focus-ring-color;
    }
    .events-heading {
        font-size: 30px;
        color: #784d98;
        margin-bottom: 15px;
        border-bottom: 1px solid #f6a500;
        display: inline-block;
        padding-bottom: 5px;
    }
    .event-schedulee-bg {
        background-color: #050a30;
        border-radius: 10px;
        text-align: center;
    }
    .event-schedulee {
        writing-mode: vertical-rl;
        -webkit-writing-mode: vertical-rl;
        -ms-writing-mode: vertical-rl;
        transform: rotate(180deg);
        color: #fff;
        padding: 15px;
        vertical-align: middle;
        align-items: center;
        width: 100%;
        height: 300px;
    }
    .schedule-time {
        color: #050a30;
        background-color: #f6a500;
        width: 55%;
        position: absolute;
        left: 23%;
        border-radius: 15px;
        padding: 5px 10px;
        text-align: center;
        border-color: #f6a500;
        border-top: 1px solid;
        border-right: 2px solid;
        border-bottom: 2px solid;
        border-left: 1px solid;
        font-weight: bold;
    }
    .es-events1 {
        background-color: #050a30;
        margin-top: 45px;
        padding: 35px 15px 15px 15px;
        border-radius: 20px;
        height: 275px;
    }
    .es-events1 ul li {
        margin-bottom: 8px;
        font-size: 14px;
    }

    .es-events2 {
        background-color: #050a30;
        margin-top: 45px;
        padding: 35px 15px 15px 15px;
        border-radius: 20px;
        height: 530px;
    }
    .es-events2 ul li {
        margin-bottom: 8px;
        font-size: 13px;
    }
    .btn-small {
        padding: 5px 5px;
        font-size: 10px;
        line-height: 12px;
        border-radius: 0.2rem;
        background-color: #f6a500;
        border-color: #f6a500;
    }
    @media (max-width: 640px) {
        .es-events1,
        .es-events2 {
            height: auto;
        }
        .event-schedulee {
            height: auto;
            writing-mode: horizontal-tb;
            -webkit-writing-mode: horizontal-tb;
            -ms-writing-mode: horizontal-tb;
            transform: rotate(360deg);
        }
        .events-heading {
            font-size: 20px;
        }
    }
    @media (max-width: 576px) {
        .events-heading {
            font-size: 16px;
        }
        .btn-small {
            padding: 5px 2px;
            font-size: 10px;
            line-height: 12px;
            border-radius: 0.2rem;
        }
    }
    .tooltip {
        position: absolute;
        z-index: 1070;
        display: block;
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        font-style: normal;
        font-weight: 400;
        line-height: 1.5;
        text-align: left;
        text-align: start;
        text-decoration: none;
        text-shadow: none;
        text-transform: none;
        letter-spacing: normal;
        word-break: normal;
        word-spacing: normal;
        white-space: normal;
        line-break: auto;
        font-size: 0.875rem;
        word-wrap: break-word;
        opacity: 0;
    }
    .tooltip.show {
        opacity: 1;
    }
    .tooltip .arrow {
        position: absolute;
        display: block;
        width: 0.8rem;
        height: 0.4rem;
    }
    .tooltip .arrow::before {
        position: absolute;
        content: "";
        border-color: transparent;
        border-style: solid;
    }
    .bs-tooltip-auto[x-placement^="top"],
    .bs-tooltip-top {
        padding: 0.4rem 0;
    }
    .bs-tooltip-auto[x-placement^="top"] .arrow,
    .bs-tooltip-top .arrow {
        bottom: 0;
    }
    .bs-tooltip-auto[x-placement^="top"] .arrow::before,
    .bs-tooltip-top .arrow::before {
        top: 0;
        border-width: 0.4rem 0.4rem 0;
        border-top-color: #ff9900;
    }
    .bs-tooltip-auto[x-placement^="right"],
    .bs-tooltip-right {
        padding: 0 0.4rem;
    }
    .bs-tooltip-auto[x-placement^="right"] .arrow,
    .bs-tooltip-right .arrow {
        left: 0;
        width: 0.4rem;
        height: 0.8rem;
    }
    .bs-tooltip-auto[x-placement^="right"] .arrow::before,
    .bs-tooltip-right .arrow::before {
        right: 0;
        border-width: 0.4rem 0.4rem 0.4rem 0;
        border-right-color: #ff9900;
    }
    .bs-tooltip-auto[x-placement^="bottom"],
    .bs-tooltip-bottom {
        padding: 0.4rem 0;
    }
    .bs-tooltip-auto[x-placement^="bottom"] .arrow,
    .bs-tooltip-bottom .arrow {
        top: 0;
    }
    .bs-tooltip-auto[x-placement^="bottom"] .arrow::before,
    .bs-tooltip-bottom .arrow::before {
        bottom: 0;
        border-width: 0 0.4rem 0.4rem;
        border-bottom-color: #ff9900;
    }
    .bs-tooltip-auto[x-placement^="left"],
    .bs-tooltip-left {
        padding: 0 0.4rem;
    }
    .bs-tooltip-auto[x-placement^="left"] .arrow,
    .bs-tooltip-left .arrow {
        right: 0;
        width: 0.4rem;
        height: 0.8rem;
    }
    .bs-tooltip-auto[x-placement^="left"] .arrow::before,
    .bs-tooltip-left .arrow::before {
        left: 0;
        border-width: 0.4rem 0 0.4rem 0.4rem;
        border-left-color: #ff9900;
    }
    .tooltip-inner {
        max-width: 300px;
        padding: 0.25rem 0.5rem;
        color: #050a30;
        text-align: center;
        background-color: #ff9900;
        border-radius: 0.25rem;
    }

    /* popup */
    div#myModal1,
    div#myModal2 {
        padding-right: 0px;
    }
    .modal-dialog {
        max-width: 100%;
    }
    .floor-plan1,
    .floor-plan2 {
        width: 100%;
        position: relative;
        cursor: pointer;
    }
    .one {
        position: absolute;
        left: 47%;
        top: 35%;
        z-index: 1;
    }
    .two {
        position: absolute;
        left: 53%;
        top: 34.6%;
        z-index: 1;
    }
    .three {
        position: absolute;
        left: 45%;
        top: 43.5%;
        z-index: 1;
    }
    .four {
        position: absolute;
        left: 56%;
        top: 43.5%;
        z-index: 1;
    }
    .five {
        position: absolute;
        left: 21.8%;
        top: 62.2%;
        z-index: 1;
    }
    .six {
        position: absolute;
        left: 22%;
        top: 35%;
        z-index: 1;
    }
    .seven {
        position: absolute;
        left: 53%;
        top: 58%;
        z-index: 1;
    }
    .eight {
        position: absolute;
        left: 62%;
        top: 64%;
        z-index: 1;
    }
    .nine {
        position: absolute;
        left: 69%;
        top: 36.5%;
        z-index: 1;
    }
    .ten {
        position: absolute;
        left: 66%;
        top: 46%;
        z-index: 1;
    }
    .eleven {
        position: absolute;
        left: 87%;
        top: 44%;
        z-index: 1;
    }
    .twelve {
        position: absolute;
        left: 88%;
        top: 55%;
        z-index: 1; 
    }
    .thirteen {
        position: absolute;
        left: 43%;
        top: 33%;
        z-index: 1;
    }
    .fourteen {
        position: absolute;
        left: 37%;
        top: 37%;
        z-index: 1;
    }
    .fifteen {
        position: absolute;
        left: 43%;
        top: 41%;
        z-index: 1;
    }
    .sixteen {
        position: absolute;
        left: 31%;
        top: 39%;
        z-index: 1;
    }
    .seventeen {
        position: absolute;
        left: 26%;
        top: 45%;
        z-index: 1;
    }
    .eighteen {
        position: absolute;
        left: 30%;
        top: 52%;
        z-index: 1;
    }
    .nineteen {
        position: absolute;
        left: 43%;
        top: 58%;
        z-index: 1;
    }
    .twenty {
        position: absolute;
        left: 43%;
        top: 64%;
        z-index: 1;
    }
    .twentyone {
        position: absolute;
        left: 54%;
        top: 44%;
        z-index: 1;
    }
    .twentytwo {
        position: absolute;
        left: 21.8%;
        top: 50%;
        z-index: 1;
    }
    .twentythree {
        position: absolute;
        left: 69%;
        top: 21%;
        z-index: 1;
    }
    .ml-one {
        position: absolute;
        left: 47.5%;
        top: 17%;
        z-index: 1;  
    }
    .ml-two {
        position: absolute;
        left: 54%;
        top: 16.9%;
        z-index: 1;
    }
    .ml-three {
        position: absolute;
        left: 55.8%;
        top: 21%;
        z-index: 1;
    }
    .ml-four {
        position: absolute;
        left: 45%;
        top: 21%;
        z-index: 1;
    }
    .ml-five {
        position: absolute;
        left: 22%;
        top: 31.5%;
        z-index: 1;
    }
    .ml-six {
        position: absolute;
        left: 22%;
        top: 17%;
        z-index: 1;
    }
    .ml-seven {
        position: absolute;
        left: 54%;
        top: 29%;
        z-index: 1;
    }
    .ml-eight {
        position: absolute;
        left: 63%;
        top: 32.3%;
        z-index: 1;
    }
    .ml-nine {
        position: absolute;
        left: 70%;
        top: 18.4%;
        z-index: 1;
    }
    .ml-ten {
        position: absolute;
        left: 66%;
        top: 22.9%;
        z-index: 1;
    }
    .ml-eleven {
        position: absolute;
        left: 88%;
        top: 22.3%;
        z-index: 1; 
    }
    .ml-twelve {
        position: absolute;
        left: 88%;
        top: 27.8%;
        z-index: 1;
    }
    .ml-thirteen {
        position: absolute;
        left: 43%;
        top: 66%;
        z-index: 1;
    }
    .ml-fourteen {
        position: absolute;
        left: 37%;
        top: 68.4%;
        z-index: 1;
    }
    .ml-fifteen {
        position: absolute;
        left: 43%;
        top: 70.2%;
        z-index: 1;
    }
    .ml-sixteen {
        position: absolute;
        left: 31%;
        top: 69.6%;
        z-index: 1;
    }
    .ml-seventeen {
        position: absolute;
        left: 26%;
        top: 72.5%;
        z-index: 1;
    }
    .ml-eighteen {
        position: absolute;
        left: 30%;
        top: 76.1%;
        z-index: 1;
    }
    .ml-ninteen {
        position: absolute;
        left: 43%;
        top: 79.5%;
        z-index: 1;
    }
    .ml-twenty {
        position: absolute;
        left: 43%;
        top: 82.5%;
        z-index: 1;
    }
    .ml-twentyone {
        position: absolute;
        left: 55%;
        top: 72.5%;
        z-index: 1;
    }
    .ml-twentytwo {
        position: absolute;
        left: 67%;
        top: 67%;
        z-index: 1;
    }
    .ml-twentytwo {
        position: absolute;
        left: 22%;
        top: 25%;
        z-index: 1;
    }
    .ml-twentythree {
        position: absolute;
        left: 70%;
        top: 10%;
        z-index: 1;
    }
    .img-number{
        cursor: pointer;
    }
    .es-tabs .nav-link.active {
        border-bottom: none !important;
        background-color: #ff9900;
        border-left: 0px;
        border-right: 0px;
        border-top: 0px;
        color: #ffffff;
    }
    .px-4.border {
        color: #050a30;
    }
    .fade:not(.show) {
        opacity: 1;
    }
    .modal-content{
        border: 1px solid rgb(255 153 0);
    }
    .modal-header{
        border-bottom: 1px solid rgb(255 153 0);
    }
</style>
<section class="container-fluid my-3 my-lg-5 px-0 px-sm-3">
    <div class="row mx-0 mx-sm-3">
        <div class="col-12 shadow-small py-0 pt-1 px-1">
            <div class="row mx-2 mx-md-3">
                <div class="col-12 px-0">
                    <h3 class="events-heading pt-3">
                        6th Global Convention High Level Schedule
                    </h3>
                </div>
                <div class="col-12 px-0">
                    <div>
                        <ul class="nav nav-tabs es-tabs fs16 horizontal-scroll" role="tablist">
                            <li class="nav-item d-inline-flex">
                                <a class="nav-link px-4 border active" href="#by-location" role="tab" data-toggle="tab">By Location</a>
                            </li>
                            <li class="nav-item d-inline-flex">
                                <a class="nav-link px-4 border" href="#by-map" role="tab" data-toggle="tab">By Map</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="by-location">
                        <div class="col-12 shadow-small py-0 pt-1 px-1">
                            <div class="row">
                                <div class="col-12 p30 px-sm-30">
                                    <div class="row">
                                        <div class="col-12 col-md-2 col-lg-1 my-auto">
                                            <div class="event-schedulee-bg">
                                                <div class="event-schedulee">
                                                    Matrimony Starts on Friday July 1st with Informal Gathering
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-10 col-lg-11">
                                            <div class="row pl-0">
                                                <div class="col-12 col-md-6 col-lg-3 px-2 px-md-1 py-3">
                                                    <div class="schedule-time">
                                                        July 2<sup>nd</sup>, 2022 <br />
                                                        Saturday <br />
                                                        Day 1 - Arrival Day
                                                    </div>

                                                    <div class="es-events2">
                                                        <ul class="pl-3 mb-0 text-white">
                                                            <div class="row">
                                                                <div class="col-12 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Check in and Registration
                                                                    </li>
                                                                </div>
                                                                <!-- <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="one">View Location 1</button>
                                                                </div> -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Inaugural
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <!-- <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="two">View Location 2</button> -->
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Banquet
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <!-- <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="three">View Location 3</button> -->
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Youth Banquet for Youth below High School
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="sixteen">View Location 16</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Youth Banquet for Youth High School and Above
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="thirteen">View Location 13</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Matrimony 1
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="nineteen">View Location 19</button>
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Matrimony 2
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="twenty">View Location 20</button>
                                                                </div>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 col-lg-3 px-2 px-md-1 py-3">
                                                    <div class="schedule-time">
                                                        July 3<sup>rd</sup>, 2022 <br />
                                                        Sunday <br />
                                                        Day 2
                                                    </div>
                                                    <div class="es-events2">
                                                        <ul class="pl-3 mb-0 text-white">
                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Parade by Chapters
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <!-- <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="seven">View Location 7</button> -->
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Grand Kalyanam Event
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <!-- <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="eight">View Location 8</button> -->
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Matrimony 1
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="nineteen">View Location 19</button>
                                                                </div>
                                                            </div>
                                                             <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Matrimony 2
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="twenty">View Location 20</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Shathamanam Bhavathi
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="two">View Location 2</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Business Conference
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <!-- <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="eleven">View Location 11</button> -->
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Stocks and Equity Conference
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="one">View Location 1</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Youth, Women&rsquo;s 
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="fifteen">View Location 15</button>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>CME
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="fourteen">View Location 14</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        V Got Talent
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="two">View Location 2</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        NRIVA Pageant
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="thirteen">View Location 13</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Real Estate / Legal
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="three">View Location 3</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Health and Wellness
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <!-- <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="three">View Location 3</button> -->
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Mega Musical Night
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <!-- <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="four">View Location 4</button> -->
                                                                </div>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 col-lg-3 px-2 px-md-1 py-3">
                                                    <div class="schedule-time">
                                                        July 4<sup>th</sup>, 2022 <br />
                                                        Monday <br />
                                                        Day 3
                                                    </div>
                                                    <div class="es-events2">
                                                        <ul class="pl-3 mb-0 text-white">
                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Spiritual
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <!-- <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="five">View Location 5</button> -->
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        In Conversation with Sadhguru
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="nineteen">View Location 19</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Keynote by Sadhguru
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="nineteen">View Location 19</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Business Conference
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <!-- <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="eight">View Location 8</button> -->
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Stocks and Equity Conference
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="one">View Location 1</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Youth
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="fifteen">View Location 15</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Women's
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="fifteen">View Location 15</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        V Got Talent
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="two">View Location 2</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        NRIVA Pageant
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="thirteen">View Location 13</button>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Appreciation Night
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <!-- <button class="btn btn-small" data-toggle="modal" data-attribute="myModal2" data-location="fourteen">View Location 14</button> -->
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Musical Concert
                                                                    </li>
                                                                </div>
                                                                <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <!-- <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="one">View Location 1</button> -->
                                                                </div>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 col-lg-3 px-2 px-md-1 py-3">
                                                    <div class="schedule-time">
                                                        July 5<sup>th</sup>, 2022 <br />
                                                        Tuesday <br />
                                                        Day 4 - Checkout
                                                    </div>
                                                    <div class="es-events2">
                                                        <ul class="pl-3 mb-0 text-white">
                                                            <div class="row">
                                                                <div class="col-7 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Check out
                                                                    </li>
                                                                </div>
                                                                <!-- <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="two">View Location 2</button>
                                                                </div> -->
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-12 pl-3 pr-2 my-auto">
                                                                    <li>
                                                                        Fly or Drive to Home or Extend the stay at discounted rates in the same hotel
                                                                    </li>
                                                                </div>
                                                                <!-- <div class="col-5 pl-0 pr-2 my-auto">
                                                                    <button class="btn btn-small" data-toggle="modal" data-attribute="myModal1" data-location="three">View Location 3</button>
                                                                </div> -->
                                                            </div>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade in" id="by-map">
                        <div class="col-12 shadow-small py-0 pt-1 px-1 my-auto">
                            <div class="row">
                                <div class="col-12">
                                    <h3 class="pt-4 text-center text-orange mb-0">By Map</h3>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 col-lg-8">
                                            <div>
                                                <img src="images/floor-plan/first-floor.jpg" class="img-fluid floor-plan1" />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/1.png"
                                                    class="img-fluid img-number ml-one"
                                                    id="ml-one"
                                                    data-toggle="tooltip"
                                                    data-placement="top"
                                                    title="&nbsp;1. Jul 03 - BIG &amp; Equity - 11.00 am to 6 pm &nbsp;2. Jul 04 - BIG &amp; Equity - 11.00 am to 6 pm"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/2.png"
                                                    class="img-fluid img-number ml-two"
                                                    id="ml-two"
                                                    data-toggle="tooltip"
                                                    title="1. Jul 03 - Sathmanam Bhavathi - 11.30 am to&nbsp; 3 pm&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. Jul 04 - Pattukune Pattu Chira/VGOT - 11.30 am to 3 pm&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                                    data-placement="right"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/3.png"
                                                    class="img-fluid img-number ml-three"
                                                    id="ml-three"
                                                    data-toggle="tooltip"
                                                    title="1. Jul 03 - Real Estate / Legal - noon to 3 pm"
                                                    data-placement="bottom"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/4.png"
                                                    class="img-fluid img-number ml-four"
                                                    id="ml-four"
                                                    data-toggle="tooltip"
                                                    title="1. Jul 03 - Vendor Booths - All day &emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;2. Jul 04 - Vendor Booths&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;"
                                                    data-placement="left"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/5.png"
                                                    class="img-fluid img-number ml-five"
                                                    id="ml-five"
                                                    data-toggle="tooltip"
                                                    title=""
                                                    data-placement="right"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/6.png"
                                                    class="img-fluid img-number ml-six"
                                                    id="ml-six"
                                                    data-toggle="tooltip"
                                                    title=""
                                                    data-placement="left"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/7.png"
                                                    class="img-fluid img-number ml-seven"
                                                    id="ml-seven"
                                                    data-toggle="tooltip"
                                                    title=""
                                                    data-placement="top"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/8.png"
                                                    class="img-fluid img-number ml-eight"
                                                    id="ml-eight"
                                                    data-toggle="tooltip"
                                                    title="1. Jul 02 - Make up - 1 pm to 6 pm&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp; 2. Jul 03 - Make up - 1 pm to 6 pm&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp; 3. Jul 04 - Make up - Noon to 5 pm&emsp;&emsp;&emsp;&emsp;&nbsp;"
                                                    data-placement="top"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/9.png"
                                                    class="img-fluid img-number ml-nine"
                                                    id="ml-nine"
                                                    data-toggle="tooltip"
                                                    data-placement="top"
                                                    title=""
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/10.png"
                                                    class="img-fluid img-number ml-ten"
                                                    id="ml-ten"
                                                    data-toggle="tooltip"
                                                    title="1. Jul 02 - Make up - 1 pm to 6 pm&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp; 2. Jul 03 - Make up - 1 pm to 6 pm&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp; 3. Jul 04 - Make up - Noon to 5 pm&emsp;&emsp;&emsp;&emsp;&nbsp;"
                                                    data-placement="right"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/11.png"
                                                    class="img-fluid img-number ml-eleven"
                                                    id="ml-eleven"
                                                    data-toggle="tooltip"
                                                    title="1. Jul 02 - War room - 7 am to midnight &emsp;&nbsp;&nbsp; 2. Jul 03 - War room &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp; 3. Jul 04 - War room &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                                    data-placement="bottom"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/12.png"
                                                    class="img-fluid img-number ml-twelve"
                                                    id="ml-twelve"
                                                    data-toggle="tooltip"
                                                    title=""
                                                    data-placement="left"
                                                />
                                            </div>
                                            <div>
                                                <img src="images/floor-plan/second-floor.jpg" class="img-fluid floor-plan2" />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/13.png"
                                                    class="img-fluid img-number ml-thirteen"
                                                    id="ml-thirteen"
                                                    data-toggle="tooltip"
                                                    title="" data-placement="right" data-original-title="1. Jul 02 - Youth Banquet - 5.30 pm to&nbsp;&nbsp; &emsp;&emsp; midnight&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp; 2. Jul 03 - Pageant - Noon to 5 pm&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. Jul 04 - Pageant - Noon to 5 pm&emsp;&emsp;&emsp;&emsp;"
                                                    data-placement="right"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/14.png"
                                                    class="img-fluid img-number ml-fourteen"
                                                    id="ml-fourteen"
                                                    data-toggle="tooltip"
                                                    title="1. Jul 03 - CME - Noon to 5 pm&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; 2. Jul 04 - CME - Noon to 2 pm&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"
                                                    data-placement="left"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/15.png"
                                                    class="img-fluid img-number ml-fifteen"
                                                    id="ml-fifteen"
                                                    data-toggle="tooltip"
                                                    title="1. Jul 03 - Youth - Noon to 3 pm&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp; 2. Jul 03 - Women - 3 pm to 5 pm&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp; 3. Jul 04 - Youth - Noon to 3 pm&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp; 4. Jul 04 - Women - 3 pm to 5 pm&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;"
                                                    data-placement="right"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/16.png"
                                                    class="img-fluid img-number ml-sixteen"
                                                    id="ml-sixteen"
                                                    data-toggle="tooltip"
                                                    title="&nbsp;&nbsp;&nbsp;1. Jul 02 - Kids Banquet - 5.30 pm to 9 pm&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;2. Jul 03 - All day&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;3. Jul 04 - Vysya Hostel reunion - 3 pm to 5 pm &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"
                                                    data-placement="left"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/17.png"
                                                    class="img-fluid img-number ml-seventeen"
                                                    id="ml-seventeen"
                                                    data-toggle="tooltip"
                                                    title=""
                                                    data-placement="right"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/18.png"
                                                    class="img-fluid img-number ml-eighteen"
                                                    id="ml-eighteen"
                                                    data-toggle="tooltip"
                                                    title="&nbsp;1. Jul 03 - Dress change for Pageant - Noon to 5 pm &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;2. Jul 04 - Dress change for Pageant - Noon to 5 pm&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;"
                                                    data-placement="left"
                                                />
                                            </div>
                                            <div class="">
                                                <img src="images/numbers/19.png" class="img-fluid img-number ml-ninteen" id="ml-ninteen" data-toggle="tooltip" title="&nbsp;&nbsp;&nbsp;1. Jul 02 - Matrimony 1 - 7 am to 5 pm&emsp;&emsp;&nbsp;&nbsp;&nbsp;2. Jul 03 - Matrimony 1 - 7 am to 5 pm&emsp;&nbsp;&nbsp;&nbsp; 3. Jul 04 - Sadguru Event - Noon to 3 pm" data-placement="top" title="" data-toggle="tooltip" id="ml-ninteen" class="img-fluid img-number ml-ninteen" data-placement="top">
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/20.png"
                                                    class="img-fluid img-number ml-twenty"
                                                    id="ml-twenty"
                                                    data-toggle="tooltip"
                                                    title="&nbsp;&nbsp;&nbsp;1. Jul 02 - Matrimony 2 - 7 am to 5 pm&emsp;&emsp;&nbsp;&nbsp;&nbsp;2. Jul 03 - Matrimony 2 - 7 am to 5 pm&emsp;&nbsp;&nbsp;&nbsp; 3. Jul 04 - VVN Meet&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;"
                                                    data-placement="left"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/21.png"
                                                    class="img-fluid img-number ml-twentyone"
                                                    id="ml-twentyone"
                                                    data-toggle="tooltip"
                                                    title="&nbsp;1. Jul 03 - Dress change for Pageant - Noon to 5 pm &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;2. Jul 04 - Dress change for Pageant - Noon to 5 pm&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;"
                                                    data-placement="left"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/22.png"
                                                    class="img-fluid img-number ml-twentytwo"
                                                    id="ml-twentytwo"
                                                    data-toggle="tooltip"
                                                    title=""
                                                    data-placement="right"
                                                />
                                            </div>
                                            <div class="">
                                                <img
                                                    src="images/numbers/23.png"
                                                    class="img-fluid img-number ml-twentythree"
                                                    id="ml-twentythree"
                                                    data-toggle="tooltip"
                                                    title="1. Jul 02 - VVIP  (Lunch and Dinner) - 11 am to 2 pm and 6.30 pm to 9 pm&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. Jul 03 - VVIP  (Lunch and Dinner) - 11 am to 2 pm and 6.30 pm to 9 pm&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. Jul 04 - VVIP  (Lunch and Dinner) - 11 am to 2 pm and 6.30 pm to 9 pm&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                                    data-placement="right"
                                                />
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-4 py-2 py-lg-3 mt-3">
                                            <h5 class="text-orange mb-2 text-decoration-underline">Room Names</h5>
                                            <h6>1. Schaumburg Ballroom East</h6>
                                            <h6>2. Schaumburg Ballroom West</h6>
                                            <h6>3. Schaumburg Ballroom A, B , C and D</h6>
                                            <h6>4. Schaumburg Ballroom E, F, G and H</h6>
                                            <h6>5. Exploration Hall</h6>
                                            <h6>6. Adventure Hall</h6>
                                            <h6>7. Inspiration</h6>
                                            <h6>8. Innovation</h6>
                                            <h6>9. Connection</h6>
                                            <h6>10. Imagination</h6>
                                            <h6>11. Perfection Boardroom</h6>
                                            <h6>12. Knowledge</h6>
                                            <h6>13. UTOPIA Ballroom C & D</h6>
                                            <h6>14. UTOPIA Ballroom B</h6>
                                            <h6>15. UTOPIA A</h6>
                                            <h6>16. EUPHORIA</h6>
                                            <h6>17. Serenity</h6>
                                            <h6>18. Prosperity</h6>
                                            <h6>19. NIRVANA Ballroom A and B</h6>
                                            <h6>20. NIRVANA Ballroom C</h6>
                                            <h6>21. Epiphany</h6>
                                            <h6>22. Discovery Hall</h6>
                                            <h6>23. Schaumburg Public House</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid">
    <!-- The Modal -->
    <div class="modal" id="myModal1" tabindex="-1">
        <div class="container bg-white mt-4 border-radius-10">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h5 class="modal-title">First Floor</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                <div>
                                    <img src="images/floor-plan/first-floor.jpg" class="img-fluid floor-plan1 w-100" id="first-floor" />
                                </div>
                                <div class="">
                                    <img
                                        src="images/numbers/1.png"
                                        class="img-fluid img-number one"
                                        id="one"
                                        data-toggle="tooltip"
                                        data-image="first-floor"
                                        data-placement="top"
                                        title="&nbsp;1. Jul 03 - BIG &amp; Equity - 11.00 am to 6 pm &nbsp;2. Jul 04 - BIG &amp; Equity - 11.00 am to 6 pm"
                                    />
                                </div>
                                <div class="">
                                    <img
                                        src="images/numbers/2.png"
                                        class="img-fluid img-number two"
                                        id="two"
                                        data-toggle="tooltip"
                                        data-image="first-floor"
                                        title="1. Jul 03 - Sathmanam Bhavathi - 11.30 am to&nbsp; 3 pm&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. Jul 04 - Pattukune Pattu Chira/VGOT - 11.30 am to 3 pm&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                        data-placement="right"
                                    />
                                </div>
                                <div class="">
                                    <img
                                        src="images/numbers/3.png"
                                        class="img-fluid img-number three"
                                        id="three"
                                        data-toggle="tooltip"
                                        data-image="first-floor"
                                        title="1. Jul 03 - Real Estate / Legal - noon to 3 pm"
                                        data-placement="bottom"
                                    />
                                </div>
                                <div class="">
                                    <img
                                        src="images/numbers/4.png"
                                        class="img-fluid img-number four"
                                        id="four"
                                        data-toggle="tooltip"
                                        data-image="first-floor"
                                        title="1. Jul 03 - Vendor Booths - All day &emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;2. Jul 04 - Vendor Booths&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;"
                                        data-placement="left"
                                    />
                                </div>
                                <div class="">
                                    <img
                                        src="images/numbers/5.png"
                                        class="img-fluid img-number five"
                                        id="five"
                                        data-toggle="tooltip"
                                        data-image="first-floor"
                                        title=""
                                        data-placement="right"
                                    />
                                </div>
                                <div class="">
                                    <img
                                        src="images/numbers/6.png"
                                        class="img-fluid img-number six"
                                        id="six"
                                        data-toggle="tooltip"
                                        data-image="first-floor"
                                        title=""
                                        data-placement="left"
                                    />
                                </div>
                                <div class="">
                                    <img src="images/numbers/7.png" class="img-fluid img-number seven" id="seven" data-toggle="tooltip" data-image="first-floor" title="" data-placement="top" />
                                </div>
                                <div class="">
                                    <img
                                        src="images/numbers/8.png"
                                        class="img-fluid img-number eight"
                                        id="eight"
                                        data-toggle="tooltip"
                                        data-image="first-floor"
                                        data-placement="top"
                                        title="1. Jul 02 - Make up - 1 pm to 6 pm&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp; 2. Jul 03 - Make up - 1 pm to 6 pm&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp; 3. Jul 04 - Make up - Noon to 5 pm&emsp;&emsp;&emsp;&emsp;&nbsp;"
                                    />
                                </div>
                                <div class="">
                                    <img
                                        src="images/numbers/9.png"
                                        class="img-fluid img-number nine"
                                        id="nine"
                                        data-toggle="tooltip"
                                        data-image="first-floor"
                                        title=""
                                        data-placement="right"
                                    />
                                </div>
                                <div class="">
                                    <img
                                        src="images/numbers/10.png"
                                        class="img-fluid img-number ten"
                                        id="ten"
                                        data-toggle="tooltip"
                                        data-image="first-floor"
                                        title="1. Jul 02 - Make up - 1 pm to 6 pm&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp; 2. Jul 03 - Make up - 1 pm to 6 pm&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp; 3. Jul 04 - Make up - Noon to 5 pm&emsp;&emsp;&emsp;&emsp;&nbsp;"
                                        data-placement="bottom"
                                    />
                                </div>
                                <div class="">
                                    <img src="images/numbers/11.png" class="img-fluid img-number eleven" id="eleven" data-toggle="tooltip" data-image="first-floor" title="1. Jul 02 - War room - 7 am to midnight &emsp;&nbsp;&nbsp; 2. Jul 03 - War room &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp; 3. Jul 04 - War room &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" data-placement="left" />
                                </div>
                                <div class="">
                                    <img src="images/numbers/12.png" class="img-fluid img-number twelve" id="twelve" data-toggle="tooltip" data-image="first-floor" title="" data-placement="right" />
                                </div>
                                <div class="">
                                    <img
                                        src="images/numbers/22.png"
                                        class="img-fluid img-number twentytwo"
                                        id="twentytwo"
                                        data-toggle="tooltip"
                                        title=""
                                        data-placement="right"
                                    />
                                </div>
                                <div class="">
                                    <img
                                        src="images/numbers/23.png"
                                        class="img-fluid img-number twentythree"
                                        id="twentythree"
                                        data-toggle="tooltip"
                                        title="1. Jul 02 - VVIP  (Lunch and Dinner) - 11 am to 2 pm and 6.30 pm to 9 pm&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. Jul 03 - VVIP  (Lunch and Dinner) - 11 am to 2 pm and 6.30 pm to 9 pm&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. Jul 04 - VVIP  (Lunch and Dinner) - 11 am to 2 pm and 6.30 pm to 9 pm&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                        data-placement="right"
                                    />
                                </div>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 py-2 py-lg-3 my-auto">
                    <h5 class="text-orange mb-2 text-decoration-underline">Room Names</h5>
                    <h6>1. Schaumburg Ballroom East</h6>
                    <h6>2. Schaumburg Ballroom West</h6>
                    <h6>3. Schaumburg Ballroom A, B , C and D</h6>
                    <h6>4. Schaumburg Ballroom E, F, G and H</h6>
                    <h6>5. Exploration Hall</h6>
                    <h6>6. Adventure Hall</h6>
                    <h6>7. Inspiration</h6>
                    <h6>8. Innovation</h6>
                    <h6>9. Connection</h6>
                    <h6>10. Imagination</h6>
                    <h6>11. Perfection Boardroom</h6>
                    <h6>22. Discovery Hall</h6>
                    <h6>23. Schaumburg Public House</h6>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="modal" id="myModal2" tabindex="-1">
        <div class="container bg-white mt-4 border-radius-10">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h5 class="modal-title">Second Floor</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <img src="images/floor-plan/second-floor.jpg" class="img-fluid floor-plan2 w-100" id="second-floor" />
                                </div>
                                <div class="">
                                    <img
                                        src="images/numbers/13.png"
                                        class="img-fluid img-number thirteen"
                                        id="thirteen"
                                        data-toggle="tooltip"
                                        data-image="second-floor"
                                        title="" data-placement="right" data-original-title="1. Jul 02 - Youth Banquet - 5.30 pm to&nbsp;&nbsp; &emsp;&emsp; midnight&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp; 2. Jul 03 - Pageant - Noon to 5 pm&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. Jul 04 - Pageant - Noon to 5 pm&emsp;&emsp;&emsp;&emsp;"
                                        data-placement="left"
                                    />
                                </div>
                                <div class="">
                                    <img src="images/numbers/14.png" class="img-fluid img-number fourteen" id="fourteen" data-toggle="tooltip" data-image="second-floor" title="1. Jul 03 - CME - Noon to 5 pm&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; 2. Jul 04 - CME - Noon to 2 pm&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;" data-placement="top" />
                                </div>
                                <div class="">
                                    <img src="images/numbers/15.png" class="img-fluid img-number fifteen" id="fifteen" data-toggle="tooltip" data-image="second-floor" title="1. Jul 03 - Youth - Noon to 3 pm&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp; 2. Jul 03 - Women - 3 pm to 5 pm&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp; 3. Jul 04 - Youth - Noon to 3 pm&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp; 4. Jul 04 - Women - 3 pm to 5 pm&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;" data-placement="top" />
                                </div>
                                <div class="">
                                    <img src="images/numbers/16.png" class="img-fluid img-number sixteen" id="sixteen" data-toggle="tooltip" data-image="second-floor" title="&nbsp;&nbsp;&nbsp;1. Jul 02 - Kids Banquet - 5.30 pm to 9 pm&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;2. Jul 03 - All day&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;3. Jul 04 - Vysya Hostel reunion - 3 pm to 5 pm &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;" data-placement="top" />
                                </div>
                                <div class="">
                                    <img src="images/numbers/17.png" class="img-fluid img-number seventeen" id="seventeen" data-toggle="tooltip" data-image="second-floor" title="" data-placement="top" />
                                </div>
                                <div class="">
                                    <img src="images/numbers/18.png" class="img-fluid img-number eighteen" id="eighteen" data-toggle="tooltip" data-image="second-floor" title="&nbsp;1. Jul 03 - Dress change for Pageant - Noon to 5 pm &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;2. Jul 04 - Dress change for Pageant - Noon to 5 pm&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;" data-placement="top" />
                                </div>
                                <div class="">
                                   <img src="images/numbers/19.png" class="img-fluid img-number nineteen" id="nineteen" data-toggle="tooltip" data-image="second-floor" data-placement="top" title="&nbsp;&nbsp;&nbsp;1. Jul 02 - Matrimony 1 - 7 am to 5 pm&emsp;&emsp;&nbsp;&nbsp;&nbsp;2. Jul 03 - Matrimony 1 - 7 am to 5 pm&emsp;&nbsp;&nbsp;&nbsp; 3. Jul 04 - Sadguru Event - Noon to 3 pm" data-placement="top" title="" data-toggle="tooltip" id="ml-ninteen" class="img-fluid img-number ml-ninteen" data-placement="top" />
                                </div>
                                <div class="">
                                    <img src="images/numbers/20.png" class="img-fluid img-number twenty" id="twenty" data-toggle="tooltip" data-image="second-floor" title="&nbsp;&nbsp;&nbsp;1. Jul 02 - Matrimony 2 - 7 am to 5 pm&emsp;&emsp;&nbsp;&nbsp;&nbsp;2. Jul 03 - Matrimony 2 - 7 am to 5 pm&emsp;&nbsp;&nbsp;&nbsp; 3. Jul 04 - VVN Meet&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;" data-placement="top" />
                                </div>
                                <div class="">
                                    <img src="images/numbers/21.png" class="img-fluid img-number twentyone" id="twentyone" data-toggle="tooltip" data-image="second-floor" title="&nbsp;1. Jul 03 - Dress change for Pageant - Noon to 5 pm &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;2. Jul 04 - Dress change for Pageant - Noon to 5 pm&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;" data-placement="top" />
                                </div>
                                
                            </div>

                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 py-2 py-lg-3 my-auto">
                    <h5 class="text-orange mb-2 text-decoration-underline">Room Names</h5>
                    <h6>13. UTOPIA Ballroom C & D</h6>
                    <h6>14. UTOPIA Ballroom B</h6>
                    <h6>15. UTOPIA A</h6>
                    <h6>16. EUPHORIA</h6>
                    <h6>17. Serenity</h6>
                    <h6>18. Prosperity</h6>
                    <h6>19. NIRVANA Ballroom A and B</h6>
                    <h6>20. NIRVANA Ballroom C</h6>
                    <h6>21. Epiphany</h6>
                </div>
            </div>
        </div>
    </div>
</div>