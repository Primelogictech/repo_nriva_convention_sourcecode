<section class="container-fluid my-3 my-lg-4">
    <div class="shadow-small py-0 pt-1 px-1">
        <div class="col-12 pb-3">
            <div class="row">
                <div class="col-12 mb-3 px-0">
                    <div>
                        <img src="images/banners/donors.jpg" class="img-fluid w-100" alt="" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-12 py-0 py-lg-1">
            <div>
                <ul class="list-unstyled donors-tabs-ul d-lg-flex justify-content-center">
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="text-decoration-none donors_active_tab_btn1 donors-nav-link donors_active_tab_btn" target-id="#1">
                            <span>Grand Event Sponsors</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#2">
                            <span>Sapphire</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#3">
                            <span>Platinum</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#4">
                            <span>Gold</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#5">
                            <span>Silver</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#6">
                            <span>Bronze</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#7">
                            <span>Patron</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-12 col-lg-12 px-3 px-md-5 px-lg-5 pb-4 mb-5">
            <div class="row px-3 px-lg-4">
                <div class="col-12 col-lg-12" id="event-sponsors">
                    <div class="row"></div>
                </div>
                <div class="col-12 col-lg-12 donors_active_tab" id="1" style="">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/Praveen_Tadakamalla.jpeg" />
                                </div>
                                <div class="donor-name">
                                    Manjula &amp; Praveen Thadakamalla Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/Sridevi & Anil Grandhi Garu.jpeg" />
                                </div>
                                <div class="donor-name">
                                    Sridevi &amp; Anil Grandhi Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/Jayasimha_Sunku.jpeg" />
                                </div>
                                <div class="donor-name">
                                    Dr Asha &amp; Dr.Jayasimha Sunku Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Dr.Veda &amp; Lakshman Agadi Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Nalini &amp; Sreenivas Veeravelli Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sudha & Hanuman Nandampati Garu
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-12" id="2" style="display: none;">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Bhagayamma &amp; Dr.Rojanandam Samudrala Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Bhanu Ilindra Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Chittari Pabba Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Dr.Geetha &amp; Praveen Amudala Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Dr.Nirmala &amp; Srinivasa Tummalapenta Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Dr.Ramesh Cherivirala Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Dr.Ravi Makam Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Dr.Sree &amp; Hari Raini Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Dr.Suseela &amp; Dr.Ramulu Samudrala Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Dr.Venu Kondle Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Indira &amp; Dr.Ram Saladi Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Jyothi &amp; Suresh Badam Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Malleshwari &amp; Srinivas Pedamallu Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Pavithra &amp; Dinkar Karumuri Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Priya &amp; Suresh Chatakondu Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sadhana &amp; Shankar Setty Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sridhar Challa Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sunitha &amp; Sreenivasa Rachapalli Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Veenadhari &amp; Naveen Goli Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Vishala &amp; Ashok Ellendula Garu
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-12" id="3" style="display: none;">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Bharathi &amp; Nagendra Aytha Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Kalpana &amp; Rajesh Badam Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Lathika &amp; Venkat Belde Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Priyadarshani &amp; Venu Parepalli Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Raja Pampati Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Saritha &amp; Gangadhar Vuppala Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Subha &amp; Ashok Lakshmanan Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sunitha &amp; Shyam Padamatinti Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Uma &amp; Giri Kothamasu Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Uma &amp; Srinivas Rao Pandiri Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Vidya &amp; Prasad Maram Garu
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-12" id="4" style="display: none;">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Chaya &amp; Anand Setty Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Dr Savitha &amp; Dr.Raj Munaga Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Dr Sunitha &amp; Dr.Diwakar Jandhyam Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Dr Surekha &amp; Srinivas Pasupula Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Gayatri &amp; Ramesh Bapanapalli Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Kiran &amp; Venu Mattey Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Kishore Konduru Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Leela &amp; Sekhar Perla Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Manjula &amp; Bala Voleti Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Pavitha &amp; Keshav Guptha Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Ramadevi &amp; Ramakrishna Korrapolu Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Reetha Ravikanti &amp; Ramkumar Siripuram Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sahitha &amp; Kiran Adimulam Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Saritha &amp; Srinivas Chittimalla Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Shanta &amp; Dayakar Veerlapati Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Shilpa &amp; Satish Macha Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Soumya &amp; Prashanth Veerabomma Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sreedhar Aietha Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sreelatha &amp; Srinivas Bampalli Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sridevi &amp; Raghu Adaveni Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sushma &amp; Ravi Ellendula Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Swetha &amp; Bhargava Venishetty Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Vasantha &amp; Guru Alampalli Garu
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-12" id="5" style="display: none;">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Amruth Pabba Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Anitha &amp; Prashanth Jonnala Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Aruna &amp; Sudhakar Karumuri Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Dr.Aruna Mittapalli Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Geetha &amp; Sreedhar Chedalla Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Mrudula &amp; Manoj Singamsetti Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Pallavi &amp; Ranga Sriram Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Rajitha &amp; Krushna Dundigalla Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Ramya &amp; Satish Roddom Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Saritha &amp; Krishna Veerabrahma Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sarojini &amp; Subbarao Gopavarapu Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sharadha &amp; LN Rao Chilakala Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Srinivas Akula Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Srinivas Parsi Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Vasavi &amp; Sudhakar Chakka Garu
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-12" id="6" style="display: none;">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Hymavathi &amp; Suresh Sanka Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Latha &amp; Srinivas Tadakamalla Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Prasanna &amp; Shiva Sontha Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Rama &amp; Sudhakar Kalwa Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Ramesh Damisetty Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Vani &amp; Vamshi Gunturu Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Deepa &amp; Peddanna Akinapally Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Geetha &amp; Balanand Pinni Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Harini &amp; Siva Pasumarthi Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Kalyani &amp; Kiran Athuluri Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Krupa Oleti Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Lalitha &amp; Kiran Vooda Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Latha &amp; Naresh Vuthunuri Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Lavanya &amp; Kiran Bollam Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Laxman Kaparthy Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Mounica &amp; HaskarSanthosh kedarisetty Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Neelima &amp; Anil Boddu Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Niveditha &amp; Praveen Chinta Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Pramila &amp; Krishna Pachigolla Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Rashmi &amp; Nagendra Boggarapu Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Satya &amp; Ratnakar Karumuri Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sowmya &amp; Ram Bachu Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Srini Karnati Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Sushma &amp; Shankar Checka Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Syamala &amp; Rajesh Mandalapu Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Vandana &amp; Ram Kolluru Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Vani &amp; Praveen Singirikonda Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Vishali &amp; Naresh Potta Garu
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-12" id="7" style="display: none;">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Shruthi &amp; Raghu Vamsi Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Supriya &amp; Sailesh Maddi Garu
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                            <div class="donors-block">
                                <div>
                                    <img alt="" src="images/no-image1.jpg" />
                                </div>
                                <div class="donor-name">
                                    Krishna Charagondla Garu
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>