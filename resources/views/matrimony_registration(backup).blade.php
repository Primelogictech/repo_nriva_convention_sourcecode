@extends('layouts.user.base')
@section('content')
<style type="text/css">
    .card {
        background-color: transparent;
        border: 0px solid;
    }
    .card-header {
        padding: 0px;
    }
    .card-header a {
        background: #784d98;
        color: #fff;
        padding: 0.75rem 1.25rem;
        width: 100%;
        display: block;
    }
    .p-radio-btn {
        position: absolute;
        top: 3px;
    }
    .card-header a::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/plus.png);
    }
    .card-header a[aria-expanded="true"]::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/minus.png);
    }
</style>
<section class="container-fluid my-3 my-lg-5">
   <div class="container">
      <div class="row">
         <div class="col-12 shadow-small py-0 pt-1 px-1">
             <div class="row">
                <div class="col-12 pb-5" style="text-align:center;margin-top: 30px;">
                    <div>
                        <h4><b>Matrimony Registration</b></h4>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
               <button type="button" class="close" data-dismiss="alert">×</button>
               <strong>{{ $message }}</strong>
            </div>
            @endif
            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
               <button type="button" class="close" data-dismiss="alert">×</button>
               <strong>{{ $message }}</strong>
            </div>
            @endif
             @if ($message = Request::get('error'))
            <div class="alert alert-danger alert-block">
               <button type="button" class="close" data-dismiss="alert">×</button>
               <strong>This member not registred with nriva eedujodu. Please register in Nriva.org with eedujodu membership.</strong>
            </div>
            @endif
            <div class="row px-1 mx-0 mx-lg-5 px-lg-0">
               <p style="margin-bottom: 10px;"><b>Note : </b> Only NRIVA Matrimony registered members are allowed to register for convention matrimony. If you are not registered in nriva matrimony, Please visit <a href="https://nriva.org" target="_blank">www.nriva.org</a> to register for matrimony and comeback here to continue for convention matrimony registration</p>
               <div class="col-12 col-lg-12 shadow-small px-sm-20 p40 p-md-4 mb-4">
                  <form id='form' action="{{url('matrimony_registration')}}" method="post" enctype="multipart/form-data">
                     <div class="row">
                        @csrf
                        <!-- Personal Information -->
                        <div class="col-12">
                           <h5 class="text-violet py-2">Personal Information</h5>
                           <div class="form-row">
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Email Id: ( Your registered email Id with NRIVA matrimony )</label><span class="text-red">*</span>
                                 <div>
                                    <input type="email" required {{ Auth::user()? 'readonly' : '' }}  name="email" id="email" value="{{  Auth::user()->email ?? '' }}" class="form-control" placeholder="Email Id" required="" @if($user) readonly @endif />
                                 </div>
                                 
                                    
                              </div>
                               <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>NRIVA Profile Id:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" name="profile_id" id="profile_id" value="{{  $user->member_id ?? '' }}" class="form-control" required placeholder="Profile Id" @if($user) readonly @endif/>
                                 </div>
                              </div>
                              
                           </div>
                           
                           <div class="form-row">
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>First Name:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" name="first_name" id="first_name" value="{{  Auth::user()->first_name ?? '' }}" class="form-control" required placeholder="First Name" readonly />
                                 </div>
                              </div>
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Last Name:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" required name="last_name" id="last_name" value="{{  Auth::user()->last_name ?? '' }}" class="form-control" placeholder="Last Name" readonly />
                                 </div>
                              </div>
                           </div>
                           <div class="form-row">
                             <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Mobile:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" required name="phone" id="phone" value="{{  Auth::user()->mobile ?? '' }}" class="form-control" placeholder="Mobile" required="" readonly />
                                 </div>
                              </div>
                            {{--   <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Age:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" required name="age" id="age" value="{{  $user->age ?? '' }}" class="form-control" placeholder="Age" readonly/>
                                 </div>
                              </div> --}}
                                <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Gothram:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" name="gothram" id="gothram" value="{{  $user->gothram ?? '' }}" class="form-control" required placeholder="Gothram" readonly/>
                                 </div>
                              </div>
                           </div>
                           <div class="form-row">
                            
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Rasi:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" required name="rasi" id="rasi" value="{{  $user->star ?? '' }}" class="form-control" placeholder="Rasi" readonly/>
                                 </div>
                              </div>
                           </div>
                          
                        </div>
                        <!-- End of Personal Information -->
                        <!-- Contact Information -->
                        <div class="col-12">
                           <h5 class="py-2 text-violet">Contact Information</h5>
                           <div class="form-row">
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Address line 1:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" name="address1" value="{{ Auth::user()->address ?? "" }}" id="address1" class="form-control" placeholder="Address line 1"  readonly/>
                                 </div>
                              </div>
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Address line 2:</label>
                                 <div>
                                    <input type="text" name="address2" value="{{ Auth::user()->address2 ?? ""  }}" id="zip_code" class="form-control" placeholder="Address line 2" readonly/>
                                 </div>
                              </div>
                           </div>
                           <div class="form-row">
                            <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>City:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" name="city" value="{{ Auth::user()->city?? "" }}" id="city" class="form-control" placeholder="City"  readonly/>
                                 </div>
                              </div>
                              
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>State:</label><span class="text-red">*</span>
                                 <div>
                                    <!-- <select name="state" class="form-control" readonly>
                                       <option value="">Select State</option>
                                       @foreach ($states as $state)
                                       <option value="{{ $state->state }}" @if(Auth::user()) @if(Auth::user()->state==$state->state) {{'selected'}} @endif  @endif>{{ $state->state }}</option>
                                       @endforeach
                                    </select> -->
                                    <input type="text" class="form-control" name="state" readonly value="{{Auth::user()->state??''}}">
                                 </div>
                              </div>
                           </div>
                           <div class="form-row">
                              
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Zip Code:</label><span class="text-red">*</span>
                                 <div>
                                    <input type="text" name="zipcode" value="{{ Auth::user()->zip_code ?? ""  }}" id="zip_code" class="form-control" placeholder="Zip Code" readonly />
                                 </div>
                              </div>
                              <div class="form-group col-12 col-md-6 col-lg-6">
                                 <label>Country:</label><span class="text-red">*</span>
                                 <div>
                                    <!-- <select name="country" required class="form-control" readonly>
                                       <option>Select Country</option>
                                       <option selected>United States</option>
                                    </select> -->
                                    <input type="text" class="form-control" name="country" readonly value="{{Auth::user()->country??''}}">
                                 </div>
                              </div>
                           </div>

                            <div class="col-12 col-sm-6 col-md-6 my-auto">
                                    <div class="pb-4 pb-md-2">
                                        <input type="checkbox" class="input-checkbox " required name="agrement">
                                        <span class="pl25 fs16 pointer text-orange font-weight-bold"  data-toggle="modal" data-target="#t_and_c" >I accept Terms and Conditions</span>
                                    </div>
                                </div>

                            <div class="form-row">
                            <div class="col-12 col-sm-6 col-md-12 my-2 my-md-auto">
                                    <div class="text-center text-sm-right">
                                        <input type="submit" class="btn btn-lg btn-danger text-uppercase px-5 " value="Submit" name="">
                                        
                                    </div>
                                </div>
                            </div>

                        </div>
                     </div>
                  </form>
               </div>
            <div class="col-12">
            <div id="accordion" class="o-accordion">
                <ul class="list-unstyled">
                    <li class="pb15 mb15 border-bottom">
                        <div class="card">
                            <div class="card-header">
                                <a class="card-link fs18" data-toggle="collapse" href="#collapseOne">
                                    What are the Matrimonial Events conducted and their Schedule?
                                </a>
                            </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                <div class="card-body p20">
                                    <ul class="list-unstyled matrimony-details-list fs16">
                                        <li>
                                            Young Adult Social Mixer at Hyatt Place in Suburban Collection Showplace.
                                            <span class="text-danger font-weight-bold">(July 4<span class="fs13">th</span>&nbsp;7.00pm – 10.00pm)</span>
                                        </li>
                                        <li>
                                            Parents Meet up <span class="text-danger font-weight-bold">(July 5<span class="fs13">th</span>&nbsp;, 9.00 am to 2.30pm)</span>
                                        </li>
                                        <li>
                                            Young Adults Meet up <span class="text-danger font-weight-bold">(July 5<span class="fs13">th</span>&nbsp;3.00pm to 7.00pm)</span>
                                        </li>
                                        <li>
                                            One–On-One Sessions for Parents and Young Adults&nbsp;
                                            <span class="text-danger font-weight-bold">(July 6th , between 9.00 am to 5.30 pm by appointment<span class="fs13">&nbsp;</span>)</span>
                                        </li>
                                        <li>Matrimony Networking &amp; Consulting Session&nbsp;<span class="text-danger font-weight-bold">(July 6th 2.00pm to 5.00pm)</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="pb15 mb15 border-bottom">
                        <div class="card">
                            <div class="card-header fs18">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                    How do you register for Matrimony Events?
                                </a>
                            </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-unstyled matrimony-details-list fs16">
                                        <li>Click on the <a class="text-danger font-weight-bold" href="#matrimony-registration">(Registration link)</a>&nbsp;on this page</li>
                                        <li>
                                            If you have registered in the previous years for Matrimony, use your Eedu Jodu ID to Resubmit otherwise register as New.
                                        </li>
                                        <li>
                                            If your Eedu Jodu ID does not work,&nbsp; Register as New.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="pb15 mb15 border-bottom">
                        <div class="card">
                            <div class="card-header fs18">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                    Do Parents need a Separate Registration?
                                </a>
                            </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <p class="mb-0">
                                        No. Parents Registration is linked with the Respective Groom/Bride Matrimony Convention Registration
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="pb15 mb15 border-bottom">
                        <div class="card">
                            <div class="card-header fs18">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                                    Are Walk-ins allowed?
                                </a>
                            </div>
                            <div id="collapseFour" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <p class="mb-0">
                                        Walk-ins are Welcome, but they do not get all the Registration Privileges
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="pb15 mb15 border-bottom">
                        <div class="card">
                            <div class="card-header fs18">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseFive">
                                    What are the Registration Privileges?
                                </a>
                            </div>
                            <div id="collapseFive" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-unstyled matrimony-details-list fs16">
                                        <li>Designated Priority Seating</li>
                                        <li>Better reach of your Profile</li>
                                        <li>Assigned Relationship Manager Assistance</li>
                                        <li>Access to Printed Profile Directory</li>
                                        <li>Opt for One-On-One Sessions</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="pb15 mb15 border-bottom">
                        <div class="card">
                            <div class="card-header fs18">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseSix">
                                    Do you Charge for the Registration?
                                </a>
                            </div>
                            <div id="collapseSix" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-unstyled matrimony-details-list fs16">
                                        <li>
                                            All these Matrimony Events are Sponsored and Paid by NRIVA.
                                        </li>
                                        <li>
                                            During Social Mixer Event, Liquor is available for purchase
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="pb15 mb15 border-bottom">
                        <div class="card">
                            <div class="card-header fs18">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseSeven">
                                    What are the Highlights of Young Adults Social Mixer Event?
                                </a>
                            </div>
                            <div id="collapseSeven" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-unstyled matrimony-details-list fs16">
                                        <li>Ice Breaking Sessions/DJ</li>
                                        <li>Social Hour</li>
                                        <li>Fun Activities</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="pb15 mb15 border-bottom">
                        <div class="card">
                            <div class="card-header fs18">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseEight">
                                    How is Parents meet conducted?
                                </a>
                            </div>
                            <div id="collapseEight" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-unstyled matrimony-details-list fs16">
                                        <li>
                                            All Participating Parents are divided into small groups with similar interest.
                                        </li>
                                        <li>
                                            These Small Groups are formed&nbsp; based on your Partner Prefernce
                                        </li>
                                        <li>
                                            Every Family gets an Opportunity to be introduced to all the available Profiles
                                        </li>
                                        <li>
                                            Printed Profile Directories of Convention attendees will be provided
                                        </li>
                                        <li>
                                            Once participants identify their prospective matches, they can opt for One-On-One Session
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="pb15 mb15 border-bottom">
                        <div class="card">
                            <div class="card-header fs18">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseNine">
                                    How is Young Adults meet conducted?
                                </a>
                            </div>
                            <div id="collapseNine" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-unstyled matrimony-details-list fs16">
                                        <li>
                                            Default seating arrangement is made based on your partner prefernce.
                                        </li>
                                        <li>
                                            Seating arrangements can be reviewed and particpants have option to change if logistics permit
                                        </li>
                                        <li>
                                            Once participants identify their prospective matches, they can opt for One-On-One Session
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="pb15 mb15 border-bottom">
                        <div class="card">
                            <div class="card-header fs18">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseTen">
                                    We are not an active profile as off now, Can we attend this event for our future requirement?
                                </a>
                            </div>
                            <div id="collapseTen" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <p class="mb-0">
                                        Yes, You can attend. You are considered as Walk-in and will be seated in the location designated for walk-ins.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
         </div>
      </div>
   </div>
</section>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Registration</h4>
        </div>

        <div class="modal-body">
            <div class="alert alert-success">Please enter nriva password and procced to registration</div>
            <form class="" action="{{url('newAuthorize')}}" method="post">
                 @csrf
                <input type="hidden" name="email" id="new_email">
             <div class="form-row">
                    
                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Password:</label><span style="color:red;">*</span>
                        <div>
                            <input type="password" name="password" id="password"  class="form-control" placeholder="Enter Password" required />
                        </div>
                    </div>
                </div>
                 <button type="submit" class="btn btn-primary">Submit</button>
            </form>

          
        </div>
         <div class="modal-footer">
        <a href="{{url('/')}}" class="btn btn-danger" >Close</a>
      </div>
        
      </div>
      
    </div>
  </div>

  <div class="modal fade" id="t_and_c" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Terms and Conditions</h4>
        </div>

        <div class="modal-body">
            <div class="">
                <ul>
                    <li>Terms and Conditions</li>
                    <li>Terms and Conditions</li>
                    <li>Terms and Conditions</li>
                    <li>Terms and Conditions</li>
                </ul>
            </div>
        </div>
         <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
        
      </div>
      
    </div>
  </div>


@section('javascript')
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
<script>

     $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        });
          $.validator.addMethod("numberss", function(value, element) {
            return this.optional(element) || value == value.match(/^[0-9) (-]+$/);
        });


        // $("#form").validate({
           
            
        //     rules: {
                
        //         first_name: "required alpha",
        //         last_name: "required alpha",
        //         age: "required numberss",
        //         gothram: "required",
        //         rasi: "required",
        //         state: "required",
        //         country: "required",
        //         address: "required",
        //         email: "required",
        //         mobile :{
        //             maxlength: 14,
        //             numberss:true
        //         },
        //         'zip_code': {
        //                 digits: true
        //             },
        //     },
        //      messages: {
               
        //           "first_name": {
        //             alpha: "First name should not contain numbers.",
        //         },
        //          "last_name": {
        //             alpha: "Last name should not contain numbers.",
        //         },
                 
        //          "mobile": {
        //             maxlength: "Max length is  14.",
        //             numberss: "Numbers only"
        //         }
        //     },
        // });

 @if(!Auth::user())
$('#email').on('change',function(){
        checking();

    });
$('#profile_id').on('change',function(){
        checking();

    });
function checking() {
  $('.email_valid_msg').text('')
        var email = $('#email').val();
        var profile_id = $('#profile_id').val();
        $.ajax({
            type: 'GET',
            url: "{{url('check_matrimony_email')}}?email="+email+"&profile_id="+profile_id,
            success:function(data){
              if(data!="failed"){
                $('#new_email').val(data);
                $('#myModal').modal({backdrop: 'static', keyboard: false});
                $('#myModal').modal('show');
              }else{
                window.location.href = "{{url('matrimony_registration')}}?error=not_registered";
              }
             
           
        }
      });
}
@endif
</script>

@endsection

@endsection