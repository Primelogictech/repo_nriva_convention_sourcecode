<style type="text/css">
    .card {
        background-color: transparent;
        border: 0px solid;
    }
    .card-header {
        padding: 0px;
    }
    .card-header a {
        background: #784d98;
        color: #fff;
        padding: 0.75rem 1.25rem;
        width: 100%;
        display: block;
    }
    .p-radio-btn {
        position: absolute;
        top: 3px;
    }
    .card-header a::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/plus.png);
    }
    .card-header a[aria-expanded="true"]::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/minus.png);
    }
    .flower-list li {
        background: url(images/flower-icon1.png) no-repeat scroll left 3px;
    }
</style>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="text-violet text-center mb-4">Frequently Asked Questions</h5>
                                <div id="accordion" class="o-accordion">
                                    <ul class="list-unstyled">
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header">
                                                    <a class="card-link fs18" data-toggle="collapse" href="#collapseOne">
                                                        Is breakfast available in all Hotels
                                                    </a>
                                                </div>
                                                <div id="collapseOne" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Breakfast is available only in Convention Center Hotel and Hyatt Regency
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                                        Is the transportation available from Hotels to Convention Center
                                                    </a>
                                                </div>
                                                <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            We provide shuttle from
                                                        </p>
                                                        <p>
                                                            Renaissance Schaumburg Convention Center Hotel<br />
                                                            1551 N. THOREAU DR.,<br />
                                                            Schaumburg, Illinois 60173, United States
                                                        </p>
                                                        <p>To</p>
                                                        <p>
                                                            Hyatt Regency Schaumburg, Chicago<br />
                                                            1800 East Golf Road<br />
                                                            Schaumburg, Illinois 60173 United States
                                                        </p>
                                                        <p>Only</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                                        I am a Patron Donor($1500) and entitled to only 2nights which 2 night will i be assigned rooms
                                                    </a>
                                                </div>
                                                <div id="collapseThree" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Check in on Saturday July 2nd and Check out on Monday July 4th
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                                                        I am a Patron Donor ($1500) and entitled to only 2nights what if i want 3rd night
                                                    </a>
                                                </div>
                                                <div id="collapseFour" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Please send a request to <a href="mailto: hotels@nriva.org" target="_blank">hotels@nriva.org</a> and we will reserve the room for 3rd night and you are responsible for 3rd night
                                                            payment including taxes
                                                        </p>
                                                        <p>
                                                            Please use the below link to book additional room/days as per your need.
                                                        </p>
                                                        <div>
                                                            <a href="https://www.hyatt.com/en-US/group-booking/CHIRW/G-NVIB/OPEN" target="_blank">https://www.hyatt.com/en-US/group-booking/CHIRW/G-NVIB/OPEN</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseFive">
                                                        I am a Patron (No Hotel) Donor ($1250) and need rooms to stay where can I book
                                                    </a>
                                                </div>
                                                <div id="collapseFive" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            There are hotels nearby convention center which you can book. Discount codes will be updated soon please look back in Preferred Hotels section
                                                        </p>
                                                        <p>
                                                            Please use the below link to book additional room/days as per your need.
                                                        </p>
                                                        <div>
                                                            <a href="https://www.hyatt.com/en-US/group-booking/CHIRW/G-NVIB/OPEN" target="_blank">https://www.hyatt.com/en-US/group-booking/CHIRW/G-NVIB/OPEN</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseSix">
                                                        We are travelling with Parents/Family and need additional rooms how can we request
                                                    </a>
                                                </div>
                                                <div id="collapseSix" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>Please send your request to <a href="mailto: hotels@nriva.org" target="_blank">hotels@nriva.org</a> and we will try to accommodate but cannot guarantee in the same hotel</p>
                                                        <p>
                                                            Please use the below link to book additional room/days as per your need.
                                                        </p>
                                                        <div>
                                                            <a href="https://www.hyatt.com/en-US/group-booking/CHIRW/G-NVIB/OPEN" target="_blank">https://www.hyatt.com/en-US/group-booking/CHIRW/G-NVIB/OPEN</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseSeven">
                                                        Are there discount codes for Flights
                                                    </a>
                                                </div>
                                                <div id="collapseSeven" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            No discount codes available for Flights
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseEight">
                                                        Which Airport is closer to Convention Center
                                                    </a>
                                                </div>
                                                <div id="collapseEight" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Chicagoland has 2 Major airports Ohare (ORD) and Midway (MDW). Ohare is closer to the convention center which is 20minutes away
                                                        </p>
                                                        <p>
                                                            Midway is about 1 hour away
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseNine">
                                                        Any Sightseeing is provided for the Convention Attendees
                                                    </a>
                                                </div>
                                                <div id="collapseNine" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Please look back on the convention website for more details in near future
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseTen">
                                                        Whom can we contact if we have any questions
                                                    </a>
                                                </div>
                                                <div id="collapseTen" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            For Registration questions please email to <a href="mailto: registrations@nriva.org" target="_blank">registrations@nriva.org</a> For Hotels and accommodations questions please
                                                            email to <a href="mailto: hotels@nriva.org" target="_blank">hotels@nriva.org</a> For all other questions please use Contact Us (
                                                            <a href="https://convention.nriva.org/contact-us" target="_blank">https://convention.nriva.org/contact-us</a> ) page
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseEleven">
                                                        Is transportation provided from Airports to Convention Centers
                                                    </a>
                                                </div>
                                                <div id="collapseEleven" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Please arrange your own transportation to and from the Airport for additional information please see under Transportation section
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwelve">
                                                        I am eligible for 3night hotel accommodation can I choose which 3days
                                                    </a>
                                                </div>
                                                <div id="collapseTwelve" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Unfortunately no and you will have to Checkin on Saturday July 2nd at 4:00PM and Checkout on Tuesday July 5th at 11:00AM
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseThirteen">
                                                        We are planning to come on July 1st and like to have a room in Convention Hotel(Renaissance) how can we book it.
                                                    </a>
                                                </div>
                                                <div id="collapseThirteen" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            We have no availability at Convention Hotel(Renaissance) for July 1st check in and you can book at our other discounted hotel Hyatt. You can click on Book now button on the Hotels
                                                            page and able to book it. Thank you for Understanding.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseFourteen">
                                                        Do we get an acknowledgment email after the registration is complete?
                                                    </a>
                                                </div>
                                                <div id="collapseFourteen" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Yes a email will be sent with registration id and benefits.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseFifteen">
                                                        How many Admission tickets do I get assigned as a Donor?
                                                    </a>
                                                </div>
                                                <div id="collapseFifteen" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Let us explain this using an example.
                                                        </p>
                                                        <p>
                                                            For a Platinum Sponsor the flyer says 4VVIP Tickets and 8 VIP Tickets. It means the total tickets you will get are 8 tickets out of which 4 are VVIP tickets and 4 VIP Tickets.
                                                            Similar logic applies for other Donor types as well.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseSixteen">
                                                        We are 4 people attending the convention and need Double Bedroom as part of our package how can we get it.
                                                    </a>
                                                </div>
                                                <div id="collapseSixteen" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Please send a request to <a href="mailto: hotels@nriva.org" target="_blank">hotels@nriva.org</a> and we will try to accommodate the request. Please provide the registration id
                                                            along with the email.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseSeventeen">
                                                        Where is the registration desk to collect the admission tickets?
                                                    </a>
                                                </div>
                                                <div id="collapseSeventeen" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            There is a separate registration desk at the Convention center where you can pick the Registration package along with admission tickets. You can collect this before or after your
                                                            hotel checkin.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseEighteen">
                                                        What are the timings shuttle service will be available between Convention center and Hyatt hotel?
                                                    </a>
                                                </div>
                                                <div id="collapseEighteen" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p class="mb-1">07/02 - 2 PM to Midnight</p>
                                                        <p class="mb-1">07/03 - 7 AM to Midnight</p>
                                                        <p class="mb-1">07/04 - 7 AM to Midnight</p>
                                                        <p>
                                                            There are 3 shuttles that will run every few minutes between the hotel and convention center.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseNineteen">
                                                        Do we have Security during the convention?
                                                    </a>
                                                </div>
                                                <div id="collapseNineteen" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            We have security planned in the convention center, and Armed Personnel at the convention center. Though we planned for security it is everyone's responsibility to be aware of their
                                                            surroundings and if you are driving your own cars pick up your family members at the entrance, so no one is walking in the parking lots by themselves.
                                                        </p>
                                                        <p>
                                                            Both the Hotels are in a very safe suburbs of Chicago and are centrally located. Buses will pick up at the entrance and drop off at the entrance. Also, both the hotels have the
                                                            provision for safety locker boxes. But please as a general practice be safe and no arrangements are fool proof. Use your own caution and judgement regarding your valuables and
                                                            property.
                                                        </p>
                                                        <p>
                                                            We will work with the security committee to provide some basic guidelines for security. Considering the current conditions, we have boosted the security more than what we normally
                                                            do.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 shadow-small py-0 pt-1 px-1 mt-4">
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="text-violet text-center mb-4">Shathamanam Bhavathi FAQ's</h5>
                                <div id="accordion" class="o-accordion">
                                    <ul class="list-unstyled">
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header">
                                                    <a class="card-link fs18" data-toggle="collapse" href="#sbcollapseOne">
                                                        What is the event ?
                                                    </a>
                                                </div>
                                                <div id="sbcollapseOne" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Shastabdhya Purti.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseTwo">
                                                        Venue of the event?
                                                    </a>
                                                </div>
                                                <div id="sbcollapseTwo" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Schaumburg, IL, Convention Centre BallRoom-B.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseThree">
                                                        What is the event ?
                                                    </a>
                                                </div>
                                                <div id="sbcollapseThree" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Date: 07/03/2022 Sunday.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseFour">
                                                        What time the event starts and how long it is going to be?
                                                    </a>
                                                </div>
                                                <div id="sbcollapseFour" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            The event is going to start at 12 o'clock noon and will go till 4pm approx.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseFive">
                                                        What age group is eligible for this event?
                                                    </a>
                                                </div>
                                                <div id="sbcollapseFive" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Anyone who is 60+ & above, 70+, 80+ .
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseSix">
                                                        Who is eligible for the Sowbhagyam packet?
                                                    </a>
                                                </div>
                                                <div id="sbcollapseSix" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Only the parents who are registered for the Shatamanam Bhavathi will receive the sowbhagyam packet(Pasupu /Kumkum gift bag).
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseSeven">
                                                        Do we need to bring anything?
                                                    </a>
                                                </div>
                                                <div id="sbcollapseSeven" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            You should dress up nicely as it's your special day and also come with your kids and family. We want you to take tons & tons of happiness with you.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseEight">
                                                        Is there any particular clothing attire we should follow?
                                                    </a>
                                                </div>
                                                <div id="sbcollapseEight" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Dress up traditionally, if possible wear Pattu saree and Panchi-vani (wear comfortable clothing ).
                                                        </p>
                                                        <h6 class="fs18">
                                                            Whom to communicate with?
                                                        </h6>
                                                        <p>
                                                            Convention team will communicate the event timings and which room to come and what time exactly the program starts Keep watching for the whatsapp messages
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseNine">
                                                        Are we having any meetings prior to the event?
                                                    </a>
                                                </div>
                                                <div id="sbcollapseNine" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            Zoom meetings are going to be conducted prior to the event for the easy flow.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#sbcollapseTen">
                                                        Shathamanam Bhavathi timings and the events:
                                                    </a>
                                                </div>
                                                <div id="sbcollapseTen" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <h6 class="mb-3">SHATHAMAANAM BHAVATHI</h6>
                                                        <ul class="flower-list fs16">
                                                            <li>12 noon : procession with parents</li>
                                                            <li>12:15 deeparadhana</li>
                                                            <li>12:30 anugna</li>
                                                            <li>12:45 vigneshwara puja</li>
                                                            <li>1:00 Sankalpam</li>
                                                            <li>1:15 punyahavaachanam</li>
                                                            <li>1:30 108 Kalasha stapanam</li>
                                                            <li>2:00 Sri Rudra japam</li>
                                                            <li>Ayushya japam</li>
                                                            <li>Maha mruthynjaya japam</li>
                                                            <li>Sahasra chandra japam</li>
                                                            <li>Sudarshana japam & puja Archana aarathi.</li>
                                                            <li>3:00 paada puja</li>
                                                            <li>3:15 mala dharana & Kumkuma dharana</li>
                                                            <li>3:30 aarathi</li>
                                                            <li>3:45 prasada viniyog</li>
                                                            <li>4:00 shanti mantra.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>