         <div class="row mt-2">
             <div class="col-12 payment_types_div">
                <h5 class="text-violet py-2">Payment Types</h5>
                <div class="row pb-2">
                    @foreach ($RegistrationContent->patment_types as $patment_type)
                    <div class="col-6 col-md-3 col-lg-2">
                        <div class="position-relative {{$RegistrationContent->paymenttype( $patment_type)->name }}_payment_div">
                            <input type="radio" id="payment_{{$RegistrationContent->paymenttype( $patment_type)->name }}" class="p-radio-btn payment-button" data-name="{{$RegistrationContent->paymenttype( $patment_type)->name }}" value="{{$RegistrationContent->paymenttype( $patment_type)->id }}" name="payment_type" />
                            <span class="fs16 pl-4 pl-md-4">{{ucfirst($RegistrationContent->paymenttype( $patment_type)->name )}}</span>
                        </div>
                    </div>

                     {{-- <span class="position-relative pr-3"><input type="radio" class="p-radio-btn" name="payment_type" /><span class="fs16 pl-4 pl-md-4">Cheque/Cash</span></span> --}}
                     @endforeach
                     
                     @if(Auth::user()) 
                       @if(Request::segment(1)=="exhibits-reservation" &&Auth::user()->free_exbhit_registrations > 0 )

                       <div class="col-6 col-md-3 col-lg-2 free_registration">
                        <div class="position-relative">
                            <input type="radio" id="payment_free" class="p-radio-btn payment-button" data-name="free" value="free" name="payment_type" />
                            <span class="fs16 pl-4 pl-md-4">Included in Sponsorship benefits Of Your Doner Package</span>
                        </div>
                    </div>

                        @endif
                       @endif
                     <div class="payment-button error"></div>
                 </div>
             </div>
         </div>
         <hr class="dashed-hr">
         <div class="row">
             <!-- paypal payment -->

             <div class="col-12" id="paypal" style="display: none;">
                 <h5 class="text-violet mb-3">Payment by using Paypal</h5>
                 <div class="paypal-note">
                 </div>
                 <div class="form-row">
                 </div>
             </div>

             <!-- paypal payment end -->

             <div class="col-12" id="check_payment" style="display: none;">
                 <h5 class="text-violet mb-3">Payment by using check</h5>
                 <div class="check-note">
                 </div>
                 <div class="form-row">
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                         <label class="mb3">Check  Number: </label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="cheque_number"  class="form-control cheque_form_field" placeholder="Check  Number">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                         <label class="mb3">Bank Name:</label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="bank_name"  class="form-control cheque_form_field" placeholder="Check  Number">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                         <label class="mb3">Check Date </label><span class="text-red">*</span>
                         <div>
                             <input type="date" name="cheque_date"  class="form-control cheque_form_field" placeholder="Check  Date">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                         <label class="mb3">Check Received By:</label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="check_received_by"  class="form-control cheque_form_field" placeholder="Check Received By">
                         </div>
                     </div>
                 </div>
             </div>

             <!-- zelle -->
             <div class="col-12" id="zelle" style="display: none;">
                 <h5 class="text-violet mb-3">Payment by using Zelle</h5>
                 <div class="zelle-note">
                 </div>
                 <div class="form-row">
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                         <label class="mb3">Zelle Reference Number</label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="Zelle_Reference_Number"  class="form-control zelle_form_field" placeholder="Zelle Reference Number">
                         </div>
                     </div>
                 </div>
             </div>
             <!-- zelle end -->
             <!-- othre Payments -->
             <div class="col-12" id="other" style="display: none;">
                 <h5 class="text-violet mb-3">Payment by using other payments</h5>
                 <div class="other-note">
                 </div>
                 <div class="form-row">
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                         <label class="mb3">On Behalf Of </label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="payee_name"  class="form-control other_payment_form_field" placeholder="Payee Name">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                         <label class="mb3">Company Name</label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="company_name"  class="form-control other_payment_form_field" placeholder="Company Name">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                         <label class="mb3">Transaction Date </label><span class="text-red">*</span>
                         <div>
                             <input type="date" name="transaction_date"  class="form-control other_payment_form_field" placeholder="Transaction Date">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                         <label class="mb3">Payment Reference Number</label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="transaction_id"  class="form-control other_payment_form_field" placeholder="Payment Reference Number">
                         </div>
                     </div>
                      <div class="form-group col-12 col-md-6 col-lg-6 mb-3">
                                            <label class="mb3">Please upload supporting document </label><span class="text-red">*</span>
                                            <div>
                                                <input type="file" name="other_document"  class="form-control other_payment_form_field" style="height: 44px;">
                                            </div>
                                        </div>


                 </div>
             </div>
             <!-- othre Payments end -->

             @section('eaxtra-javascript')
             <script>
                 paymenttypes = ({!!html_entity_decode($paymenttypes) !!})
                 $(".payment-button").click(function(e) {
                     if ($(this).prop("checked")) {
                         if ($(this).data('name') == "{{ config('conventions.paypal_name_db') }}") {
                             $('#paypal').show();
                             $('#check_payment').hide();
                             $('#zelle').hide();
                             $('#other').hide();
                             paymenttypes.forEach(data => {
                                 if (data.name == "{{ config('conventions.paypal_name_db') }}") {
                                     $('.paypal-note').html(data.note)
                                 }
                             });
                             $('.cheque_form_field').prop("required", false);
                             $('.zelle_form_field').prop("required", false);
                             $('.other_payment_form_field').prop("required", false);

                         }

                         if ($(this).data('name') == "{{ config('conventions.check_name_db') }}") {
                             $('#check_payment').show();
                             $('#paypal').hide();
                             $('#zelle').hide();
                             $('#other').hide();

                             paymenttypes.forEach(data => {
                                 if (data.name == "{{ config('conventions.check_name_db') }}") {
                                     $('.check-note').html(data.note)
                                 }
                             });
                             $('.cheque_form_field').prop("required", true);
                             $('.zelle_form_field').prop("required", false);
                             $('.other_payment_form_field').prop("required", false);
                         }

                         if ($(this).data('name') == "{{ config('conventions.zelle_name_db') }}") {
                             $('#zelle').show();
                             $('#paypal').hide();
                             $('#check_payment').hide();
                             $('#other').hide();
                             paymenttypes.forEach(data => {
                                 if (data.name == "{{ config('conventions.zelle_name_db') }}") {
                                     $('.zelle-note').html(data.note)
                                 }

                             });
                             $('.zelle_form_field').prop("required", true);
                             $('.cheque_form_field').prop("required", false);
                             $('.other_payment_form_field').prop("required", false);

                         }
                         if ($(this).data('name') ==  "{{ config('conventions.other_name_db') }}") {

                             $('#other').show();
                             $('#zelle').hide();
                             $('#paypal').hide();
                             $('#check_payment').hide();
                             paymenttypes.forEach(data => {
                                 if (data.name == "{{ config('conventions.other_name_db') }}") {
                                     $('.other-note').html(data.note)
                                 }
                             });
                             $('.zelle_form_field').prop("required", false);
                             $('.cheque_form_field').prop("required", false);
                             $('.other_payment_form_field').prop("required", true);

                         }
                         //zelle
                     }
                 });
             </script>
             @endsection
