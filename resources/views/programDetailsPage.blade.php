@extends('layouts.user.base')
@section('content')

<style type="text/css">
    p{
        font-size: 16px;
        margin-bottom: 20px;
    }
</style>
    

<section class="container-fluid my-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12 col-md-5 col-lg-4 my-1 px-0 px-md-3">
                <div class="p5 border">
                    <img src="{{asset(config('conventions.program_display').$program->image_url)}}" class="img-fluid w-100" alt="lakshmi kalayanam">
                </div>
            </div>
            <div class="col-12 col-md-7 col-lg-8 shadow-small px-4 py-3 my-1">
                <h5 class="text-danger">Contact Details:</h5>
                <h5><b>Chair :</b></h5>
                <?php $j=1; ?>
                @foreach($Committe_members as $member)

                @if($member->designation->name == "Chair")
                <div class="fs16">
                    <label>{{$j++}}.</label>

                    
                    <span class="text-skyblue">{{$member->member->name}}</span>
                </div>
                @endif
                @endforeach
                <!-- <div class="fs16">
                    <label><i class="fas fa-mobile-alt"></i> :</label>
                    <span class="text-skyblue">{{$program->mobile_number}}</span>
                </div> -->
                <h5><b>Co-Chairs :</b></h5>
                <?php $i=1; ?>
                 @foreach($Committe_members as $member)

                @if($member->designation->name == "Co-Chair")
                <div class="fs16">
                    <label>{{$i++}}.</label>
                    <span class="text-skyblue">{{$member->member->name}}</span>
                </div>
                @endif
                @endforeach
            </div>
        </div>

    
    
     <h6>Program Date and Time : <b>{!! $program->date  !!} {!! $program->programtime  !!} </b></h6>
     

      {!! $program->page_content  !!}




    </div>
</section>



@section('javascript')

@endsection


@endsection