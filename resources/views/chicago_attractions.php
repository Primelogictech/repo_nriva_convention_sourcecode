<style type="text/css">
	.fade:not(.show) {
        opacity: 1;
    }
    .nav-link {
	    color: #050a30;
	}
    .sc-tabs .nav-link.active {
	    border-bottom: none !important;
	    background-color: #050A30;
	    border-left: 0px;
	    border-right: 0px;
	    border-top: 0px;
	    color: #ffffff;
	}
</style>

<div class="container">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-10 offset-lg-1">
            <div class="row">
                <div class="col-12">
                    <h4 class="text-center mb-4 text-orange">Chicago Attractions</h4>
                </div>
                <div class="col-12">
                    <div>
                        <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll" role="tablist">
                            <li class="nav-item d-inline-flex">
                                <a class="nav-link px-2 px-sm-3 px-md-4 border active" href="#architectural" role="tab" data-toggle="tab">Architectural&nbsp;/&nbsp;Cultural Tours</a>
                            </li>
                            <li class="nav-item d-inline-flex">
                                <a class="nav-link px-2 px-sm-3 px-md-4 border" href="#attractions" role="tab" data-toggle="tab">Attractions</a>
                            </li>
                            <li class="nav-item d-inline-flex">
                                <a class="nav-link px-2 px-sm-3 px-md-4 border" id="reg-page" href="#beaches" role="tab" data-toggle="tab">Beaches&nbsp;/&nbsp;Gardens&nbsp;/&nbsp;Parks</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="architectural">
                            <div class="col-12 shadow-small py-2">
                                <div class="row px-3">
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/Chicago_Segway_Tours.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Chicago Segway Tours: </span>
                                                    <span class="fs15">Explore Chicago in the day or night on a Segway tour.</span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>300 East Monroe, Chicago, IL 60601, USA</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(312)-552-5100</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://chicagosegways.com/" target="_blank" class="text-decoration-underline text-break fs15">https://chicagosegways.com/</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/Chicago_Architecture_Center.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Chicago Architecture Center: </span>
                                                    <span class="fs15">Enjoy Chicago’s mesmerizing architecture on boat, bus, walking, bike, or Segway on of their 85 tours.</span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>111 E. Wacker Dr. Chicago, IL 60601</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(312)-922-3432</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://www.architecture.org/" target="_blank" class="text-decoration-underline text-break fs15">https://www.architecture.org/</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/Chicago_Line_Cruises.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Chicago Line Cruises: </span>
                                                    <span class="fs15">Chill the evening cruising the Chicago river or the Michigan lake and take the architecture tour in one of their three vessels.</span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>465 North McClurg Ct. Chicago, IL 60611</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(312)-527-2002</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://www.chicagoline.com/" target="_blank" class="text-decoration-underline text-break fs15">https://www.chicagoline.com/</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/wendellaboats.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Wendella Tours & Cruises: </span>
                                                    <span class="fs15">Chicago’s original live narrated boat tours highlighting Chicago’s history and architecture.</span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>400 N Michigan Ave, Chicago, IL 60611</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(312) 337-1446</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="wendellaboats.com" target="_blank" class="text-decoration-underline text-break fs15">wendellaboats.com</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Attractions -->

                        <div role="tabpanel" class="tab-pane fade in" id="attractions">
                            <div class="col-12 shadow-small py-2">
                                <div class="row px-3">
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/360_Chicago.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">360 Chicago Observation Deck: </span>
                                                    <span class="fs15">Enjoy the 360degree breath-taking view of the Chicago skyline and lakefront from 1,000feet above the Magnificent Mile. Experience the view with a “TILT” from the John Hancock Tower. </span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>875 North Michigan Avenue</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(888)-875-8439</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://360chicago.com/" target="_blank" class="text-decoration-underline text-break fs15">https://360chicago.com/</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/skydeck.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Skydeck Chicago: </span>
                                                    <span class="fs15">Glass ledge and the 360degree Skydeck on the tallest building offers view spanning 4 states and across 50miles on a clear sky day!</span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>233 S Wacker Dr, Chicago, IL 60606</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(312)-875-9447</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://theskydeck.com/" target="_blank" class="text-decoration-underline text-break fs15">https://theskydeck.com/</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/navy_pier.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Navy Pier: </span>
                                                    <span class="fs15">City’s most visited destination featuring more than 50acres of parks, shops, live music, restaurants, and attractions on the shoreline of lake Michigan. Best place to watch the 4th of July fireworks.</span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>600 E Grand Ave, Chicago, IL 60611</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(312) 595-7437</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://navypier.org/" target="_blank" class="text-decoration-underline text-break fs15">https://navypier.org/</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/msichicago.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Museum of Science and Industry: </span>
                                                    <span class="fs15">Explore and learn at the largest science museum in the Western Hemisphere. Fun filled hands-on learning experience.</span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>5700 S DuSable Lake Shore Dr, Chicago, IL 60637</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(773)-684-1414</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://www.msichicago.org/" target="_blank" class="text-decoration-underline text-break fs15">https://www.msichicago.org/</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/city_pass.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Chicago CityPass: </span>
                                                    <span class="fs15">Save upto 50% off combined admission to Chicago’s top attractions, including the Shedd Aquarium.</span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>233 S Wacker Dr, Chicago, IL 60606</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(888)-330-5008</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://www.citypass.com/" target="_blank" class="text-decoration-underline text-break fs15">https://www.citypass.com/</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/artic.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Art Institute of Chicago: </span>
                                                    <span class="fs15">Voted the #1 museum in the world by TripAdvisor. Place where Swami Vivekananda introduced the eastern culture to the west with his groundbreaking speech on Hinduism at the World’s Parliament of Religions.</span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>111 S Michigan Ave, Chicago, IL 60603</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(312)-443-3600</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://www.artic.edu/" target="_blank" class="text-decoration-underline text-break fs15">https://www.artic.edu/</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/bahai_temple.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Bahai House of Worship: </span>
                                                    <span class="fs15">One of the only eight continental Baha’i houses of worship in the world - like the lotus temple of New Delhi.</span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>100 Linden Ave, Wilmette, IL 60091</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(847)-853-2300</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://www.bahai.us/bahai-temple/" target="_blank" class="text-decoration-underline text-break fs15">https://www.bahai.us/bahai-temple/</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/ipzoo.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Lincoln Park Zoo: </span>
                                                    <span class="fs15">Enjoy the free 35-acre and fourth oldest zoos in the North America. Kids can enjoy a quick train ride, carousel, and meet the animals!</span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>2158-2202 N Cannon Dr, Chicago, IL 60614</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(312)-742-2000</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://www.lpzoo.org/" target="_blank" class="text-decoration-underline text-break fs15">https://www.lpzoo.org/</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Beaches -->

                        <div role="tabpanel" class="tab-pane fade in" id="beaches">
                            <div class="col-12 shadow-small py-2">
                                <div class="row px-3">
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/buckingham-fountain.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Buckingham Fountain: </span>
                                                    <span class="fs15">Popular tourist attraction that offers music, light, and water show from dusk until 11pm. Perfect place to end a busy day.</span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>301 S Columbus Dr, Chicago, IL 60605</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(312)-742-7529</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://www.chicagoparkdistrict.com/parks-facilities/clarence-f-buckingham-memorial-fountain" target="_blank" class="text-decoration-underline text-break fs15">https://www.chicagoparkdistrict.com/parks-facilities/clarence-f-buckingham-memorial-fountain</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/millennium_park.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Millennium Park / The BEAN: </span>
                                                    <span class="fs15">Center for award-winning landscape features the Jay Pritzker Pavilion, Crown Fountain, Lurie garden, and the Cloud Gate (BEAN) sculpture. </span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>201 E Randolph St, Chicago, IL 60602</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(312)-742-1168</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://www.chicago.gov/city/en/depts/dca/supp_info/millennium_park.html" target="_blank" class="text-decoration-underline text-break fs15">https://www.chicago.gov/city/en/depts/dca/supp_info/millennium_park.html</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/chicagobotanic.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">Chicago Botanic Garden: </span>
                                                    <span class="fs15">Stroll through the day across the ever-changing beauty of 27 spectacular gardens spanning over 385 acres. </span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>1000 Lake Cook Rd, Glencoe, IL 60022</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">(847)-835-5440</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://www.chicagobotanic.org/" target="_blank" class="text-decoration-underline text-break fs15">https://www.chicagobotanic.org/</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 shadow-small py-3 my-2">
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                                <div>
                                                    <img src="images/chicago-attractions/north_avenue_beach.jpg" class="img-fluid mx-auto d-block w-100" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-6 pl-3 pl-lg-0 py-3 my-auto py-lg-0">
                                                <div class="pb-3">
                                                    <span class="text-violet fs18 font-weight-bold">North Avenue Beach: </span>
                                                    <span class="fs15">One of the most popular beaches in Chicago offering a view of downtown skyline. This is a perfect beach for relaxation and water sports activities. </span>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Address</div>
                                                    <div class="fs15">
                                                        <div>1601 N. Jean-Baptiste Pointe DuSable Lake Shore Drive Chicago, IL 60611</div>
                                                    </div>
                                                </div>
                                                <div class="pb-3">
                                                    <div class="fs16 font-weight-bold text-violet">Phone</div>
                                                    <div data-locator="hotel-phone" class="fs15">Main 312-74-BEACH</div>
                                                </div>
                                                <div>
                                                	<span class="fs16 text-violet font-weight-bold">Website: </span><a href="https://www.chicagoparkdistrict.com/parks-facilities/north-avenue-beach" target="_blank" class="text-decoration-underline text-break fs15">https://www.chicagoparkdistrict.com/parks-facilities/north-avenue-beach</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
