@extends('layouts.user.base')
@section('content')

<style type="text/css">
    .banner-bg {
        background-image: url(images/banner-bg.png);
    }
</style>

<section class="container-fluid px-0">
    <div class="row">
        <div class="col-12 pt-1 pb-0 py-lg-0 px-1 p-lg-0">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <!-- <ol class="carousel-indicators carousel">
                    <li data-target="#carouselExampleControls" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleControls" data-slide-to="1"></li>               
                </ol> -->
                <ol class="carousel-indicators banner-carousel-indicators">
                    <li data-target="#carousel1" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel1" data-slide-to="1"></li>
                    <li data-target="#carousel1" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner">
                    @foreach ($banners as $banner)
                    <div class="carousel-item  {{$loop->first ? 'active' : ''}} ">
                        <img class="d-block w-100" src="{{asset(config('conventions.banner_display').$banner->image_url)}}" alt="First slide" />
                    </div>
                    @endforeach
                   <!--  <div class="carousel-item active">
                        <img class="d-block w-100" src="images/banner.png" alt="Second slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="images/banner.jpg" alt="Third slide" />
                    </div>  -->
                </div>
                {{-- <a class="banner-carousel-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="banner-carousel-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a> --}}
            </div>
        </div>
    </div>
</section>

<!-- End of Carousel Banners -->

<!-- Donors section -->

<div class="container-fluid px-3 px-md-4 px-lg-5 pt-3 pb-5 my-3 bg-navyblue" id="our-Donors">
    <div id="carousel2" class="carousel slide" data-ride="carousel" data-interval="3000">
        <ol class="carousel-indicators">
            <li data-target="#carousel2" data-slide-to="0" class="active"></li>
            <li data-target="#carousel2" data-slide-to="1"></li>
            <li data-target="#carousel2" data-slide-to="2"></li>
            <li data-target="#carousel2" data-slide-to="3"></li>
            <li data-target="#carousel2" data-slide-to="4"></li>
            <li data-target="#carousel2" data-slide-to="5"></li>
            <li data-target="#carousel2" data-slide-to="6"></li>
            <li data-target="#carousel2" data-slide-to="7"></li>
            <!-- <li data-target="#carousel2" data-slide-to="8"></li>
            <li data-target="#carousel2" data-slide-to="9"></li>
            <li data-target="#carousel2" data-slide-to="10"></li>
            <li data-target="#carousel2" data-slide-to="11"></li>
            <li data-target="#carousel2" data-slide-to="12"></li> -->
        </ol>
        <div class="carousel-inner">
            @php($x=0)
            @foreach($users_for_slide as $key=>$val)
                @foreach($val as $k=>$users)
                @if($x==0)
                    <div class="carousel-item active">
                @else
                    <div class="carousel-item">
                @endif
                {{$x++}}
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5 class="mb-0">Thank you to our {{explode('~',$key)[0]}}  {{ explode('~',$key)[0]!='Event Sponsors'? 'Donors':''}} </h5>
                            <div class="fs16 text-redd font-weight-bold">${{explode('~',$key)[1]}}+</div>
                        </div>
                    </div>
                    @foreach($users as $user)
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-4 px-md-2">
                                <img src="images/{{$user['img_url']}}" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">{{$user['name']}}</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!--<div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-4 px-md-2">
                                <img src="images/vijay-chavva.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Vijay Chavva</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-4 px-md-2">
                                <img src="images/chaya_setty.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mrs. Chaya Shetty</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-4 px-md-2">
                                <img src="images/Srinivas_Chittimalla.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Srinivas Chittimalla</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-4 px-md-2">
                                <img src="images/Kishore_Konduru.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Kishore Konduru</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-4 px-md-2">
                                <img src="images/Raja_Pampati.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Raja Pampati</div>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
                @endforeach
            @endforeach
            <!--<div class="carousel-item">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5 class="mb-0">Thank you to our Grand Event Sponsors</h5>
                            <div class="fs16 text-redd font-weight-bold">$50000+</div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-10 offset-lg-1">
                        <div class="row">
                            <div class="col-12 col-sm-4 col-md-4 col-lg-20 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Srinivasarao_tp.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. TP Srinivasa Rao</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-20 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/vijay-chavva.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. Vijay Chavva</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-20 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/chaya_setty.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mrs. Chaya Shetty</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-20 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Srinivas_Chittimalla.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. Srinivas Chittimalla</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-20 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Kishore_Konduru.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. Kishore Konduru</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5 class="mb-0">Thank you to our Sapphire Donors</h5>
                            <div class="fs16 text-redd font-weight-bold">$25000+</div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Srinivasarao_tp.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. TP Srinivasa Rao</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/vijay-chavva.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Vijay Chavva</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/chaya_setty.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mrs. Chaya Shetty</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Srinivas_Chittimalla.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Srinivas Chittimalla</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Kishore_Konduru.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Kishore Konduru</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Raja_Pampati.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Raja Pampati</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5 class="mb-0">Thank you to our Sapphire Donors</h5>
                            <div class="fs16 text-redd font-weight-bold">$25000 +</div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-8 offset-lg-2">
                        <div class="row">
                            <div class="col-12 col-sm-4 col-md-4 col-lg-3 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Srinivasarao_tp.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. TP Srinivasa Rao</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-3 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/vijay-chavva.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. Vijay Chavva</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-3 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/chaya_setty.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mrs. Chaya Shetty</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-3 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Srinivas_Chittimalla.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. Srinivas Chittimalla</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5 class="mb-0">Thank you to our Diamond Donors</h5>
                            <div class="fs16 text-redd font-weight-bold">$20000 +</div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Srinivasarao_tp.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. TP Srinivasa Rao</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/vijay-chavva.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Vijay Chavva</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/chaya_setty.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mrs. Chaya Shetty</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Srinivas_Chittimalla.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Srinivas Chittimalla</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Kishore_Konduru.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Kishore Konduru</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Raja_Pampati.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Raja Pampati</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5 class="mb-0">Thank you to our Diamond Donors</h5>
                            <div class="fs16 text-redd font-weight-bold">$20000 +</div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-6 offset-lg-3">
                        <div class="row">
                            <div class="col-12 col-sm-4 col-md-4 col-lg-4 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Srinivasarao_tp.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. TP Srinivasa Rao</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-4 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/vijay-chavva.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. Vijay Chavva</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-4 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/chaya_setty.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mrs. Chaya Shetty</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5 class="mb-0">Thank you to our Platinum Donors</h5>
                            <div class="fs16 text-redd font-weight-bold">$15000 +</div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Srinivasarao_tp.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. TP Srinivasa Rao</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/vijay-chavva.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Vijay Chavva</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/chaya_setty.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mrs. Chaya Shetty</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Srinivas_Chittimalla.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Srinivas Chittimalla</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Kishore_Konduru.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Kishore Konduru</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 py-3 py-lg-0">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Raja_Pampati.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Raja Pampati</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5 class="mb-0">Thank you to our Platinum Donors</h5>
                            <div class="fs16 text-redd font-weight-bold">$15000 +</div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-8 offset-sm-2 col-md-12 col-lg-4 offset-lg-4">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-4 col-lg-6 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Srinivasarao_tp.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. TP Srinivasa Rao</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-6 py-3 py-lg-0">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/vijay-chavva.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. Vijay Chavva</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--<div class="carousel-item">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5>Thank you to our Gold Donors</h5>
                            <h6>$10000 +</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Srinivasarao_tp.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. TP Srinivasa Rao</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/vijay-chavva.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Vijay Chavva</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/chaya_setty.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mrs. Chaya Shetty</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Srinivas_Chittimalla.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Srinivas Chittimalla</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Kishore_Konduru.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Kishore Konduru</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Raja_Pampati.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Raja Pampati</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5>Thank you to our Gold Donors</h5>
                            <h6>$10000 +</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 offset-sm-4 col-md-4 offset-md-4 col-lg-2 offset-lg-5">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Srinivasarao_tp.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. TP Srinivasa Rao</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5>Thank you to our Silver Donors</h5>
                            <h6>$5000 +</h6>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-8 offset-lg-2">
                        <div class="row">
                            <div class="col-12 col-sm-4 col-md-4 col-lg-3">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Srinivasarao_tp.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. TP Srinivasa Rao</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-3">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/vijay-chavva.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. Vijay Chavva</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-3">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/chaya_setty.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mrs. Chaya Shetty</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-3">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Srinivas_Chittimalla.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. Srinivas Chittimalla</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5>Thank you to our Bronze Donors</h5>
                            <h6>$2500 +</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Srinivasarao_tp.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. TP Srinivasa Rao</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/vijay-chavva.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Vijay Chavva</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/chaya_setty.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mrs. Chaya Shetty</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Srinivas_Chittimalla.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Srinivas Chittimalla</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Kishore_Konduru.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Kishore Konduru</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2">
                        <div class="donorss">
                            <div class="py5 px-2 px-4 px-md-2">
                                <img src="images/Raja_Pampati.jpg" class="img-fluid" alt="" />
                            </div>
                            <div class="text-center">
                                <div class="donor-namee">Mr. Raja Pampati</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-white">
                            <h5>Thanks to our Patron Donors</h5>
                            <h6>$1500 +</h6>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-10 offset-lg-1">
                        <div class="row">
                            <div class="col-12 col-sm-4 col-md-4 col-lg-20">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Srinivasarao_tp.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. TP Srinivasa Rao</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-20">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/vijay-chavva.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. Vijay Chavva</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-20">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/chaya_setty.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mrs. Chaya Shetty</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-20">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Srinivas_Chittimalla.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. Srinivas Chittimalla</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-4 col-lg-20">
                                <div class="donorss">
                                    <div class="py5 px-2 px-4 px-md-2">
                                        <img src="images/Kishore_Konduru.jpg" class="img-fluid" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <div class="donor-namee">Mr. Kishore Konduru</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <a class="donorss-carousel-prev" href="#carousel2" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon donorss-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="donorss-carousel-next" href="#carousel2" role="button" data-slide="next">
            <span class="carousel-control-next-icon donorss-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<!-- Donors section -->

<!-- Messages in Desktop View -->

{{-- 
<section class="container mt-5" id="messages">
    <div class="row">
        <!-- President Message -->
          @foreach ($messages  as $message)
            <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-0 col-lg-4 py-3 py-lg-3 msg_bg">
                <div class="text-center mb-3">
                    <h5 class="mb-3 d-inline border-bottom-1">{{$message->designation->name??''}}'s Message</h5>
                </div>
                <div class="messages-block">
                    <div class="px-3">
                        <div class="borders">
                            <img src="{{asset(config('conventions.message_display').$message->image_url)}}" class="mx-auto d-block rounded-circle" alt="" />
                        </div>
                        <div class="pt-0 pb-2">
                            <div class="text-center mb-1 fs15 font-weight-bold">{{$message->name}}</div>
                            <div class="member-role">{{$message->designation->name}}</div>
                        </div>
                    </div>
                    <div class="px-3 bg-white">
                        <p class="py-3 mb-0 text-center description">
                            {!! substr($message->message, 0, 100) !!}....
                        </p>
                        <div class="text-center pt-3">
                            <a href="{{url('message_content',$message->id)}}">
                                <i class="fas fa-chevron-circle-right text-dark fs20"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
          @endforeach
    </div>
</section> --}}

<!-- Messages in desktop view -->

<div class="container mt-3 d-none d-lg-block" id="messages">
    <div id="carousel3" class="carousel slide" data-ride="carousel" data-interval="300000">
        <!-- <ol class="carousel-indicators">
            <li data-target="#carousel3" data-slide-to="0" class="active"></li>
            <li data-target="#carousel3" data-slide-to="1"></li>
        </ol> -->
        <div class="carousel-inner">
         @foreach ($messages->chunk(3)  as $message_chunk)
            <div class="carousel-item {{$loop->first ? 'active' : ''}} ">
                <div class="row">
                    @foreach ($message_chunk as $message)
                        <div class=" col-lg-4 py-3 py-lg-3">
                            <div class="text-center mb-3">
                                <h5 class="mb-3 d-inline border-bottom-1">{{$message->designation->name??''}}'s Message</h5>
                            </div>
                            <div class="messages-block">
                                <div class="px-3">
                                    <div class="borders">
                                        <img src="{{asset(config('conventions.message_display').$message->image_url)}}" class="mx-auto d-block rounded-circle" alt="" />
                                    </div>
                                    <div class="pt-0 pb-2">
                                        <div class="text-center mb-1 fs15 font-weight-bold text-white">{{$message->name}}</div>
                                        <div class="member-role">{{$message->designation->name??''}}</div>
                                    </div>
                                    <div class="text-center" >
                                        <a href="{{url('message_content',$message->id)}}" class="text-white">Read {{$message->designation->name??''}}'s Message ....</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                      @endforeach

                   
                </div>
            </div>
             @endforeach
           {{--  <div class="carousel-item">
                <div class="row">
                
                    <div class=" col-lg-4 py-3 py-lg-3 msg_bg">
                        <div class="text-center mb-3">
                            <h5 class="mb-3 d-inline border-bottom-1">Co-Convenor's Message</h5>
                        </div>
                        <div class="messages-block">
                            <div class="px-3">
                                <div class="borders">
                                    <img src="images/Praveen_Amudala.jpg" class="mx-auto d-block rounded-circle" alt="" />
                                </div>
                                <div class="pt-0 pb-2">
                                    <div class="text-center mb-1 fs15 font-weight-bold text-white">Mr. Praveen Amudala</div>
                                    <div class="member-role">Co-Convenor</div>
                                </div>
                            </div>
                            <div class="text-center">
                                <a href="#" class="text-white">Read Co-Convenor's Message .....</a>
                            </div>
                        </div>
                    </div>

                    <div class=" col-lg-4 py-3 py-lg-3 msg_bg">
                        <div class="text-center mb-3">
                            <h5 class="mb-3 d-inline border-bottom-1">Co-Convenor's Message</h5>
                        </div>
                        <div class="messages-block">
                            <div class="px-3">
                                <div class="borders">
                                    <img src="images/Sreeni_Rachapalli.jpg" class="mx-auto d-block rounded-circle" alt="" />
                                </div>
                                <div class="pt-0 pb-2">
                                    <div class="text-center mb-1 fs15 font-weight-bold text-white">Mr. Srinivas Rachapalli</div>
                                    <div class="member-role">Co-Convenor</div>
                                </div>
                            </div>
                            <div class="text-center">
                                <a href="#" class="text-white">Read Co-Convenor's Message .....</a>
                            </div>
                        </div>
                    </div>

                    <div class=" col-lg-4 py-3 py-lg-3 msg_bg">
                        <div class="text-center mb-3">
                            <h5 class="mb-3 d-inline border-bottom-1">Secretary's Message</h5>
                        </div>
                        <div class="messages-block">
                            <div class="px-3">
                                <div class="borders">
                                    <img src="images/Suresh_Badam.jpg" class="mx-auto d-block rounded-circle" alt="" />
                                </div>
                                <div class="pt-0 pb-2">
                                    <div class="text-center mb-1 fs15 font-weight-bold text-white">Mr. Suresh Badam</div>
                                    <div class="member-role">Secretary</div>
                                </div>
                            </div>
                            <div class="text-center">
                                <a href="secretary_message_details.php" class="text-white">Read Secretary's Message .....</a>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div> --}}
        </div>

        <a class="message-carousel-prev" href="#carousel3" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon bg-img-none" aria-hidden="true"><i class="fas fa-angle-left"></i></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="message-carousel-next" href="#carousel3" role="button" data-slide="next">
            <span class="carousel-control-next-icon bg-img-none" aria-hidden="true"><i class="fas fa-angle-right"></i></span>
            <span class="sr-only">Next</span>
        </a>        
    </div>
</div>

<!-- End of Messages in desktop view -->

<!-- Messages in Mobile view -->

<section class="container mt-5 d-block d-lg-none" id="messages">
    <div class="row">
        <!-- President Message -->

        <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-0 py-3 py-lg-3">
            <div class="text-center mb-3">
                <h5 class="mb-3 d-inline border-bottom-1">President's Message</h5>
            </div>
            <div class="messages-block">
                <div class="px-3">
                    <div class="borders">
                        <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle" alt="" />
                    </div>
                    <div class="pt-0 pb-2">
                        <div class="text-center mb-1 fs15 font-weight-bold text-white">Mr. Hari Raini</div>
                        <div class="member-role">President</div>
                    </div>
                    <div class="text-center">
                        <a href="president_message_details.php" class="text-white">Read President's Message .....</a>
                        <!-- <i class="fas fa-chevron-right"></i> -->
                    </div>
                </div>
            </div>
        </div>

        <!-- Chairman Message -->

        <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-0 py-3 py-lg-3 msg_bg">
            <div class="text-center mb-3">
                <h5 class="mb-3 d-inline border-bottom-1">Chairman's Message</h5>
            </div>
            <div class="messages-block">
                <div class="px-3">
                    <div class="borders">
                        <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle" alt="" />
                    </div>
                    <div class="pt-0 pb-2">
                        <div class="text-center mb-1 fs15 font-weight-bold text-white">Dr. Jayasimha Sunku</div>
                        <div class="member-role">Chairman</div>
                    </div>
                </div>
                <div class="text-center">
                    <a href="chairman_message_details.php" class="text-white">Read Chairman's Message .....</a>
                </div>
            </div>
        </div>

        <!-- Convenor Message -->

        <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-0 py-3 py-lg-3 msg_bg">
            <div class="text-center mb-3">
                <h5 class="mb-3 d-inline border-bottom-1">Convenor's Message</h5>
            </div>
            <div class="messages-block">
                <div class="px-3">
                    <div class="borders">
                        <img src="images/dinkar.jpg" class="mx-auto d-block rounded-circle" alt="" />
                    </div>
                    <div class="pt-0 pb-2">
                        <div class="text-center mb-1 fs15 font-weight-bold text-white">Mr. Dinkar Karumuri</div>
                        <div class="member-role">Convenor</div>
                    </div>
                </div>
                <div class="text-center">
                    <a href="convenor_message_details.php" class="text-white">Read Convenor's Message .....</a>
                </div>
            </div>
        </div>

        <!-- Co-Convenor Message -->

        <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-0 py-3 py-lg-3 msg_bg">
            <div class="text-center mb-3">
                <h5 class="mb-3 d-inline border-bottom-1">Co-Convenor's Message</h5>
            </div>
            <div class="messages-block">
                <div class="px-3">
                    <div class="borders">
                        <img src="images/Praveen_Amudala.jpg" class="mx-auto d-block rounded-circle" alt="" />
                    </div>
                    <div class="pt-0 pb-2">
                        <div class="text-center mb-1 fs15 font-weight-bold text-white">Mr. Praveen Amudala</div>
                        <div class="member-role">Co-Convenor</div>
                    </div>
                </div>
                <div class="text-center">
                    <a href="#" class="text-white">Read Co-Convenor's Message .....</a>
                </div>
            </div>
        </div>

        <!-- Co-Convenor Message -->

        <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-0 py-3 py-lg-3 msg_bg">
            <div class="text-center mb-3">
                <h5 class="mb-3 d-inline border-bottom-1">Co-Convenor's Message</h5>
            </div>
            <div class="messages-block">
                <div class="px-3">
                    <div class="borders">
                        <img src="images/Sreeni_Rachapalli.jpg" class="mx-auto d-block rounded-circle" alt="" />
                    </div>
                    <div class="pt-0 pb-2">
                        <div class="text-center mb-1 fs15 font-weight-bold text-white">Mr. Srinivas Rachapalli</div>
                        <div class="member-role">Co-Convenor</div>
                    </div>
                </div>
                <div class="text-center">
                    <a href="#" class="text-white">Read Co-Convenor's Message .....</a>
                </div>
            </div>
        </div>

        <!-- Secretary Message -->

        <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-0 py-3 py-lg-3 msg_bg">
            <div class="text-center mb-3">
                <h5 class="mb-3 d-inline border-bottom-1">Secretary's Message</h5>
            </div>
            <div class="messages-block">
                <div class="px-3">
                    <div class="borders">
                        <img src="images/Suresh_Badam.jpg" class="mx-auto d-block rounded-circle" alt="" />
                    </div>
                    <div class="pt-0 pb-2">
                        <div class="text-center mb-1 fs15 font-weight-bold text-white">Mr. Suresh Badam</div>
                        <div class="member-role">Secretary</div>
                    </div>
                </div>
                <div class="text-center">
                    <a href="secretary_message_details.php" class="text-white">Read Suresh Badam's Message .....</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Messages in Mobile view -->

<!-- Our Donors -->

{{-- <div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    Our Donors
                </div>
            </div>
        </div>
    </div>
</div>
<section class="container-md">
    <div class="row">
        <div class="col-12 col-lg-8 offset-md-2 px-md-3 px-lg-0 py-0 py-lg-1 px-3 px-lg-5">
            <div class="px-0 px-lg-5">
                <ul class="list-unstyled donors-tabs-ul d-lg-inline-flex">
                    @foreach($donortypes as $donortype)
                        <li class="py-1 px-lg-4 donors-nav-item">
                            <a class="donors-nav-link text-decoration-none  {{$loop->first ? ' donors_active_tab_btn first-date': '' }}" data-id="{{$donortype->id}}" data-url="{{env('APP_URL')}}/api/get-donor-with-typeid/{{$donortype->id}}" >
                                <span>{{$donortype->name}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-12 col-lg-12 px-3 px-md-5 ">
        @if(count($donortypes)>0)
             <div class="row donors_active_tab donors-add-div" >
            </div>
            
        @else
            <div class="row">
                <div class="col-12 my-5 py-5">
                    <h4 class="text-center">Coming Soon</h4>
                </div>
            </div>
        @endif

        </div>
    </div>
</section>
 --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    Our Donors
                </div>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid">
    <div class="row">
        <div class="col-12 col-lg-12 py-0 py-lg-1 px-3 px-lg-5">
            <div class="px-0 px-lg-2">
                <ul class="list-unstyled donors-tabs-ul d-lg-inline-flex">
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none donors_active_tab_btn" target-id="#event-sponsors">

                            <span>Grand&nbsp;Event&nbsp;Sponsors</span>
                            <h6 class="text-center">$50000+</h6>
                        </a>
                    </li>
                    @foreach ($donor_users_arr as $key=>$donor)
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#{{$loop->iteration}}">
                            <span>{{explode('~',$key)[0]}}</span>
                            <h6 class="text-center">{{explode('~',$key)[1]}}+</h6>
                        </a>
                    </li>
                    @endforeach
                    {{--<li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#sapphire">
                            <span>Sapphire</span>
                            <h6 class="text-center">$25000+</h6>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#diamond">
                            <span>Diamond</span>
                            <h6 class="text-center">$20000+</h6>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#platinum">
                            <span>Platinum</span>
                            <h6 class="text-center">$15000+</h6>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#gold">
                            <span>Gold</span>
                            <h6 class="text-center">$10000+</h6>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#silver">
                            <span>Silver</span>
                            <h6 class="text-center">$5000+</h6>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#bronze">
                            <span>Bronze</span>
                            <h6 class="text-center">$2500+</h6>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 donors-nav-item">
                        <a class="donors-nav-link text-decoration-none" target-id="#patron">
                            <span>Patron</span>
                            <h6 class="text-center">$1500+</h6>
                        </a>
                    </li>--}}
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid px-3 px-md-5 px-lg-5">
    <div class="row px-3 px-lg-4">
        
        <div class="col-12 col-lg-12 donors_active_tab" id="event-sponsors">
            <div class="row">
                
                @foreach ($grand_event_users as $key=>$donor)
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/{{$donor['img_url']}}" alt="" />
                        </div>
                        <div class="donor-name">{{$donor['name']}}</div>
                    </div>
                </div>
                @endforeach
                {{--<div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/sunitha_rachapalli.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mrs. Sunita Rachapalli</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/giri_kothamasu.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Giri Kothamasu</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/kiran_adimulam.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Kiran Adimulam</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/venu_mattey.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Venugopal Mattey</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/ravi_makam.jpg" alt="" />
                        </div>
                        <div class="donor-name">Dr. Ravi Makam</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/srinivas-s.jpg" alt="" />
                        </div>
                        <div class="donor-name">Dr. Srinivas Seela</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/Chittari_BOD.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Chittari Pabba</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/Bhanu_Illendra.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Bhanu Babu Ilindra</div>
                    </div>
                </div>--}}
            </div>
        </div>
        @foreach ($donor_users_arr as $key=>$donors)
        <div class="col-12 col-lg-12" id="{{$loop->iteration}}" style="display: none;">
            <div class="row">
                @foreach ($donors as $donor)
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/{{$donor['img_url']}}" alt="" />
                        </div>
                        <div class="donor-name">{{$donor['name']}}</div>
                    </div>
                </div>
                @endforeach
                {{--<div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>--}}
            </div>
        </div>
        @endforeach
        {{--<div class="col-12 col-lg-12" id="diamond" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="platinum" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-12" id="gold" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="silver" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="bronze" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="patron" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="donors-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="donor-name">Mr. Donor</div>
                    </div>
                </div>
            </div>
        </div>--}}
    </div>
</section>



<!-- End of Our Donors -->

<!-- Pillars Of Our Success Leadership -->

{{-- <div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div class="text-transform-none">
                    Our Leadership Team
                </div>
            </div>
        </div>
    </div>
</div>
 <section class="container-fluid">
    <div class="row">
        <div class="col-12 col-lg-12 px-3 px-md-5 py-0 py-lg-1">
            <div class="px-0">
                <ul class="list-unstyled leadership-tabs-ul d-lg-inline-flex">
                     @foreach ($leadershiptypes as $leadershiptype)
                        <li class="py-1 px-lg-3 leadership-nav-item">
                            <a class="leadership-nav-link text-decoration-none {{$loop->first ? 'leadership_active_tab_btn first-leadership-type': '' }}"  data-id="{{$leadershiptype->id}}" target-id="#convention-leadership" data-url="{{env('APP_URL')}}/api/get-leadership-with-typeid/{{$leadershiptype->id}}">
                                <span>{{$leadershiptype->name}}</span>
                            </a>
                        </li>
                     @endforeach
                </ul>
            </div>
        </div>
    </div>
</section> 
 --}}
 {{-- 
<section class="container-md">
    <div class="row">
        <div class="col-12 col-lg-12 px-md-5">
            <div class="row leadership_active_tab leadership-add-div" id="convention-leadership">
                <div class=""></div>
               
            </div>
        </div>
    </div>
</section> --}}


<div class="container-fluid" id="Our_Leadership_Team">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div class="text-transform-none" >
                    Our Leadership Team
                </div>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid px-lg-2">
    <div class="row px-0 px-lg-3">
        <div class="col-12 col-lg-12 px-3 px-md-5 py-0 py-lg-1">
            <div class="px-0">
                <ul class="list-unstyled leadership-tabs-ul d-lg-inline-flex">
                    <li class="py-1 px-lg-3 leadership-nav-item">
                        <a class="leadership-nav-link text-decoration-none leadership_active_tab_btn" target-id="#convention-leadership">
                            <span>Convention&nbsp;Leadership</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 leadership-nav-item">
                        <a class="leadership-nav-link text-decoration-none" target-id="#convention-advisors">
                            <span>Convention&nbsp;Advisors</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 leadership-nav-item">
                        <a class="leadership-nav-link text-decoration-none" target-id="#executive-committee">
                            <span>Executive&nbsp;Committee</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 leadership-nav-item">
                        <a class="leadership-nav-link text-decoration-none" target-id="#convention-management">
                            <span>CMO</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 leadership-nav-item">
                        <a class="leadership-nav-link text-decoration-none" target-id="#board-of-directors">
                            <span>Board&nbsp;Of&nbsp;Directors</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 leadership-nav-item">
                        <a class="leadership-nav-link text-decoration-none" target-id="#advisory-council">
                            <span>Advisory&nbsp;Council</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-3 leadership-nav-item">
                        <a class="leadership-nav-link text-decoration-none" target-id="#convenor-circle">
                            <span>Convenor&nbsp;Circle</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="container-md leadership_active_tab" id="convention-leadership">
    <div class="row px-0 px-lg-5">
        <div class="col-12 col-lg-12 px-3 px-md-5">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/Hari_Raini.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Hari Raini</div>
                        <div class="leader-designation">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" alt="" />
                        </div>
                        <div class="leader-name">Dr. Jayasimha Sunku</div>
                        <div class="leader-designation">Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/Srinivasa_Rao_Pandiri.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Srinivasa Rao Pandiri</div>
                        <div class="leader-designation">President Elect</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/Ravi_Ellendula.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Ravi Ellendula</div>
                        <div class="leader-designation">General Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/dinkar.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Dinkar Karumuri</div>
                        <div class="leader-designation">Convenor</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="d-md-none d-lg-block col-lg-20 px-3 px-lg-2"></div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/Praveen_Amudala.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Praveen Amudala</div>
                        <div class="leader-designation">Co-Convenor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/Sreeni_Rachapalli.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Srinivas Rachapalli</div>
                        <div class="leader-designation">Co-Convenor</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/Suresh_Badam.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Suresh Badam</div>
                        <div class="leader-designation">Secretary</div>
                    </div>
                </div>
                <div class="d-md-none d-lg-block col-lg-20 px-3 px-lg-2"></div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid px-3 px-md-5 px-lg-5">
    <div class="row px-3 px-lg-4">
        <div class="col-12 col-lg-8 offset-lg-2 px-3 px-lg-4" id="convention-advisors" style="display: none;">
            <div class="row mx-lg-4 px-lg-5">
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Nagender_Aytha.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Nagender Aytha</div>
                        <div class="leader-designation">Immediate Past President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/shanker_shetty.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Shankar Setty</div>
                        <div class="leader-designation">Immediate Past Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/srinivas_pedamallu.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Srinivas Pedamallu</div>
                        <div class="leader-designation">Board of Director</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="executive-committee" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Hari_Raini.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Hari Raini</div>
                        <div class="leader-designation">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Srinivasa_Rao_Pandiri.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Srinivasa Rao Pandiri</div>
                        <div class="leader-designation">President Elect</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Nagender_Aytha.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Nagender Aytha</div>
                        <div class="leader-designation">Past President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Ravi_Ellendula.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Ravi Ellendula</div>
                        <div class="leader-designation">General Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/dinkar.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Dinkar Karumuri</div>
                        <div class="leader-designation">Treasurer</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/chaya_setty.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mrs. Chaya Setty</div>
                        <div class="leader-designation">Joint Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/rajesh.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Rajesh Badam</div>
                        <div class="leader-designation">Joint Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Gangadhar_Upalla.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Gangadhar Vuppala</div>
                        <div class="leader-designation">Joint Treasurer</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Venkat_Belde.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Venkat Belde</div>
                        <div class="leader-designation">Joint Treasurer</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Srinivas_Chittimalla.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Srinivas Chittimalla</div>
                        <div class="leader-designation">Cultural Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Sridhar_Aytha.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Sreedhar Aietha</div>
                        <div class="leader-designation">EC Member</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Prashanth_Veerabomma.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Prashanth Gupta Veerabomma</div>
                        <div class="leader-designation">EC Member</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Kishore_Konduru.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Kishore Konduru</div>
                        <div class="leader-designation">EC Member</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Raja_Pampati.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Raja Pampati</div>
                        <div class="leader-designation">EC Member</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Ashok_Ellendula.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Ashok Ellendula</div>
                        <div class="leader-designation">EC Member</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Praveen_Amudala.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Praveen Amudala</div>
                        <div class="leader-designation">EC Member</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Sekhar_Perla.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Sekhar Perla</div>
                        <div class="leader-designation">EC Member</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="convention-management" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/chaya_setty.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mrs. Chaya Shetty</div>
                        <!-- <div class="leader-designation">Member</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/sunitha_rachapalli.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mrs. Sunita Rachapalli</div>
                        <!-- <div class="leader-designation">Member</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/giri_kothamasu.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Giri Kothamasu</div>
                        <!-- <div class="leader-designation">Joint Treasurer</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/manjunatha_kunigal.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Manjunatha Kunigal</div>
                        <!-- <div class="leader-designation">President</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/kiran_adimulam.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Kiran Adimulam</div>
                        <!-- <div class="leader-designation">Chairman</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/venu_mattey.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Venugopal Mattey</div>
                        <!-- <div class="leader-designation">Joint Treasurer</div> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="board-of-directors" style="display: none;">
            <div class="row">  
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" alt="" />
                        </div>
                        <div class="leader-name">Dr. Jayasimha Sunku</div>
                        <div class="leader-designation">Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/shanker_shetty.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Shankar Setty</div>
                        <div class="leader-designation">Past Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/ramesh_bapanapalli.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Ramesh Bapanapalli</div>
                        <div class="leader-designation">Board Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/ravi_makam.jpg" alt="" />
                        </div>
                        <div class="leader-name">Dr. Ravi Makam</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/srinivas_pedamallu.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Srinivas Pedamallu</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/srinivas_veeravalli.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Srinivas Veeravalli</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/srinivas-s.jpg" alt="" />
                        </div>
                        <div class="leader-name">Dr. Srinivas Seela</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Chittari_BOD.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Chittari Pabba</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Ramulu_Samudrala.jpg" alt="" />
                        </div>
                        <div class="leader-name">Dr. Ramulu Samudrala</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Srinivasarao.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Srinivasa Tummalapenta</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Bhanu_Illendra.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Bhanu Babu Ilindra</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Rojanandam_Samudrala.jpg" alt="" />
                        </div>
                        <div class="leader-name">Dr. Rojanandam Samudrala</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Sridhar_Challa.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Sridhar Challa</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Venu_Kondle.jpg" alt="" />
                        </div>
                        <div class="leader-name">Dr. Venu Kondle</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/naveenhires.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Naveen Goli</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Suresh_Chatakondu.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Suresh Chatakondu</div>
                        <div class="leader-designation">Board Of Director</div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 col-lg-10 offset-lg-1" id="advisory-council" style="display: none;">
            <div class="row">  
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/anand-garlapati.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Anand Garlapati</div>
                        <div class="leader-designation"></div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/vijay-chavva.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Vijay Chavva</div>
                        <div class="leader-designation"></div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/Nagender_Aytha.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Nagender Aytha</div>
                        <div class="leader-designation"></div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/shanker_shetty.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Shankar Setty</div>
                        <div class="leader-designation"></div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-20 px-3 px-lg-1">
                    <div class="leader-block">
                        <div>
                            <img src="images/bala_voleti.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Bala Voleti</div>
                        <div class="leader-designation"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="convenor-circle" style="display: none;">
            <div class="row">      
                <div class="col-12 col-sm-6 col-md-4 col-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/dinkar.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Dinkar Karumuri</div>
                        <div class="leader-designation">Convenor, 6th Global Convention</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/Vijaybhaskar_Pallerla.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Vijaybhaskar Pallerla</div>
                        <div class="leader-designation">Convenor, 5th Global Convention</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/Srinivasarao_tp.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. TP Srinivasa Rao</div>
                        <div class="leader-designation">Convenor, 4th Global Convention</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/Srinivas_Akula(Columbus).jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Srinivas Akula</div>
                        <div class="leader-designation">Convenor, 3rd Global Convention</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/Hanuman_Nandanampati.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Hanuman Nandanampati</div>
                        <div class="leader-designation">Convenor, 2nd Global Convention</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2">
                    <div class="leader-block">
                        <div>
                            <img src="images/no-image1.jpg" alt="" />
                        </div>
                        <div class="leader-name">Mr. Ramesh Kalvala</div>
                        <div class="leader-designation">Convenor, 1st Global Convention</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End of Pillars Of Our Success Leadership -->



<!-- Convention Committe -->

<section class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    Convention Committees
                </div>
            </div>
        </div>
    </div>
  

    <div class="container">
        <div class="row">
            <div class="col-12 px-0">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="5000">
                    <ol class="carousel-indicators carosel">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                    </ol>
                    <div class="carousel-inner carosel-inner">
                        @foreach($committes->chunk(16) as $committe_lists )
                            <div class="carousel-item {{$loop->first? 'active' : ''}}">
                                <div class="col-12">
                                    <div class="row pb-5">
                                         @foreach($committe_lists as $committe)
                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                <a  href="{{url('more-committe-members', $committe->id)}}"  class="text-decoration-none">
                                                    <div class="gc-{{ ($loop->iteration % 16)+1 }} border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="{{asset(config('conventions.committe_display').$committe->image_url)}}" class="img-fluid mx-auto d-block" alt="">
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">{{ $committe->name }}</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="carosel_slider-btn">
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carosel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carosel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <div>
                        <div class="text-right">
                            <a  href="{{url('show-all-committees')}}"  class="text-decoration-none carosel_view-all-btn font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<!-- End of Convention Committe -->





<!-- Convention Programs -->

<section class="container-fluid my-5 mb-lg-5 mt-lg-125">
    <div class="row">
        <div class="col-12 pb-4">
            <div class="main-heading">
                <div>
                    Convention Programs
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @foreach($programs as $program)
            <div class="col-12 col-md-4 my-3" data-aos="fade-down">
                <a {{-- href="{{url('programs',$program->id)}}" --}}>
                    <div>
                        <img style="object-fit:contain;height: 220px" src="{{asset(config('conventions.program_display').$program->image_url)}}" class="img-fluid mx-auto d-block border-radius-10" alt="" />
                    </div>
                </a>
                <div class="pt-3">
                    <h5 style="padding-left:28px ;" class="text-center text-sm-center text-md-left">{{$program->program_name}}</h5>
                </div>
            </div>
            @endforeach



        </div>
    </div>
</section>

<!-- End of Convention Programs -->

<!-- Convention Invitees -->
{{-- <div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    Convention Invitees
                </div>
            </div>
        </div>
    </div>
</div>

<section class="container-md">
    <div class="row">
     @if(count($invitees)>0)
         <div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-4 offset-lg-4 py-0 py-lg-1 px-3 px-lg-3">
            <div class="px-0 px-lg-2">
                <ul class="list-unstyled invitees-tabs-ul d-lg-inline-flex">
                    <li class="py-1 px-lg-4 invitees-nav-item">
                        <a class="invitees-nav-link text-decoration-none invitees_active_tab_btn invitees_all_data" target-id="#all-invitees" data-id="all" data-url="{{env('APP_URL')}}/api/get-invitee-with-typeid/all" >
                            <span>All</span>
                        </a>
                    </li>
                    @foreach ($invitees as $invitee)
                        <li class="py-1 px-lg-4 invitees-nav-item">
                            <a class="invitees-nav-link text-decoration-none" target-id="#dignitaries" data-id="{{$invitee->id}}" data-url="{{env('APP_URL')}}/api/get-invitee-with-typeid/{{$invitee->id}}">
                                <span>{{$invitee->name}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-12 col-lg-12 px-3 px-md-5">
            <div class="row invitees_active_tab invitees-add-div" >
            </div>
        </div> 
        @else
            <div class="col-12">
                <h4 class="text-white text-center mb-0">Coming Soon</h4>
            </div>
        @endif
    </div>
</section> --}}

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    Convention Invitees
                </div>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="row">
        <div class="col-sm-8 offset-sm-2 col-md-8 offset-md-2 col-lg-6 offset-lg-3 py-0 py-lg-1 px-3 px-md-4 px-lg-5">
            <div class="px-0 px-md-3 px-lg-5">
                <ul class="list-unstyled invitees-tabs-ul d-lg-inline-flex">
                    <li class="py-1 px-lg-4 invitees-nav-item">
                        <a class="invitees-nav-link text-decoration-none invitees_active_tab_btn" target-id="#all-invitees">
                            <span>All</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-4 invitees-nav-item">
                        <a class="invitees-nav-link text-decoration-none" target-id="#dignitaries">
                            <span>Dignitaries</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-4 invitees-nav-item">
                        <a class="invitees-nav-link text-decoration-none" target-id="#entertainment">
                            <span>Entertainment</span>
                        </a>
                    </li>
                    <li class="py-1 px-lg-4 invitees-nav-item">
                        <a class="invitees-nav-link text-decoration-none" target-id="#speakers">
                            <span>Speakers</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid px-3 px-md-5 px-lg-5">
    <div class="row px-3 px-lg-4">
        <div class="col-12 col-lg-12 invitees_active_tab" id="all-invitees">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">MR. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">MR. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="dignitaries" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">MR. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">MR. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="entertainment" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">MR. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-12" id="speakers" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">MR. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">MR. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 px-3 px-lg-1">
                    <div class="invitees-block">
                        <div>
                            <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                        </div>
                        <div class="invitee-name">Mr. Invitee</div>
                        <!-- <div class="invitee-designation">Member of XYZ</div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- End of Convention Invitees -->



<!-- Events & Schedule -->

<section class="container-fluid my-5">
    <div class="row">
        <div class="col-12 pb-4">
            <div class="main-heading">
                <div>
                    Events & Schedule
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
           @forelse($events as $event)
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                <div class="event-details ed-card ed-card1">
                    <div>
                        <img src="{{asset(config('conventions.event_display').$event->image_url)}}" class="img-fluid w-100 border-radius-top-right-15" alt="" />
                    </div>
                    <div class="p-3 event-date1">
                        <div class="fs18 text-white text-left py-2">
                            <span>{{ $event->from_date->format('D') }},</span>
                            <span> {{ \Carbon\Carbon::parse($event->from_date)->isoFormat('MMM Do YYYY')}}</span>
                        </div>
                        <div class="text-white text-center event-schedule">
                            <span class="text-white">DATE:</span> {{ \Carbon\Carbon::parse($event->from_date)->isoFormat('MMM Do YYYY')}} <br />
                            {{$event->event_name}}
                            {{-- <a href="{{url('event-schedule',$event->id)}}" class="text-white">more... </a> --}}
                        </div>
                    </div>
                    <div class="row event-time-n-kn px15">
                        <div class="col-6 px-0">
                            <div class="event-time-block">
                                <div class="event-time">
                                    <span><i class="far fa-clock pr-2"></i></span>
                                    <span>8:00 AM to 4:00 PM</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 px-0">
                            <div class="event-km">
                                <a {{-- href="{{url('event-schedule',$event->id)}}" --}} class="text-decoration-none text-white">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             @empty
                <div class="col-12 py-5">
                    <h4 class="text-center text-skyblue">Coming Soon ...</h4>
                </div>
            @endforelse
        </div>
    </div>
</section>

<!-- End of Events & Schedule -->

<!-- Youtube Vedios -->
<section class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    Convention Video Gallery
                </div>
            </div>
        </div>
    </div>
</section>

<section class=" py70 mb-5">
    <div class="container">
        <div class="row">
             @forelse($videos as $video)
                <div class="col-12 col-md-4 col-lg-4 my-3">
                    <div class="position-relative inner-shadow">
                        <a href="https://www.youtube.com/embed/{{$video->youtube_video_id}}" data-fancybox-group="" class="various fancybox.iframe d-block">
                            <span class="video-icon-hover">&nbsp;</span>
                            <img src="https://img.youtube.com/vi/{{$video->youtube_video_id}}/3.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                        </a>
                    </div>
                    <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">{{$video->name}} </div>
                </div>

                <div class="col-12 pt-3 px-0">
                    <div class="text-right">
                        <a href="{{url('show-all-videos')}}" class="text-decoration-none font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>
                    </div>
                 </div>
            @empty
                <div class="col-12 py-5">
                    <h4 class="text-center text-skyblue">Coming Soon ...</h4>
                </div>
            @endforelse
        </div>
        
    </div>
</section>

<!-- End of Youtube Vedios -->

<!-- Donors Section -->

<!-- <section class="container-fluid bg-white py-5">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    More About Convention Event
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="counter-block my-2">
                <div class="counter-width counter-card-bg1">
                    <div class="my-auto">
                        <span class="fs18">0</span>
                        <span class="float-right"><img src="images/counting-icons/donors.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Donors</h5>
                    </div>
                </div>
            </div>
            <div class="counter-block my-2">
                <div class="counter-width counter-card-bg2">
                    <div class="my-auto">
                        <span class="fs18">0</span>
                        <span class="float-right"><img src="images/counting-icons/visitors.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Site Visitors</h5>
                    </div>
                </div>
            </div>
            <div class="counter-block my-2">
                <div class="counter-width counter-card-bg3">
                    <div class="my-auto">
                        <span class="fs18">0</span>
                        <span class="float-right"><img src="images/counting-icons/registrations.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Registrations</h5>
                    </div>
                </div>
            </div>
            <div class="counter-block my-2">
                <div class="counter-width counter-card-bg4">
                    <div class="my-auto">
                        <span class="fs18">0</span>
                        <span class="float-right"><img src="images/counting-icons/invitees.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Invitees</h5>
                    </div>
                </div>
            </div>
            <div class="counter-block my-2 counter-block-5">
                <div class="counter-width counter-card-bg5">
                    <div class="my-auto">
                        <span class="fs18">0</span>
                        <span class="float-right"><img src="images/counting-icons/performance.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Performance</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->


{{-- this part of is to clone not to show in page -- start--}}
<div class="donor-template" style="display:none">
     <div class="col-12 col-sm-6 col-md-6 col-lg-3 main">
        <div class="donors-block">
            <div>
                <img src="images/no-image1.jpg" class="donor-image" alt="" />
            </div>
            <div class="donor-name">Mr. Donor</div>
        </div>
    </div>
</div>

<div class='see-more-template' style="display:none">
    <div class="col-12 py-2 main">
        <div class="text-center">
            <a href="donors_more_details.php" class="btn px-3 bg-info text-white text-decoration-none border-radius-20 see-more-url">See More</a>
        </div>
    </div>
</div>

<div class="see-more-leadership-template" style="display:none">
    <div class="col-12 main">
        <div class="pb25 text-right">
            <a href="convention_leadership_committee.php" class="see-more-url text-decoration-none text-white font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>
        </div>
    </div>
</div>

<div class="leadership-template" style="display:none">
    <div class="col-12 col-sm-6 col-md-6 col-lg-3 main">
        <div class="leader-block">
            <div>
                <img class="leadership-image" src="images/Hari_Raini.jpg" alt="" />
            </div>
            <div class="leader-name leadership-name">Mr. Hari Raini</div>
            <div class="leader-designation leadership-designation">President</div>
        </div>
    </div>
</div>

<div class="invitees-template" style="display:none">
    <div class="col-12 col-sm-6 col-md-6 col-lg-3 main">
        <div class="invitees-block">
            <div>
                <img src="images/no-image1.jpg" class="img-fluid rounded-circle invitees_img invitees-image" alt="" />
            </div>
            <div class="invitee-name invitees-name">Mr. Invitee</div>
            <!-- <div class="invitee-designation invitees-designation">Member of XYZ</div> -->
        </div>
    </div>
</div>

{{-- this part of is to clone not to show in page----  end --}}

@section('javascript')

<script>
    $(".leadership-nav-link").click(function() {
        //active_tab
        ajaxCall($(this).data('url'), 'get', null, showDataInLeadership)
        $(".leadership_active_tab_btn").removeClass("leadership-nav-link");
        $(".leadership_active_tab_btn").removeClass("leadership_active_tab_btn");
        $(this).addClass("leadership-nav-link");
        $(this).addClass("leadership_active_tab_btn");
    });

    $(".invitees-nav-link").click(function() {
        //active_tab
        ajaxCall($(this).data('url'), 'get', null, showDataInInvitees)
        $(".invitees_active_tab_btn").removeClass("invitees-nav-link");
        $(".invitees_active_tab_btn").removeClass("invitees_active_tab_btn");
        $(this).addClass("invitees-nav-link");
        $(this).addClass("invitees_active_tab_btn");

    });

    $(".donors-nav-link").click(function() {
        ajaxCall($(this).data('url'), 'get', null, showDataInOurDonors)
        //active_tab
        $(".donors_active_tab_btn").removeClass("donors-nav-link");
        $(".donors_active_tab_btn").removeClass("donors_active_tab_btn");
        $(this).addClass("donors-nav-link");
        $(this).addClass("donors_active_tab_btn");
    });

    $(document).ready(function() {
        $('.first-date').addClass("donors_active_tab_btn");
        $('.first-leadership-type').addClass("leadership_active_tab_btn");
        ajaxCall($('.first-leadership-type').data('url'), 'get', null, showDataInLeadership)
        ajaxCall($('.first-date').data('url'), 'get', null, showDataInOurDonors)
        ajaxCall($('.invitees_all_data').data('url'), 'get', null, showDataInInvitees) 
    })

    function showDataInOurDonors(data) {
        $('.donors-add-div').empty()
        if (data.length == 0) {
            $('.donors-add-div').append('<center> <p>No Donors</p> </center>');
        } else {
            for (let i = 0; i < data.length; i++) {
                 if(i>7){
                   // break
                }
                $(".donor-template").find('.main').first().clone()
                    .find(".donor-image").attr('src', "{{asset(config('conventions.member_display'))}}/" + data[i].member.image_url).end()
                    .find(".donor-name").text(data[i].member.name).end()
                    .appendTo($('.donors-add-div'));
            }
           // $(".see-more-template").find('.main').first().clone()
             //   .find(".see-more-url").attr('href', "{{url('more-donor-details')}}/" + data[0].sub_type_id).end()
             //   .appendTo($('.donors-add-div'));
        }
    }

    function showDataInInvitees(data) {
        $('.invitees-add-div').empty()
        if (data.length == 0) {
            $('invitees-add-div').append('<center> <p>No Data</p> </center>');
        } else {
            for (let i = 0; i < data.length; i++) {
                 if(i>7){
                   // break
                }
                $(".invitees-template").find('.main').first().clone()
                    .find(".invitees-image").attr('src', "{{asset(config('conventions.member_display'))}}/" + data[i].member.image_url).end()
                    .find(".invitees-name").text(data[i].member.name).end()
                    .find(".invitees-designation").text(data[i].designation.name).end()
                    .appendTo($('.invitees-add-div'));
            }

        }
    }

    function showDataInLeadership(data) {
        $('.leadership-add-div').empty()
        if (data.length == 0) {
            $('leadership-add-div').append('<center> <p>No Data</p> </center>');
        } else {
            for (let i = 0; i < data.length; i++) {
                if(i>7){
                   // break
                }
            if(data[i].member!=null) {
                $(".leadership-template").find('.main').first().clone()
                    .find(".leadership-image").attr('src', "{{asset(config('conventions.member_display'))}}/" + data[i].member.image_url).end()
                    .find(".leadership-name").text(data[i].member.name).end()
                    .find(".leadership-designation").text(data[i].designation.name).end()
                    .appendTo($('.leadership-add-div'));
                }
            }
            
           // $(".see-more-leadership-template").find('.main').first().clone()
            //    .find(".see-more-url").attr('href', "{{url('more-leadership-details')}}/" + data[0].sub_type_id).end()
            //    .appendTo($('.leadership-add-div'));
        }
    }
</script>


<script>
    $(".leadership-nav-link").click(function () {
        //active_tab
        $(".leadership_active_tab_btn").removeClass("leadership-nav-link");
        $(".leadership_active_tab_btn").removeClass("leadership_active_tab_btn");
        $(this).addClass("leadership-nav-link");
        $(this).addClass("leadership_active_tab_btn");

        $(".leadership_active_tab").hide();
        $(".leadership_active_tab").removeClass("leadership_active_tab");
        $($(this).attr("target-id")).addClass("leadership_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>

<script>
    $(".invitees-nav-link").click(function () {
        //active_tab
        $(".invitees_active_tab_btn").removeClass("invitees-nav-link");
        $(".invitees_active_tab_btn").removeClass("invitees_active_tab_btn");
        $(this).addClass("invitees-nav-link");
        $(this).addClass("invitees_active_tab_btn");

        $(".invitees_active_tab").hide();
        $(".invitees_active_tab").removeClass("invitees_active_tab");
        $($(this).attr("target-id")).addClass("invitees_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>

<script>
    $(".donors-nav-link").click(function () {
        //active_tab
        $(".donors_active_tab_btn").removeClass("donors-nav-link");
        $(".donors_active_tab_btn").removeClass("donors_active_tab_btn");
        $(this).addClass("donors-nav-link");
        $(this).addClass("donors_active_tab_btn");

        $(".donors_active_tab").hide();
        $(".donors_active_tab").removeClass("donors_active_tab");
        $($(this).attr("target-id")).addClass("donors_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>

<script type="text/javascript">
    $("#invitees-carosel", "#invitees-carosel1", "#invitees-carosel2", "#invitees-carosel3").carousel({
        interval: 6000,
        wrap: true,
        keyboard: true,
    });
</script>

@endsection


@endsection