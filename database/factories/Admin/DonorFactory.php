<?php

namespace Database\Factories\Admin;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Admin\Donor;
use App\Models\Admin\Donortype;

class DonorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Donor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'donortype_id' => Donortype::factory(),
            'donor_name' => $this->faker->regexify('[A-Za-z0-9]{100}'),
            'description' => $this->faker->text,
            'image_url' => $this->faker->regexify('[A-Za-z0-9]{100}'),
        ];
    }
}
