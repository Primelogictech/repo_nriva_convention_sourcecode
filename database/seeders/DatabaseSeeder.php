<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       // \App\Models\Admin\ExhibitorType::factory(5)->create();
        \App\Models\Admin\Committe::factory(30)->create();
        \App\Models\Admin\Benefittype::factory(5)->create();
        \App\Models\Admin\Paymenttype::factory(1)->create();
          \App\Models\Admin\Member::factory(5)->create();
        \App\Models\Admin\Video::factory(5)->create();
        \App\Models\Admin\Donor::factory(5)->create();
        \App\Models\Admin\Event::factory(5)->create();
        \App\Models\Admin\Schedule::factory(5)->create();
        \App\Models\Admin\Program::factory(5)->create();
        \App\Models\Admin\Invitee::factory(3)->create();
        \App\Models\Admin\LeadershipType::factory(3)->create();
        \App\Models\Admin\SponsorCategoryType::factory(3)->create();
        $this->call(LogoTableSeeder::class);
        $this->call(MasterTableSeeder::class);
        $this->call(HomePageTablesSeeder::class);
        $this->call(DonortypeTableSeeder::class);
        $this->call(RegistrationPageContentSeeder::class);

        $path = 'database/sql/states.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('states table seeded!');

    }
}
