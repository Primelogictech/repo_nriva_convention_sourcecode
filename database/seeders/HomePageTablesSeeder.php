<?php

namespace Database\Seeders;

use App\Models\Admin\Banner;
use App\Models\Admin\Message;
use App\Models\Admin\Venue;
use Illuminate\Database\Seeder;

class HomePageTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Venue::create([
            'Event_date_time' => "2021-08-17 13:19:14",
            'end_date' => "2021-08-19 13:19:14",
            'location' => "location",
        ]);
        for ($i=1; $i < 4; $i++) {
            Banner::create([
                'image_url' => "banner_".$i,
            ]);

            Message::create([
                'name' => "name" . $i,
                'designation_id' => rand(1,3),
                'message' => "<p>hello</p>",
                'image_url' => "message_".$i.".jpg",
            ]);

        }

    }
}
