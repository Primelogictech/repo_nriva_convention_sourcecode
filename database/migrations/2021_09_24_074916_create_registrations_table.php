<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convention_registrations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('registration_id')->nullable();
            $table->string('registration_type_name');
            $table->string('full_name')->nullable();
            $table->string('company_name')->nullable();
            $table->string('category')->nullable();
            $table->string('tax_ID')->nullable();
            $table->string('mailing_address')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('email')->nullable();
            $table->string('fax')->nullable();
            $table->string('website_url')->nullable();
            $table->string('booth_type')->nullable();
            $table->json('extra_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
