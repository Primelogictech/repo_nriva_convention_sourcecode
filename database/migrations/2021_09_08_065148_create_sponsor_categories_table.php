<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSponsorCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //convention_sponsor_categories
        Schema::create('convention_sponsor_categories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_type_id');
            $table->foreignId('donor_type_id')->comment("null if Individual");
            $table->float('amount', 8, 2)->nullable();
            $table->float('amount_before', 8, 2)->nullable();
            $table->float('amount_after', 8, 2)->nullable();
            $table->dateTime('price_change_date')->nullable();
            $table->string('benefits');
            $table->boolean('status')->default(1)->comment("active->1 inactive->0");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sponsor_categories');
    }
}
