<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSouvenirRegPkgDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convention_souvenir_reg_pkg_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('souvenir_sponsor_type_id');
            $table->foreignId('user_id');
            $table->foreignId('registration_id');
            $table->integer('count')->default(0);
            $table->integer('total_amount')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('souvenir_reg_pkg_details');
    }
}
