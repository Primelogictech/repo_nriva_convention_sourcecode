<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBenefitTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convention_benefit_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('has_count')->comment("has count->1 no count->0")->default(0);
            $table->boolean('has_input')->comment("has input->1 no input->0")->default(0);
            $table->boolean('has_image')->comment("has image->1 no image->0")->default(0);
            $table->string('count')->nullable();
            $table->boolean('status')->default(1)->comment("active->1 inactive->0");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefit_types');
    }
}
