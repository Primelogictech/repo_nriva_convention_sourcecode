-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2022 at 08:17 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_nrivajan20`
--

-- --------------------------------------------------------

--
-- Table structure for table `convention_menu_items`
--

CREATE TABLE `convention_menu_items` (
  `id` int(11) NOT NULL,
  `menu_name` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `is_main_menu` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0-No,1-Yes,2-no submenu',
  `slug` varchar(150) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `convention_menu_items`
--

INSERT INTO `convention_menu_items` (`id`, `menu_name`, `parent_id`, `is_main_menu`, `slug`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 0, 2, 'admin', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(2, 'Registrations', 0, 1, '', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(5, 'Member Registrations', 2, 0, 'admin/registrations', 0, 0, '2022-01-23 22:30:19', '2022-01-28 17:21:38'),
(6, 'Exhibits Registrations', 2, 0, 'admin/exhibit-registrations', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(7, 'Shathamanam Bhavathi Registrations', 2, 0, 'admin/satamanam-bhavati-registrations', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(8, 'Matrimony Registrations', 2, 0, 'admin/matrimonyRegistrations', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(9, 'Mr, Mrs, Miss and Master NRIVA Registrations', 2, 0, 'admin/mr_missRegistrations', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(10, 'Home Page', 0, 1, '', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(11, 'Upload Banners', 10, 0, 'admin/banner', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(12, 'Change Venue Date&Time', 10, 0, 'admin/venue', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(13, 'Add Messages', 10, 0, 'admin/message', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(14, 'Members', 10, 0, 'admin/member', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(15, 'Events', 10, 0, 'admin/event', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(16, 'Schedule', 10, 0, 'admin/schedule', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(17, 'Youtube videos', 10, 0, 'admin/video', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(18, 'Content Management', 0, 1, '', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(19, 'Menu Management', 18, 0, 'admin/menu', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(20, 'Menu item Management', 18, 0, 'admin/submenu', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(21, 'Manage Pages', 18, 0, 'admin/page', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(22, 'Registation Page Conent', 18, 0, 'registration-page-content-update', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(23, 'Master', 0, 1, '', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(24, 'Sponser Category Type', 23, 0, 'admin/sponsor-category-type', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(25, 'Manage Sponser Category', 23, 0, 'admin/sponsor-category', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(26, 'Upload Right Side Logo', 23, 0, 'admin/right-logo', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(27, 'Upload Left Side Logo', 23, 0, 'admin/left-logo', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(28, 'Leadership Type', 23, 0, 'admin/leadership-type', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(29, 'Invitees Type', 23, 0, 'admin/invitee', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(30, 'Donor Type', 23, 0, 'admin/donortype', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(31, 'Designation Type', 23, 0, 'admin/designation', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(32, 'Payment Type', 23, 0, 'admin/paymenttype', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(33, 'Benifits Type', 23, 0, 'admin/benefittype', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(34, 'Exhibitor Type', 23, 0, 'admin/exhibitor-type', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(35, 'Committies', 23, 0, 'admin/committe', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(36, 'Mr And Miss Age', 23, 0, 'admin/mrandmisstype', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19'),
(37, 'Menu Management', 23, 0, 'admin/admin-menu', 0, 0, '2022-01-23 22:30:19', '2022-01-23 22:30:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `convention_menu_items`
--
ALTER TABLE `convention_menu_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `convention_menu_items`
--
ALTER TABLE `convention_menu_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
