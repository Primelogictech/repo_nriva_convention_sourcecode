<?php

namespace Tests\Feature\Http\Controllers\HomePage;

use App\Models\Admin\Member;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\HomePage\MemberController
 */
class MemberControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $members = Member::factory()->count(3)->create();

        $response = $this->get(route('member.index'));

        $response->assertOk();
        $response->assertViewIs('member.index');
        $response->assertViewHas('members');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('member.create'));

        $response->assertOk();
        $response->assertViewIs('member.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\MemberController::class,
            'store',
            \App\Http\Requests\MemberStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $name = $this->faker->name;
        $description = $this->faker->text;

        $response = $this->post(route('member.store'), [
            'name' => $name,
            'description' => $description,
        ]);

        $members = Member::query()
            ->where('name', $name)
            ->where('description', $description)
            ->get();
        $this->assertCount(1, $members);
        $member = $members->first();

        $response->assertRedirect(route('member.index'));
        $response->assertSessionHas('member.id', $member->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $member = Member::factory()->create();

        $response = $this->get(route('member.show', $member));

        $response->assertOk();
        $response->assertViewIs('member.show');
        $response->assertViewHas('member');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $member = Member::factory()->create();

        $response = $this->get(route('member.edit', $member));

        $response->assertOk();
        $response->assertViewIs('member.edit');
        $response->assertViewHas('member');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\MemberController::class,
            'update',
            \App\Http\Requests\MemberUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $member = Member::factory()->create();
        $name = $this->faker->name;
        $description = $this->faker->text;

        $response = $this->put(route('member.update', $member), [
            'name' => $name,
            'description' => $description,
        ]);

        $member->refresh();

        $response->assertRedirect(route('member.index'));
        $response->assertSessionHas('member.id', $member->id);

        $this->assertEquals($name, $member->name);
        $this->assertEquals($description, $member->description);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $member = Member::factory()->create();

        $response = $this->delete(route('member.destroy', $member));

        $response->assertRedirect(route('member.index'));

        $this->assertDeleted($member);
    }
}
