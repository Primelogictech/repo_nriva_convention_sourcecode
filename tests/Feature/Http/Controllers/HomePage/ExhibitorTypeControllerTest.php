<?php

namespace Tests\Feature\Http\Controllers\HomePage;

use App\Models\Admin\ExhibitorType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\HomePage\ExhibitorTypeController
 */
class ExhibitorTypeControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $exhibitorTypes = ExhibitorType::factory()->count(3)->create();

        $response = $this->get(route('exhibitor-type.index'));

        $response->assertOk();
        $response->assertViewIs('exhibitorType.index');
        $response->assertViewHas('exhibitorTypes');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('exhibitor-type.create'));

        $response->assertOk();
        $response->assertViewIs('exhibitorType.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\ExhibitorTypeController::class,
            'store',
            \App\Http\Requests\ExhibitorTypeStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $name = $this->faker->name;
        $size = $this->faker->word;
        $price_before = $this->faker->randomFloat(/** float_attributes **/);
        $price_after = $this->faker->randomFloat(/** float_attributes **/);

        $response = $this->post(route('exhibitor-type.store'), [
            'name' => $name,
            'size' => $size,
            'price_before' => $price_before,
            'price_after' => $price_after,
        ]);

        $exhibitorTypes = ExhibitorType::query()
            ->where('name', $name)
            ->where('size', $size)
            ->where('price_before', $price_before)
            ->where('price_after', $price_after)
            ->get();
        $this->assertCount(1, $exhibitorTypes);
        $exhibitorType = $exhibitorTypes->first();

        $response->assertRedirect(route('exhibitorType.index'));
        $response->assertSessionHas('exhibitorType.id', $exhibitorType->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $exhibitorType = ExhibitorType::factory()->create();

        $response = $this->get(route('exhibitor-type.show', $exhibitorType));

        $response->assertOk();
        $response->assertViewIs('exhibitorType.show');
        $response->assertViewHas('exhibitorType');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $exhibitorType = ExhibitorType::factory()->create();

        $response = $this->get(route('exhibitor-type.edit', $exhibitorType));

        $response->assertOk();
        $response->assertViewIs('exhibitorType.edit');
        $response->assertViewHas('exhibitorType');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\ExhibitorTypeController::class,
            'update',
            \App\Http\Requests\ExhibitorTypeUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $exhibitorType = ExhibitorType::factory()->create();
        $name = $this->faker->name;
        $size = $this->faker->word;
        $price_before = $this->faker->randomFloat(/** float_attributes **/);
        $price_after = $this->faker->randomFloat(/** float_attributes **/);

        $response = $this->put(route('exhibitor-type.update', $exhibitorType), [
            'name' => $name,
            'size' => $size,
            'price_before' => $price_before,
            'price_after' => $price_after,
        ]);

        $exhibitorType->refresh();

        $response->assertRedirect(route('exhibitorType.index'));
        $response->assertSessionHas('exhibitorType.id', $exhibitorType->id);

        $this->assertEquals($name, $exhibitorType->name);
        $this->assertEquals($size, $exhibitorType->size);
        $this->assertEquals($price_before, $exhibitorType->price_before);
        $this->assertEquals($price_after, $exhibitorType->price_after);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $exhibitorType = ExhibitorType::factory()->create();

        $response = $this->delete(route('exhibitor-type.destroy', $exhibitorType));

        $response->assertRedirect(route('exhibitorType.index'));

        $this->assertDeleted($exhibitorType);
    }
}
