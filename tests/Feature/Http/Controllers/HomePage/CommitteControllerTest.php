<?php

namespace Tests\Feature\Http\Controllers\HomePage;

use App\Models\Admin\Committe;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\HomePage\CommitteController
 */
class CommitteControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $committes = Committe::factory()->count(3)->create();

        $response = $this->get(route('committe.index'));

        $response->assertOk();
        $response->assertViewIs('committe.index');
        $response->assertViewHas('committes');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('committe.create'));

        $response->assertOk();
        $response->assertViewIs('committe.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\CommitteController::class,
            'store',
            \App\Http\Requests\CommitteStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $name = $this->faker->name;
        $image_url = $this->faker->word;

        $response = $this->post(route('committe.store'), [
            'name' => $name,
            'image_url' => $image_url,
        ]);

        $committes = Committe::query()
            ->where('name', $name)
            ->where('image_url', $image_url)
            ->get();
        $this->assertCount(1, $committes);
        $committe = $committes->first();

        $response->assertRedirect(route('committe.index'));
        $response->assertSessionHas('committe.id', $committe->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $committe = Committe::factory()->create();

        $response = $this->get(route('committe.show', $committe));

        $response->assertOk();
        $response->assertViewIs('committe.show');
        $response->assertViewHas('committe');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $committe = Committe::factory()->create();

        $response = $this->get(route('committe.edit', $committe));

        $response->assertOk();
        $response->assertViewIs('committe.edit');
        $response->assertViewHas('committe');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\CommitteController::class,
            'update',
            \App\Http\Requests\CommitteUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $committe = Committe::factory()->create();
        $name = $this->faker->name;
        $image_url = $this->faker->word;

        $response = $this->put(route('committe.update', $committe), [
            'name' => $name,
            'image_url' => $image_url,
        ]);

        $committe->refresh();

        $response->assertRedirect(route('committe.index'));
        $response->assertSessionHas('committe.id', $committe->id);

        $this->assertEquals($name, $committe->name);
        $this->assertEquals($image_url, $committe->image_url);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $committe = Committe::factory()->create();

        $response = $this->delete(route('committe.destroy', $committe));

        $response->assertRedirect(route('committe.index'));

        $this->assertDeleted($committe);
    }
}
