<?php

namespace Tests\Feature\Http\Controllers\HomePage;

use App\Models\Admin\SponsorCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\HomePage\SponsorCategoryController
 */
class SponsorCategoryControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $sponsorCategories = SponsorCategory::factory()->count(3)->create();

        $response = $this->get(route('sponsor-category.index'));

        $response->assertOk();
        $response->assertViewIs('sponsorCategory.index');
        $response->assertViewHas('sponsorCategories');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('sponsor-category.create'));

        $response->assertOk();
        $response->assertViewIs('sponsorCategory.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\SponsorCategoryController::class,
            'store',
            \App\Http\Requests\SponsorCategoryStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $name = $this->faker->name;

        $response = $this->post(route('sponsor-category.store'), [
            'name' => $name,
        ]);

        $sponsorCategories = SponsorCategory::query()
            ->where('name', $name)
            ->get();
        $this->assertCount(1, $sponsorCategories);
        $sponsorCategory = $sponsorCategories->first();

        $response->assertRedirect(route('sponsorCategory.index'));
        $response->assertSessionHas('sponsorCategory.id', $sponsorCategory->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $sponsorCategory = SponsorCategory::factory()->create();

        $response = $this->get(route('sponsor-category.show', $sponsorCategory));

        $response->assertOk();
        $response->assertViewIs('sponsorCategory.show');
        $response->assertViewHas('sponsorCategory');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $sponsorCategory = SponsorCategory::factory()->create();

        $response = $this->get(route('sponsor-category.edit', $sponsorCategory));

        $response->assertOk();
        $response->assertViewIs('sponsorCategory.edit');
        $response->assertViewHas('sponsorCategory');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\SponsorCategoryController::class,
            'update',
            \App\Http\Requests\SponsorCategoryUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $sponsorCategory = SponsorCategory::factory()->create();
        $name = $this->faker->name;

        $response = $this->put(route('sponsor-category.update', $sponsorCategory), [
            'name' => $name,
        ]);

        $sponsorCategory->refresh();

        $response->assertRedirect(route('sponsorCategory.index'));
        $response->assertSessionHas('sponsorCategory.id', $sponsorCategory->id);

        $this->assertEquals($name, $sponsorCategory->name);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $sponsorCategory = SponsorCategory::factory()->create();

        $response = $this->delete(route('sponsor-category.destroy', $sponsorCategory));

        $response->assertRedirect(route('sponsorCategory.index'));

        $this->assertDeleted($sponsorCategory);
    }
}
