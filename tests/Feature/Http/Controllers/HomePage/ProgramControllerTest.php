<?php

namespace Tests\Feature\Http\Controllers\HomePage;

use App\Models\Admin\Program;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\HomePage\ProgramController
 */
class ProgramControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $programs = Program::factory()->count(3)->create();

        $response = $this->get(route('program.index'));

        $response->assertOk();
        $response->assertViewIs('program.index');
        $response->assertViewHas('programs');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('program.create'));

        $response->assertOk();
        $response->assertViewIs('program.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\ProgramController::class,
            'store',
            \App\Http\Requests\ProgramStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $chair_name = $this->faker->word;
        $mobile_number = $this->faker->word;
        $co_chair_name = $this->faker->word;
        $program_name = $this->faker->word;
        $Location = $this->faker->word;
        $date = $this->faker->dateTime();
        $page_content = $this->faker->text;
        $status = $this->faker->boolean;

        $response = $this->post(route('program.store'), [
            'chair_name' => $chair_name,
            'mobile_number' => $mobile_number,
            'co_chair_name' => $co_chair_name,
            'program_name' => $program_name,
            'Location' => $Location,
            'date' => $date,
            'page_content' => $page_content,
            'status' => $status,
        ]);

        $programs = Program::query()
            ->where('chair_name', $chair_name)
            ->where('mobile_number', $mobile_number)
            ->where('co_chair_name', $co_chair_name)
            ->where('program_name', $program_name)
            ->where('Location', $Location)
            ->where('date', $date)
            ->where('page_content', $page_content)
            ->where('status', $status)
            ->get();
        $this->assertCount(1, $programs);
        $program = $programs->first();

        $response->assertRedirect(route('program.index'));
        $response->assertSessionHas('program.id', $program->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $program = Program::factory()->create();

        $response = $this->get(route('program.show', $program));

        $response->assertOk();
        $response->assertViewIs('program.show');
        $response->assertViewHas('program');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $program = Program::factory()->create();

        $response = $this->get(route('program.edit', $program));

        $response->assertOk();
        $response->assertViewIs('program.edit');
        $response->assertViewHas('program');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\ProgramController::class,
            'update',
            \App\Http\Requests\ProgramUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $program = Program::factory()->create();
        $chair_name = $this->faker->word;
        $mobile_number = $this->faker->word;
        $co_chair_name = $this->faker->word;
        $program_name = $this->faker->word;
        $Location = $this->faker->word;
        $date = $this->faker->dateTime();
        $page_content = $this->faker->text;
        $status = $this->faker->boolean;

        $response = $this->put(route('program.update', $program), [
            'chair_name' => $chair_name,
            'mobile_number' => $mobile_number,
            'co_chair_name' => $co_chair_name,
            'program_name' => $program_name,
            'Location' => $Location,
            'date' => $date,
            'page_content' => $page_content,
            'status' => $status,
        ]);

        $program->refresh();

        $response->assertRedirect(route('program.index'));
        $response->assertSessionHas('program.id', $program->id);

        $this->assertEquals($chair_name, $program->chair_name);
        $this->assertEquals($mobile_number, $program->mobile_number);
        $this->assertEquals($co_chair_name, $program->co_chair_name);
        $this->assertEquals($program_name, $program->program_name);
        $this->assertEquals($Location, $program->Location);
        $this->assertEquals($date, $program->date);
        $this->assertEquals($page_content, $program->page_content);
        $this->assertEquals($status, $program->status);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $program = Program::factory()->create();

        $response = $this->delete(route('program.destroy', $program));

        $response->assertRedirect(route('program.index'));

        $this->assertDeleted($program);
    }
}
