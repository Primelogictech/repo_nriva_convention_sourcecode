<?php

namespace Tests\Feature\Http\Controllers\Admin;

use App\Models\Admin\SponsorCategoryType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\Admin\SponsorCategoryTypeController
 */
class SponsorCategoryTypeControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $sponsorCategoryTypes = SponsorCategoryType::factory()->count(3)->create();

        $response = $this->get(route('sponsor-category-type.index'));

        $response->assertOk();
        $response->assertViewIs('sponsorCategoryType.index');
        $response->assertViewHas('sponsorCategoryTypes');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('sponsor-category-type.create'));

        $response->assertOk();
        $response->assertViewIs('sponsorCategoryType.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\Admin\SponsorCategoryTypeController::class,
            'store',
            \App\Http\Requests\SponsorCategoryTypeStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $name = $this->faker->name;
        $status = $this->faker->boolean;

        $response = $this->post(route('sponsor-category-type.store'), [
            'name' => $name,
            'status' => $status,
        ]);

        $sponsorCategoryTypes = SponsorCategoryType::query()
            ->where('name', $name)
            ->where('status', $status)
            ->get();
        $this->assertCount(1, $sponsorCategoryTypes);
        $sponsorCategoryType = $sponsorCategoryTypes->first();

        $response->assertRedirect(route('sponsorCategoryType.index'));
        $response->assertSessionHas('sponsorCategoryType.id', $sponsorCategoryType->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $sponsorCategoryType = SponsorCategoryType::factory()->create();

        $response = $this->get(route('sponsor-category-type.show', $sponsorCategoryType));

        $response->assertOk();
        $response->assertViewIs('sponsorCategoryType.show');
        $response->assertViewHas('sponsorCategoryType');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $sponsorCategoryType = SponsorCategoryType::factory()->create();

        $response = $this->get(route('sponsor-category-type.edit', $sponsorCategoryType));

        $response->assertOk();
        $response->assertViewIs('sponsorCategoryType.edit');
        $response->assertViewHas('sponsorCategoryType');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\Admin\SponsorCategoryTypeController::class,
            'update',
            \App\Http\Requests\SponsorCategoryTypeUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $sponsorCategoryType = SponsorCategoryType::factory()->create();
        $name = $this->faker->name;
        $status = $this->faker->boolean;

        $response = $this->put(route('sponsor-category-type.update', $sponsorCategoryType), [
            'name' => $name,
            'status' => $status,
        ]);

        $sponsorCategoryType->refresh();

        $response->assertRedirect(route('sponsorCategoryType.index'));
        $response->assertSessionHas('sponsorCategoryType.id', $sponsorCategoryType->id);

        $this->assertEquals($name, $sponsorCategoryType->name);
        $this->assertEquals($status, $sponsorCategoryType->status);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $sponsorCategoryType = SponsorCategoryType::factory()->create();

        $response = $this->delete(route('sponsor-category-type.destroy', $sponsorCategoryType));

        $response->assertRedirect(route('sponsorCategoryType.index'));

        $this->assertDeleted($sponsorCategoryType);
    }
}
