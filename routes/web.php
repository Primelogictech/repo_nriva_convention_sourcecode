<?php

use App\Http\Controllers\Admin\HomePage\BannerController;
use App\Http\Controllers\Admin\HomePage\VenueController;
use App\Http\Controllers\Admin\HomePage\MessageController;
use App\Http\Controllers\Admin\Menu\MenuController;
use App\Http\Controllers\Admin\Menu\SubMenuController;
use App\Http\Controllers\Admin\Menu\PageController;
use App\Http\Controllers\Admin\DesignationController;
use App\Http\Controllers\Admin\DonortypeController;
use App\Http\Controllers\Admin\MrAndMissController;
use App\Http\Controllers\Admin\InviteeController;
use App\Http\Controllers\Admin\LeadershipTypeController;
use App\Http\Controllers\Admin\SponsorCategoryTypeController;
use App\Http\Controllers\Admin\LeftLogoController;
use App\Http\Controllers\Admin\RightLogoController;
use App\Http\Controllers\Admin\HomePage\ProgramController;
use App\Http\Controllers\Admin\HomePage\EventController;
use App\Http\Controllers\Admin\HomePage\ScheduleController;
use App\Http\Controllers\Admin\HomePage\DonorController;
use App\Http\Controllers\Admin\HomePage\VideoController;
use App\Http\Controllers\Admin\HomePage\MemberController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\AdminRegistrationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\BusinessConferenceRegistrationController;
use App\Http\Controllers\YouthBanquetRegistrationController;
use App\Http\Controllers\SouvenirRegistrationController;
use App\Http\Controllers\CorporateSponsorsRegistrationController;
use App\Http\Controllers\Admin\PaymenttypeController;
use App\Http\Controllers\Admin\SponsorCategoryController;
use App\Http\Controllers\Admin\PaymentController;
use App\Http\Controllers\Admin\CorporateSponsorTypeController;
use App\Http\Controllers\Admin\BenefittypeController;
use App\Http\Controllers\Admin\CommitteController;
use App\Http\Controllers\Admin\SouvenirSponsorController;
use App\Http\Controllers\Admin\ExhibitorTypeController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Admin\VouchersController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'] );
Route::get('otp_send', [RegisteredUserController::class, 'send_verify_email']);
Route::post('new_registertion', [RegisteredUserController::class, 'new_registertion']);
Route::get('send-otp-for-login', [RegistrationController::class, 'sendOtpForLogin'])->name('send-otp-for-login');


Route::prefix('admin')->middleware(['admin','auth'])->group(function () {
    
    Route::get('/', [DashboardController::class, 'index'] );

    Route::resource('banner', BannerController::class);
    Route::put('banner-status-update', [BannerController::class ,'updateStatus']);

    Route::resource('venue', VenueController::class);

    Route::resource('message', MessageController::class);
    Route::put('message-status-update', [MessageController::class , 'updateStatus']);

    Route::get('admin-menu', [MenuController::class, 'adminMenuList']);
    Route::get('add-admin-menu', [MenuController::class, 'adminMenucreate']);
    Route::post('store-admin-menu', [MenuController::class, 'adminMenuStore']);
    Route::get('admin-menu-edit/{id}', [MenuController::class, 'adminedit']);
    Route::post('update-admin-menu', [MenuController::class, 'adminMenuUpdate']);

    Route::resource('menu', MenuController::class);
    Route::put('menu-status-update', [MenuController::class, 'updateStatus']);

    Route::resource('submenu', SubMenuController::class);
    Route::put('submenu-status-update', [SubMenuController::class, 'updateStatus']);
    Route::get('get-submenu-with-menu/{id}/{edit?}', [SubMenuController::class, 'GetSubmenuWithMenu']);

    Route::resource('page', PageController::class);
    Route::put('page-status-update', [PageController::class, 'updateStatus']);

    Route::resource('donor', DonorController::class);
    Route::put('donor-status-update', [DonorController::class, 'updateStatus']);



    Route::resource('video', VideoController::class);
    Route::put('video-status-update', [VideoController::class, 'updateStatus']);

    Route::resource('member', MemberController::class);
    Route::put('member-status-update', [MemberController::class, 'updateStatus']);
    Route::get('assigne-roles/{id}', [MemberController::class, 'showAssigneRolesPage']);
    Route::post('store-assigne-roles', [MemberController::class, 'storeAssigneRolesPage']);
    Route::get('assigne-permissions/{id}', [MemberController::class, 'showAssignePermissionsPage']);
    Route::post('store-assigne-permissions', [MemberController::class, 'storeAssignePermissionsPage']);

    // master routes
    Route::resource('designation', DesignationController::class);
    Route::put('designation-status-update', [DesignationController::class, 'updateStatus']);

    Route::resource('donortype', DonortypeController::class);
    Route::put('donortype-status-update', [DonortypeController::class, 'updateStatus']);

    Route::resource('mrandmisstype', MrAndMissController::class);
    Route::put('mrandmiss-status-update', [MrAndMissController::class, 'updateStatus']);

    
    Route::resource('souvenir-sponsortype', SouvenirSponsorController::class);
    Route::put('souvenir-sponsor-status-update', [SouvenirSponsorController::class, 'updateStatus']);

    Route::resource('invitee', InviteeController::class);
    Route::put('invitee-status-update', [InviteeController::class, 'updateStatus']);

    Route::resource('leadership-type', LeadershipTypeController::class);
    Route::put('leadership-type-status-update', [LeadershipTypeController::class, 'updateStatus']);

    Route::resource('sponsor-category-type', SponsorCategoryTypeController::class);
    Route::put('sponsor-category-type-status-update', [SponsorCategoryTypeController::class, 'updateStatus']);

    Route::resource('left-logo', LeftLogoController::class);
    Route::put('left-logo-status-update', [LeftLogoController::class, 'updateStatus']);
    Route::resource('right-logo', RightLogoController::class);
    Route::put('right-logo-status-update', [RightLogoController::class, 'updateStatus']);

    Route::resource('program', ProgramController::class);
    Route::put('program-status-update', [ProgramController::class, 'updateStatus']);

    Route::resource('event', EventController::class);
    Route::put('event-status-update', [EventController::class, 'updateStatus']);

    Route::resource('schedule', ScheduleController::class);

    Route::resource('sponsor-category', SponsorCategoryController::class);
    Route::put('sponsor-category-status-update', [SponsorCategoryController::class, 'updateStatus']);

    Route::resource('paymenttype', PaymenttypeController::class);
    Route::put('paymenttype-status-update', [PaymenttypeController::class, 'updateStatus']);

    Route::resource('benefittype', BenefittypeController::class);
    Route::put('benefittype-status-update', [BenefittypeController::class, 'updateStatus']);

    Route::resource('committe', CommitteController::class);
    Route::put('committe-status-update', [CommitteController::class, 'updateStatus']);

    Route::resource('exhibitor-type', ExhibitorTypeController::class);
    Route::put('exhibitor-type-status-update', [ExhibitorTypeController::class, 'updateStatus']);

    Route::resource('corporatesponsors', CorporateSponsorTypeController::class);
    Route::put('corporate-sponsors-type-status-update', [CorporateSponsorTypeController::class, 'updateStatus']);

    Route::get('treasury-admin', [RegistrationController::class, 'showRegistrationsForTresurer']);
    Route::put('update-fulfilment-status/{user_id}', [RegistrationController::class, 'updateFulfilmentStatus']);

    Route::get('delete-registration/{reg_id}', [RegistrationController::class, 'deleteRegistration']);
    Route::get('registrations', [RegistrationController::class, 'showRegistrations']);
    Route::get('assigne-features/{id}', [RegistrationController::class, 'showAssigneFeatures']);
    Route::put('assigne-features-update', [RegistrationController::class, 'updateAssigneFeatures']);
    Route::get('exhibit-registrations', [AdminRegistrationController::class, 'exhibitRegistrations']);
    Route::get('youth-banquet-registrations', [AdminRegistrationController::class, 'YouthBanquetRegistrations']);
    Route::get('corporate-sponsors', [AdminRegistrationController::class, 'CorporateSponsorsRegistrations']);
     Route::post('conventionUserImages', [AdminRegistrationController::class, 'conventionUserImages']);
     Route::resource('vouchers', VouchersController::class);
    Route::put('vouchers-status-update', [VouchersController::class, 'updateStatus']);

    Route::put('update-payment-amount', [AdminRegistrationController::class, 'updatePaymentAmount']);

    Route::post('donorImport', [RegistrationController::class, 'ImportRegesterUserStore']);
    Route::get('youth-activities-registrations', [AdminRegistrationController::class, 'youthActivitiesRegistrations']);
    Route::get('matrimonyRegistrations', [AdminRegistrationController::class, 'matrimonyRegistrations']);
    Route::post('matrimonyRegistrations', [AdminRegistrationController::class, 'matrimonyRegistrations']);
    Route::post('createbatch', [AdminRegistrationController::class, 'createbatch']);
    Route::post('removebatchusers', [AdminRegistrationController::class, 'removebatchusers']);

    Route::get('batchusers/{id}', [AdminRegistrationController::class, 'batchusers']);
    Route::get('viewbatch', [AdminRegistrationController::class, 'viewbatch']);
    Route::put('update_branch_user_order', [AdminRegistrationController::class, 'update_branch_user_order']);
    Route::get('matrimony_profile_downloade/{id}', [AdminRegistrationController::class, 'matrimonyProfileDownloade']);
    Route::get('mr_missRegistrations', [AdminRegistrationController::class, 'mrmissRegistrations']);
    Route::get('satamanam-bhavati-registrations', [AdminRegistrationController::class, 'SatamanamBhavatiRegistrations']);
    Route::get('souvenirRegistrations', [AdminRegistrationController::class, 'souvenirRegistrations']);
    Route::get('registrationsList', [AdminRegistrationController::class, 'registrationsList']);
    Route::post('registrationsList', [AdminRegistrationController::class, 'registrationsList']);
    Route::post('ImportRegistrations', [AdminRegistrationController::class, 'ImportRegistrations']);
    Route::post('checkedinRoom', [AdminRegistrationController::class, 'checkedinRoom']);

    Route::get('update_notes', [AdminRegistrationController::class, 'update_notes']);
    Route::get('update_tickets', [AdminRegistrationController::class, 'update_tickets']);
    Route::get('auditlogs', [AdminRegistrationController::class, 'auditLogs']);

    Route::get('add-note/{id}', [RegistrationController::class, 'showAddNoteForm']);
    Route::post('add-note', [RegistrationController::class, 'storeAddNoteForm'])->name('registration.addnote');
    
    Route::get('user/edit/{id}', [AdminRegistrationController::class, 'editMemberDetails']);
    Route::post('user/edit', [AdminRegistrationController::class, 'updateMemberDetails'])->name('registration.user.edit');

    Route::get('payment-history/{user_id}', [RegistrationController::class, 'adminSidePaymentHistory']);
    Route::put('update-payment-status/{payment_id}', [PaymentController::class, 'updatePaymentStatus']);
    
    Route::get('downlode-banner-image/{id}', [RegistrationController::class, 'download']);
    Route::post('members_search', [MemberController::class, 'members_search']);

    Route::get('ResendMail', [RegistrationController::class, 'ResendConventionRegMail']);
    Route::get('SendMailYouthBanquet', [YouthBanquetRegistrationController::class, 'SendYouthBanquetMail']);
    Route::get('ResendExhibitRegMail', [RegistrationController::class, 'ResendExhibitRegMail']);


    
});

Route::get('reload-captcha', [HomeController::class, 'reloadCaptcha']);

Route::get('payment', [PaymentController::class, 'index']);
Route::get('payment/status', [PaymentController::class, 'status'])->name('payment.status');
Route::get('paypal/status', [PaymentController::class, 'paypalStatus'])->name('paypal.status');
Route::get('padingpayment/status', [PaymentController::class, 'pendingstatus'])->name('pendingpayment.status');
Route::get('print-ticket', [RegistrationController::class, 'printTicket']);
 Route::get('registration-success', [RegistrationController::class, 'registrationsuccess']);
/* Route::get('/admin', function () {
    return view('admin/dashboard');
}); */
/* Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
 */

require __DIR__.'/auth.php';

Route::middleware(['auth'])->group(function () {
    Route::get('viewfeatuees', [RegistrationController::class, 'viewFeatures'])->name('viewfeatures');
    Route::get('paymenthistory', [RegistrationController::class, 'PaymentHistory'])->name('PaymentHistory');
    Route::get('pay-pending-amount', [PaymentController::class, 'payPendingAmount'])->name('pay-pending-amount');
    Route::post('pay-pending-amount', [PaymentController::class, 'storePayPendingAmount'])->name('pay-pending-amount');
    Route::get('myaccount', [RegistrationController::class, 'myaccount'])->name('myaccount');
    Route::get('change_password', [RegistrationController::class, 'change_password'])->name('change_password');
    Route::post('change_password', [RegistrationController::class, 'update_password'])->name('update_password');
     Route::get('upgrade', [RegistrationController::class, 'showUpgradeForm'])->name('upgrade');
    Route::post('upgrade', [RegistrationController::class, 'storeUpgradeForm'])->name('upgrade');
     Route::get('reg-pkg-upgrade', [RegistrationController::class, 'showRegPkgUpgradeForm'])->name('reg-pkg-upgrade');
    Route::post('reg-pkg-upgrade', [RegistrationController::class, 'storeRegPkgUpgradeForm'])->name('reg-pkg-upgrade');
    Route::get('exhibits-reservation-list', [RegistrationController::class, 'exibitRegestrationList']);
    
   
});
 Route::get('exhibits-reservation', [RegistrationController::class, 'exibitRegestration']);
 
    Route::get('show-registration-details/{id}', [RegistrationController::class, 'showRegistrationDetails']);

Route::get('youthActivities-regestration', [RegistrationController::class, 'youthActivitiesRegestration']);
Route::post('youthActivities-regestration', [RegistrationController::class, 'storeYouthActivitiesRegestration']);


Route::get('shathamanam-bhavathi-registration', [RegistrationController::class, 'ShathamanamBhavathiRegistration']);
Route::post('shathamanam-bhavathi-registration', [RegistrationController::class, 'storeShathamanamBhavathiRegistration']);

Route::get('souvenir-registration', [SouvenirRegistrationController::class, 'SouvenirRegistration']);
Route::post('souvenir-registration', [SouvenirRegistrationController::class, 'storeSouvenirRegistration']);
Route::delete('souvenir-registration-image-delete/{id}', [SouvenirRegistrationController::class, 'SouvenirImageDeleteRegistration']);

Route::get('business_conference_registration', [BusinessConferenceRegistrationController::class, 'showRegistration']);
Route::get('corporate_sponsors_registration', [CorporateSponsorsRegistrationController::class, 'showRegistration']);
Route::post('corporate_sponsors_registration', [CorporateSponsorsRegistrationController::class, 'PostSouvenirRegistration']);
Route::get('youth_banquet_registration', [YouthBanquetRegistrationController::class, 'showyouth_banquetRegistration']);
Route::get('youth-banquet-consent', [YouthBanquetRegistrationController::class, 'showyouth_banquet_consent']);
Route::post('youth-banquet-consent', [YouthBanquetRegistrationController::class, 'showyouth_banquet_consent']);
Route::post('youth-banquet-consent-store', [YouthBanquetRegistrationController::class, 'storeyouth_banquet_consent']);
Route::post('youth_banquet_registration', [YouthBanquetRegistrationController::class, 'Post_youth_banquetRegistration']);
Route::get('corporate-sponsors-registration-success', [CorporateSponsorsRegistrationController::class, 'registrationsuccess']);
Route::get('corporate-sponsor-paypal/status', [CorporateSponsorsRegistrationController::class, 'paypalStatus'])->name('corporatesponsorpayment.status');

Route::get('souvenir-email-verification', [SouvenirRegistrationController::class, 'email_verification']);
Route::post('souvenir_new_registertion', [SouvenirRegistrationController::class, 'new_registertion']);
Route::get('souvenir-paypal/status', [SouvenirRegistrationController::class, 'paypalStatus'])->name('souvenirpayment.status');


Route::get('/bookticket', function () {
    return redirect('/registration');
});

Route::get('registration', [RegistrationController::class, 'ShowRegistrationForm'])->name('registration');
Route::get('email_verification', [RegistrationController::class, 'email_verification'])->name('email_verification');
Route::get('check_email', [RegistrationController::class, 'check_email'])->name('check_email');
Route::post('exhibits-reservation', [RegistrationController::class, 'stoteExibitRegestrationForm']);
Route::post('update_banner_image', [RegistrationController::class, 'updateBannerImage']);


Route::post('bookticketstore', [RegistrationController::class, 'RegesterUserStore']);


Route::get('registration-page-content-update', [RegistrationController::class, 'RegistrationPageContentedit'])->name('registration-page-content-update');
Route::patch('registration-page-content-update', [RegistrationController::class, 'RegistrationPageContentUpdate'])->name('registration-page-content-update');

Route::get('/contact-us', [HomeController::class, 'showContactUs']);
Route::post('/contact-us', [HomeController::class, 'ContactUsSendMail']);

Route::get('/show-all-donors', [HomeController::class, 'showAllDonors']);

/* excel export routes */
Route::get('exhibits-registration-export', [AdminRegistrationController::class, 'ExhibitRegistrationsExport']);
Route::get('convention-registration-export', [AdminRegistrationController::class, 'ConventionRegistrationsExport']);
Route::get('satamanam-bhavati-registration-export', [AdminRegistrationController::class, 'satamanamBhavatiRegistrationsExport']);
Route::get('matrimony-registration-export', [AdminRegistrationController::class, 'matrimonyRegistrationsExport']);
Route::get('corporate-sonsors-registration-export', [AdminRegistrationController::class, 'corporateSonsorsRegistrationsExport']);
Route::get('youth-banquet-registration-export', [AdminRegistrationController::class, 'youthBanquetRegistrationsExport']);

Route::get('show-all-committees', [HomeController::class, 'showAllCommittees']);
Route::get('message_content/{id}', [HomeController::class, 'messageContent']);
Route::get('programs/{id}', [HomeController::class, 'programDetailsPage']);
Route::get('event-schedule/{id}', [HomeController::class, 'eventSchedule']);
Route::get('show-all-videos', [HomeController::class, 'showAllVideos']);
Route::get('more-donor-details/{id}', [HomeController::class, 'moreDonerDetails']);
Route::get('more-leadership-details/{id}', [HomeController::class, 'moreLeadershipDetails']);
Route::get('more-invitee-details/{id}', [HomeController::class, 'moreInviteeDetails']);
Route::get('more-committe-members/{id}', [HomeController::class, 'moreCommitteMembers']);
Route::get('show-events-on-calander', [HomeController::class, 'showEventsOnCalander']);


Route::get('api/get-schedule-on-date/{id}', [ScheduleController::class, 'getScheduleDatails']);
Route::get('api/get-donor-with-typeid/{id}', [HomeController::class, 'getDonorsWithTypeId']);
Route::get('api/get-leadership-with-typeid/{id}', [HomeController::class, 'getLeadershipsWithTypeId']);
Route::get('api/get-invitee-with-typeid/{id}', [HomeController::class, 'getinviteeWithTypeId']);
Route::get('api/get-invitee-with-typeid/{id}', [HomeController::class, 'getinviteeWithTypeId']);

Route::get('api/get-events-in-date', [HomeController::class, 'geteventsIndates']);
Route::get('api/get-details', [RegistrationController::class, 'getdetailsfromNriva']);

Route::put('api/update-is-required-fields', [RegistrationController::class, 'updateIsrequiredFields']);

Route::get('matrimony_registration', [RegistrationController::class, 'matrimony_registration']);
Route::post('matrimony_registration', [RegistrationController::class, 'matrimony_registration_store']);
Route::post('check_matrimony_email', [RegistrationController::class, 'check_matrimony_email'])->name('check_matrimony_email');
Route::post('newAuthorize', [RegisteredUserController::class, 'newAuthorize']);

Route::get('mr_mis_registration', [RegistrationController::class, 'mr_mis_registration']);
Route::get('mr_mis_registration_edit', [RegistrationController::class, 'mr_mis_registrationEdit']);
Route::post('mr_mis_registration', [RegistrationController::class, 'mr_mis_registration_store']);
Route::get('voucher_verification', [RegistrationController::class, 'voucher_verification'])->name('voucher_verification');



Route::get('/clear', function() {

    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    return "Cleared!";
});

Route::get('/linkstorage', function () {
    Artisan::call('storage:link');
});

Route::get('{slug}', [HomeController::class, 'dinamicPage']);




















