<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Admin\Venue;
use App\Models\Admin\Menu;
use App\Models\Admin\Logo;
use App\Models\Admin\Program;
use App\Models\MenuItem;
use App\Models\PermitionsType;
use App\Models\UserPermission;
use DB;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {   
        view()->composer('layouts.user.header', function ($view) {
            $data['venue'] = Venue::first();

           $data['menu'] = Menu::with( ['submenus' => function($query)
           {
                $query->where('status', 1)->orderBy(DB::raw('ISNULL(display_order), display_order'));
           }])->where('name', '!=','Programs')
               ->where('parent_id', 0)->where('status', 1)->orderBy(DB::raw('ISNULL(display_order), display_order'))
               ->get();

                $data['programsMenu']= Program::whereHas('committes',function($q){
            $q->where('is_program',1);

        })->get();

            $data['left_logo'] = Logo::where('type', 'left')->first();
            $data['right_logo'] = Logo::where('type', 'right')->first();
            $view->with('data', $data);
        });
        

        view()->composer('layouts.user.footer', function ($view) {
            $data['visitors_count'] = DB::table('convention_track_users')->count();
            $view->with('data', $data);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        view()->composer('layouts.admin.sidebar', function ($view) {
           $users=UserPermission::all();
           $user_menu_array=array();
           foreach($users as $user){
            $menuItems = MenuItem::whereIn('id', explode(',',$user->menu_id))->get();
            
            foreach ($menuItems as $key => $value) {
                
                if($value->parent_id!=0){
                    $user_menu_array[$user->member_id][$value->parent_id][]=array('id'=>$value->id,'name'=>$value->menu_name,'slug'=>$value->slug);
                }
                
            }
           } 
           $menuItems = MenuItem::all();
            $menu_array=array();
            $main_menu_name=array();
            foreach ($menuItems as $key => $value) {
                
                if($value->is_main_menu==1 || $value->is_main_menu==2){
                    $main_menu_name[$value->id]=$value->menu_name;
                }
                if($value->parent_id!=0){
                    $menu_array[$value->parent_id][]=array('id'=>$value->id,'name'=>$value->menu_name,'slug'=>$value->slug);
                }
                
            }
            $data['menu_array']=$menu_array;
            $data['main_menu_name']=$main_menu_name;
            $data['user_menu_array']=$user_menu_array;
            $view->with('data', $data);
        });
    }
}
