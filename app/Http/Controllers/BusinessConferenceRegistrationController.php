<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BusinessConferenceRegistrationController extends Controller
{
    public function showRegistration()
    {
        return view('registration.showBusinessConferenceRegistrationForm');
    }
}
