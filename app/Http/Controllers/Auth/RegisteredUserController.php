<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
    public function new_registertion(Request $request)
    {
        $request->validate([
           'email' => 'required|string|email|max:255|unique:convention_users',
            'otp'=>'required|exists:convention_otp_verification,otp']);

        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        $user->registration_id = "CON".sprintf("%04d", $user->id);
        $user->save();

        event(new Registered($user));

        Auth::login($user);

        if($request->register_type=="mr_miss"){
            return redirect('mr_mis_registration')->with('success','You have Authenticated successfully. Please continue to Miss. and Mr. NRIVA Registration');
        }

        return redirect('bookticket')->with('success','You have Authenticated successfully. Please continue to convention registration');
    }

     public function newAuthorize(Request $request)
    {

        $oldnrivauser = \DB::table('users')->where([
          'email' =>  $request->email,
        ])->first();
         
      if($oldnrivauser){
            if(\Hash::check($request->password, $oldnrivauser->password)){
             
            $user= User::where('email', $oldnrivauser->email)->first();
                if(!isset($user)){
                    $member = \DB::table('members')->where([
                        'user_id' =>  $oldnrivauser->id ])->first();
                $data = [
                    'email' => $oldnrivauser->email,
                    'first_name' => $oldnrivauser->first_name,
                    'last_name' => $oldnrivauser->last_name,
                    'phone_code' => $oldnrivauser->phone_code,
                    'mobile' => $oldnrivauser->mobile,
                    'country' => $member->country,
                    'state' => $member->state,
                    'city' => $member->city,
                    'zip_code' => $member->zipcode,
                    'address' => $member->address,
                    'address2' => $member->address2,
                    'member_id' => $oldnrivauser->member_id,
                ];
                    $user = User::create($data);
                    $user = User::where('email', $oldnrivauser->email)->first();
                   $user->registration_id = "CON".sprintf("%04d", $user->id);
                    $user->save();
                }
                 Auth::login($user);
            $request->session()->regenerate();
            return redirect('matrimony_registration');
           }else{
             return redirect('matrimony_registration')->with('error','Password Incorrect Please Enter Valid Password');
           }
        }else{
            return redirect('matrimony_registration')->with('error','Email Incorrect Please Enter Valid Email');

        }
    }
}
