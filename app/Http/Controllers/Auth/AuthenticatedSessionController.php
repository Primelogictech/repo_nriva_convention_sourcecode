<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
use App\Models\User;
use App\Models\UserPermission;
use Session;


class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        //https://nriva.apttechsol.com/public/api/check-credientials
      //  $response = Http::get(config('conventions.NRIVA_URL') . '/api/get-check-credientials?email=' . $request->email);

       $this->validate($request, [
        'email'   => 'required|email',
         'password' => 'required_without:login_otp',
         'login_otp' => 'required_without:password',
      ]);
       //dd($request);
      if( (!empty($request->login_otp))  && ($request->login_with=='with_otp')  ){
         $user=User::where('email',$request->email)
                ->where('login_otp',$request->login_otp)->first();
        
        if($user){
            Auth::login($user);
            $request->session()->regenerate();
            $user->login_otp=null;
            $user->save();
            ## menu permission related code --start
            $menu_permission_record = UserPermission::where('member_id', $user->member_id)
                                        ->first();
            if($menu_permission_record){
                        return redirect('admin');
                    }
            ## menu permission related code --end
            if($request->redirect_type=="conventions"){
                return redirect('registration');
            }elseif($request->redirect_type=="mr_miss"){
                return redirect('mr_mis_registration');
            }
            if(in_array($user->email,Config('conventions.admin_emails'))){
                return redirect('admin/registrations');
            }else{
                 return redirect()->route('myaccount');
            }
        }else{
            return redirect()->route('login')->with('error','Invalid Login Details'); 
        }
      }elseif(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            Auth::login($user);
            $request->session()->regenerate();
            ## menu permission related code --start
            $menu_permission_record = UserPermission::where('member_id', $user->member_id)
                                        ->first();
            if($menu_permission_record){
                        return redirect('admin');
                    }
            ## menu permission related code --end
                    if($request->redirect_type=="conventions"){
                        return redirect('registration');
                    }elseif($request->redirect_type=="mr_miss"){
                        return redirect('mr_mis_registration');
                    }
                    if(in_array($user->email,Config('conventions.admin_emails'))){
                        return redirect('admin/registrations');
                    }else{
                         return redirect()->route('myaccount');
                    }
                    
        }else {

          $oldnrivauser = \DB::table('users')->where([
          'email' =>  $request->email,
        ])->first();
          $logindetails =false;
      if($oldnrivauser){
            if(\Hash::check($request->password, $oldnrivauser->password)){
              $logindetails =true;
              }
        }
          
        $is_newUser = false;
         if($logindetails){
            $user= User::where('email', $oldnrivauser->email)->first();
                if(!isset($user)){
                    $this->createUser($oldnrivauser);
                    $is_newUser=true;
                    $user = User::where('email', $oldnrivauser->email)->first();
                    $user->member_id = $oldnrivauser->member_id;
                    $user->registration_id = "CON".sprintf("%04d", $user->id);
                    $user->save();
                }
            //Session::put('user', $user);

            Auth::login($user);
                            
            $request->session()->regenerate();
            ## menu permission related code --start

            $menu_permission_record = UserPermission::where('member_id', $user->member_id)
                                        ->first();
            if($menu_permission_record){
                        return redirect('admin');
                    }
            ## menu permission related code --end    
                if($request->redirect_type=="conventions"){
                            return redirect('registration');
                }
                elseif($request->redirect_type=="mr_miss"){
                    return redirect('mr_mis_registration');
                }
                if(in_array($user->email,Config('conventions.admin_emails'))){
                        return redirect('admin/registrations');
                }
                else{
                        if($is_newUser){
                            return redirect()->route('myaccount');
                        }
                        return redirect()->route('registration');
                }
                    
        }else {
            return redirect()->route('login')->with('error','Invalid Login Details');
        }

        }

      


    }

    public function createUser($response)
    {
       $member = \DB::table('members')->where([
          'user_id' =>  $response->id ])->first();
        if($member){
            $membersignificants = \DB::table('membersignificants')->where([
                'member_id' =>  $member->id, 
                'relationship'=>'Spouse'
                ])->first();
        }
        $data = [
            'email' => $response->email,
            'first_name' => $response->first_name,
            'last_name' => $response->last_name,
            'phone_code' => $response->phone_code,
            'mobile' => $response->mobile,
            'country' => $member->country,
            'state' => $member->state,
            'city' => $member->city,
            'zip_code' => $member->zipcode,
            'address' => $member->address,
            'address2' => $member->address2,
            'spouse_first_name' => $membersignificants->first_name??"",
            'spouse_last_name' => $membersignificants->last_name??"",
        ];
        $user = User::create($data);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
