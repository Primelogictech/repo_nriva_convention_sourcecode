<?php

namespace App\Http\Controllers;

use App\Models\Admin\SponsorCategory;
use App\Models\Admin\SponsorCategoryType;
use App\Models\Admin\RegistrationContent;
use App\Http\Controllers\Admin\PaymentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use App\Models\Admin\ExhibitorType;

use App\Http\Controllers\Controller;
use App\Models\Admin\Donortype;
use App\Models\Admin\Paymenttype;
use App\Models\Admin\BannerImages;
use App\Models\Admin\Vouchers;
use App\Models\User;
use App\Models\Kids;
use App\Models\Registration;
use App\Models\Matrimony_model;
use App\Models\Mr_Miss_Model;
use App\Models\CorporateSponsorReg;
use App\Models\YouthBanquet_Model;
use App\Models\Admin\MrAndMissTypes;
use App\Models\Payment as ModelPayment;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use App\Models\Admin\Venue;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Carbon\Carbon;
use URL;
use Session;
use Config;
use DB;
use Excel;
use App\Models\Otp_model;
use Mail;
use App\Mail\NewConventionRegistration;
use App\Mail\NewExhibitRegistration;
use App\Mail\NewMrAndMissRegistration;
use App\Mail\NewMatrimonyRegistration;
use App\Mail\NewShathamanamBhavathiRegistration;
use App\Mail\ImageNotification;
use App\Imports\UsersImport;
use File;
class RegistrationController extends Controller
{

    public function __construct()
    {
        $this->_api_context = "";
    }


    public function ShowRegistrationForm()
    {
        session::forget('payment_id');
        session::forget('sponsor_category');
        session::forget('category_id');
        session::forget('Individual_amount');
         if(Auth::user()){
        $user=User::find(Auth::user()->id);
            if(!empty($user->total_amount)){
                return redirect('myaccount');
            }
        }
        else{
            $user=array();
        }
        $SponsorCategoryTypes= SponsorCategoryType::where('status',1)->get();
        $Individuals= SponsorCategory::where('status',1)->where('donor_type_id', 0)->get();
        //dd($Individuals);
        $donors= SponsorCategory::with('donortype','benfits')->where('status',1)->where('donor_type_id','!=' ,0)->get();
        
        $RegistrationContent = RegistrationContent::first();
        $paymenttype = Paymenttype::where('status',1)->get();
        $states=DB::table('states')->get();
        $chapters=DB::table('chapters')->where('status',1)->orderby('name')->get();
        $registrationtype=null;
       // dd($donors[0]->benfits[0]-> pivot->count);
        return view('registration',compact("SponsorCategoryTypes", 'Individuals', 'donors', 'RegistrationContent', 'user', 'paymenttype', 'states','registrationtype','chapters'));
    }
//
    public function RegesterUserStore(Request $request)
    {   
        if(!($request->has('is_upgrade') && $request->is_upgrade='upgrade')){
            $final_rules= [
                'email' => 'required|string|email|max:255|unique:convention_users',
            ];
        }else{
            $final_rules=[];
        }
        if ($request->hasfile('benfitimage')) {
              $rules1= [
                  'benfitimage.*' => ['image','mimes:jpg,png','max:5120',
                ],
            ];
            $final_rules=array_merge($final_rules, $rules1);
        }
            $request->validate($final_rules);

           $check_user= User::where('email',$request->email)->first();
        if(!$check_user){
            $user = User::create([
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            $user->registration_id = "CON".sprintf("%04d", $user->id);
            $user->save();
            event(new Registered($user));

            Auth::login($user);
        }


         
          $kids_details=array();
        if(!empty($request->Kid_name[0]) ){
            foreach ($request->Kid_name as $key => $value) {
                $a=[
                    'name' => $request->Kid_name[$key],
                    'age' => $request->Kid_age[$key]
                ];
                array_push($kids_details, $a);
            }
        }


        if(!empty($request->sponsor_category)){
            $SponsorCategory=SponsorCategory::find($request->sponsor_category);
            $benfits_Ids= $SponsorCategory->benfits->pluck('id')->toArray();
        }


        $user = user::where('email',$request->email)->first();

        $user->spouse_first_name = $request->spouse_first_name;
        $user->spouse_last_name = $request->spouse_last_name;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->address = $request->address;
        $user->address2 = $request->address2;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->country = $request->country;
        $user->zip_code = $request->zip_code;
        $user->mobile = $request->mobile;
        $user->kids_details = $kids_details;
        $user->chapter = $request->chapter;
        $user->save();



        if($request->is_upgrade== 'upgrade'){
            $is_upgrade = true;
            $request->category= 'Donor';
            if($request->sponsor_category==null){
                $request->sponsor_category=$user->sponsorship_category_id;
            }
        }else {
            $is_upgrade = false;
        }
        if($request->has('pay_partial_amount')){
            $paying_amount=$request->paying_amount;
        }else{
            /*if($request->category == 'Donor'){
                $paying_amount= $request->donation_amount_hidden;
                if($is_upgrade){
                    $paying_amount = $paying_amount- $user->amount_paid;
                }
            }else{
                $paying_amount= $request->registration_amount_hidden;
            }*/

            //dd($request->donation_amount_hidden,$request->registration_amount_hidden,$user->amount_paid);

            $paying_amount = $request->donation_amount_hidden+$request->registration_amount_hidden- $user->amount_paid;
           // dd($paying_amount);
        }
        $Paymenttype = Paymenttype::find($request->payment_type);

        $user = User::where('email', $request->email)->first();

        if (!isset($user)) {
            $user = User::create($request->all());
            if ($request->hasfile('image')) {
                $file_name = 'user' . '_' . $user->id . '.' . $request->image->getClientOriginalExtension();
                $request->file('image')->storeAs(config('conventions.user_upload'), $file_name);
                user::where('id', $user->id)->update(['image_url' => $file_name]);
            }
        }
        
            //$SponsorCategory  = SponsorCategory::find($request->sponsor_category);
            $user = User::where('email', $request->email)->first();

             if ($request->hasfile('benfitimage')) {
              foreach ($request->benfitimage as $key => $image) {
                  $key=explode("_",$key);
                    if($key[0]==$request->sponsor_category){
                        $key=$key[1];
                            $file_name = 'benfit_id' . '_' .$key."_". $user->id . '.' . $image->getClientOriginalExtension();
                            $image->storeAs(config('conventions.benfit_image_upload'), $file_name);
                                $temp = [
                                    'user_id' => $user->id,
                                    'benfit_id' => $key,
                                    'image_url' => $file_name,
                                ];
                            $BannerImages = BannerImages::create($temp);
                    }
                }
            }

            // call payment methord
           /* if($Paymenttype->name== Config('conventions.paypal_name_db')){
                $user= Auth::user();
                $user->total_amount = $SponsorCategory->amount;
                $user->save();
                return  $this->PaypalPayment($paying_amount, $user->id, $request, $Paymenttype->id, $is_upgrade);
            }else{
                $this->Dopayment($request,  $user,  $paying_amount,$is_upgrade);

                $user->sponsorship_category_id = $SponsorCategory->id;
                if($request->has('pay_partial_amount')){
                    $user->payment_status = 'Partial Paid';
                }else{
                    $user->payment_status = 'Paid';
                }
                $user->total_amount = $SponsorCategory->amount;
                if($is_upgrade){
                    $user->amount_paid = $user->amount_paid+$paying_amount;
                }else{
                    $user->amount_paid = $paying_amount;
                }
                $user->registration_type_id = $category->id;
                $user->save();
                return redirect('myaccount');
            }
        }

        if($request->category== "Family / Individual")
        {
            $category = SponsorCategoryType::where('name', 'Family / Individual')->first();*/
            $SponsorCategory  = SponsorCategory::find($request->sponsor_category);
            //dd($SponsorCategory->benfits);
            $individual_registration=[];
            $total_amount = 0;
            foreach ($request->count as $key => $value) {
                $Individual = SponsorCategory::where('status', 1)->where('id',$key)->first();
                $a=[
                    $key => $value
                ];
                $individual_registration = $a+ $individual_registration;
            }

            $total_amount = $request->registration_amount_hidden+$request->donation_amount_hidden;
            $user = User::where('email', $request->email)->first();
            // call payment methord

            $package_details=array();
            $all_details=array();

            array_push($package_details, array('package_name'=> "Convention Registration"));
            if($request->registration_amount_hidden){
                array_push($all_details, array('Registration Package'));
            }
            
            if($request->donation_amount_hidden){
                array_push($all_details, array($SponsorCategory->donortype->name??''));
            }
            array_push($package_details, array('package_details'=> $all_details));
             
            $request->merge(['package_details' =>$package_details]);

            if ($Paymenttype->name == Config('conventions.paypal_name_db')) {
                Session::put('individual_registration', $individual_registration);
                Session::put('Individual_amount', $total_amount);
                Session::put('registration_amount', $request->registration_amount_hidden);
                Session::put('donor_amount', $request->donation_amount_hidden);
                return  $this->PaypalPayment($paying_amount, $user->id, $request, $Paymenttype->id);
            } else {
                $payment_details = $this->Dopayment($request,  $user,  $paying_amount);

                $user->sponsorship_category_id = $SponsorCategory->id??0;
                $user->individual_registration =  $individual_registration;
                $user->payment_status = 'Pending';
                $user->total_amount = $total_amount;
                $user->amount_paid = $user->amount_paid+$paying_amount;
                $user->registration_amount = $request->registration_amount_hidden;
                $user->donor_amount = $request->donation_amount_hidden;
                $user->free_exbhit_registrations = 0;
                if($user->sponsorship_category_id>0){
                    foreach($SponsorCategory->benfits as $benfits){
                    if($benfits->name=='Vendor booth spaces at the event'){

                        $user->free_exbhit_registrations = (int)$benfits->pivot->count??0;
                        $user->vendor_booth_space_count = (int)$benfits->pivot->count??0;

                        }
                    }
                }
                //dd($user->free_exbhit_registrations);
                $user->save();

                //myaccount.blade
                 Mail::to($request->email)
                 ->cc('registration@nriva.org')
                 ->send(new NewConventionRegistration($user,$Paymenttype));

                 $images=BannerImages::where('user_id',$user->id)->get();
                 if(count($images)>0){
                     Mail::to('souvenir@nriva.org')
                     ->send(new ImageNotification($user));
                 }

                return redirect('myaccount')->with('message-suc', "Convention Registration Completed Successfully"); //;
            }
        }
public function showUpgradeForm()
    {
        session::forget('payment_id');
        session::forget('sponsor_category');
        session::forget('category_id');
        session::forget('Individual_amount');
        session::forget('is_upgrade');
         if(Auth::user()){
        $user=User::find(Auth::user()->id);
           
        }
        else{
            $user=array();
        }
        $SponsorCategoryTypes= SponsorCategoryType::where('status',1)->get();
        $Individuals= SponsorCategory::where('status',1)->where('donor_type_id', 0)->get();
        $donors= SponsorCategory::with('donortype','benfits')->where('status',1)->where('donor_type_id','!=' ,0)->get();
        $RegistrationContent = RegistrationContent::first();
        $paymenttype = Paymenttype::where('status',1)->get();
        $states=DB::table('states')->get();
        $chapters=DB::table('chapters')->orderby('name')->get();
       // dd($donors[0]->benfits[0]-> pivot->count);
        $registrationtype=1;
        return view('registration',compact("SponsorCategoryTypes", 'Individuals', 'donors', 'RegistrationContent', 'user', 'paymenttype', 'states','registrationtype','chapters'));
    }
    public function PaypalPayment($amount, $user_id, $request,$payment_id, $is_upgrade=false)
    {
        $name = "";
        $name = $request->package_details[0]['package_name'].' - ';
        foreach($request->package_details[1]['package_details']  as $pkg_dts){
            $name = $name .  $pkg_dts[0].', ';
        }

        $settings = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(config('conventions.PAYPAL_SANDBOX_CLIENT_ID'), config('conventions.PAYPAL_SANDBOX_CLIENT_SECRET') ));
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings);
        $payer = new Payer();

        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $userCurrency = 'USD';
        $pay_amount = $amount;
        $item_1->setName($name)
        ->setCurrency($userCurrency)
            ->setQuantity(1)
            ->setPrice($pay_amount);
       

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency($userCurrency)
            ->setTotal($pay_amount);
        $timestamp = Carbon::now()->toDateTimeString();

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setInvoiceNumber($timestamp . '_' . rand(10000,99999))
            ->setItemList($item_list)
            ->setDescription('Transaction Details');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('payment.status'))
        ->setCancelUrl(URL::route('payment.status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
        ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
            if($is_upgrade){
                $account_status= 'Plan Upgrade';
            }else{
                $account_status= 'Initial Payment';
            }
            $temp = [
                'user_id' => $user_id,
                'payment_amount' => $pay_amount,
                'payment_methord' => $payment_id,
                'unique_id_for_payment' => $payment->id,
                'more_info' => $request->more_info,
                'payment_status' => 'pending',
                'account_status' => $account_status,
                'package_details' =>$request->package_details,
                'payment_made_towards' => "Convention Registration"
            ];
            $Modelpayment = ModelPayment::create($temp);

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {

            if (\Config::get('app.debug')) {
                dd($ex);
                return redirect()->back()->withErrors('Connection timeout');
            } else {
                return redirect()->back()->withErrors('Some error occur, sorry for inconvenient');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        Session::put('payment_id', $Modelpayment->id);
        Session::put('sponsor_category', $request->sponsor_category);
        $SponsorCategoryType=SponsorCategoryType::where('name', $request->category)->first();
        Session::put('category_id', $SponsorCategoryType->id??'');

        if (isset($redirect_url)) {
            return redirect($redirect_url);
        }
        
    }

    public function Dopayment($request, $user,  $paying_amount,$is_upgrade=false)
    {

        if ($is_upgrade) {
            $account_status = 'Plan Upgrade';
        } else {
            $account_status = 'Initial Payment';
        }
        $Paymenttype=Paymenttype::find($request->payment_type);

        if($Paymenttype->name== Config('conventions.check_name_db')){
              $more_info = [
                'cheque_date' => $request->cheque_date,
                'bank_name' => $request->bank_name,
                'check_received_by' =>  $request->check_received_by
            ];
            $temp=[
                'user_id'=> $user->id,
                'payment_amount'=> $paying_amount,
                'payment_methord'=> $Paymenttype->id,
                'unique_id_for_payment'=>$request->cheque_number,
                'more_info'=> $more_info,
                'payment_status' => 'Pending',
                'account_status' => $account_status,
                'package_details' =>$request->package_details,
                'payment_made_towards' => "Convention Registration"
            ];
            $payment = ModelPayment::create($temp);

        }
        if($Paymenttype->name== Config('conventions.zelle_name_db')){
           
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->Zelle_Reference_Number,
                'payment_status' => 'Inprocess',
                'account_status' => $account_status,
                'package_details' =>$request->package_details,
                'payment_made_towards' => "Convention Registration"
            ];
            $payment = ModelPayment::create($temp);
        }

        if ($Paymenttype->name == Config('conventions.other_name_db')) {
            $file_name='';
            if ($request->hasfile('other_document')) {
                $file_name = 'document' . '_' . time().$user->id . '.' . $request->other_document->getClientOriginalExtension();
                $request->file('other_document')->storeAs(config('conventions.user_upload'), $file_name);
               
            }

            $more_info=[
                'transaction_date'=> $request->transaction_date,
                'payee_name'=> $request->payee_name,
                'company_name'=> $request->company_name,
                'document'=> $file_name,
            ];
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->transaction_id,
                'more_info' => $more_info,
                'payment_status' => 'Inprocess',
                'account_status' => $account_status,
                'package_details' =>$request->package_details,
                'payment_made_towards' => "Convention Registration"
            ];
            $payment = ModelPayment::create($temp);
        }



    }

    public function RegistrationPageContentedit()
    {
        $RegistrationContent=RegistrationContent::first();
        $paymenttypes = Paymenttype::where('status', 1)->get();
        return view('admin.ContentMangement.Registration.contentupdate', compact('RegistrationContent', 'paymenttypes'));
    }

    public function RegistrationPageContentUpdate(Request $request)
    {
        $RegistrationContent = RegistrationContent::first();
        $RegistrationContent->update($request->all());
        foreach ($request->payment_note as $id => $note) {
            Paymenttype::where('id', $id)->update(['note' => $note]);
        }
        $paymenttypes = Paymenttype::where('status', 1)->get();
        return view('admin.ContentMangement.Registration.contentupdate', compact('RegistrationContent', 'paymenttypes'));
    }

    public function getdetailsfromNriva(Request $request)
    {
     //   $response = Http::get('http://localhost/nriva/public/api/get-details?email='.$request->email);
        $response = Http::get( Config('conventions.NRIVA_URL').'/api/get-details?email='.$request->email);
        return $response;
    }

    public function showRegistrationsForTresurer()
    { 
        $users=User::with('registrationtype')->where('email','!=', Config('conventions.admin_email'))->orderBy('created_at', 'desc')->get();
        $fulfilment_statuses=array("Hold Initiation"=>"Hold Initiation","Ready for Initiation"=>"Ready for Initiation");
        $isForTreasury=1;
        return view('admin.registrations.index', compact('users','fulfilment_statuses','isForTreasury'));

    }
    public function updateFulfilmentStatus(Request $request, $user_id)
    {
        $result=0;
        
        User::where('id',$user_id)->update([
                'fulfilment_status'=>$request->status
            ]); 
        
        $result=1;

        return $result;



    }
    public function deleteRegistration($reg_id)
    {   
        User::where('id',$reg_id)->delete(); 
        
      return redirect('admin/registrations')->with('message-suc', "Convention Registration Deleted Successfully");



    }

    public function showRegistrations()
    { 
        $users=User::with('registrationtype')->where('email','!=', Config('conventions.admin_email'))->orderBy('created_at', 'desc')->get();
        $isForTreasury=0;
        return view('admin.registrations.index', compact('users','isForTreasury'));

    }

    public function showAssigneFeatures($id)
    {
        $user=User::FindOrFail($id);
        $user_benfits= $user->benfits;
        $bannerimages= $user->bannerimages;
        return view('admin.registrations.assigneFeatures', compact('user','user_benfits','bannerimages'));
    }

    public function updateAssigneFeatures(Request $request)
    {
        $user=User::find($request->user_id);
        $ben=$user->benfits()->where('benfit_id', $request->id)->first();
        if(isset($ben)){
            return   $user->benfits()->updateExistingPivot($request->id,['content' => $request->content]);
        }else{
            return   $user->benfits()->attach($request->id,['content' => $request->content]);
        }
    }

    public function ResendConventionRegMail(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $Payment=ModelPayment::where('payment_made_towards','Convention Registration')->where('user_id',$user->id)->first();
        if(!($user && $Payment )){
            return redirect('admin/registrations')->withErrors("Some thing went Wrong");
        }
        $Paymenttype=Paymenttype::where('id',$Payment->payment_methord)->first();
        Mail::to($request->email)
       // ->cc('registration@nriva.org')
        ->send(new NewConventionRegistration($user,$Paymenttype,$Payment));
        return redirect('admin/registrations')->with('message-suc', "Email Sent Successfully");
    } 

    public function ResendExhibitRegMail(Request $request)
    {
        $exhibitUser = Registration::where('id', $request->id)->first();
        
        if($exhibitUser){
            $payment=$exhibitUser->paymentdata;
            $paymenttype=$payment->paymentmethord;
        }
       
        if(!($exhibitUser && $payment)){
            return redirect('admin/exhibit-registrations')->withErrors("Some thing went Wrong");
        }

        Mail::to($exhibitUser->email)
        //->cc('exhibits@nriva.org')
        ->send(new NewExhibitRegistration($exhibitUser,$paymenttype,$payment));

        return redirect('admin/exhibit-registrations')->with('message-suc', "Email Sent Successfully");
    } 

    public function updateIsrequiredFields(Request $request)
    {
        $val=null;
        $field=null;
        $request->val=='Yes'? $val=1 : $val=0;
        $request->type=='Exbhit'? $field='is_exbhit_required' : $field='is_shathamanam_bhavathi_required';

        $user=User::find($request->user_id);
        $user->{$field}=$val;
        $user->save();
    }


    public function myaccount()
    {
        $user=auth::user();
        $registrations= Registration::where('user_id',$user->id)->get();
        $exhibit_registrations_count = Registration::where('user_id',$user->id)->where('registration_type_name','Exhibit')->count();
        $matrimony_registrations =Matrimony_model::join('users','convention_matrimony_registrations.email','=','users.email')
        ->select('users.created_at as nriva_created_at','convention_matrimony_registrations.*','users.email','users.member_id','users.first_name','users.last_name')
            ->where('convention_matrimony_registrations.email',Auth::user()->email)
            ->orwhere('convention_matrimony_registrations.convention_id',Auth::user()->registration_id)
            ->get();
        $mr_miss= Mr_Miss_Model::where(['user_id'=>Auth::user()->id])->get();
        $MrAndMissTypes = MrAndMissTypes::where('status',1)->get();
        $shathamanamBhavathiRegistrations=Registration::where('registration_type_name','ShathamanamBhavathi')->where('email',$user->email)->get();
        $SouvenirRegistrations=Registration::with('souvenirDetails')->where('registration_type_name','Souvenir')->where('email',$user->email)->get();
        $CorporateSponsorReg=CorporateSponsorReg::with('paymentdata')->where('email',$user->email)->get();
        $youth_banquet_reg=YouthBanquet_Model::where('convention_id',$user->registration_id)->orderby('created_at',"desc")->get();
        return view('myaccount', compact('registrations','shathamanamBhavathiRegistrations','matrimony_registrations','mr_miss','MrAndMissTypes','SouvenirRegistrations','exhibit_registrations_count','CorporateSponsorReg','youth_banquet_reg'));
    }


    public function PaymentHistory()
    {
            $payments=ModelPayment::where('user_id', Auth::user()->id)->with('paymentmethord')->get();
            return view('PaymentHistory',compact('payments'));
    }

    public function adminSidePaymentHistory($user_id)
    {
        $payments = ModelPayment::where('user_id', $user_id)->with('paymentmethord')->get();
        return view('admin.registrations.adminSidePymentHistory', compact('payments'));
    }

    public function printTicket()
    {
        $venue = Venue::first();

 //       $payments = ModelPayment::where('user_id', Auth::user()->id)->with('paymentmethord')->get();
        return view('printticket', compact('venue'));
    }

    public function viewfeatures()
    {
        $user= Auth::user();
        $categorydetails= $user->categorydetails;
        $benfits=$user->benfits;
        $bannerimages=$user->bannerimages;
        return view('viewfeatures', compact('categorydetails', 'benfits','bannerimages'));
    }

    public function showAddNoteForm($id)
    {
        $user =User::findOrFail($id);
        return view('admin.registrations.addNote', compact('user'));
    }

    public function storeAddNoteForm(Request $request)
    {
        //registration_note
        User::where('id', $request->user_id)->update([
            'registration_note'=>$request->registration_note
        ]);
        return $this->showAddNoteForm($request->user_id);
    }


    public function exibitRegestration(Request $request)
    {
       // dd($request->all());
        $exhibitsTypes= ExhibitorType::where('status',1)->get();
        $RegistrationContent = RegistrationContent::first();
        $paymenttypes = Paymenttype::where('status', 1)->get();

        $Venue = Venue::first();

        return view('registration.showexhibitsRegistrationForm', compact('exhibitsTypes', 'RegistrationContent', 'paymenttypes', 'Venue','request'));
    }
    public function exibitRegestrationList($value='')
    {
        $list = Registration::where('email',Auth::user()->email)->with(['paymentdata','exhibitorTypes'])->where('registration_type_name','Exhibit')->get();
        if( count($list)<1){
            return redirect('exhibits-reservation');
        }
         return view('registration.exhibitsList', compact('list'));
    }

    public function youthActivitiesRegestration()
    {
        return view('registration.youthActivitiesRegistration');
    }

    public function ShathamanamBhavathiRegistration()
    {
        return view('registration.ShowShathamanamBhavathiRegistration');
    }

    public function storeYouthActivitiesRegestration(Request $request)
    {
           $data=array_merge($request->all(),
        [
            'user_id' => Auth::user()->id??0,
            'registration_type_name' => "YouthActivities"
            ]
        );
            $Registration = Registration::create($data);
            $Registration->registration_id="YOU".sprintf("%04d", $Registration->id);
            $Registration->save();
        return view('registration.youthActivitiesRegistration')->with('message-suc', "Youth Activities registered successfully");;
    }

    public function storeShathamanamBhavathiRegistration(Request $request)
    {
        $user=User::where('email',$request->email)->first();
        if(empty($user)){
            return redirect('shathamanam-bhavathi-registration')->withErrors("Convention Registration is must for Shathamanam Bhavathi Registration, click <a href='/registration'>Here</a> for Convention Registraion");
        }
        
                $count=0; 
                if($user){
                    if ($user->categorydetails){
                        foreach($user->categorydetails->benfits as $benfits){
                            if($benfits->name=='Shathamanam Bhavathi Tickets'){
                                $count= explode(",",$benfits->pivot->count)[0];
                                $count=((int)$count[0]);
                                $count=$count/2;
                            }
                        }
                    }
                    if($user->registration_amount>0){
                        $count=1;
                    }
                }
                if($count==0){
                    return redirect()->back()->withErrors("Your registration does not include Shathamanam Bhavathi Registration");
                }


        if(!empty($request->father_or_in_laws_name)){
        foreach($request->father_or_in_laws_name as $key=>$value ){
                if(!empty($request->father_or_in_laws_name[$key])){
                    $extra_data['ticket_'.($key+1)]['father_or_in_laws_name'] = $request->father_or_in_laws_name[$key];
                    $extra_data['ticket_'.($key+1)]['father_In_law_dob']=$request->father_In_law_dob[$key];
                    $extra_data['ticket_'.($key+1)]['father_In_law_naksthram']=$request->father_In_law_naksthram[$key];
                    $extra_data['ticket_'.($key+1)]['father_In_law_rasi']=$request->father_In_law_rasi[$key];
                    $extra_data['ticket_'.($key+1)]['father_In_law_gothram'] = $request->father_In_law_gothram[$key];
    
                    $extra_data['ticket_'.($key+1)]['mother_or_in_laws_name'] = $request->mother_or_in_laws_name[$key];
                    $extra_data['ticket_'.($key+1)]['mother_In_law_dob']=$request->mother_In_law_dob[$key];
                    $extra_data['ticket_'.($key+1)]['mother_In_law_naksthram']=$request->mother_In_law_naksthram[$key];
                    $extra_data['ticket_'.($key+1)]['mother_In_law_rasi']=$request->mother_In_law_rasi[$key];
                    $extra_data['ticket_'.($key+1)]['mother_In_law_gothram']=$request->mother_In_law_gothram[$key];
                }
                
            }
        }
        $extra_data['number_of_participants'] = $request->number_of_participants;
        $extra_data['instructions_to_sponsors'] = $request->instructions_to_sponsors;

            $data=array_merge($request->all(),
        [
            'user_id' => $user->id??0,
            'registration_type_name' => "ShathamanamBhavathi",
            'extra_data' => $extra_data
            ]
        );
            $Registration = Registration::create($data);
            $Registration->registration_id="SAT".sprintf("%04d", $Registration->id);
            $Registration->save();

            Mail::to($request->email)->cc('shathamanam@nriva.org')->send(new NewShathamanamBhavathiRegistration($user));
            if(Auth::user()){
                return redirect('myaccount')->with('message-suc', "Shathamanam Bhavathi registered successfully");
                }else{
                    return redirect('shathamanam-bhavathi-registration')->with('message-suc', "Shathamanam Bhavathi registered successfully");
                }
    }

    public function stoteExibitRegestrationForm(Request $request )
    {
        //dd($request->all());
        $ExhibitorType_registration=[];
            $total_amount = 0;
            foreach ($request->count as $key => $value) {
                $a=[
                    $key => $value
                ];
                $ExhibitorType_registration = $a+ $ExhibitorType_registration;
            }

       // $ExhibitorType=ExhibitorType::find($request->booth_type);
        $all_details=array();

        array_push($all_details, array('package_name'=> "Exhibit Registration"));
        array_push($all_details, array('package_details'=> $ExhibitorType_registration));

        $request->merge([
            'package_details' => $all_details
        ]); 

        $data=array_merge($request->all(),
        [
            'user_id' => Auth::user()->id??0,
            'registration_type_name' => "Exhibit",
            ]
        );
          $Registration = Registration::create($data);
          $Registration->booth_type=$ExhibitorType_registration;
          $Registration->registration_id="EXE".sprintf("%04d", $Registration->id);
          $Registration->save();
        $payment_made_towards = "Paid Towards Exhibit registration";
        $paymenttype= Paymenttype::find($request->payment_type);
        if($paymenttype){
        if($paymenttype->name== Config('conventions.check_name_db')){
            $payment =App(PaymentController::Class)->PayWithCheck(Auth::user()->id??0, $request->amount, $paymenttype->id, $payment_made_towards, $request );
        }

        if ($paymenttype->name == Config('conventions.zelle_name_db')) {
            $payment =App(PaymentController::Class)->PayWithZelle(Auth::user()->id??0, $request->amount, $paymenttype->id, $payment_made_towards, $request);
        }

        if ($paymenttype->name == Config('conventions.other_name_db')) {
            $payment =App(PaymentController::Class)->PayWithOther(Auth::user()->id??0, $request->amount, $paymenttype->id, $payment_made_towards, $request);
        }

       

        if ($paymenttype->name == Config('conventions.paypal_name_db')) {
                $temp = [
                    'user_id' => Auth::user()->id??0,
                    'payment_amount' => $request->amount,
                    'paid_amount' => $request->amount-$request->discount_cal_amount,
                    'discount_amount' => $request->discount_cal_amount,
                    'discount_code' => $request->discount_code,
                    'package_details' => $all_details,
                    'payment_methord' => $paymenttype->id,
                    'payment_status' => 'Pending',
                    'payment_made_towards' => 'Paid Towards Exhibit registration'
                ];
                $payment = ModelPayment::create($temp);

                Session::put('exhibitUserId', $Registration->id);
                Session::put('discount_amount', $request->discount_cal_amount);

                $Registration->payment_id=$payment->id;
                $Registration->save();
            return  App(PaymentController::Class)
                ->PayWithPayPal($request->amount-$request->discount_cal_amount, 'Exhibit Regestration ',  $payment->id, 'paypal.status');
            }
        }
     if ($request->payment_type == "free" && Auth::user()) {
             array_push($all_details, array('payment_type'=> "Free Exhibit Registration"));
           $temp = [
                    'user_id' => Auth::user()->id,
                    'payment_amount' => 0,
                    'package_details' => $all_details,
                    'payment_methord' => 0,
                    'payment_status' => 'Paid',
                    'payment_made_towards' => 'Paid Towards Exhibit registration'
                ];
                $payment = ModelPayment::create($temp);
                $users = User::where('id',Auth::user()->id)->first();
                $users->free_exbhit_registrations=$users->free_exbhit_registrations-count($request->count);
                $users->save();
        }
        $Registration->payment_id=$payment->id;
        $Registration->save();

        $exhibitUser=Registration::where('id',$Registration->id)->first();

        Mail::to($request->email)->cc('exhibits@nriva.org')->send(new NewExhibitRegistration($exhibitUser,$paymenttype,$payment));
        return redirect(url('registration-success')."?id=$payment->id&reg_id=$Registration->id")->with('message-suc', "Exhibit registered successfully");
    }
    public function registrationsuccess(Request $request){
        $payment = ModelPayment::where("id","$request->id")->first();
        $user = Registration::where("id",$request->reg_id)->first();
        $exhibitsTypes = ExhibitorType::where("id",$user->booth_type)->first();
       return view('registration.registration-success',compact('payment','user','exhibitsTypes'));
    }


    public function showRegistrationDetails($id)
    {
        $Registration=Registration::where('id',$id)->where('user_id', auth::user()->id)->firstOrFail();
        return view('registration.showRegistrationDetails',compact('Registration'));
    }


    public function download($id) {
        $BannerImages = BannerImages::findorFail($id);
        return \Storage::download(  config('conventions.benfit_image_upload') . '/'. $BannerImages->image_url);
    }


    public function updateBannerImage(Request $request)
    {
        if ($request->hasfile('benfitimage')) {
            $key=$request->benfit_id;
            $image=$request->benfitimage[$key];
            $user_id =Auth::user()->id;
             $BannerImage=BannerImages::where('user_id', $user_id)
                ->where('benfit_id',$request->benfit_id)->first();
                
               if($BannerImage){
                if(file_exists(config('conventions.benfit_image_display').$BannerImage->image_url)){
                    unlink(config('conventions.benfit_image_display').$BannerImage->image_url);  
                }
               }

                $file_name = 'benfit_id' . '_' .$key."_". $user_id . '.' . $image->getClientOriginalExtension();
                $image->storeAs(config('conventions.benfit_image_upload'), $file_name);
               
                $images_count=BannerImages::where('user_id',$user_id)->get();
                if(count($images_count)>1){
                    $images=BannerImages::where('user_id',$user_id)->delete();
                }
                $BannerImages = BannerImages::updateOrCreate([
                    'user_id' => $user_id,
                    'benfit_id' => $key,
                ],[
                    'image_url' => $file_name,
                ]);
                //send mail
                
                $images=BannerImages::where('user_id',$user_id)->get();
                if(count($images)>0){
                    $user=User::where('id',$user_id)->first();
                    Mail::to('souvenir@nriva.org')
                    ->send(new ImageNotification($user));
                }

            }
         return redirect('viewfeatuees');
    }
     public function updateUserDate($request)
    {
        $user = User::where('email', $request->email)->first();

        $user->spouse_full_name = $request->spouse_full_name;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->address = $request->address;
        $user->address2 = $request->address2;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->country = $request->country;
        $user->zip_code = $request->zip_code;
        $user->mobile = $request->mobile;
        $user->name = $request->first_name." ".$request->last_name;

        $user->children_count = [
                            'below_6'=>$request->below_6,
                            'age_7_to_15'=>$request->age_7_to_15,
                            'age_16_to_23'=>$request->age_16_to_23
                        ];
        $user->save();

        return $user;
    }

    public function email_verification(Request $request)
    {
        $user = User::where("email",$request->email)->first();
        //in main nriva site
        $response = \DB::table('users')->where([
          'email' =>  $request->email,
        ])->first();

        if(empty($user)&&empty($response)){
            $otp = rand(10000,99999);
            
       /*  Mail::raw("Hi, your email verification code for Nriva convention registration is : $otp", function ($message) use ($request) {
          $message->to($request->email)
            ->subject("Email Verification code from Nriva Convention");
        }); */
         //Otp_model::updateOrCreate(['user'=>$request->email],['otp'=>$otp, 'status'=>1]);
            return "success";
        }else{
            return "failed";
        }
    }
    public function check_email(Request $request)
    {
        $user = User::where("email",$request->email)->first();
        if(empty($user)){ 
            return "success";
        }else{
            return "failed";
        }
    }
    public function check_matrimony_email(Request $request)
    {
        $check=Matrimony_model::where('profile_id',$request->profile_id)->first(); 
       
        $user = \DB::table('users')->join('eedujodu','eedujodu.user_id','=','users.id')
        ->join('members','members.user_id','=','users.id')
        ->leftjoin('eedujodusignificants','eedujodusignificants.eedujodu_id','=','eedujodu.id')
        ->select('eedujodu.*','users.*','members.*','eedujodusignificants.title as curr_job','eedujodusignificants.name as job_pos',)
        ->Where('member_id',$request->profile_id)
        ->where(function ($query) {
            $query->Where('is_eedujodu',1)
            ->orWhere('members.membershiptype_id', '=', 11);
        })
        ->Where('is_approved',1)
        ->first();

        if($check){
            if($check->convention_id==\Auth::user()->registration_id){
                return view('matrimony_registration_step2', compact('user','check')) ;
            }else{
                return redirect()->back()->with("user_error","Please enter a Valid Profile ID from Eedu-Jodu"); 
            }
        }
     
        if($user){
            $conuser = User::where('email',$user->email)->first();
                if(empty($user)){ 
                    return redirect()->back()->with("user_error","Registration failed! Your profile id is invalid");  
                }else if(!empty($user) && empty($conuser)  ){
                        $show_convention_id_field=true;
                    return view('matrimony_registration_step2', compact('user','show_convention_id_field')) ;
                }else{
                    return view('matrimony_registration_step2', compact('user')) ;
                }
        }else{
             return redirect()->back()->with("user_error","Registration failed! your profile id is invalid");  
        }
    }
    public function sendOtpForLogin(Request $request)
    {
        $user = User::where("email",$request->email)->first();
        if(!empty($user)){
            $otp = rand(10000,99999);
            
        Mail::raw("Hi, your Email Verification Code for Nriva convention Login is : $otp", function ($message) use ($request) {
          $message->to($request->email)
            ->subject("Nriva Convention Login Email Verification Code");
        });
        $user->login_otp=$otp;
        $user->save();
            return "success";
        }else{
            return "failed";
        }
    }
    public function matrimony_registration()
    {
        $user =array();
        if(Auth::user()){
            if((Auth::user()->total_amount==NULL) &&  (Auth::user()->registration_id!=NULL) ){
                return redirect('myaccount')->with('error', "Please Complete convention registration for matrimony registration");
            }
        }
         $states=DB::table('states')->get();
        return view('matrimony_registration',compact('user','states'));
    }
    public function matrimony_registration_store(Request $request)
    {
        if($request->has('convention_id')){
            $conuser=User::where('registration_id',$request->convention_id)->first();
            $conuser->eedu_jodu_reg_count=$conuser->eedu_jodu_reg_count+1;
            $conuser->save();
            /* if($conuser){
                if($conuser->eedu_jodu_reg_count<3){
                        $check=Matrimony_model::where('email',$request->email)->first(); 
                        if(!$check){
                         
                        }
                }else{
                    return redirect('matrimony_registration')->with('user_error',"Reached maximum of 3 registrations with this Login, if you need to add more please send an email to: Conventionmatrimony@nriva.org");
                }
            }else{
                return redirect('matrimony_registration')->with('user_error',"Invalid Convention registration id");
            } */
        }else{
            $request->convention_id=\Auth::user()->registration_id;
        }
        
        $partt=[];
        if($request->participate_in){
            foreach($request->participate_in as $key=>$value  ) {
                $key = str_replace('_',' ',$key); 
                $partt=array_merge($partt,[$key]);
            }
        }
        
        $request->merge([
            'participate_in' => $partt
        ]);
        $check=Matrimony_model::where('email',$request->email)->first(); 
        
        $Matrimony=Matrimony_model::updateOrCreate(['profile_id'=>$request->profile_id],$request->except(['_token']));
        if(!(!$Matrimony->wasRecentlyCreated && $Matrimony->wasChanged())){
            $user = Matrimony_model::where('email',$request->email)->first();
            Mail::to($request->email)->cc(['conventionmatrimony@nriva.org','sunitha.rs2018@gmail.com'])->send(new NewMatrimonyRegistration($user));
            return redirect('matrimony_registration')->with('message-suc', "Matrimony Registration Completed Successfully");
        }else{
            return redirect('matrimony_registration')->with('message-suc', "Matrimony Registration Date Updated Successfully");
        }
    }
    public function change_password($value='')
    {
         return view('change_password');
    }
    public function update_password(Request $request)
    {
        if($request->password == $request->confirmpassword){

            User::where('email',Auth::user()->email)->update(['password'=>\Hash::make($request->password)]);
            return redirect('change_password')->with('message-suc', "Password Created Successfully");

        }else{
           return redirect('change_password')->with('error', "Confirm Password not macthed"); 
        }
    }
    public function mr_mis_registration(Request $request)
    {
        $mr_type = MrAndMissTypes::where('title',$request->type)->first();
       if(empty($mr_type) || empty(Auth::user())){
        return redirect('myaccount');
    }
        $user= Mr_Miss_Model::where(['user_id'=>Auth::user()->id,'title'=>$request->type])->first();
        $chapters = DB::table('chapters')->orderby('name')->get();
        return view('mr_mis_registration',compact('user','chapters','mr_type'));
    }
    public function mr_mis_registration_store(Request $request)
    {
        if(empty($request->id)){
        $this->validate($request, [
        'email'   => 'required|email|unique:convention_mr_miss_registrations',
              ]);
            }
        $request['user_id']=Auth::user()->id;
        Mr_Miss_Model::updateOrCreate(['id'=>$request->id],$request->except(['_token']));
        $user = Mr_Miss_Model::where(['email'=>$request->email])->first();
        Mail::to($request->email)->send(new NewMrAndMissRegistration($user));
        return redirect('myaccount')->with('message-suc', "You have successfully registered for Miss and Mr. NRIVA.");
    }

    public function ImportRegesterUserStore(Request $request)
    {   
        

        $this->validate($request, [
          'select_file'  => 'required|mimes:xls,xlsx'
         ]);

         $path = $request->file('select_file')->getRealPath();
         $uploaded_filename = $request->file('select_file')->getClientOriginalName();

         $data = Excel::toArray(new UsersImport,$request->file('select_file'));
         //dd($data[0]);
         $invalid_email=$email_missing=$email_exists=$sponsor_missing=$success_cnt=0;
         $invalid_records_byemail=$missing_records_byemail=$missing_records_bysponser=array();
         if(count($data[0]) > 0)
         {
          foreach($data[0] as $key => $value)
          { 

            $email=$value['email'];
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $invalid_records_byemail[]=$value['email'];
                $invalid_email++;
                continue;
            }
            if(!$email){
                $missing_records_byemail[]=$value['mrsmr_name'];
                $email_missing++;
                continue;
            }
            $donation_amount=$value['donation_amount'];
            $donation_amount=floatval(preg_replace("/[^-0-9\.]/","",$donation_amount));
            $paying_amount=0;
           if(!empty($value['paid_amount'])){
            $paying_amount = floatval(preg_replace("/[^-0-9\.]/","",$value['paid_amount']));
           }
            if(!empty($value['sponsor_level'])){
                $donors= Donortype::where('name' ,$value['sponsor_level'])->first();
                if(!$donors){
                    $missing_records_bysponser[]=$value['mrsmr_name'];
                    $sponsor_missing++;
                    continue;
                }
                $SponsorCategory=SponsorCategory::where('donor_type_id',$donors->id)->first();
                if(!$SponsorCategory){
                    $missing_records_bysponser[]=$value['mrsmr_name'];
                    $sponsor_missing++;
                    continue;
                }
                $SponsorCategory=SponsorCategory::find($SponsorCategory->id);
                $benfits_Ids= $SponsorCategory->benfits->pluck('id')->toArray();
                
                
            }
            $kids_details=null;
            $user = user::where('email',$email)->first();
            if(isset($user)){
                $missing_records_byemail[]=$value['mrsmr_name'];
                $email_exists++;
                continue;
            }

           
           $Paymenttype = Paymenttype::find(4);

            $user = User::where('email', $email)->first();
            if (!isset($user)) {
                $uname=explode('&',$value['mrsmr_name']);
                if(count($uname)>1){
                    $uname=explode(' ',trim($uname[1]));
                }
                else{
                    $uname=explode(' ',$uname[0]);
                }
                $lname=$fname='';
                $fname = $uname[0];
                if(isset($uname[1])){
                    $lname = $uname[1];
                }
                #added for wife&husband names
                $fname=$value['mrsmr_name'];
                $lname='';
                $user_temp=[
            'first_name'  => $fname,
            'last_name'  => $lname,
            'email' => $email,
            'mobile' => $value['phone'],
            'kids_details' => $kids_details,
            "spouse_first_name" => $value['wife_first_name'],
            "spouse_last_name" => $value['last_name'],
            "chapter" => $value['chapter'],
            "leadership" => $value['leadership'],
            "is_imported_record"=>1,
            ];
            //'chapter' => $row['chapter'],
            //'leadership' => $row['leadership'],
            
                $user = User::create($user_temp);

                $mainsite_user = DB::table('users')->where('email',$user->email)->first();
                if($mainsite_user){
                    $user->member_id=$mainsite_user->member_id;
                }
                $user->registration_id = "CON".sprintf("%04d", $user->id);
                $user->save();
            }
            $user = User::where('email', $email)->first();

            $SponsorCategory  = SponsorCategory::where('donor_type_id',$donors->id)->first();
            
            $individual_registration=null;
            $total_amount = $donation_amount;
            $user = User::where('email', $email)->first();
            // call payment methord

            $package_details=array();
            $all_details=array();

            array_push($package_details, array('package_name'=> "Convention Registration"));
            
            array_push($all_details, array($SponsorCategory->donortype->name??''));
            
            array_push($package_details, array('package_details'=> $all_details));

               if ($Paymenttype->name == Config('conventions.other_name_db')) {
                    $more_info=[
                        'transaction_date'=> '',
                        'payee_name'=> $value['mrsmr_name'],
                        'company_name'=> 'Offline Registration',
                    ];
                    $temp = [
                        'user_id' => $user->id,
                        'payment_amount' => $paying_amount,
                        'payment_methord' => $Paymenttype->id,
                        'unique_id_for_payment' => '',
                        'more_info' => $more_info,
                        'payment_status' => 'Pending',
                        'status' => 1,
                        'account_status' => 'Initial Payment',
                        'package_details' =>$package_details,
                        'payment_made_towards' => "Convention Registration"
                    ];
                    $payment = ModelPayment::create($temp);
                }
                $user->sponsorship_category_id = $SponsorCategory->id??0;
                $user->individual_registration =  $individual_registration;
                $user->payment_status = 'pending';
                $user->total_amount = $total_amount;
                $user->amount_paid = $paying_amount;
                $user->registration_amount = 0;
                $user->donor_amount = $total_amount;
                $user->free_exbhit_registrations = 0;
                if($user->sponsorship_category_id>0){
                    foreach($SponsorCategory->benfits as $benfits){
                    if($benfits->name=='Vendor booth spaces at the event'){

                        $user->free_exbhit_registrations = (int)$benfits->pivot->count??0;
                        $user->vendor_booth_space_count = (int)$benfits->pivot->count??0;
                        }
                    }
                }
                //dd($user->free_exbhit_registrations);
                $user->save();
                $success_cnt++;
                echo 'Successfully : '.$email.'<br>';
                
                //myaccount.blade
                Mail::to($email)
                 ->cc('registration@convention.nriva.org')
                 ->send(new NewConventionRegistration($user,$Paymenttype));
                /*if($key==2){
                    return redirect('admin/registrations')->with('message-suc', "Convention Registration Completed Successfully. Total created: ".$success_cnt);
                }*/
                //return redirect('myaccount')->with('message-suc', "Convention Registration Completed Successfully");
          } // foreach end 
        }  //count if condition end
        ##log file creation code --start
        $data="\n========Start at ".date('Y-m-d_H-i-s')."================";
        $data .="\nFile Name: ".$uploaded_filename."  \nTotal created: ".$success_cnt."\nDuplicate Email: ".$email_exists."\n".implode("\n",$missing_records_byemail)."\nSponsor missing: ".$sponsor_missing."\n".implode("\n",$missing_records_bysponser)."\nInvalid Emails: ".$sponsor_missing."\n".implode("\n",$invalid_records_byemail);
        $data.="\n========End at ".date('Y-m-d_H-i-s')."================";
         $file = date('Y-m-d'). '_file.txt';
      $destinationPath=public_path()."/upload/";
      if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
      File::append($destinationPath.$file,$data);
      ##log file creation code --end
        return redirect('admin/registrations')->with('message-suc', "Convention Registration Completed Successfully. Total created: ".$success_cnt." Duplicate Email: ".$email_exists." Sponsor missing: ".$sponsor_missing." Invalid Emails: ".$invalid_email);
        echo 'Successfully Imported';exit;

            
        }

        public function voucher_verification(Request $request)
    {
        $vouchers = Vouchers::where('name',$request->name)
                                ->where('status',1)
                                ->first();
        if($vouchers){
            return json_encode($vouchers);
            
        }else{
            return json_encode("failed");
        }
    }

}
