<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\CorporateSponsortype;
use App\Models\Admin\RegistrationContent;
use App\Models\Admin\Paymenttype;
use App\Http\Controllers\Admin\PaymentController;
use App\Models\CorporateSponsorReg;
use App\Mail\CorporateSponsorsRegistrationNotification;
use App\Models\Payment as ModelPayment;
use Mail;
use Auth;
use DB;
use Session;
use Config;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class CorporateSponsorsRegistrationController extends Controller
{
    
    public function __construct()
    {
        $settings = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(config('conventions.PAYPAL_SANDBOX_CLIENT_ID'), config('conventions.PAYPAL_SANDBOX_CLIENT_SECRET')));
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings);
    }
    
    public function showRegistration()
    {
        $CorporateSponsortypes= CorporateSponsortype::where('status',1)->get();
        $RegistrationContent = RegistrationContent::first();
        $paymenttypes = Paymenttype::where('status',1)->get();
        return view('registration.ShowCorporateSponsorsRegistrationForm', compact('CorporateSponsortypes','RegistrationContent','paymenttypes'));
    }
    
    public function PostSouvenirRegistration(Request $request)
    {   
        $is_alredy_reg=CorporateSponsorReg::where('email',$request->email)->first();
        if($is_alredy_reg){
         //   return redirect(url('/corporate_sponsors_registration/'))->withErrors('You alredy registered');
        }

        //data prepation
        $CorporateSponsortype= CorporateSponsortype::find($request->corporate_sponsor_type);
        $request->amount=$CorporateSponsortype->amount;
        $request->merge(['corporate_sponsor_type_id'=>$CorporateSponsortype->id]);
        $CorporateSponsorReg= CorporateSponsorReg::Create($request->all());
        //save user
        if($request->hasFile('logo_url')){
            $image=$request->logo_url;
            $file_name = 'logo_'.$CorporateSponsorReg->id.'.' . $image->getClientOriginalExtension();
            $image->storeAs(config('conventions.corporate_sponsors_image_upload'), $file_name);
            $CorporateSponsorReg->logo_url=$file_name;
            $CorporateSponsorReg->save();
        }
        if($request->hasFile('image_url')){
            $image=$request->image_url;
            $file_name = 'image_'.$CorporateSponsorReg->id.'.' . $image->getClientOriginalExtension();
            $file_name=$image->storeAs(config('conventions.corporate_sponsors_image_upload'), $file_name);
            $CorporateSponsorReg->logo_url=$file_name;
            $CorporateSponsorReg->save();
        }
      

        if($CorporateSponsorReg->wasRecentlyCreated){
            $CorporateSponsorReg->registration_id="CSR".sprintf("%04d", $CorporateSponsorReg->id);
            $CorporateSponsorReg->save();
            $paymenttype= Paymenttype::find($request->payment_type);

            if($paymenttype){
                if ($paymenttype->name == Config('conventions.paypal_name_db')) {
                    return $this->processPayment($request,$request->amount,$CorporateSponsorReg, $paymenttype);
                }
            }

            $paymentDetails=$this->processPayment($request,$request->amount,$CorporateSponsorReg, $paymenttype);
            $CorporateSponsorReg->payment_id=$paymentDetails->id;
            $CorporateSponsorReg->save(); 

            Mail::to($CorporateSponsorReg->email)->cc('corpsponsorship@nriva.org')->send(new CorporateSponsorsRegistrationNotification($CorporateSponsorReg));
            return redirect(url('corporate-sponsors-registration-success')."?id=$paymentDetails->id&reg_id=$CorporateSponsorReg->id&type='CorporateSponsorReg'")->with('message-suc', "successfully");
        }

     
    }

    public function registrationsuccess(Request $request){
        $payment = ModelPayment::where("id","$request->id")->first();
        $user = CorporateSponsorReg::where("id",$request->reg_id)->first();
       $Type = CorporateSponsortype::where("id",$user->corporate_sponsor_type_id)->first();
       return view('registration.corporate_sponsor_reg_sucess',compact('payment','user','Type'));
    }



    public function processPayment($request,$amount, $Registration, $paymenttype)
    {
      $payment_made_towards = "Paid Towards Corporate Sponsor registration";

      if($paymenttype){
      if($paymenttype->name== Config('conventions.check_name_db')){
          return App(PaymentController::Class)->PayWithCheck(Auth::user()->id??0, $amount, $paymenttype->id, $payment_made_towards, $request );
      }

      if ($paymenttype->name == Config('conventions.zelle_name_db')) {
          return App(PaymentController::Class)->PayWithZelle(Auth::user()->id??0, $amount, $paymenttype->id, $payment_made_towards, $request);
      }

      if ($paymenttype->name == Config('conventions.other_name_db')) {
        $request->merge([
            'Payment_made_through' => $request->payee_name,
            'company_name' => $request->company_name,
        ]); 
          return App(PaymentController::Class)->PayWithOther(Auth::user()->id??0, $amount, $paymenttype->id, $payment_made_towards, $request);
      }

      if ($paymenttype->name == Config('conventions.paypal_name_db')) {
              $temp = [
                  'user_id' => Auth::user()->id??0,
                  'payment_amount' => $amount,
                  'package_details' => $request->package_details,
                  'payment_methord' => $paymenttype->id,
                  'payment_status' => 'Pending',
                  'payment_made_towards' => $payment_made_towards
              ];
              $payment = ModelPayment::create($temp);
              Session::put('CorpSpoRegUserId', $Registration->id);

              $Registration->payment_id=$payment->id;
              $Registration->save();
          return  App(PaymentController::Class)
              ->PayWithPayPal($amount, 'Corporate Sponsor Regestration ',  $payment->id, 'corporatesponsorpayment.status');
          }
      }
     

        
    }
    
  

    public function paypalStatus(Request $request)
    {
        
        $token = $request->get('token');
        $payerId = $request->get('PayerID');
        $payment_id = Session::get('paypal_payment_id');
        $CorpSpoRegUserId = Session::get('CorpSpoRegUserId');
        $Registration=CorporateSponsorReg::where('id',$CorpSpoRegUserId)->first();
        $ModelPayment = ModelPayment::find(Session::get('payment_id'));

        if (empty($payerId) || empty($token)) {
            $result = ['state' => 'User Cancelled'];
            $updateData = [
                'more_info' =>  json_encode($result),
                'payment_status' => "User Cancelled",
                'status'=> 0
            ];
            $ModelPayment->fill($updateData)->save();
            return redirect(url('/corporate_sponsors_registration/'))->withErrors('Payment failed');
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        Session::forget('paypal_payment_id');


        if ($result->getState() == 'approved' && $ModelPayment) {

            $transactions = $result->getTransactions();
            $transaction = $transactions[0];
            $relatedResources = $transaction->getRelatedResources();
            $relatedResource = $relatedResources[0];
            $order = $relatedResource->getSale();

            $updateData = [
                'unique_id_for_payment' => $order->getId(),
                'more_info' =>  $result->toarray(),
                'payment_status' => "Paid",
                'status' => 1
            ];
            $ModelPayment->fill($updateData)->save();
            $paymenttype= Paymenttype::find(1);
            Mail::to($Registration->email)->cc('corpsponsorship@nriva.org')->send(new CorporateSponsorsRegistrationNotification($Registration));
           // Mail::to($exhibitUser->email)->send(new SouvenirRegistrationNotification($Registration));
            return redirect(url('corporate-sponsors-registration-success')."?id=$ModelPayment->id&reg_id=$Registration->id&type='CorporateSponsorReg'")->with('message-suc', "successfully");
        } else {
            $updateData = [
                'more_info' => $result->toarray(),
                'status' => 0
            ];

            $donation->fill($updateData)->save();
            return redirect(url('/corporate_sponsors_registration/'))->withErrors("Payment failed");
        }

    }


  
}
