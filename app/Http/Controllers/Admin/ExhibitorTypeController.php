<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExhibitorTypeStoreRequest;
use App\Http\Requests\ExhibitorTypeUpdateRequest;
use App\Models\Admin\ExhibitorType;
use Illuminate\Http\Request;

class ExhibitorTypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $exhibitorTypes = ExhibitorType::all();
        return view('admin.master.exhibitorType.index', compact('exhibitorTypes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.master.exhibitorType.create');
    }

    /**
     * @param \App\Http\Requests\ExhibitorTypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExhibitorTypeStoreRequest $request)
    {
        $request = $this->dataPreparation($request);

        $exhibitorType = ExhibitorType::create($request);

      //  $request->session()->flash('exhibitorType.id', $exhibitorType->id);

        return redirect()->route('exhibitor-type.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\ExhibitorType $exhibitorType
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ExhibitorType $exhibitorType)
    {
        return view('admin.master.exhibitorType.show', compact('exhibitorType'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\ExhibitorType $exhibitorType
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ExhibitorType $exhibitorType)
    {
        return view('admin.master.exhibitorType.edit', compact('exhibitorType'));
    }

    /**
     * @param \App\Http\Requests\ExhibitorTypeUpdateRequest $request
     * @param \App\Models\Admin\ExhibitorType $exhibitorType
     * @return \Illuminate\Http\Response
     */
    public function update(ExhibitorTypeUpdateRequest $request, ExhibitorType $exhibitorType)
    {
        $request = $this->dataPreparation($request);
        $exhibitorType->update($request);

       // $request->session()->flash('exhibitorType.id', $exhibitorType->id);

        return redirect()->route('exhibitor-type.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\ExhibitorType $exhibitorType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ExhibitorType $exhibitorType)
    {
        return  $exhibitorType->delete();

        return redirect()->route('exhibitorType.index');
    }

    public function updateStatus(Request $request)
    {
        return ExhibitorType::where('id', $request->id)->update(['status' => $request->status]);
    }


    public function dataPreparation($request)
    {
        $temp=array();
        foreach ($request->size as $key => $value) {
            array_push($temp,[
                'size' => $request->size[$key],
                'price_before' => $request->price_before[$key],
                'price_after' => $request->price_after[$key],
                'till_date' => $request->till_date[$key]
            ] );
        }

        $data=[
            'name'=> $request->name,
            'size_price'=> $temp
        ];
        return $data;
    }
}
