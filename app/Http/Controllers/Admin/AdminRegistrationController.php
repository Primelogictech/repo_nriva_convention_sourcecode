<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\Models\User;
use GuzzleHttp\Client;
use App\Models\Payment;
use App\Models\Registration;
use Illuminate\Http\Request;
use App\Models\Batches_Model;
use App\Models\Mr_Miss_Model;
use App\Models\Matrimony_model;
use App\Models\YouthBanquet_Model;
use App\Models\CorporateSponsorReg;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Admin\SponsorCategory;
use Illuminate\Support\Facades\Route;
use App\Exports\ExhibitRegistrationsExport;
use SebastianBergmann\LinesOfCode\Exception;
use App\Exports\MatrimonyRegistrationsExport;
use App\Exports\ConventionRegistrationsExport;
use App\Exports\YouthBanquetRegistrationsExport;
use App\Http\Requests\ConventionUserImageRequest;
use App\Exports\CorporateSonsorsRegistrationsExport;
use App\Exports\SatamanamBhavatiRegistrationsExport;
use App\Models\RegistrationsList;
use App\Models\AuditLog_Model;
use Auth;
use App\Imports\AdminDonorImport;

class AdminRegistrationController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function exhibitRegistrations()
    {
        $registrations=Registration::with(['paymentdata','exhibitorTypes'])->where('registration_type_name','Exhibit')->orderBy('created_at', 'desc')->get();
        return view('admin.registrations.exhibitRegistrations', compact('registrations') );
    } 

    public function YouthBanquetRegistrations($value='')
    {
        $registrations=YouthBanquet_Model::orderBy('created_at', 'desc')->get();
        return view('admin.registrations.YouthBanquetRegistrations', compact('registrations') );
    }



     public function youthActivitiesRegistrations()
    {
        $registrations=Registration::where('registration_type_name','YouthActivities')->orderBy('created_at', 'desc')->get();
        return view('admin.registrations.exhibitRegistrations', compact('registrations') );
    }

    public function SatamanamBhavatiRegistrations()
    {
        $registrations=Registration::where('registration_type_name','ShathamanamBhavathi')->orderBy('created_at', 'desc')->get();
        return view('admin.registrations.satamanamBhavatiRegistrations', compact('registrations') );
    }

    public function matrimonyRegistrations(Request $request)
    {
        
        $filter=false;
        if(count($request->input())>0){
            $filter=True;
        }

        $query=Matrimony_model::join('users','convention_matrimony_registrations.email','=','users.email')
            ->leftjoin('members','members.user_id','=','users.id')
            ->leftjoin('eedujodu','users.id','=','eedujodu.user_id');
           
            if($filter ){
                if(isset($request->min_age) && !empty($request->min_age)){
                    list($year,$month,$day) = explode("-", date("Y-m-d"));
                    $range = $year - $request->min_age;
                    $dob_year = "{$range}-{$month}-{$day}";
                    //$query->whereRaw("str_to_date(`members`.`dob`,'%m-%d-%Y') <= '".$dob_year."'");
                    $query->whereRaw("str_to_date(`members`.`dob`,'%m-%d-%Y') <= '".$range."-12-31'");
                }
                if(isset($request->max_age) && !empty($request->max_age)){
                    list($year,$month,$day) = explode("-", date("Y-m-d"));
                    $range = $year - $request->max_age;
                    $dob_year= "{$range}-{$month}-{$day}";
                    //$query->whereRaw("str_to_date(`members`.`dob`,'%m-%d-%Y') >= '".$dob_year."'");
                    $query->whereRaw("str_to_date(`members`.`dob`,'%m-%d-%Y') >= '".$range."-01-01'");
                }

                if(isset($request->Gender) && !empty($request->Gender)){
                    $query->where("members.gender",$request->Gender);
                }

                if(isset($request->sort_age) && !empty($request->sort_age)){
                    $query->orderBy('mem_dob',$request->sort_age);
                }

                if(isset($request->visa_status) && !empty($request->visa_status) && $request->visa_status!=''){
                    if($request->visa_status=='US Citizenship'){
                        $query->where(function($query) use ($request){
                            $query->orWhere('eedujodu.legal_status','=','US Citizenship');
                           $query->orWhere('eedujodu.legal_status_description','like','%US Citizen%');
                           $query->orWhere('eedujodu.legal_status','like','%US Citizen%');
                           $query->orWhere(DB::Raw("REPLACE(eedujodu.legal_status, ' ', '')"),'like','%USCitizen%');
                           $query->orWhere(DB::Raw("REPLACE(eedujodu.legal_status_description, ' ', '')"),'like','%USCitizen%');
                       });
                    }elseif($request->visa_status=='H1-B VISA'){
                        $query->where(function($query) use ($request){
                            $query->orWhere('eedujodu.legal_status','=','H1-B VISA');
                           $query->orWhere('eedujodu.legal_status_description','H-1B Visa');
                       });
                    }
                    else{
                        $query->where('eedujodu.legal_status',$request->visa_status);
                    }
                }

                if(isset($request->citizenship) && !empty($request->citizenship) && $request->citizenship!='' ){
                    $query->where('eedujodu.citizen_of',$request->citizenship);
                }
            }else{
                $query->orderBy('convention_matrimony_registrations.created_at','desc');
            }

            $legal_status=DB::table('legal_status')->where('status',1)->pluck('name');
            $citizen_of=DB::table('citizen_of')->where('status',1)->pluck('name');
        $registrations=$query->select('eedujodu.*','members.dob','members.gender','users.*','convention_matrimony_registrations.*','convention_matrimony_registrations.created_at as created_at','users.created_at as nriva_created_at',DB::Raw("str_to_date(`members`.`dob`,'%m-%d-%Y') as mem_dob"))
        ->get();
        $batches = Batches_Model::groupBy("batch_name")->get();
    
        return view('admin.registrations.matrimonyRegistrations', compact('registrations','request','legal_status','citizen_of',"batches") );
    }
    public function viewbatch(Request $request)
    {
        $batches = Batches_Model::groupBy("batch_name")->get();
         return view('admin.registrations.viewbatches', compact("batches") );
    }

    public function mrmissRegistrations($value='')
    {
         $registrations=Mr_Miss_Model::where('status',1)->orderBy('created_at','desc')->get();
        return view('admin.registrations.mr_missRegistrations', compact('registrations') );
    }

    public function souvenirRegistrations()
    {
        $registrations=Registration::where('registration_type_name','Souvenir')->orderBy('created_at', 'desc')->get();
        return view('admin.registrations.souvenirRegistrations', compact('registrations') );
    }

    public function CorporateSponsorsRegistrations()
    {
        $registrations=CorporateSponsorReg::orderBy('created_at', 'desc')->get();
        return view('admin.registrations.corporateSponsorsRegistrations', compact('registrations') );
    }


    /*Excel  export functions */
    public function ExhibitRegistrationsExport() 
    {
        return Excel::download(new ExhibitRegistrationsExport, 'ExhibitRegistrations.xlsx');
    }

    public function ConventionRegistrationsExport() 
    {
        return Excel::download(new ConventionRegistrationsExport, 'ConventionRegistrations.xlsx');
    }

    public function satamanamBhavatiRegistrationsExport() 
    {
        return Excel::download(new SatamanamBhavatiRegistrationsExport, 'SatamanamBhavatiRegistration.xlsx');
    }

    public function matrimonyRegistrationsExport(Request $request) 
    {
        return Excel::download(new MatrimonyRegistrationsExport($request->name), 'MatrimonyRegistration.xlsx');
    } 

    
    public function corporateSonsorsRegistrationsExport() 
    {
        return Excel::download(new CorporateSonsorsRegistrationsExport, 'CorporateSponsorsRegistration.xlsx');
    } 

    public function youthBanquetRegistrationsExport() 
    {
        return Excel::download(new YouthBanquetRegistrationsExport, 'YouthBanquetRegistrations.xlsx');
    } 
    

    public function conventionUserImages(ConventionUserImageRequest $request)
    {
        if ($request->hasfile('image')) {
                $file_name = 'user' . '_' . $request->id . '.' . $request->image->getClientOriginalExtension();
                $request->file('image')->storeAs(config('conventions.user_upload'), $file_name);
                User::where('id', $request->id)->update(['image_url' => $file_name]);
            }
             return redirect('admin/registrations')->with('message-suc', "Image Upload Successfully");
    }
    public function editMemberDetails($id)
    {
        $user=User::findorfail($id);
        $states=DB::table('states')->get();
        $chapters=DB::table('chapters')->orderby('name')->get();
        $donors=SponsorCategory::with('donortype')->where('status',1)->where('donor_type_id','!=' ,0)->get();
        return view ('admin.registrations.editMemberData', compact('user','states','chapters','donors'));
    }

    
    public function updatePaymentAmount(Request $request)
    {
        $payment=Payment::find($request->id);
        $old_amount = $payment->payment_amount;
        $payment->payment_amount=$request->amount;
        $payment->paid_amount=$request->amount;
        $payment->save();

        $user=User::findOrfail($payment->user_id);
        $new_amount_with_out_latest_payment_amount=$user->amount_paid-$old_amount;
        $user->amount_paid=$new_amount_with_out_latest_payment_amount+$request->amount;
        $user->save();
    }

    public function updateMemberDetails(Request $request)
    {
        $user=User::findOrfail($request->user_id);
        $data=array();

        $data=[
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'spouse_first_name'=>$request->spouse_first_name,
            'spouse_last_name'=>$request->spouse_last_name,
            'address'=>$request->address,
            'address2'=>$request->address2,
            'city'=>$request->city,
            'state'=>$request->state,
            'zip_code'=>$request->zip_code,
            'country'=>$request->country,
            'chapter'=>$request->chapter,
        ];
    
        if($user->sponsorship_category_id!=$request->sponsorship_category_id){
            $donor=SponsorCategory::with('donortype')
                        ->where('id',$request->sponsorship_category_id)
                        ->where('donor_type_id','!=' ,0)->first();

            $with_out_donor_amount=$user->total_amount-$user->donor_amount;
            
            $data['total_amount']=$with_out_donor_amount+$donor->amount;
            $data['donor_amount']=$donor->amount;
            $data['sponsorship_category_id']=$request->sponsorship_category_id;
        }

        $user->update($data);

        return redirect()->back()->with('message-suc', "Details Updated Successfully");
    }
    public function matrimonyProfileDownloade($idorbatchname)
    {
        ini_set('memory_limit','1048M');
        $id = (int)$idorbatchname;

        if(empty($id)){
            $ids=Batches_Model::where('batch_name',$idorbatchname)->select("matrimony_id")->orderBy('order_id',"ASC")->get()->toArray();
        }
        $query=Matrimony_model::join('users','convention_matrimony_registrations.email','=','users.email')
            ->join('members','users.id','=','members.user_id')
            ->join('eedujodu','users.id','=','eedujodu.user_id')
            ->leftjoin('eedujodusignificants','eedujodu.id','=','eedujodusignificants.eedujodu_id');
           if(empty($id)){
                $query->whereIn('convention_matrimony_registrations.id',$ids)
                ->join('convention_batches',function($join) use ($idorbatchname) {
                    $join->on('convention_matrimony_registrations.id','=','convention_batches.matrimony_id');
                $join->on('convention_batches.batch_name','=',DB::raw("'".$idorbatchname."'"));
                });
                 $query->orderBy(DB::raw('ISNULL(convention_batches.order_id), convention_batches.order_id'));
           }else{
            $query->where('convention_matrimony_registrations.id',$id);
           }
          $data['users'] = $query->select('users.id as user_id','eedujodu.*','members.*','users.*','convention_matrimony_registrations.*','convention_matrimony_registrations.created_at as created_at','users.created_at as nriva_created_at')
            ->get();
           
            foreach($data['users'] as $user){
                $url=config('conventions.NRIVA_URL').'/uploads/documents/Eedu-Jodu/'.$user->user_id.'/'.$user->profile_image;
                if(!$this->checkIfImageExist($url)){
                    $user_imgs=DB::table('user_images')->where('user_id',$user->user_id)->get(); 
                    foreach($user_imgs as $user_img){
                        $url=config('conventions.NRIVA_URL').'/uploads/documents/Eedu-Jodu/'.$user->user_id.'/'.$user_img->name;
                        if($this->checkIfImageExist($url)){
                            break;
                        }
                    }
                }
                $user['img_path']=$url;
            }
          //return view('admin/matrimony_profile',$data);

         $pdf = \Barryvdh\DomPDF\Facade\Pdf::loadView('admin/matrimony_profile', $data)->setPaper('a4', 'landscape');
          if(empty($id)){
             return $pdf->download($idorbatchname.'.pdf');
          }else{

            if(array_key_exists('member_id',$data)){
                $member_id=$data['member_id'];
            }else{
                $member_id=$data['users'][0]['member_id'];
            }
        return $pdf->download($member_id.'.pdf'); 

        }
    }

    public function checkIfImageExist($url)
    {
        $res=Http::get($url);
        if($res->status()=='200'){
            return true;
        }else{
            return false;
        }
    }

    public function createbatch(Request $request)
    {
        $exist = Batches_Model::where(["batch_name"=>$request->batch_name])->first();
        if(!$exist){
        $ids = explode(',',$request->selectedIds);
        foreach ($ids as $key => $value) {
            Batches_Model::create(["batch_name"=>$request->batch_name,
                                    "matrimony_id"=>$value,
                                    "order_id"=>$key+1,
                                ]);
            }
        }else{
            return redirect(url('admin/matrimonyRegistrations'))->withErrors("Batch Name Already Exist");
        }
        return redirect(url('admin/matrimonyRegistrations'));
    }

    public function batchusers($name)
    {
        $ids=Batches_Model::where('batch_name',$name)->select("matrimony_id")->orderBy('order_id',"ASC")->get()->toArray();
        if(empty($ids)){
            return redirect(url('admin/matrimonyRegistrations'));
        }
          $registrations=Matrimony_model::whereIn('convention_matrimony_registrations.id',$ids)->join('convention_batches',function($join) use ($name) {
            $join->on('convention_matrimony_registrations.id','=','convention_batches.matrimony_id');
          $join->on('convention_batches.batch_name','=',DB::raw("'".$name."'"));
        })->join('users','convention_matrimony_registrations.email','=','users.email')
            ->leftjoin('members','members.user_id','=','users.id')
            ->leftjoin('eedujodu','users.id','=','eedujodu.user_id')
            ->select('eedujodu.*','members.dob','members.gender','users.*','convention_matrimony_registrations.*','convention_matrimony_registrations.created_at as created_at','users.created_at as nriva_created_at','convention_batches.order_id as orderby',DB::Raw("str_to_date(`members`.`dob`,'%m-%d-%Y') as mem_dob"))
           // ->orderBy('convention_batches.order_id','ASC')
           ->orderBy(DB::raw('ISNULL(convention_batches.order_id), convention_batches.order_id'))
            ->get();
        //$registrations=$registrations->sortBy('orderby');
        //dd($registrations->toArray());
         return view('admin.registrations.batchmatrimonyRegistrations', compact('registrations','name') );
    }
    public function update_branch_user_order(Request $request)
    {
        $ids=Batches_Model::where('batch_name',$request->branchname)->where('matrimony_id',$request->id)->update(["order_id"=>$request->order_no]);
        return "success";
    }
    public function removebatchusers(Request $request)
    {
        $exist = Batches_Model::where(["batch_name"=>$request->batch_name])->first();
        if($exist){
        $ids = explode(',',$request->selectedIds);
        foreach ($ids as $key => $value) {
            Batches_Model::where(["batch_name"=>$request->batch_name,
                                    "matrimony_id"=>$value])->delete();
            }
        }else{
            return redirect(url('admin/matrimonyRegistrations'))->withErrors("Batch Name Not Found");
        }
        return redirect(url('admin/batchusers/'.$request->batch_name));
    }
    public function registrationsList(Request $request)
    {
        if($request->reset){
            $request->con_id=null;
            $request->email=null;
            $request->full_name=null;
            $request->keyword=null;
        }
        
        $query = RegistrationsList::where('status',1);
        if(isset($request->email)){
            $query->where('email',$request->email);
        }
        if(isset($request->con_id)){
            $query->where('convention_id',$request->con_id);
        }
        if(isset($request->full_name)){
            $query->where('first_name',$request->full_name);
        }
        if(isset($request->keyword)){
            $query->where('first_name','like','%'.$request->keyword."%");
            $query->orwhere('last_name','like','%'.$request->keyword."%");
            $query->orwhere('email','like','%'.$request->keyword."%");
            $query->orwhere('mobile','like','%'.$request->keyword."%");
            $query->orwhere('chapter','like','%'.$request->keyword."%");
            $query->orwhere('package_name','like','%'.$request->keyword."%");
        }
        $registrationsList = $query->get()->toArray();
        return view('admin.registrations.registrationsList' , compact('registrationsList','request'));
    }
     public function ImportRegistrations(Request $request)
    {   
        $this->validate($request, [
          'select_file'  => 'required|mimes:xls,xlsx'
         ]);

         $path = $request->file('select_file')->getRealPath();
         $uploaded_filename = $request->file('select_file')->getClientOriginalName();

         $data = Excel::toArray(new AdminDonorImport,$request->file('select_file'));
         
         foreach ($data[0] as $key => $value) {
            $exist= RegistrationsList::where('email',$value['email'])->first();
            if(!$exist){
             RegistrationsList::create([
                'convention_id'=>$value['convention_registration_id'],'first_name'=>$value['beneficiary_name_nriva_member'],  'email'=>$value['email'], 'mobile'=>$value['phone'], 'chapter'=>$value['chapter'], 'package_name'=>$value['sponsor_category'], 'assignee'=>$value['assignee'], 'attending_convention'=>$value['attending_convention'], 'banquet_tickets_count'=>$value['banquet_tickets_count'], 'main_event_tickets_count'=>$value['main_event_tickets_count'], 'youth_banquet_count'=>$value['youth_banquet_count'], 'vip_banquet_tickets_count'=>$value['vip_banquet_tickets_count'], 'vip_admissions_count'=>$value['vip_admissions_count'], 'vvip_banquet_count'=>$value['vvip_banquet_count'], 'vvip_admissions_count'=>$value['vvip_admissions_count'], 'hotel_count'=>$value['hotel_count']
             ]);
            }
         }
         return redirect('admin/registrationsList')->with('message-suc', "Uploaded Successfully");

     }

     public function update_tickets(Request $request)
        {
         
            $statusChangeEmail=\Cookie::get('ConloginUserEmail');
            if(Auth::user()){
                $statusChangeEmail=Auth::user()->email;
            }
            $user = RegistrationsList::where('id', $request->user_id)->first();  

            AuditLog_Model::create([
                'registration_id'=>$user->id,
                'convention_id'=>$user->convention_id,
                'user_name'=>$user->first_name,
                'user_email'=>$user->email,
                'old_status'=>$user->ticket_status,
                'new_status'=>$request->status,
                'reg_type'=>"Upload User Status Changes",
                'more_info'=>"Ticket Status Change",
                'statusChanged_by'=>$statusChangeEmail
            ]); 
            $user->check_in=$request->status;
            $user->hotel_name=$request->hotel_name;
            $user->roomnum=$request->roomnum;
            $user->save();
            return "success";
        }
        public function update_notes(Request $request)
        {
            $statusChangeEmail=\Cookie::get('ConloginUserEmail');
            if(Auth::user()){
                $statusChangeEmail=Auth::user()->email;
            }
            $user = RegistrationsList::where('id', $request->user_id)->first();
            AuditLog_Model::create([
                'registration_id'=>$user->id,
                'convention_id'=>$user->convention_id,
                'user_name'=>$user->first_name,
                'user_email'=>$user->email,
                'old_status'=>$user->notes,
                'new_status'=>$request->notes,
                'reg_type'=>"Upload User notes Changes",
                'more_info'=>"Notes Update in convention",
                'statusChanged_by'=>$statusChangeEmail
            ]); 
            $user->comments=$request->notes;
            $user->save();
            return "success";
        }
        public function checkedinRoom(Request $request)
        {
            $user = RegistrationsList::where('id', $request->user_id)->first();
            $user->check_in=1;
            $user->roomnum=$request->roomnum;
            $user->hotel_name=$request->hotel_name;
            $user->save();
            return redirect('admin/registrationsList')->with('message-suc', "Hostel Checked In Successfully");
        }

        public function auditLogs()
    {
       
        $audit= AuditLog_Model::orderBy('created_at',"desc")->get();
        
        return view('admin.master.auditLog.index', compact('audit'));
    }

 

}
