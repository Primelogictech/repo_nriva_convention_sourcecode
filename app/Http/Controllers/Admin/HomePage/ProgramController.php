<?php

namespace App\Http\Controllers\Admin\HomePage;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProgramStoreRequest;
use App\Http\Requests\ProgramUpdateRequest;
use App\Models\Admin\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $programs = Program::all();
        return view('admin.homePageContentUpdate.program.index', compact('programs'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.homePageContentUpdate.program.create');
    }

    /**
     * @param \App\Http\Requests\ProgramStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProgramStoreRequest $request)
    {
        $program = Program::create($request->validated());

        $file_name = 'program' . '_' . $program->id . '.' . $request->image->getClientOriginalExtension();
        UploadFile(config('conventions.program_upload'), $request->file('image'), $file_name);
        $program->image_url= $file_name;
        $program->save();

        $request->session()->flash('program.id', $program->id);

        return redirect()->route('program.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Program $program
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Program $program)
    {
        return view('admin.homePageContentUpdate.program.show', compact('program'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Program $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Program $program)
    {
        return view('admin.homePageContentUpdate.program.edit', compact('program'));
    }

    /**
     * @param \App\Http\Requests\ProgramUpdateRequest $request
     * @param \App\Models\Admin\Program $program
     * @return \Illuminate\Http\Response
     */
    public function update(ProgramUpdateRequest $request, Program $program)
    {
        $program->update($request->validated());
        if($request->hasFile('image')){
            $file_name = 'program' . '_' . $program->id . '.' . $request->image->getClientOriginalExtension();
            $request->file('image')->storeAs(config('conventions.program_upload'), $file_name);
            Program::where('id', $program->id)->update(['image_url' => $file_name]);
        }

        return redirect()->route('program.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Program $program
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Program $program)
    {
        
        if ( ($program->image_url=!null)  && (file_exists(config('conventions.program_display') . $program->image_url))) {
            unlink(config('conventions.program_display') . $program->image_url);
        }
        return $program->delete();
    }

    public function updateStatus(Request $request)
    {
        return Program::where('id', $request->id)->update(['status' => $request->status]);
    }
}
