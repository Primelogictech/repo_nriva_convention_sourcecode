<?php

namespace App\Http\Controllers\Admin\HomePage;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommitteStoreRequest;
use App\Http\Requests\MemberStoreRequest;
use App\Http\Requests\MemberUpdateRequest;
use App\Models\Admin\Committe;
use App\Models\Admin\Designation;
use App\Models\Admin\Donortype;
use App\Models\Admin\Invitee;
use App\Models\Admin\RolesAndDesignations;
use App\Models\Admin\LeadershipType;
use App\Models\Admin\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\MenuItem;
use App\Models\PermitionsType;
use App\Models\UserPermission;
use Auth;
class MemberController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $members = Member::withCount('roles')->get();
        return view('admin.homePageContentUpdate.member.index', compact('members'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function members_search(Request $request)
    {
        $users = \DB::table('users')->select('first_name','last_name','member_id')->where('first_name', 'like', $request->keyword . '%')->get()->toArray();
        $names='<ul id="country-list">';
        foreach($users as $user){
            $names.='<li class="text_name">'.$user->first_name.' '.$user->last_name.'-'.$user->member_id.'</li>';
        }
        $names.='</ul>';
        return $names;
       
    }
    public function create(Request $request)
    {
        

        return view('admin.homePageContentUpdate.member.create');
    }

    /**
     * @param \App\Http\Requests\MemberStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberStoreRequest $request)
    {   
        $member = Member::create($request->validated());

        if ($request->hasFile('image')) {
            $file_name = 'member' . '_' . $member->id . '.' . $request->image->getClientOriginalExtension();
            $request->file('image')->storeAs(config('conventions.member_upload'), $file_name);
        } else {
            $file_name = null;
        }

        $member->image_url = $file_name;
        $member->member_id = $request->member_id;
        $member->save();

        $request->session()->flash('member.id', $member->id);

        return redirect()->route('member.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Member $member
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Member $member)
    {
        return view('admin.homePageContentUpdate.member.show', compact('member'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Member $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Member $member)
    {
        return view('admin.homePageContentUpdate.member.edit', compact('member'));
    }

    /**
     * @param \App\Http\Requests\MemberUpdateRequest $request
     * @param \App\Models\Admin\Member $member
     * @return \Illuminate\Http\Response
     */
    public function update(MemberUpdateRequest $request, Member $member)
    {
        
        $member->update($request->validated());

        if ($request->hasFile('image')) {
            $file_name = 'member' . '_' . $member->id . '.' . $request->image->getClientOriginalExtension();
            $request->file('image')->storeAs(config('conventions.member_upload'), $file_name);

            $member->image_url = $file_name;
            $member->save();
        }

        $request->session()->flash('member.id', $member->id);

        return redirect()->route('member.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Member $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Member $member)
    {
        return  $member->delete();
    }


    public function updateStatus(Request $request)
    {
        return Member::where('id', $request->id)->update(['status' => $request->status]);
    }

    public function showAssigneRolesPage($member_id)
    {
        $member = Member::FindOrFail($member_id);
        $leadershiptypes=LeadershipType::where('status',1)->get();
        $invitees=Invitee::where('status',1)->get();
        $donortypes=Donortype::where('status',1)->get();
        $committes=Committe::where('status',1)->get();
        $designations=Designation::where('status',1)->get();
        $rolesAndDesignations=RolesAndDesignations::where('member_id', $member_id )->get();
        return view('admin.homePageContentUpdate.member.assigneRole', compact('member','leadershiptypes', 'invitees', 'donortypes', 'designations', 'rolesAndDesignations', 'committes'));
    }

    public function storeAssigneRolesPage(Request $request)
    {

        $final_rules=[];
        if ($request->has('Leadership_check')) {
            $rules = [
                'Leadership.*.leadershiptype' => ['required'],
                'Leadership.*.designation' => ['required'],
            ];
            $final_rules=array_merge($final_rules, $rules);
        }

        if ($request->has('Invitees_check')) {
            $rules = [
                'invitees.*.inviteestype' => ['required'],
                'invitees.*.designation' => ['required'],
            ];
            $final_rules=array_merge($final_rules, $rules);
        }

        if ($request->has('donors_check')) {
            $rules = [
                'donor.*.donorsType' => ['required'],
                'donor.*.designation' => ['required'],
            ];
            $final_rules=array_merge($final_rules, $rules);
        }

        if ($request->has('committes_check')) {
            $rules = [
                'committe.*.committe' => ['required'],
                'committe.*.designation' => ['required'],
            ];
            $final_rules = array_merge($final_rules, $rules);
        }
        $request->validate($final_rules);

        // need to update the code
        $old_ids=RolesAndDesignations::where('member_id', $request->member_id)->pluck('id')->toArray();
        $ids = array();

        if($request->has('Leadership_check')){
            $type = 'leadershiptype';
            foreach ($request->Leadership as $key => $value) {
                $temp =[
                    'member_id' => $request->member_id,
                    'type' => $type,
                    'sub_type_id' =>$value['leadershiptype'],
                    'designation_id' => $value['designation']
                ];
                $id=$this->createOrUpdate($request, $value, $temp,$type);
                array_push($ids, $id);
           }
        }

        if ($request->has('Invitees_check')) {
            $type = 'inviteestype';
            foreach ($request->invitees as $key => $value) {
                $temp = [
                    'member_id' => $request->member_id,
                    'type' => $type,
                    'sub_type_id' => $value['inviteestype'],
                    'designation_id' => $value['designation']
                ];
                //$RolesAndDesignations = RolesAndDesignations::create($temp);
                $id = $this->createOrUpdate($request, $value, $temp, $type);
                array_push($ids, $id);
            }
        }

        if ($request->has('donors_check')) {
            $type = 'donorsType';
            foreach ($request->donor as $key => $value) {
                $temp = [
                    'member_id' => $request->member_id,
                    'type' => $type,
                    'sub_type_id' => $value['donorsType'],
                    'designation_id' => $value['designation']
                ];
                //$RolesAndDesignations = RolesAndDesignations::create($temp);
                $id = $this->createOrUpdate($request, $value, $temp, $type);
                array_push($ids, $id);
            }
        }

        if ($request->has('committes_check')) {
            $type = 'committe';
            foreach ($request->committe as $key => $value) {
                $temp = [
                    'member_id' => $request->member_id,
                    'type' => $type,
                    'sub_type_id' => $value['committe'],
                    'designation_id' => $value['designation']
                ];
                //$RolesAndDesignations = RolesAndDesignations::create($temp);
                $id = $this->createOrUpdate($request, $value, $temp, $type);
                array_push($ids, $id);
            }
        }

        $to_be_deleted = array_diff($old_ids,$ids);
        RolesAndDesignations::destroy($to_be_deleted);
       return Redirect('admin/assigne-roles/'.$request->member_id)
       ->with('message-suc', 'Roles and Designation Added/Updated');;
    }

    public function showAssignePermissionsPage($member_id)
    {
        $member = Member::where('member_id', $member_id)
                                        ->first();
        $record = UserPermission::where('member_id', $member_id)
                                        ->first();

        $menuItems = MenuItem::all();
        $PermitionsTypes = PermitionsType::all();

        $menu_array=array();
            $main_menu_name=array();
            foreach ($menuItems as $key => $value) {
                
                if($value->is_main_menu==1 || $value->is_main_menu==2){
                    $main_menu_name[$value->id]=$value->menu_name;
                }
                if($value->parent_id!=0){
                    $menu_array[$value->parent_id][]=array('id'=>$value->id,'name'=>$value->menu_name,'slug'=>$value->slug);
                }
                
            }
            
        return view('admin.homePageContentUpdate.member.assignePermissions', compact('member','menu_array','main_menu_name','record','PermitionsTypes'));
    }

    public function storeAssignePermissionsPage(Request $request)
    {
      
        $userId = Auth::id();
        $permission_ids=array();
        $menu_page_ids=array();
        if($request->permission_ids){
            $permission_ids=$request->permission_ids;
        }
        if($request->menu_page_ids){
            $menu_page_ids=$request->menu_page_ids;
        }
        $temp = [
                    'member_id' => $request->member_id,
                    'menu_id' => implode(',',$menu_page_ids),
                    'permission_type_id' => implode(',',$permission_ids),
                    'created_by' => $userId,
                    'created_on' => date('Y-m-d H-i-s')
                ];
        
        $old_record = UserPermission::where('member_id', $request->member_id)
                                        ->first();
        if (!$old_record) {
            $UserPermission = UserPermission::create($temp);
        } else {
            $old_record->menu_id = implode(',',$menu_page_ids);
            $old_record->permission_type_id = implode(',',$permission_ids);
            $old_record->save();
        }
       return Redirect('admin/assigne-permissions/'.$request->member_id)
       ->with('message-suc', 'Permissions Added/Updated');
    }

    public function createOrUpdate($request, $value,$temp,$type)
    {
        $old_record = RolesAndDesignations::where('member_id', $request->member_id)
                                        ->where('type', $type)
                                        ->where('sub_type_id', $value[$type])
                                        ->first();
        if (!$old_record) {
            $RolesAndDesignations = RolesAndDesignations::create($temp);
            return $RolesAndDesignations->id;
        } else {
            $old_record->designation_id = $value['designation'];
            $old_record->save();
            return $old_record->id;
        }
    }

}
