<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\MrAndMissTypes;
use Illuminate\Http\Request;

class MrAndMissController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $MrAndMissTypes = MrAndMissTypes::all();
        return view('admin.master.MrAndMissTypes.index', compact('MrAndMissTypes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      //
        return view('admin.master.MrAndMissTypes.create');
    }

    /**
     * @param \App\Http\Requests\MrAndMissTypesStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $MrAndMissTypes = MrAndMissTypes::create($request->except(['_token']));

        $request->session()->flash('MrAndMissTypes.id', $MrAndMissTypes->id);
        return redirect()->route('mrandmisstype.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\MrAndMissTypes $MrAndMissTypes
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return view('admin.master.MrAndMissTypes.show', compact('MrAndMissTypes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\MrAndMissTypes $MrAndMissTypes
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
        $MrAndMissTypes = MrAndMissTypes::where('id',$request->id)->first();
        return view('admin.master.MrAndMissTypes.edit', compact('MrAndMissTypes'));
    }

    /**
     * @param \App\Http\Requests\MrAndMissTypesUpdateRequest $request
     * @param \App\Models\Admin\MrAndMissTypes $MrAndMissTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $MrAndMissTypes = MrAndMissTypes::where('id',$request->id)->update($request->except(['_token','_method','submit']));

        return redirect()->route('mrandmisstype.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\MrAndMissTypes $MrAndMissTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $MrAndMissTypes = MrAndMissTypes::where($request->id)->first();
        return  $MrAndMissTypes->delete();

    }

    public function updateStatus(Request $request)
    {
        return MrAndMissTypes::where('id', $request->id)->update(['status' => $request->status]);
    }
}
