<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SponsorCategoryStoreRequest;
use App\Http\Requests\SponsorCategoryUpdateRequest;
use App\Models\Admin\Benefittype;
use App\Models\Admin\Donortype;
use App\Models\Admin\SponsorCategory;
use App\Models\Admin\SponsorCategoryType;
use Illuminate\Http\Request;

class SponsorCategoryController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sponsorCategories = SponsorCategory::with('sponsorcategorytype', 'donortype')->get();

        return view('admin.master.sponsorCategory.index', compact('sponsorCategories'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $SponsorCategoryTypes=SponsorCategoryType::where('status',1)->get();
        $Donortypes=Donortype::where('status',1)->get();
        $benfits=Benefittype::where('status',1)->get();
        return view('admin.master.sponsorCategory.create', compact('SponsorCategoryTypes', 'Donortypes', 'benfits'));
    }

    /**
     * @param \App\Http\Requests\SponsorCategoryStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->type_id=="Donor"){
                $rules= [
                    'donor_amount' => ['required'],
                    'donor_type_id' => ['required'],
                    'benifits' => ['required'],
                ];
        }else{
            $rules= [
                'category.*' => ['required'],
                'amount_before.*' => ['required'],
                'amount_after.*' => ['required'],
                'price_change_date.*' => ['required'],
                ];
        }
        $request->validate($rules);

        $SponsorCategoryType=SponsorCategoryType::where('name', $request->type_id)->first();
        if($request->type_id == 'Family / Individual'){
            foreach ($request->category as $key => $value) {
                $temp=[
                    'category_type_id'=> $SponsorCategoryType->id,
                    'amount_before'=> $request->amount_before[$key],
                    'amount_after'=> $request->amount_after[$key],
                    'price_change_date'=> $request->price_change_date[$key],
                    'benefits'=> [$request->category[$key]],
                ];
                $sponsorCategory = SponsorCategory::create($temp);
            }
        }

        if ($request->type_id == 'Donor') {
            $temp=[
                'category_type_id' => $SponsorCategoryType->id,
                'donor_type_id' =>  $request->donor_type_id,
                'amount' => $request->donor_amount,
            ];
            $sponsorCategory = SponsorCategory::create($temp);
            foreach ($request->benifits as $key => $value) {
                $sponsorCategory->benfits()->attach($value, ['count' => $request->count[$value], 'display_order' => $request->display_order[$value]]);
            }
        }


        $request->session()->flash('sponsorCategory.id', $sponsorCategory->id);

        return redirect()->route('sponsor-category.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\SponsorCategory $sponsorCategory
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, SponsorCategory $sponsorCategory)
    {

        return view('admin.master.sponsorCategory.show', compact('sponsorCategory'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\SponsorCategory $sponsorCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, SponsorCategory $sponsorCategory)
    {

            if($request->type_id=="Donor"){
                $rules= [
                    'donor_amount' => ['required'],
                    'donor_type_id' => ['required'],
                    'benifits' => ['required'],
                ];
        }else{
            $rules= [
                'category.*' => ['required'],
                'amount_before.*' => ['required'],
                'amount_after.*' => ['required'],
                'price_change_date.*' => ['required'],
                ];
        }
        $request->validate($rules);

        $benfitstypes = Benefittype::where('status', 1)->get();
        $SponsorCategoryTypes = SponsorCategoryType::where('status', 1)->get();
        $Donortypes = Donortype::where('status', 1)->get();
        return view('admin.master.sponsorCategory.edit', compact('sponsorCategory', 'Donortypes', 'SponsorCategoryTypes', 'benfitstypes'));
    }

    /**
     * @param \App\Http\Requests\SponsorCategoryUpdateRequest $request
     * @param \App\Models\Admin\SponsorCategory $sponsorCategory
     * @return \Illuminate\Http\Response
     */
    public function update(SponsorCategoryUpdateRequest $request, SponsorCategory $sponsorCategory)
    {
         if($request->type_id=="Donor"){
                $rules= [
                    'donor_amount' => ['required'],
                    'donor_type_id' => ['required'],
                ];
        }else{
            $rules= [
                'category.*' => ['required'],
                'amount_before.*' => ['required'],
                'amount_after.*' => ['required'],
                'price_change_date.*' => ['required'],
                ];
        }
        $request->validate($rules);
        $SponsorCategoryType = SponsorCategoryType::where('name', $request->type_id)->first();
        if ($request->type_id == 'Family / Individual') {
            foreach ($request->category as $key => $value) {
                $temp = [
                    'category_type_id' => $SponsorCategoryType->id,
                    'benefits' => [$request->category[$key]],
                    'amount_before'=> $request->amount_before[$key],
                    'amount_after'=> $request->amount_after[$key],
                    'price_change_date'=> $request->price_change_date[$key],
                ];
                $a=SponsorCategory::where('id', $sponsorCategory->id)->update($temp);
            }
        }

        if ($request->type_id == 'Donor') {
            $temp = [
                'category_type_id' => $SponsorCategoryType->id,
                'donor_type_id' =>  $request->donor_type_id,
                'amount' => $request->donor_amount,
                //'benefits' => $request->benifits,
            ];
             SponsorCategory::where('id', $sponsorCategory->id)->update($temp);

             foreach ($request->benifits as $key => $value) {
                $data[$value] = ['count' => $request->count[$value],'display_order' => $request->display_order[$value] ];
            }
            $sponsorCategory=SponsorCategory::find($sponsorCategory->id)->benfits()->sync($data);
        }

       // $sponsorCategory->update($request->validated());

       // $request->session()->flash('sponsorCategory.id', $sponsorCategory->id);

        return redirect()->route('sponsor-category.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\SponsorCategory $sponsorCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, SponsorCategory $sponsorCategory)
    {
          $SponsorCategoryType = SponsorCategoryType::where('id', $sponsorCategory->category_type_id)->first();
          if($SponsorCategoryType->name== "Family / Individual"){
            return $sponsorCategory->delete();
          }else{
             $sponsorCategory->benfits()->detach();
            return $sponsorCategory->delete();
          }

        //redirect()->route('sponsorCategory.index');
    }

    public function updateStatus(Request $request)
    {
        return SponsorCategory::where('id', $request->id)->update(['status' => $request->status]);
    }
}
