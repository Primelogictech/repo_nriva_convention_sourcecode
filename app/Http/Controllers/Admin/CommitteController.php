<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommitteStoreRequest;
use App\Http\Requests\CommitteUpdateRequest;
use App\Models\Admin\Committe;
use Illuminate\Http\Request;
use App\Models\Admin\RegistrationForms;
use App\Models\Admin\Program;

class CommitteController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $committes = Committe::all();

        return view('admin.master.committe.index', compact('committes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $registartionforms = RegistrationForms::where('status',1)->get();
        return view('admin.master.committe.create',compact('registartionforms'));
    }

    /**
     * @param \App\Http\Requests\CommitteStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommitteStoreRequest $request)
    {
        
        $committe = Committe::create($request->validated());
        $file_name = 'committe' . '_' . $committe->id . '.' . $request->image->getClientOriginalExtension();
        UploadFile(config('conventions.committe_upload'), $request->file('image'), $file_name);
        $banner_name = 'committebanner' . '_' . $committe->id . '.' . $request->image->getClientOriginalExtension();
        UploadFile(config('conventions.committe_upload'), $request->file('banner_image'), $banner_name);
        $committe->image_url=$file_name;
        $committe->banner_image=$banner_name;
        $committe->save();

        if($request->is_program > 0){

            $program = Program::create([
            'programtime'=>$request->programtime,
            'program_name'=>$request->program_name,
            'Location'=>$request->Location,
            'date'=>$request->date,
            'page_content'=>$request->page_content
            ]);

            $file_name = 'program' . '_' . $program->id . '.' . $request->image->getClientOriginalExtension();
            UploadFile(config('conventions.program_upload'), $request->file('programimage'), $file_name);
            $program->image_url= $file_name;
            $program->committee_id= $committe->id;
            $program->save();
        }

        $request->session()->flash('committe.id', $committe->id);

        return redirect()->route('committe.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Committe $committe
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Committe $committe)
    {
        return view('admin.master.committe.show', compact('committe'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Committe $committe
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Committe $committe)
    {
        $registartionforms = RegistrationForms::where('status',1)->get();
        return view('admin.master.committe.edit', compact('committe','registartionforms'));
    }

    /**
     * @param \App\Http\Requests\CommitteUpdateRequest $request
     * @param \App\Models\Admin\Committe $committe
     * @return \Illuminate\Http\Response
     */
    public function update(CommitteUpdateRequest $request, Committe $committe)
    {
         $committe->update($request->validated());
      
        if ($request->hasfile('image')) {
            $file_name = 'committe' . '_' . $committe->id . '.' . $request->image->getClientOriginalExtension();
            $request->file('image')->storeAs(config('conventions.committe_upload'), $file_name);
            Committe::where('id', $committe->id)->update(['image_url' => $file_name]);
        }
        if(!$request->is_program){
            Committe::where('id', $committe->id)->update(['is_program' => 0]);
        }

        if ($request->hasfile('image')) {
        $banner_name = 'committebanner' . '_' . $committe->id . '.' . $request->banner_image->getClientOriginalExtension();
        $request->file('banner_image')->storeAs(config('conventions.committe_upload'), $banner_name);
            Committe::where('id', $committe->id)->update(['banner_image' => $banner_name]);
        }

        if($request->is_program > 0){

            $program = Program::updateOrCreate(['committee_id'=>$committe->id],[
                'programtime'=>$request->programtime,
            'program_name'=>$request->program_name,
            'Location'=>$request->Location,
            'date'=>$request->date,
            'page_content'=>$request->page_content
            ]);

            if ($request->hasfile('programimage')) {
            $file_name = 'program' . '_' . $program->id . '.' . $request->programimage->getClientOriginalExtension();
            UploadFile(config('conventions.program_upload'), $request->file('programimage'), $file_name);
            Program::where('committee_id', $committe->id)->update(['image_url' => $file_name]);
            }
        }

        $request->session()->flash('committe.id', $committe->id);

        return redirect()->route('committe.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Committe $committe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Committe $committe)
    {
        return $committe->delete();

        return redirect()->route('committe.index');
    }

    public function updateStatus(Request $request)
    {
        
        return Committe::where('id', $request->id)->update(['status' => $request->status]);
    }
}
