<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\SouvenirSponsorTypes;
use Illuminate\Http\Request;

class SouvenirSponsorController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $SouvenirSponsorTypes = SouvenirSponsorTypes::all();
        return view('admin.master.SouvenirSponsorTypes.index', compact('SouvenirSponsorTypes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      //
        return view('admin.master.SouvenirSponsorTypes.create');
    }

    /**
     * @param \App\Http\Requests\SouvenirSponsorTypesStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $SouvenirSponsorTypes = SouvenirSponsorTypes::create($request->except(['_token']));
        $request->session()->flash('SouvenirSponsorTypes.id', $SouvenirSponsorTypes->id);
        return redirect()->route('souvenir-sponsortype.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\SouvenirSponsorTypes $SouvenirSponsorTypes
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return view('admin.master.SouvenirSponsorTypes.show', compact('SouvenirSponsorTypes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\SouvenirSponsorTypes $SouvenirSponsorTypes
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
        $SouvenirSponsorTypes = SouvenirSponsorTypes::where('id',$request->id)->first();
        return view('admin.master.SouvenirSponsorTypes.edit', compact('SouvenirSponsorTypes'));
    }

    /**
     * @param \App\Http\Requests\SouvenirSponsorTypesUpdateRequest $request
     * @param \App\Models\Admin\SouvenirSponsorTypes $SouvenirSponsorTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $SouvenirSponsorTypes = SouvenirSponsorTypes::where('id',$request->id)->update($request->except(['_token','_method','submit']));

        return redirect()->route('souvenir-sponsortype.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\SouvenirSponsorTypes $SouvenirSponsorTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $SouvenirSponsorTypes = SouvenirSponsorTypes::where($request->id)->first();
        return  $SouvenirSponsorTypes->delete();

    }

    public function updateStatus(Request $request)
    {
        return SouvenirSponsorTypes::where('id', $request->id)->update(['status' => $request->status]);
    }
}
