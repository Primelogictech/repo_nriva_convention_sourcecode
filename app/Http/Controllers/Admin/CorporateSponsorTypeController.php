<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\CorporateSponsortype;
use Illuminate\Http\Request;

class CorporateSponsorTypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $CorporateSponsortypes = CorporateSponsortype::all();
        return view('admin.master.corporateSponsorType.index', compact('CorporateSponsortypes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      //
        return view('admin.master.corporateSponsorType.create');
    }

    /**
     * @param \App\Http\Requests\DonortypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules= [
            'name' => ['required'],
            'amount' => ['required'],
          //  'display_order' => ['integer'],
        ];
        $request->validate($rules);
        $CorporateSponsortype = CorporateSponsortype::create($request->all());

        $request->session()->flash('CorporateSponsortype.id', $CorporateSponsortype->id);
        return redirect()->route('corporatesponsors.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Donortype $donortype
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, CorporateSponsortype $CorporateSponsortype)
    {
        return view('admin.master.donortype.show', compact('CorporateSponsortype'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Donortype $donortype
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,  $CorporateSponsortype)
    {
        $CorporateSponsortype=CorporateSponsortype::find($CorporateSponsortype); 
        return view('admin.master.corporateSponsorType.edit', compact('CorporateSponsortype'));
    }

    /**
     * @param \App\Http\Requests\DonortypeUpdateRequest $request
     * @param \App\Models\Admin\Donortype $donortype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $CorporateSponsortype)
    {
        $CorporateSponsortype=CorporateSponsortype::find($CorporateSponsortype); 
        $rules= [
            'name' => ['required'],
            'amount' => ['required'],
          //  'display_order' => ['integer'],
        ];
        $request->validate($rules);
          $CorporateSponsortype->update($request->all());

        $request->session()->flash('CorporateSponsortype.id', $CorporateSponsortype->id);

        return redirect()->route('corporatesponsors.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Donortype $donortype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $CorporateSponsortype)
    {
        $CorporateSponsortype=CorporateSponsortype::find($CorporateSponsortype); 
        return   $CorporateSponsortype->delete();

    }

    public function updateStatus(Request $request)
    {
        return CorporateSponsortype::where('id', $request->id)->update(['status' => $request->status]);
    }
}
