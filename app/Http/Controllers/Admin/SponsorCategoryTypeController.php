<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SponsorCategoryTypeStoreRequest;
use App\Http\Requests\SponsorCategoryTypeUpdateRequest;
use App\Models\Admin\SponsorCategoryType;
use Illuminate\Http\Request;

class SponsorCategoryTypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sponsorCategoryTypes = SponsorCategoryType::all();

        return view('admin.master.sponsorCategoryType.index', compact('sponsorCategoryTypes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.master.sponsorCategoryType.create');
    }

    /**
     * @param \App\Http\Requests\SponsorCategoryTypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SponsorCategoryTypeStoreRequest $request)
    {
        $sponsorCategoryType = SponsorCategoryType::create($request->validated());

        $request->session()->flash('sponsorCategoryType.id', $sponsorCategoryType->id);

        return redirect()->route('sponsor-category-type.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\SponsorCategoryType $sponsorCategoryType
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, SponsorCategoryType $sponsorCategoryType)
    {
        return view('admin.master.sponsorCategoryType.show', compact('sponsorCategoryType'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\SponsorCategoryType $sponsorCategoryType
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, SponsorCategoryType $sponsorCategoryType)
    {
        return view('admin.master.sponsorCategoryType.edit', compact('sponsorCategoryType'));
    }

    /**
     * @param \App\Http\Requests\SponsorCategoryTypeUpdateRequest $request
     * @param \App\Models\Admin\SponsorCategoryType $sponsorCategoryType
     * @return \Illuminate\Http\Response
     */
    public function update(SponsorCategoryTypeUpdateRequest $request, SponsorCategoryType $sponsorCategoryType)
    {
        $sponsorCategoryType->update($request->validated());

        $request->session()->flash('sponsorCategoryType.id', $sponsorCategoryType->id);

        return redirect()->route('sponsor-category-type.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\SponsorCategoryType $sponsorCategoryType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, SponsorCategoryType $sponsorCategoryType)
    {
        return  $sponsorCategoryType->delete();
    }

    public function updateStatus(Request $request)
    {
        return SponsorCategoryType::where('id', $request->id)->update(['status' => $request->status]);
    }
}
