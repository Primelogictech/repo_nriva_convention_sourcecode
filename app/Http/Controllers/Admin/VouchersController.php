<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\VouchersStoreRequest;
use App\Http\Requests\VouchersUpdateRequest;
use App\Models\Admin\Vouchers;
use Illuminate\Http\Request;

class VouchersController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vouchers = Vouchers::all();

        return view('admin.master.vouchers.index', compact('vouchers'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.master.vouchers.create');
    }

    /**
     * @param \App\Http\Requests\VouchersStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(VouchersStoreRequest $request)
    {
        $vouchers = Vouchers::create($request->validated());

        $request->session()->flash('vouchers.id', $vouchers->id);

        return redirect()->route('vouchers.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Vouchers $Vouchers
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $vouchers)
    {
        return view('admin.master.vouchers.show', compact('vouchers'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Vouchers $Vouchers
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,  $id)
    {
        $vouchers=Vouchers::findOrFail($id);
        return view('admin.master.vouchers.edit', compact('vouchers'));
    }

    /**
     * @param \App\Http\Requests\VouchersUpdateRequest $request
     * @param \App\Models\Admin\Vouchers $Vouchers
     * @return \Illuminate\Http\Response
     */
    public function update(VouchersUpdateRequest $request, $id)
    {
        $vouchers=Vouchers::findOrFail($id);
        $vouchers->update($request->validated());
        $request->session()->flash('vouchers.id', $vouchers->id);

        return redirect()->route('vouchers.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Vouchers $Vouchers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Vouchers $vouchers)
    {
        return $vouchers->delete();

    }

    public function updateStatus(Request $request)
    {
        return Vouchers::where('id', $request->id)->update(['status' => $request->status]);
    }
}
