<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LeadershipTypeStoreRequest;
use App\Http\Requests\LeadershipTypeUpdateRequest;
use App\Models\Admin\LeadershipType;
use Illuminate\Http\Request;

class LeadershipTypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $leadershipTypes = LeadershipType::all();

        return view('admin.master.leadershipType.index', compact('leadershipTypes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.master.leadershipType.create');
    }

    /**
     * @param \App\Http\Requests\LeadershipTypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeadershipTypeStoreRequest $request)
    {
        $leadershipType = LeadershipType::create($request->validated());

        $request->session()->flash('leadershipType.id', $leadershipType->id);

        return redirect()->route('leadership-type.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\LeadershipType $leadershipType
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, LeadershipType $leadershipType)
    {
        return view('admin.master.leadershipType.show', compact('leadershipType'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\LeadershipType $leadershipType
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, LeadershipType $leadershipType)
    {
        return view('admin.master.leadershipType.edit', compact('leadershipType'));
    }

    /**
     * @param \App\Http\Requests\LeadershipTypeUpdateRequest $request
     * @param \App\Models\Admin\LeadershipType $leadershipType
     * @return \Illuminate\Http\Response
     */
    public function update(LeadershipTypeUpdateRequest $request, LeadershipType $leadershipType)
    {
        $leadershipType->update($request->validated());

        $request->session()->flash('leadershipType.id', $leadershipType->id);

        return redirect()->route('leadership-type.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\LeadershipType $leadershipType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, LeadershipType $leadershipType)
    {
        return $leadershipType->delete();
    }

    public function updateStatus(Request $request)
    {
        return LeadershipType::where('id', $request->id)->update(['status' => $request->status]);
    }
}
