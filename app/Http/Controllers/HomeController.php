<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Venue;
use App\Models\Admin\Menu;
use App\Models\Admin\Message;
use App\Models\Admin\Banner;
use App\Models\Admin\Committe;
use App\Models\Admin\Donor;
use App\Models\Admin\Donortype;
use App\Models\Admin\Event;
use App\Models\Admin\Invitee;
use App\Models\Admin\LeadershipType;
use App\Models\Admin\Program;
use App\Models\Admin\RolesAndDesignations;
use App\Models\Admin\Video;
use App\Models\User;
use App\Models\Admin\SponsorCategory;
use PHPUnit\TextUI\XmlConfiguration\Group;
use DB;
use Mail;

class HomeController extends Controller
{
    public function index()
    {
        $banners=Banner::where('status',1)->get();
        $venue= Venue::first();
        $messages= Message::with('designation')->where('status',1)->get();
        $programs= Program::whereHas('committes',function($q){
           
            $q->where('is_program',1);

        })->get();
        $events= Event::where('status', 1)->get();
        $donortypes= Donortype::where('status',1)->orderBy(DB::raw('ISNULL(display_order), display_order'))->get();
        $leadershiptypes = LeadershipType::where('status', 1)->orderBy(DB::raw('ISNULL(display_order), display_order'))->get();
        $invitees = Invitee::where('status', 1)->orderBy(DB::raw('ISNULL(display_order), display_order'))->get();
        $videos= Video::where('status',1)->orderBy(DB::raw('ISNULL(display_order), display_order'))->take(6)->get();
        $committes= Committe::where('status',1)->orderBy(DB::raw('ISNULL(display_order), display_order'))->get();
       
        $users_for_slide=$grand_event_users=$donor_users_arr=array();
        $donor_users=User::orderBy('donor_amount', 'DESC')->orderBy('first_name', 'ASC')->get();
        foreach($donor_users as $val){
            $SponsorCategory=SponsorCategory::find($val->sponsorship_category_id);
            if(isset($SponsorCategory)){

                $donor_level_name=Donortype::find($SponsorCategory->donor_type_id);
                if(isset($donor_level_name)){
                    $img = isset($val->image_url)?"public/storage/user/".$val->image_url:'images/no-image1.jpg';
                    $donor_users_arr[$donor_level_name->name.'~'.$SponsorCategory->amount][]=array('name'=>$val->first_name.' '.$val->last_name,'img_url'=>$img,'donor_amount'=>$val->donor_amount);
                    if($val->total_amount>=50000){
                        $grand_event_users[]=array('name'=>$val->first_name.' '.$val->last_name,'img_url'=>$img,'donor_amount'=>$val->donor_amount);
                    }
                    if($SponsorCategory->amount<=50000 && $SponsorCategory->amount>=15000){
                        $users_for_slide[$donor_level_name->name.'~'.$SponsorCategory->amount][]=array('name'=>$val->first_name.' '.$val->last_name,'img_url'=>$img,'donor_amount'=>$val->donor_amount);
                    }
                    
                }
                
            }
            
        }

         foreach ($users_for_slide as $key => $value) {
             $this->array_sort_by_column($value, 'donor_amount');
        }
        foreach ($users_for_slide as $key => $value) {
            $users_for_slide[$key]=array_chunk($value, 6);
        }
        $this->array_sort_by_column($grand_event_users, 'name');
        return view('welcome', compact('banners', 'venue', 'messages', 'programs', 'events', 'donortypes', 'videos', 'leadershiptypes', 'invitees', 'committes','donor_users_arr','grand_event_users','users_for_slide'));
    }
    public function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key => $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }



    public function dinamicPage($slug)
    {
        
        $page_data=Committe::where('slug',$slug)->first();
        if($page_data){
          return  $this->moreCommitteMembers($page_data->id);
        }

        $page_data=Menu::where('slug',$slug)->firstOrfail();

        if($page_data==null){
            return abort(404);
        }

        //compact('banners', 'venue', 'messages')
        $page_content=$page_data->page_content;
        return view('dinamicPage',compact('page_content') );
    }

    public function messageContent($id)
    {
        $message=Message::FindOrFail($id);
        return view('fullmessage', compact('message'));
    }

    public function programDetailsPage($id)
    {
        $program = Program::FindOrFail($id);
         $Committe_members  =  RolesAndDesignations::with('member', 'designation')
        ->whereHas('member', function ($query) {
            $query->where('status', 1);
        })
        ->where('type', 'committe')
        ->where(function($q){
            $q->where('designation_id',30);
            $q->orWhere('designation_id',31);

        })
        ->where('sub_type_id', $program->committee_id)
            ->get();
        return view('programDetailsPage', compact('program','Committe_members'));
    }

    public function eventSchedule($event_id)
    {
        $event= Event::with(['schedules'=>function ($q)
        {
            $q->groupby('date');
        }])->FindOrFail($event_id);
        return view('eventScheduleDetails', compact('event'));
    }

    public function moreDonerDetails($id)
    {
        $donors  =  RolesAndDesignations::with('member')
        ->whereHas('member', function ($query) {
            $query->where('status', 1);
        })
            ->where('type', 'donorsType')
            ->where('sub_type_id', $id)
            ->get();
        $Donortype =Donortype::find($id);
        return view('moreDonorDetails', compact('donors', 'Donortype') );
    }

    public function moreLeadershipDetails($id)
    {
        $Leaders  =  RolesAndDesignations::with('member', 'designation')
        ->whereHas('member', function ($query) {
            $query->where('status', 1);
        })
        ->where('type', 'leadershiptype')
        ->where('sub_type_id', $id)
            ->get();
        $LeadershipTypes = LeadershipType::find($id);
        return view('moreLeaderDetails', compact('Leaders', 'LeadershipTypes'));
    }

    public function moreInviteeDetails($id)
    {
        $Invitees  =  RolesAndDesignations::with('member', 'designation')
        ->whereHas('member', function ($query) {
            $query->where('status', 1);
        })
        ->where('type', 'inviteestype')
        ->where('sub_type_id', $id)
        ->get();
        $invitee = Invitee::find($id);
        return view('moreInveiteDetails', compact('Leaders', 'invitees'));
    }

    public function moreCommitteMembers($id)
    {
        $Committe_members  =  RolesAndDesignations::with('member', 'designation')
        ->leftjoin('convention_designations','convention_roles_and_designations.designation_id','=' ,'convention_designations.id')
        ->leftjoin('convention_members','convention_roles_and_designations.member_id','=' ,'convention_members.id')
        ->select('convention_roles_and_designations.*','convention_designations.display_order as designations_order','convention_members.*','convention_roles_and_designations.display_order as member_order')
        ->where('type', 'committe')
        ->where('sub_type_id', $id) 
        ->where('convention_members.status',1)
       ->orderBy(DB::raw('ISNULL(designations_order), designations_order'))
       ->orderBy(DB::raw('ISNULL(member_order), member_order'))
       ->orderBy('convention_members.name')
       ->get();
        $committe = Committe::find($id);
       

        return view('moreCommitteeMembers', compact('Committe_members', 'committe'));
    }

    public function showAllCommittees()
    {
        $committes= Committe::where('status',1)->orderBy(DB::raw('ISNULL(display_order), display_order'))->get();

        return view('showAllcommittees', compact('committes'));
    }

    public function showAllDonors()
    {
        $users_for_slide=$grand_event_users=$donor_users_arr=array();
        $donor_users=User::orderBy('donor_amount', 'DESC')->orderBy('first_name', 'ASC')->get();
        foreach($donor_users as $val){
            $SponsorCategory=SponsorCategory::find($val->sponsorship_category_id);
            if(isset($SponsorCategory)){

                $donor_level_name=Donortype::find($SponsorCategory->donor_type_id);
                if(isset($donor_level_name)){
                    $img = isset($val->image_url)?"public/storage/user/".$val->image_url:'images/no-image1.jpg';
                    $donor_users_arr[$donor_level_name->name.'~'.$SponsorCategory->amount][]=array('name'=>$val->first_name.' '.$val->last_name,'img_url'=>$img,'donor_amount'=>$val->donor_amount);
                    if($val->total_amount>=50000){
                        $grand_event_users[]=array('name'=>$val->first_name.' '.$val->last_name,'img_url'=>$img,'donor_amount'=>$val->donor_amount);
                    }
                    if($SponsorCategory->amount<=50000 && $SponsorCategory->amount>=15000){
                        $users_for_slide[$donor_level_name->name.'~'.$SponsorCategory->amount][]=array('name'=>$val->first_name.' '.$val->last_name,'img_url'=>$img,'donor_amount'=>$val->donor_amount);
                    }
                    
                }
                
            }
            
        }

        return view('showAllDonors',compact('donor_users_arr'));
    }

    


    public function showAllVideos()
    {
        $videos =Video::where('status',1)->get();
        return view('showAllVideos', compact( 'videos') );
    }

    public function getDonorsWithTypeId($id)
    {
        return RolesAndDesignations::with('member')
            ->whereHas('member', function ($query) {
                $query->where('status', 1);
            })
            ->where('type', 'donorsType')
            ->where('sub_type_id',$id)
            ->get();
    }

    public function getLeadershipsWithTypeId($id)
    {
        return RolesAndDesignations::with('member','designation')
        ->whereHas('member', function ($query) {
            $query->where('status', 1);
        })
        ->where('type', 'leadershiptype')
        ->where('sub_type_id', $id)
        ->get();
    }
    public function getinviteeWithTypeId($id)
    {
        if($id=='all'){
            return RolesAndDesignations::with('member','designation')
            ->whereHas('member', function ($query) {
                $query->where('status', 1);
            })
            ->where('type', 'inviteestype')
            ->get();
        }
        return RolesAndDesignations::with('member', 'designation')
        ->whereHas('member', function ($query) {
            $query->where('status', 1);
        })
        ->where('type', 'inviteestype')
        ->where('sub_type_id', $id)
            ->get();
    }

    public function showEventsOnCalander()
    {
        $venue=Venue::first();

        return view('calander' , compact('venue'));
    }

    public function geteventsInLeftSide(Request $request)
    {
        return  Event::
            whereBetween('from_date', [$request->start, $request->end])
            ->orwhereBetween('to_date', [$request->start, $request->end])
            ->get();
    }




    public function geteventsIndates(Request $request)
    {
        $events = Event::
       // whereDate('from_date', '>', $request->start)
        whereBetween('from_date', [$request->start, $request->end])
        ->orwhereBetween('to_date', [$request->start, $request->end])
                    ->with('schedules')->get();
        $data=[];
        foreach ($events as $key => $value) {
            $temp=[
                'title'=> $value->event_name,
                'start'=> ($value->from_date)->format('Y-m-d'),
                'end'=> ($value->to_date)->format('Y-m-d'),
                'url' =>  url('event-schedule', $value->id)
            ];
            array_push($data, $temp);

            foreach ($value->schedules as $schedule) {
                $temp = [
                    'title' => $schedule->program_name,
                    'start' => $schedule->date ." " . $schedule->from_time,
                    'end' =>  $schedule->date . " " .  $schedule->to_time,
                    'url' =>  url('event-schedule', $value->id)
                ];
                array_push($data, $temp);
            }
        }

        return $data;
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha' => captcha_img()]);
    }


    public function showContactUs()
    {
        $committes= Committe::where('status',1)->orderBy(DB::raw('ISNULL(display_order), display_order'))->get();
        
        return view('showContactUs',compact('committes'));
    }


    public function ContactUsSendMail(Request $request)
    {
        $rules= [
            'name' => ['required'],
            'Subject' => ['required'],
            'email' => ['required'],
            'message' => ['required'],
            'committee_mail' => ['required'],
        ];
        $request->validate($rules);

         /* Mail::raw( $request->message.  $request->email  , function ($message) use ($request) {
            $message->to($request->committee_mail)
            ->cc(['narendar.yarava@telanganaus.org','narenyarava@yahoo.com'])
            ->bcc('arun.e@primelogictech.com')
            ->subject( 'Mail form ' . ucfirst($request->name) .' - '.$request->Subject);
          });  */

          Mail::send([], [], function ($message) use ($request) {
            $message->to($request->committee_mail)
              ->subject('Mail form ' . ucfirst($request->name) .' - '.$request->Subject)
              //->cc(['narendar.yarava@telanganaus.org','narenyarava@yahoo.com','web.inbox@ttaconvention.org'])
              //->bcc('arun.e@primelogictech.com')
              ->setBody('<p>'.$request->message.'</p><br><b>From</b>:<br>'.$request->email, 'text/html'); // for HTML rich messages
          });

          
           Mail::send('mails.ContactusConformation', $request->all(),  function ($message) use ($request) {
            $message->to($request->email)
              ->subject('Convention Team');
          });


        return redirect()->back()->with('message-suc','Your message sent successfully');
    }



}
