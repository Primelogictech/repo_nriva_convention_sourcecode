<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\SouvenirSponsorTypes;
use App\Models\Admin\RegistrationContent;
use App\Models\Admin\Paymenttype;
use App\Models\Registration;
use App\Http\Controllers\Admin\PaymentController;
use App\Models\Otp_model;
use App\Models\User;
use App\Models\SouvenirRegPkgDetail;
use App\Models\SouvenirImage;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Mail\SouvenirRegistrationNotification;
use App\Models\Admin\SponsorCategoryType;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Carbon\Carbon;
use App\Models\Payment as ModelPayment;
use Mail;
use DB;
use Session;
use Config;

class SouvenirRegistrationController extends Controller
{
    
    public function __construct()
    {

        $settings = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(config('conventions.PAYPAL_SANDBOX_CLIENT_ID'), config('conventions.PAYPAL_SANDBOX_CLIENT_SECRET')));
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings);
    }
    
    public function SouvenirRegistration()
    {
        
        $SouvenirSponsorTypes=SouvenirSponsorTypes::where('status',1)->orderBy('display_order')->get();
        $RegistrationContent = RegistrationContent::first();
        $paymenttypes = Paymenttype::where('status',1)->get();
        $user=auth::user();
        if($user){
            $email=$user->email;
            $Registration=Registration::with('souvenirDetails','souvenirImages')
                        ->where('registration_type_name' , "Souvenir")
                        ->where('email',$email)
                        ->first();
        }else{
            $Registration=null;
        }
        
        return view('registration.ShowSouvenirRegistrationForm',compact('SouvenirSponsorTypes','RegistrationContent','paymenttypes','Registration'));
    }

    public function SouvenirRegistrationEdit()
    {
        $SouvenirSponsorTypes=SouvenirSponsorTypes::where('status',1)->orderBy('display_order')->get();
        $RegistrationContent = RegistrationContent::first();
        $paymenttypes = Paymenttype::where('status',1)->get();
        
        return view('registration.ShowSouvenirRegistrationForm',compact('SouvenirSponsorTypes','RegistrationContent','paymenttypes'));
    }


    public function new_registertion(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255|unique:convention_users',
            'otp'=>'required|exists:convention_otp_verification,otp'
        ]);

        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);


       $exist = \DB::table('convention_users')->where('email',$request->email)->first();
        $user->member_id = $user->id;
       /*  $user->first_name = $exist->first_name??'';
        $user->last_name = $exist->last_name??'';
        $user->address = $exist->street??'';
        $user->city = $exist->city??'';
        $user->state = $exist->state??'';
        $user->country = $exist->country??'';
        $user->spouse_full_name = $exist->spouse_first_name??'';
        $user->zip_code = $exist->zipcode??'';
        $user->mobile = $exist->mobile_number??''; */
       
        $user->registration_id = "CON".sprintf("%04d", $user->id);
        $user->save();

        event(new Registered($user));

        Auth::login($user);
        return redirect('souvenir-registration')->with('success','You have Authenticated successfully. Please continue to registration');
    }

    public function email_verification(Request $request)
    {
        $user = User::where("email",$request->email)->first();

        if(empty($user)){
            $otp = rand(10000,99999);
            
        Mail::raw("Hi, your email verification code for Souvenir registration is : $otp", function ($message) use ($request) {
          $message->to($request->email)
            ->subject("Email Verification code from Souvenir");
        });
         Otp_model::updateOrCreate(['user'=>$request->email],['otp'=>$otp, 'status'=>1]);
            return "success";
        }else{
            return "failed";
        }
    }
    
    
    public function storeSouvenirRegistration(Request $request)
    {
        $Registration=Registration::where('email',$request->email)
                ->where('registration_type_name' , "Souvenir")->first();
        $user=User::where('email',$request->email)->first();

        $more_info['referred_By']=$request->referred_By;
        $more_info['address']=$request->address;
        $data=array_merge($request->all(),
        [
            'user_id' => Auth::user()->id??0,
            'registration_type_name' => "Souvenir",
            'company_name' => $request->company_name1,
            'extra_data' => $more_info,
            ]
        );

        $Registration = Registration::updateOrCreate(['email'=>$request->email,'registration_type_name' => "Souvenir"], $data);

        if ($request->hasfile('image')) {
            
            foreach ($request->image as $key => $image) {
                    $file_name = 'souvenir_image'.'_'.$key. '_' .$Registration->id."_". $user->id . '.' . $image->getClientOriginalExtension();
                    $image->storeAs(config('conventions.souvenir_image_upload'), $file_name);
                        $temp = [
                            'souvenir_sponsor_type_id' => $request->souvenir_sponsor_type_id[$key],
                            'user_id' => $user->id,
                            'registration_id' => $Registration->id,
                            'image_url' => $file_name,
                        ];
                    SouvenirImage::updateOrCreate(
                    [
                        "id" => $request->souvenir_image_id[$key] 
                    ],
                        $temp);
            }
        }

        if($Registration->wasRecentlyCreated){
            $Registration->registration_id="SOU".sprintf("%04d", $Registration->id);
          
            $Registration->save();

            $amount=0;
            $SouvenirSponsorTypes_registration=[];
            foreach ($request->count as $key => $value) {
                if($value){
                    $SponsorCategoryType=SouvenirSponsorTypes::find($key);
        
                    $sub_amont=$SponsorCategoryType->amount*$value;
                    $amount=$amount+ $sub_amont;
        
                     $reg=SouvenirRegPkgDetail::Create([
                        'souvenir_sponsor_type_id'=>$key,
                        'user_id' =>  $user->id,
                        'registration_id' => $Registration->id,
                        'count' => $value,
                        'total_amount' => $sub_amont,
                    ]); 
                    $a=[
                        $SponsorCategoryType->name => $value
                    ];
                    $SouvenirSponsorTypes_registration = $a+ $SouvenirSponsorTypes_registration;
                }
            }

           $all_details=array();

           array_push($all_details, array('package_name'=> "Souvenir Registration"));
           array_push($all_details, array('package_details'=> $SouvenirSponsorTypes_registration));
   
           $request->merge([
               'package_details' => $all_details
           ]); 
            $paymenttype= Paymenttype::find($request->payment_type);

            if($paymenttype){
                if ($paymenttype->name == Config('conventions.paypal_name_db')) {
                    return $this->processPayment($request,$amount,$Registration, $paymenttype);
                }
            }

            $this->processPayment($request,$amount,$Registration, $paymenttype);
              Mail::to($request->email)->send(new SouvenirRegistrationNotification($Registration));
        }
      return redirect(url('myaccount'))->with('message-suc', "souvenir registered successfully");
    }


    public function processPayment($request,$amount, $Registration, $paymenttype)
    {
      $payment_made_towards = "Paid Towards Souvenir registration";

      if($paymenttype){
      if($paymenttype->name== Config('conventions.check_name_db')){
          $payment =App(PaymentController::Class)->PayWithCheck(Auth::user()->id??0, $amount, $paymenttype->id, $payment_made_towards, $request );
      }

      if ($paymenttype->name == Config('conventions.zelle_name_db')) {
          $payment =App(PaymentController::Class)->PayWithZelle(Auth::user()->id??0, $amount, $paymenttype->id, $payment_made_towards, $request);
      }

      if ($paymenttype->name == Config('conventions.other_name_db')) {
        $request->merge([
            'Payment_made_through' => $request->payee_name,
            'company_name' => $request->company_name,
        ]); 
          $payment =App(PaymentController::Class)->PayWithOther(Auth::user()->id??0, $amount, $paymenttype->id, $payment_made_towards, $request);
      }

      if ($paymenttype->name == Config('conventions.paypal_name_db')) {
              $temp = [
                  'user_id' => Auth::user()->id??0,
                  'payment_amount' => $amount,
                  'package_details' => $request->package_details,
                  'payment_methord' => $paymenttype->id,
                  'payment_status' => 'Pending',
                  'payment_made_towards' => 'Paid Towards Souvenir registration'
              ];
              $payment = ModelPayment::create($temp);
              Session::put('SouvenirUserId', $Registration->id);

              $Registration->payment_id=$payment->id;
              $Registration->save();
          return  App(PaymentController::Class)
              ->PayWithPayPal($amount, 'Souvenir Regestration ',  $payment->id, 'souvenirpayment.status');
          }
      }
  /*  if ($request->payment_type == "free" && Auth::user()) {
           array_push($all_details, array('payment_type'=> "Free Exhibit Registration"));
         $temp = [
                  'user_id' => Auth::user()->id,
                  'payment_amount' => 0,
                  'package_details' => $all_details,
                  'payment_methord' => 0,
                  'payment_status' => 'Paid',
                  'payment_made_towards' => 'Paid Towards Exhibit registration'
              ];
              $payment = ModelPayment::create($temp);
              $users = User::where('id',Auth::user()->id)->first();
              $users->free_exbhit_registrations=$users->free_exbhit_registrations-1;
              $users->save();

      } */
      $Registration->payment_id=$payment->id;
      $Registration->save(); 

      $exhibitUser=Registration::where('id',$Registration->id)->first();
        
    }

    public function SouvenirImageDeleteRegistration($id) {
        SouvenirImage::findorFail($id)->delete($id);
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
    
    }


    public function paypalStatus(Request $request)
    {
        $token = $request->get('token');
        $payerId = $request->get('PayerID');
        $payment_id = Session::get('paypal_payment_id');
        $SouvenirUserId = Session::get('SouvenirUserId');
        $Registration=Registration::where('id',$SouvenirUserId)->first();
        $ModelPayment = ModelPayment::find(Session::get('payment_id'));

        if (empty($payerId) || empty($token)) {
            $result = ['state' => 'User Cancelled'];
            $updateData = [
                'more_info' =>  json_encode($result),
                'payment_status' => "User Cancelled",
                'status'=> 0
            ];
            $ModelPayment->fill($updateData)->save();
            return redirect(url('/souvenir-registration/'))->withErrors('Payment failed');
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        // dd($result);exit;
        $donation = SponsorCategoryType::find($request->session()->get('sponsor_category'));
        // print_r($donation_id);
        Session::forget('paypal_payment_id');

        //echo('<pre>');print($p_order);exit;

        if ($result->getState() == 'approved' && $ModelPayment) {

            $transactions = $result->getTransactions();
            $transaction = $transactions[0];
            $relatedResources = $transaction->getRelatedResources();
            $relatedResource = $relatedResources[0];
            $order = $relatedResource->getSale();

            $updateData = [
                'unique_id_for_payment' => $order->getId(),
                'more_info' =>  $result->toarray(),
                'payment_status' => "Paid",
                'status' => 1
            ];
            $ModelPayment->fill($updateData)->save();
            $paymenttype= Paymenttype::find(1);
            Mail::to($exhibitUser->email)->send(new SouvenirRegistrationNotification($Registration));
            $user = \Auth::user();
            return redirect(url('myaccount'))->with('message-suc', "souvenir registered successfully"); 
        } else {
            $updateData = [
                'more_info' => $result->toarray(),
                'status' => 0
            ];

            $donation->fill($updateData)->save();
            return redirect(url('/souvenir-registration/'))->withErrors("Payment failed");
        }

    }

    
    

  
}
