<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\PaymentController;
use App\Models\Otp_model;
use App\Models\User;
use App\Models\YouthBanquet_Model;
use App\Mail\YouthBanquetRegistrationNotification;
use App\Mail\YouthBanquetconfirmationMail;
use Mail;
use DB;
use Session;
use Config;

class YouthBanquetRegistrationController extends Controller
{
    
   
    
    public function showyouth_banquetRegistration ()
    {         
        return view('registration.youthBanquetRegistration');
    }

   

    public function Post_youth_banquetRegistration(Request $request)
    {
        $request->validate([
            'FirstName.*' => 'required|string',
            'LastName.*' => 'required|string',
            'age.*' => 'required|string',
            'phone.*' => 'required|string|max:255',
            'convention_id' => 'required|string|max:255|exists:convention_users,registration_id',
            'whatsapp' => 'required|string|max:255',
            'parent_phone' => 'required|string|max:255',
            'parent_email' => 'required|string|max:255',
            'email.*' => 'required|string|email|max:255',
        ]);

        $user=User::where('registration_id',$request->convention_id)->first();
        if($user->sponsorship_category_id==0){
            return redirect('youth_banquet_registration')->with('error', "Your convention registration is not Eligible for Youth Banquet Registration");
        }
        
     
       $user = New YouthBanquet_Model();
        $user->FirstName = implode("||",$request->FirstName) ;
        $user->LastName = implode("||",$request->LastName);
        $user->age = implode("||",$request->age);
        $user->phone = implode("||",$request->phone);
        $user->convention_id = $request->convention_id??'';
        $user->whatsapp = $request->whatsapp??'';
        $user->parent_phone = $request->parent_phone??'';
        $user->parent_email = $request->parent_email??'';
        $user->email =implode("||",$request->email); 
        $user->save();

        Mail::to($request->parent_email)->send(new YouthBanquetRegistrationNotification($user->id));
        //Auth::login($user);
        return redirect('youth_banquet_registration')->with('success','Your Registration completed successfully.');
    }


    public function SendYouthBanquetMail(Request $request)
    {
        
        $user = YouthBanquet_Model::where('email', $request->email)->first();
        /* if(!($user && $Payment )){
            return redirect('admin/registrations')->withErrors("Some thing went Wrong");
        } */
       
         Mail::to($request->email)
       
        ->send(new YouthBanquetconfirmationMail($user)); 
      
        return redirect('admin/youth-banquet-registrations')->with('message-suc', "Email Sent Successfully");
    } 


    public function showyouth_banquet_consent(Request $request)
    {     
        if($request && $request->has('parent_email')){
            $user = YouthBanquet_Model::where('parent_email', $request->parent_email)->first();  
            if($user && $user->is_consent_accepted==1){
                return redirect('youth-banquet-consent')->with('message-suc','consent already taken');
            }
            if(!$user){
                $error = \Illuminate\Validation\ValidationException::withMessages([
                'parent_email' => ['Parent mail does not exist please check'],
                ]);
                throw $error;
            }
            $user = YouthBanquet_Model::where('parent_email', $request->parent_email)->first();  
            return view('registration.youthBanquetacknowledgement',compact('user'));
        }
        return view('registration.youthBanquetacknowledgement');
    }


    public function storeyouth_banquet_consent(Request $request)
    {    
        $user = YouthBanquet_Model::where('parent_email', $request->parent_email)->first();  
        
        if(!$user && $request->agrement=='on'){
            $error = \Illuminate\Validation\ValidationException::withMessages([
            'parent_email' => ['Parent mail does not exist please check'],
            ]);
            throw $error;
        }
       
        $user->update([
            'is_consent_accepted'=>1,
        ]);
       
       return redirect('youth-banquet-consent')->with('message-suc','Consent Updated');
    }


    


}
