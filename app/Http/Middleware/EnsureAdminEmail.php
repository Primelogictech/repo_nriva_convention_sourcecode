<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use App\Models\UserPermission;
use App\Models\MenuItem;
use URL;
class EnsureAdminEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
         //dd(request()->segment(2));
        if(\Auth::user()){
            $admin_emails = Config('conventions.admin_emails');
            $menu_permission_record = UserPermission::where('member_id', Auth::user()->member_id)
                                        ->first();

            
            //dd($request->path());
            
            if(!in_array(Auth::user()->email,$admin_emails) && !$menu_permission_record){
                //dd('asdf');
                return redirect('/');
            }
            if($menu_permission_record){
                $main_url_temp=explode('/',$request->path());
                $main_url='';
                if(count($main_url_temp)>2){
                    $main_url=$main_url_temp[0].'/'.$main_url_temp[1];
                }
                $menuItems = MenuItem::whereIn('id', explode(',',$menu_permission_record->menu_id))->where('slug',$request->path())->orWhere('redirect_urls', 'like', '%' .$main_url. '%')->get();
                //$allow_paths = array('admin/payment-history');
               

                if(!in_array(Auth::user()->email,$admin_emails) && $menuItems->isEmpty() && $request->path()!='admin'&& request()->segment(2) !="batchusers" && request()->segment(2) !="matrimony_profile_downloade" && request()->segment(2) !="createbatch"){
                    return redirect('/admin')->withErrors('You have page access restrictions!');
                }
            }
            
        }
        return $next($request);
    }
}
