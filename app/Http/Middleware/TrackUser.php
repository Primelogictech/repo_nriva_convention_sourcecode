<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\TrackUser as TrackUserModel;

class TrackUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $TrackUserModel=TrackUserModel::where('ip',request()->ip())->first();
        if(!$TrackUserModel){
            TrackUserModel::create([
                'ip'=>request()->ip()
            ]);
        }

        return $next($request);
    }
}
