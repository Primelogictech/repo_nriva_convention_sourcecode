<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommitteStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'slug' => ['required', 'string'],
            'committe_email' => ['email'],
            'display_order' => ['integer', 'nullable'],
            'image' => ['required','image', 'mimes:jpg,bmp,png,jpeg', 'max:1024', 'dimensions:ratio=1/1'],
            'banner_image' => ['required','image', 'mimes:jpg,bmp,png,jpeg'],
            'is_program'=>['integer'],
            'registration_type'=>['integer']
        ];
    }
}
