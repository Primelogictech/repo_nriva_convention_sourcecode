<?php

function test()
{
    return "helper";
}


function UploadFile($path,$image, $file_name)
{
    $image->storeAs($path, $file_name);
}


function hideContent($content)
{
    $str="";
    $content=explode("@", $content);

    !isset($content[1]) ? $content[1]="" : "";
    if(strlen($content[0] < 3 )){
        $content[0] = "***" ;
    }

    for ($i=0; $i < strlen($content[0])-3 ; $i++) {
        $str=$str.'*';
    }
    return substr($content[0], 0, 3) . $str . $content[1];
}


function paymentDate($Payment_details)
{
    if(!empty($Payment_details->paymentmethord)){
    if($Payment_details->paymentmethord->name == config('conventions.paypal_name_db')){
        return $Payment_details->created_at;
    }

    if($Payment_details->paymentmethord->name == config('conventions.other_name_db')){
        if($Payment_details->more_info['company_name'] && $Payment_details->more_info['company_name']=='Offline Registration'){
            return $Payment_details->created_at;
        }
        return $Payment_details->more_info['transaction_date'];
    }

    if($Payment_details->paymentmethord->name == config('conventions.check_name_db')){
        return $Payment_details->more_info['cheque_date'];
    }

    if($Payment_details->paymentmethord->name == config('conventions.zelle_name_db')){
        return $Payment_details->created_at;
    }
    }else{
        return $Payment_details->created_at??'';
    }


}


function canUpdatePaymentStatus($User)
{
   $a= App\Models\PermitionsType::where('name','can_update_payments')->first();
   if($a){
       $b= App\Models\UserPermission::where('member_id',$User->member_id)->first();
      if($b && in_array($a->id, explode(',',$b->permission_type_id) )){
          return true;
        }
   }
   return false;
}


function paymentStatus()
{
    return [
        'Pending',
        'Inprocess',
        'Paid'
    ];
}


function checkForValadateDateFormat($date, $format = 'm-d-Y')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}

function removeBraces($string)
{
    return str_replace('"]','', str_replace('["','',$string));
}