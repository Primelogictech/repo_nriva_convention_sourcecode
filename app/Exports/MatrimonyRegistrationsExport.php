<?php

namespace App\Exports;

use App\Models\Matrimony_model;
use App\Models\Registration;
use App\Models\Batches_Model;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class MatrimonyRegistrationsExport implements FromView
{
    use Exportable;

     public function __construct($name)
    {
        $this->name=$name;

    }

    public function view(): View
    {
       $batchname = $this->name;
         $ids=Batches_Model::where('batch_name',$this->name)->select("matrimony_id")->orderBy('order_id',"ASC")->get()->toArray();
         
         if($ids){

            $registrations=Matrimony_model::join('users','convention_matrimony_registrations.email','=','users.email')
            ->select('convention_matrimony_registrations.*','convention_batches.order_id as sequence','users.*','members.dob','members.*','eedujodu.*')
            ->whereIn('convention_matrimony_registrations.id',$ids)
            ->join('members','users.id','=','members.user_id')
            ->join('eedujodu','users.id','=','eedujodu.user_id')
            ->leftjoin('eedujodusignificants','eedujodu.id','=','eedujodusignificants.eedujodu_id')
            ->join('convention_batches',function($join) use ($batchname) {
            $join->on('convention_matrimony_registrations.id','=','convention_batches.matrimony_id');
          $join->on('convention_batches.batch_name','=',DB::raw("'".$batchname."'"));
        })->orderBy('convention_batches.order_id','ASC')->get();
            //dd($registrations);

         }else{
            $registrations=Matrimony_model::join('users','convention_matrimony_registrations.email','=','users.email') 
                ->join('members','users.id','=','members.user_id')
                ->join('eedujodu','users.id','=','eedujodu.user_id')
                ->leftjoin('eedujodusignificants','eedujodu.id','=','eedujodusignificants.eedujodu_id')
            ->orderBy('convention_matrimony_registrations.created_at','desc')->get();
         }
     
        
        return view('admin.exports.matrimonyRegistration', [
            'registrations' => $registrations
        ]);
    }
}
