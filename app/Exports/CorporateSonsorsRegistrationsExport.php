<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\CorporateSponsorReg;

class CorporateSonsorsRegistrationsExport implements FromView
{
    use Exportable;
   
    public function view(): View
    {
        $registrations=CorporateSponsorReg::orderBy('created_at', 'desc')->get();
        return view('admin.exports.corporateSonsorsRegistration', [
            'registrations' => $registrations
        ]);
    }
}
