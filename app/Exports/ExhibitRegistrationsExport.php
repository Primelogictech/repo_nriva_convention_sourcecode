<?php

namespace App\Exports;
use App\Models\Registration;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class ExhibitRegistrationsExport implements FromView
{
    use Exportable;
   

    public function view(): View
    {
        $users=Registration::where('registration_type_name','Exhibit')->with('paymentdata')->get();
        return view('admin.exports.exhibitRegistration', [
            'users' => $users
        ]);
    }
}

