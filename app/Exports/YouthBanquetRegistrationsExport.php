<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\YouthBanquet_Model;

class YouthBanquetRegistrationsExport implements FromView
{
    use Exportable;

    public function view(): View
    {
        $registrations=YouthBanquet_Model::orderBy('created_at', 'desc')->get();
        return view('admin.exports.youthBanquetRegistration', [
            'registrations' => $registrations
        ]);
    }
}
