<?php

namespace App\Models;

use App\Models\Admin\Benefittype;
use App\Models\Admin\Donortype;
use App\Models\Admin\BannerImages;
use App\Models\Admin\SponsorCategory;
use App\Models\Admin\SponsorCategoryType;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = "convention_users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'registration_id',
        'email',
        'password',
        'first_name',
        'last_name',
        'spouse_first_name',
        'spouse_last_name',
        'mobile',
        'country',
        'state',
        'city',
        'zip_code',
        'address',
        'address2',
        'image_url',
        'payment_methord',
        'total_amount',
        'amount_paid',
        'registration_note',
        'registration_type_id',
        'donor_amount',
        'registration_amount',
        'login_otp',
        'title',
        'free_exbhit_registrations',
        'vendor_booth_space_count',
        'kids_details',
        'chapter',
        'is_imported_record',
        'eedu_jodu_reg_count',
        'sponsorship_category_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'individual_registration' => 'array',
        'kids_details' => 'array',
        'amount_paid' => 'integer',
        'total_amount' => 'integer',
    ];

    public function registrationtype()
    {
        return $this->belongsTo(SponsorCategoryType::class, 'registration_type_id', 'id' );
    }

    public function categorytype()
    {
        return $this->belongsTo(Donortype::class, 'sponsorship_category_id', 'id');
    }

    public function categorydetails()
    {
        return $this->belongsTo(SponsorCategory::class, 'sponsorship_category_id', 'id');
    }

    public function bannerimages()
    {
        return $this->HasMany(BannerImages::class);
    }

    public function benfits()
    {
        return $this->belongsToMany(Benefittype::class, 'convention_benefit_user', 'user_id', 'benfit_id')->withPivot('content');
    }
/* 
    public function categorytype()
    {
        return $this->belongsTo(Donortype::class, 'sponsorship_category_id', 'id');
    } */

}
