<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{
    use HasFactory;
    protected $table = "convention_user_permissions";
    protected $fillable = [
        'member_id',
        'menu_id'
    ];
}
