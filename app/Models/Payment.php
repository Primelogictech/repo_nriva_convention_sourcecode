<?php

namespace App\Models;

use App\Models\Admin\Paymenttype;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    protected $table = "convention_payments";
    protected $fillable = [
        'user_id',
        'payment_amount',
        'payment_made_towards',
        'status',
        'payment_methord',
        'unique_id_for_payment',
        'payment_status',
        'account_status',
        'more_info',
        'package_details',
        'discount_amount',
        'discount_code',
        'paid_amount',
    ];

    protected $casts = [
        'more_info' => 'array',
        'payment_amount' => 'integer',
        'package_details' => 'array',
    ];

    public function paymentmethord()
    {
        return $this->belongsTo(Paymenttype::class, 'payment_methord');
    }

}
