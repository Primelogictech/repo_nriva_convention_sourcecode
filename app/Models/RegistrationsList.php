<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class RegistrationsList extends Model
{
    use HasFactory;
    protected $table = "convention_registration_list";
    protected $fillable = ['convention_id','first_name', 'last_name', 'email', 'mobile', 'chapter', 'package_name', 'registration_date', 'hotel_rooms', 'check_in', 'comments', 'created_at', 'updated_at','roomnum', 'assignee', 'attending_convention', 'banquet_tickets_count', 'main_event_tickets_count', 'youth_banquet_count', 'vip_banquet_tickets_count', 'vip_admissions_count', 'vvip_banquet_count', 'vvip_admissions_count', 'hotel_count','hotel_name'
    ];

}
