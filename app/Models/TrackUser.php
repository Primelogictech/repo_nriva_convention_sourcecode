<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackUser extends Model
{
    use HasFactory;
    protected $table = 'convention_track_users';
    protected $fillable = [
        'ip',
    ];
}
