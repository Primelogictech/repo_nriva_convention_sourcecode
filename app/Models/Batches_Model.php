<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Batches_Model extends Model
{
	 use HasFactory;
    protected $table = "convention_batches";
    protected $fillable = [
             'batch_name', 'matrimony_id', 'order_id', 'status', 'created_at', 'updated_at'
    ];

  
   
}