<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    use HasFactory;
    protected $table = "convention_menu_items";
    protected $fillable = [
        'menu_name',
        'slug',
        'parent_id',
        'is_main_menu'
    ];
}
