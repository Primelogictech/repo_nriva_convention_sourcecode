<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SouvenirSponsor extends Model
{
	 use HasFactory;
    protected $table = "convention_souvenir_sponsor";
    protected $fillable = ['title', 'first_name', 'last_name', 'email', 'phone', 'about', 'status', 'created_at', 'updated_at','user_id','dob','chapter','city'
    ];
   
}