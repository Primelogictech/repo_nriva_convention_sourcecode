<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\SouvenirSponsorTypes;

class SouvenirRegPkgDetail extends Model
{
    use HasFactory;
    protected $table = "convention_souvenir_reg_pkg_details";
    
    protected $fillable = [
       'souvenir_sponsor_type_id',
       'user_id',
       'registration_id',
       'count',
       'total_amount',
    ];

    public function SouvenirSponsorType()
    {
        return $this->HasOne(SouvenirSponsorTypes::class, 'id', 'souvenir_sponsor_type_id');
    }


 
    


}
