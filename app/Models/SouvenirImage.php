<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SouvenirImage extends Model
{
    use HasFactory;
    protected $table = "convention_souvenir_images";
    
    protected $fillable = [
       'souvenir_sponsor_type_id',
       'user_id',
       'registration_id',
       'image_url',
    ];

 
    


}
