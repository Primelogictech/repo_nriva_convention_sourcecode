<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class YouthBanquet_Model extends Model
{
	 use HasFactory;
    protected $table = "convention_youth_banquet";
    protected $fillable = ['FirstName', 'LastName', 'age', 'convention_id', 'phone', 'email', 'whatsapp', 'parent_phone', 'parent_email', 'status', 'created_at', 'updated_at','is_consent_accepted','parent_name','Kids_info_in_consent_form'
    ];
}