<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mr_Miss_Model extends Model
{
	 use HasFactory;
    protected $table = "convention_mr_miss_registrations";
    protected $fillable = ['title', 'first_name', 'last_name', 'email', 'phone', 'about', 'status', 'created_at', 'updated_at','user_id','dob','chapter','city'
    ];
   
}