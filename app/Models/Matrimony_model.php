<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matrimony_model extends Model
{
	 use HasFactory;
    protected $table = "convention_matrimony_registrations";
    protected $fillable = [
            'profile_id',
            'nriva_user_id',
            'email',
            'participate_in',
            'note',
            'no_of_people_attending',
            'convention_id',
            'created_at',
            'updated_at'
    ];

    protected $casts = [
        'participate_in' => 'array',
    ];
   
}