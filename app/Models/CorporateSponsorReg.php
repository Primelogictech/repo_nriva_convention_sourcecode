<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CorporateSponsorReg extends Model
{
	 use HasFactory;
    protected $table = "convention_corporate_sponsor_registrations";
    protected $fillable = [
        'user_id',
        'corporate_sponsor_type_id',
        'registration_id',
        'full_name',
        'video_link',
        'company_name',
        'category',
        'tax_ID',
        'mailing_address',
        'phone_no',
        'email',
        'fax',
        'website_url',
        'extra_discriptioon',
        'payment_id',
        'logo_url',
        'image_url',
    ];

    protected $casts = [
        'participate_in' => 'array',
    ];

    public function paymentdata()
    {
        return $this->HasOne(Payment::class, 'id', 'payment_id');
    }

    public function CorporateSponsortype()
    {
        return $this->HasOne(Admin\CorporateSponsortype::class,'id','corporate_sponsor_type_id');
    }
   
}