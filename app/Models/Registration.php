<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\ExhibitorType;
use App\Models\SouvenirRegPkgDetail;
use App\Models\Payment;

class Registration extends Model
{
    use HasFactory;
    protected $table = "convention_registrations";
    protected $fillable = [
        'user_id',
        'registration_id',
        'registration_type_name',
        'full_name',
        'company_name',
        'category',
        'tax_ID',
        'mailing_address',
        'phone_no',
        'email',
        'fax',
        'website_url',
        'booth_type',
        'extra_data',
        'payment_id'
    ];

    protected $casts = [
        'extra_data' => 'array',
    ];

    public function exhibitorTypes()
    {
        return $this->HasOne(ExhibitorType::class, 'id', 'booth_type');
    }

    public function paymentdata()
    {
        return $this->HasOne(Payment::class, 'id', 'payment_id');
    }


    public function souvenirDetails()
    {
        return $this->HasMany(SouvenirRegPkgDetail::class, 'registration_id', 'id');
    }

    public function souvenirImages()
    {
        return $this->HasMany(SouvenirImage::class, 'registration_id', 'id');
    }


}
