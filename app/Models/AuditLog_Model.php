<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuditLog_Model extends Model
{
	 use HasFactory;
    protected $table = "convention_audit_log";
    protected $fillable = ['registration_id', 'old_status', 'new_status', 'reg_type', 'more_info', 'statusChanged_by', 'created_at', 'updated_at','convention_id','user_name','user_email'
    ];
   
}