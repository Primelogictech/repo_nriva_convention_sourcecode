<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    use HasFactory;
    
    protected $table = "convention_designations";
    
    protected $fillable = ['name', 'status','display_order'];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

}
