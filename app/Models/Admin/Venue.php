<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    use HasFactory;
    protected $table = "convention_venues";
    protected $fillable = ['Event_date_time', 'end_date', 'location'];
    protected $casts = [
        'Event_date_time' => 'datetime',
        'end_date' => 'datetime',
    ];
}
