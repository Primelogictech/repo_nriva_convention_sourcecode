<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class Benefittype extends Model
{
    use HasFactory;

    protected $table = "convention_benefit_types";

    protected $fillable = [
        'name',
        'status',
        'has_count',
        'has_input',
        'count',
        'has_image'
    ];
    protected $casts = [
        'count' => 'integer',
    ];

    public function donors()
    {
        return $this->belongsToMany(SponsorCategory::class, 'convention_benfit_sponsor_category')
        ->withPivot('count','display_order')
        ->orderBy(DB::raw('ISNULL(display_order), display_order'));
    }
}
