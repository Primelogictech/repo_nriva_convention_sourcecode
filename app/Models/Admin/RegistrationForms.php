<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegistrationForms extends Model
{
    use HasFactory;

    protected $table = "convention_registration_forms";

    protected $fillable = ['name', 'status'];

}
