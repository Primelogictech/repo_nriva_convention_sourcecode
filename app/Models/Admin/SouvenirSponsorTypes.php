<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SouvenirSponsorTypes extends Model
{
    use HasFactory;

    protected $table = "convention_souvenir_sponsor_types";

    protected $fillable = [
        'name',
        'amount',
        'display_order',
        'status',
    ];

}
