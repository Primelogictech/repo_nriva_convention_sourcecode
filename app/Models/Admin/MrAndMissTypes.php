<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MrAndMissTypes extends Model
{
    use HasFactory;

    protected $table = "convention_mr_miss_age";

    protected $fillable = ['title', 'name', 'min_age', 'max_age', 'status', 'created_at', 'updated_at'];

}
