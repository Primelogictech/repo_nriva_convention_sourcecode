<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\RegistrationForms;
use App\Models\Admin\Program;

class Committe extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "convention_committes";
    
    protected $fillable = [
        'name',
        'image_url',
        'slug',
        'display_order',
        'committe_email',
        'registration_type',
        'banner_image',
        'is_program'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'display_order' => 'integer',
    ];
    public function registrationForms()
    {
        return $this->HasOne(RegistrationForms::class, 'id', 'registration_type');
    }
    public function programList()
    {
        return $this->HasOne(Program::class, 'committee_id', 'id');
    }
}
