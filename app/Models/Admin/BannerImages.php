<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerImages extends Model
{
    use HasFactory;


        protected $table = "convention_banner_images";

     protected $fillable = [
         'user_id',
         'benfit_id',
         'image_url',
    ];
    protected $casts = [
        'count' => 'integer',
    ]; 

    public function benfit()
    {
        return $this->belongsTo(\App\Models\Admin\Benefittype::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

}
