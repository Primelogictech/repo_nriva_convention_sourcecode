<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    use HasFactory;
   
    protected $table = "convention_menus";

    protected $fillable = [
        'name',
        'slug',
        'parent_id',
        'page_content',
        'display_order'
    ];
    protected $casts = [
        'id' => 'integer',
        'display_order' => 'integer',
    ];
    public function submenus()
    {
        return $this->HasMany(Menu::class, 'parent_id');
    }

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }
}
