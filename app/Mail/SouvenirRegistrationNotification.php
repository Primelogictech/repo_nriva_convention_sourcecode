<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Payment;
use App\Models\Admin\Paymenttype;
use App\Models\SouvenirRegPkgDetail;

class SouvenirRegistrationNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $Paymenttype;
    public $Packages;
    public $SouvenirRegPkgDetail;
    public function __construct($user)
    {
        $this->user=$user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $payment=Payment::where('user_id',$this->user->user_id)->latest('created_at')->first();
        $this->Paymenttype =Paymenttype::where('id',$payment->payment_methord)->latest('created_at')->first();
       $this->SouvenirRegPkgDetail =SouvenirRegPkgDetail::where('registration_id',$this->user->id)->get();
        return $this->view('mails.NewSouvenirRegistration')->with([
            'user' => $this->user,
            'Paymenttype' => $this->Paymenttype ,
            'SouvenirRegPkgDetail' => $this->SouvenirRegPkgDetail,
            'payment' => $payment,
        ]);
    }
}
