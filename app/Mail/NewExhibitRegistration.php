<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Reservation;
use App\Models\Payment;

class NewExhibitRegistration extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;
    public $Paymenttype;
    public $Packages;

    public function __construct($user,$Paymenttype,$Packages)
    {
        $this->user=$user;
        $this->Paymenttype=$Paymenttype;
        $this->Packages=$Packages;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $payment=Payment::where('user_id',$this->user->id)->latest('created_at')->first();
        return $this->view('mails.NewExhibitRegistration')->with([
            'user' => $this->user,
            'Paymenttype' => $this->Paymenttype,
            'Packages' => $this->Packages,
            'payment' => $this->Packages,
        ]);
    }
}
