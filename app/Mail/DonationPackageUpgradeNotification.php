<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Payment;

class DonationPackageUpgradeNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$PaymentType)
    {
        $this->user=$user;
        $this->PaymentType=$PaymentType;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $payment=Payment::where('user_id',$this->user->id)->latest('created_at')->first();
        return $this->view('mails.DonationPackageUpgrade') 
        ->subject('Registration Upgrade Notification')
        ->with([
            'user' => $this->user,
            'paymentType' => $this->PaymentType,
            'payment' => $payment,
        ]);
    }
}
