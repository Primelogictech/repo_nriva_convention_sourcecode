<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Payment;

class NewConventionRegistration extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $PaymentType;
    public $payment;

    public function __construct($user,$PaymentType,$payment=null)
    {
        $this->user=$user;
        $this->PaymentType=$PaymentType;
        $this->payment=$payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(!$this->payment){
            $this->payment=Payment::where('user_id',$this->user->id)->latest('created_at')->first();
        }
        return $this->view('mails.NewRegistration') 
        ->with([
            'user' => $this->user,
            'PaymentType' => $this->PaymentType,
            'payment' => $this->payment,
        ]);
    }
}
