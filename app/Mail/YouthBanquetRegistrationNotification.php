<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\YouthBanquet_Model;

class YouthBanquetRegistrationNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $PaymentType;

    public function __construct($id)
    {
        $this->id=$id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $userdetails=YouthBanquet_Model::where('id',$this->id)->first();

        return $this->view('mails.YouthBanquetRegistration')->with(compact('userdetails'))
        ->with([
            'userdetails' => $userdetails
        ]);
    }
}
