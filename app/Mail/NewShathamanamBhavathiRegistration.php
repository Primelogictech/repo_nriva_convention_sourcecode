<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Registration;

class NewShathamanamBhavathiRegistration extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public function __construct($user)
    {
        $this->user=$user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $shathamanamBhavathiRegistrations=Registration::where('user_id',$this->user->id)->latest()->get();
        return $this->view('mails.NewShathamanamBhavathiRegistration')->subject('New Shathamanam Bhavathi Registration') 
        ->with(['user' => $this->user,'shathamanamBhavathiRegistrations'=>$shathamanamBhavathiRegistrations ]);
    }
}
