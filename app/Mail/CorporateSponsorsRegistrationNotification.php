<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Payment;
use App\Models\Admin\Paymenttype;
use App\Models\Admin\CorporateSponsortype;

class CorporateSponsorsRegistrationNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $Paymenttype;
    public $Packages;
    public $SouvenirRegPkgDetail;
    public function __construct($user)
    {
        $this->user=$user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $payment=Payment::where('id',$this->user->payment_id)->latest('created_at')->first();
        $this->Paymenttype = Paymenttype::where('id',$payment->payment_methord)->latest('created_at')->first();
       $this->CorporateSponsortype =CorporateSponsortype::where('id',$this->user->corporate_sponsor_type_id)->first();
       return $this->view('mails.NewCorporateSponsorRegistration')->with([
       'user' => $this->user,
            'Paymenttype' => $this->Paymenttype ,
            'CorporateSponsortype' => $this->CorporateSponsortype,
            'payment' => $payment,
        ]);
    }
}
