<?php
return array(
    // set your paypal credential

    'client_id' => config('conventions.PAYPAL_SANDBOX_CLIENT_ID'),
    'secret' => config('conventions.PAYPAL_SANDBOX_CLIENT_SECRET'),

/* 
    'client_id' => env('PAYPAL_SANDBOX_CLIENT_ID'),
    'secret' => env('PAYPAL_SANDBOX_CLIENT_SECRET'),
     */
    
    //'client_id' => 'AXjfbCWt8ArDoqpDUVU1zCZ90PnbuBOZqoPRxac3Niau0B-eNjy83f1agRQK_3YEkSfoDgUOJ8Cfu0oX',
    //'secret' => 'EMhmnXei8d8v5_3PFn8dBa1YyZq6jDfKg0dTxO0X0jstcbcsFA9lucsNa34gzWzAVs_rHFhTcfiRsKRJ',
    
    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);
