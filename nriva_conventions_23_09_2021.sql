-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 23, 2021 at 02:23 PM
-- Server version: 8.0.26-0ubuntu0.20.04.2
-- PHP Version: 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nriva_conventions`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint UNSIGNED NOT NULL,
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image_url`, `status`, `created_at`, `updated_at`) VALUES
(1, 'banner_1.jpg', 0, '2021-08-31 03:48:22', '2021-09-11 09:44:55'),
(2, 'banner_2.jpg', 1, '2021-08-31 03:48:22', '2021-09-03 20:04:35'),
(3, 'banner_3.jpg', 1, '2021-08-31 03:48:22', '2021-08-31 16:48:01'),
(7, 'banner_7.jpg', 1, '2021-09-06 16:52:08', '2021-09-07 05:28:42'),
(8, 'null', 1, '2021-09-06 16:52:17', '2021-09-06 16:52:17'),
(9, 'null', 1, '2021-09-06 16:52:28', '2021-09-06 16:52:28'),
(11, 'banner_11.jpg', 1, '2021-09-13 10:26:54', '2021-09-13 10:26:54');

-- --------------------------------------------------------

--
-- Table structure for table `benefit_types`
--

CREATE TABLE `benefit_types` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `has_count` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'has count->1 no count->0',
  `has_input` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'has input->1 no input->0',
  `count` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benefit_types`
--

INSERT INTO `benefit_types` (`id`, `name`, `has_count`, `has_input`, `count`, `status`, `created_at`, `updated_at`) VALUES
(4, 'Marcus Gerlach', 0, 0, NULL, 1, '2021-09-19 23:03:56', '2021-09-19 23:03:56'),
(5, 'Doris Jast', 0, 0, NULL, 1, '2021-09-19 23:03:56', '2021-09-19 23:03:56'),
(9, 'ben space', 0, 1, '0', 1, '2021-09-19 23:03:56', '2021-09-19 23:05:54'),
(33, 'Halle Goodwin', 0, 1, '0', 1, '2021-09-19 23:03:56', '2021-09-20 01:41:32'),
(99, 'Mrs. Clarissa McClure', 0, 0, NULL, 1, '2021-09-19 23:03:56', '2021-09-19 23:03:56');

-- --------------------------------------------------------

--
-- Table structure for table `benefit_user`
--

CREATE TABLE `benefit_user` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `benfit_id` bigint UNSIGNED NOT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benefit_user`
--

INSERT INTO `benefit_user` (`id`, `user_id`, `benfit_id`, `content`, `created_at`, `updated_at`) VALUES
(4, 3, 1, '1,2,3', NULL, NULL),
(5, 3, 2, '3,4,5', NULL, NULL),
(6, 5, 1, '1,3', NULL, NULL),
(7, 5, 2, '100,233', NULL, NULL),
(8, 3, 9, '1,2,3', NULL, NULL),
(9, 5, 33, '23,56', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `benfit_sponsor_category`
--

CREATE TABLE `benfit_sponsor_category` (
  `id` bigint UNSIGNED NOT NULL,
  `sponsor_category_id` bigint UNSIGNED NOT NULL,
  `benfit_id` bigint UNSIGNED NOT NULL,
  `count` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benfit_sponsor_category`
--

INSERT INTO `benfit_sponsor_category` (`id`, `sponsor_category_id`, `benfit_id`, `count`, `created_at`, `updated_at`) VALUES
(23, 12, 1, 12, NULL, NULL),
(30, 4, 33, NULL, NULL, NULL),
(31, 8, 4, 1, NULL, NULL),
(32, 8, 5, 3, NULL, NULL),
(33, 8, 9, 6, NULL, NULL),
(34, 5, 4, NULL, NULL, NULL),
(35, 5, 5, 2, NULL, NULL),
(36, 5, 9, NULL, NULL, NULL),
(37, 5, 33, 3, NULL, NULL),
(38, 5, 99, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `committes`
--

CREATE TABLE `committes` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `committe_email` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_order` int DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `committes`
--

INSERT INTO `committes` (`id`, `name`, `image_url`, `committe_email`, `display_order`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Anika Harber111asdf', 'committe_3.jpg', NULL, NULL, 0, '2021-09-22 04:15:12', '2021-09-22 19:17:14'),
(4, 'Lance Keebler', 'vel', NULL, NULL, 1, '2021-09-22 04:15:12', '2021-09-22 04:15:12'),
(5, 'Mr. Albin Dibbert', 'vitae', NULL, NULL, 1, '2021-09-22 04:15:12', '2021-09-22 04:15:12'),
(6, 'asdfasdf', '', NULL, NULL, 0, '2021-09-22 04:32:16', '2021-09-22 04:36:24'),
(7, '1111', 'committe_7.png', NULL, NULL, 1, '2021-09-22 04:33:29', '2021-09-22 04:41:35');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Convenor', 0, '2021-08-31 03:48:22', '2021-09-02 05:03:57'),
(4, 'President', 1, '2021-09-02 05:05:29', '2021-09-02 05:05:29'),
(5, 'Chairman', 1, '2021-09-02 05:05:38', '2021-09-02 05:05:38'),
(6, 'Convenor', 1, '2021-09-02 05:06:02', '2021-09-02 05:06:02'),
(7, 'Co-Convenor', 1, '2021-09-02 05:06:13', '2021-09-02 05:06:13');

-- --------------------------------------------------------

--
-- Table structure for table `donors`
--

CREATE TABLE `donors` (
  `id` bigint UNSIGNED NOT NULL,
  `donortype_id` bigint UNSIGNED NOT NULL,
  `donor_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donors`
--

INSERT INTO `donors` (`id`, `donortype_id`, `donor_name`, `description`, `image_url`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'NPFGrmpotX7HvI2dxyGDQQi8LFIo13dfenisUVVqQGjJW98IDhP0gEtUV5dYEcInQeF4xSptouvBFqf38X1bA5BQesyatgwi1qBS', 'Inventore reiciendis et earum hic in. Et qui omnis non et. Voluptatem voluptates qui itaque tempora.', 'EeGwqNuBCpR0pfK7G1OqWvJKC96mG1QzJaIfdrHpOE6gbSJWjSnbD7Ui28W8p4Gjgh2CEo812teiA0miAPkukymvCaiCDuXgcYoW', 1, '2021-09-03 06:28:06', '2021-09-03 06:28:06'),
(2, 2, 'tXm9jHatk3tJieVi5NeKuzWAjEL34c6fE5nPmg8obywbgRGuRF4Q1maPNtzLyMoZ1J7QdcQE78TnKm4BM2o3V9CaHwZj6Gv9CS5T', 'Hic voluptatum eius aliquam natus eum id qui. Ipsa ipsa optio voluptas quibusdam quia.', 'T7mJ9stpygsaMYbGrtFwFdsbePGYZB1LjjoSbWD19XTa748YrbNvS7n94xnBnKuRsRZ2L0MHKfbo97ZbS3FL20MXoBBEijTfZijC', 1, '2021-09-03 06:28:06', '2021-09-03 06:28:06'),
(3, 3, 'gbQOaSHjYyKIODkLLEoJ9nie7O58XFCxEyiqvN1AVhq77gA75R4GXLStLf4LUAB6uExUOiEeMqzSSbUmCPnLcDP52YlhwlGdRIbK', 'Et consectetur rerum et. Iusto sint magni exercitationem quas temporibus laudantium ut in. Eos ipsum quisquam quibusdam qui natus maxime. Et eaque provident quo a voluptate vero consequatur.', 'Pi8NIwcjF0Xtdw0Y06duHie2kZbaSayzzmYjL4eplMvf2zwMeDGnSZdjWXRS7coCYianGlSrnE2lYIWtAXzVYxb0VdC59xwWwgye', 1, '2021-09-03 06:28:06', '2021-09-03 06:28:06'),
(4, 4, 'Ufrd0YXmsskyiMpd8gjvuONJ4Lfd6f3HVgPdopOcPwxQk3vSiFwWMAHDieGUme1QKwTZy9q9hXnCTp4hG3dazmJfLCA4ucERvYM6', 'Ducimus quae dolorum dolorem voluptates ab ipsa ducimus. Occaecati quos vero aut asperiores. Fugiat voluptas quo adipisci ut. Molestias molestiae eaque et eum odio.', '8S5Df5YxDmpREe4DDzr8FD6PgIWUHHO76UE5k0XWygvfxyqhSxvmEPOCpgAdRWtjNIzJTFIp4fbLBddXJaM7sbTu1q5MJ8ERVQ2C', 1, '2021-09-03 06:28:06', '2021-09-03 06:28:06'),
(5, 5, 'BGfqH5f2fmKkR278dslyBbrSKcYLF61aU5Q7ZHx3LfEiuH4KR1WW8J0aBGICj60jIXKTX5300SBaB7n24vBgK8rq2vzgaZk5yHnm', 'Aut perferendis quos expedita totam. Perspiciatis aliquam consectetur dignissimos. Pariatur sunt nam voluptatem sunt explicabo est.', 'MS3fCA5PZGjWY8rgrS9DPBETtJi7n2Je6T5AF2fMz2f7KxhGmxtkZiezKJyywMH7xloakVbTsEiauAZ0BxZVyyduyb9fHux44WH8', 1, '2021-09-03 06:28:06', '2021-09-03 06:28:06'),
(7, 1, 'doner abc', '<p>asdfasdf</p>', 'donor_7.jpg', 1, '2021-09-02 16:59:46', '2021-09-02 17:04:35'),
(8, 2, 'dff', '<p>dsfdsf</p>', 'donor_8.jpg', 1, '2021-09-03 05:02:15', '2021-09-03 05:02:15');

-- --------------------------------------------------------

--
-- Table structure for table `donortypes`
--

CREATE TABLE `donortypes` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `display_order` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donortypes`
--

INSERT INTO `donortypes` (`id`, `name`, `status`, `display_order`, `created_at`, `updated_at`) VALUES
(1, 'asasd', 1, NULL, '2021-08-31 03:48:22', '2021-09-09 05:19:03'),
(2, 'adfasdf', 1, NULL, '2021-08-31 03:48:22', '2021-09-09 05:19:08'),
(3, 'Kendra Ledner', 0, NULL, '2021-08-31 03:48:23', '2021-09-09 05:17:06'),
(4, 'test', 0, NULL, '2021-09-02 17:53:03', '2021-09-09 05:17:07');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` bigint UNSIGNED NOT NULL,
  `event_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0	',
  `image_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `from_date`, `to_date`, `description`, `status`, `image_url`, `created_at`, `updated_at`) VALUES
(1, 'sa', '2021-09-04 00:00:00', '2021-09-05 00:00:00', NULL, 1, 'event_1.jpg', '2021-09-02 06:30:57', '2021-09-07 10:03:40'),
(2, 'aaa', '1975-10-27 00:00:00', '2006-08-24 00:00:00', NULL, 0, 'event_2.jpg', '2021-09-01 14:34:07', '2021-09-07 10:02:58'),
(3, 'pSvkCwTRw6ZeG9EGr PHmiY9CV EhIdRSt1WG3  c eeHlUQvfbepIMS mwdbhwvQP Bt5ttlUUa E3HAiOaT F2hA2', '1994-06-11 00:00:00', '1985-03-01 00:00:00', NULL, 0, 'event_3.jpg', '2021-09-01 14:34:07', '2021-09-07 10:02:57'),
(5, 'ukVfB7w8jyEr8ZOlXeiZG4f8oKcZA88PAg1mKC3CNbUQmeKbrmWOgA2QlQMy5fzsRKaD2BuD0V4Od5V1KGGRNFq7forLhMUCWrmu', '1990-02-17 00:00:00', '1997-10-17 00:00:00', NULL, 0, 'event_5.jpg', '2021-09-01 14:34:07', '2021-09-07 10:02:57'),
(6, '2ikSdhckTO7lfRYrzNw3klvZeJMxjLa5Gg8p3z14XNufq43pP5DJz1Q62S3yh8WyAV0i29GnRqoC5PXnj8EMw9YOAXHGogMtBzbf', '1983-05-03 00:00:00', '1993-11-28 00:00:00', NULL, 0, 'event_6.jpg', '2021-09-01 14:34:07', '2021-09-07 10:02:56'),
(7, 'aEAVqtuFkL2Kf0a2CIAxu5or6StdQ78uhBO6hdYsN1fA7FUjJsdG2i9LSy4Hfw9RgZwMkg6v2HWXlTT8uMJmNyjMxG50mWU5jFCH', '2009-11-03 00:07:36', '2013-01-15 13:45:36', NULL, 0, 'OFM7tPFrss34HaZDElGcXc87YoGcd8xdQ0ecNaTYmh693kIGW4QnZ6OkxABngnPQbITbqst3f5WWQgO72g0awI79JqAflSXd56Yr', '2021-09-01 14:34:07', '2021-09-07 10:02:55'),
(8, 'IoBOkkaNXi9lbcGlmQXCHfJgKnTcaltQqlj46Tno6gWLDrmAk4psp7vmQNsvT2kvVFRlO1VK7Ov88QKPXJQHm2I9jGFmueZ91i4M', '1989-05-16 22:07:56', '2004-07-22 17:51:48', NULL, 0, 's7BZyNMznnZCyVBLsiVSfDE9jMQAZAsTQnM3vsZASPevInzTuwOmwNwmsG4SvvrSe6GO5oUXwzTMurb8QPpB2foTg9PEaaE1b6jW', '2021-09-01 14:34:07', '2021-09-07 10:02:55'),
(9, 'hGTrw3Mvl2D5qKd1UaLUI7sFVjm3KzgXqU2nY91RabDyhrfTAt7jEwXvut7UcK3Xy9rKCJCVJrJsyAL009HdEuRzzjUgEowlcDcD', '1984-07-25 07:51:51', '1976-04-06 03:57:43', NULL, 0, 'DdXtbVljVVSgutOAuUInLkN9Ji4KZ7nygFQO6utlFNq6zigRnDOIeM4ixau9yvcMAkCfZcg8en6BxxxR1BqYfIuWS96wb0qQSozy', '2021-09-01 14:34:07', '2021-09-07 10:02:55'),
(10, 'xltOGNLdCUO6T2W9UawCAzReZLKED3SngUjGSyourgYN25nnG8JASVoLMqultgyUssrNSfGTEgi0gJv5fxON0ze7usTc1jUiWLKG', '1977-07-31 00:00:00', '2001-06-05 00:00:00', NULL, 0, 'event_10.png', '2021-09-01 14:34:07', '2021-09-07 10:02:54'),
(11, 'dfadsf', '2021-09-08 00:00:00', '2021-09-14 00:00:00', NULL, 0, 'event_11.jpg', '2021-09-03 04:42:37', '2021-09-07 10:02:54'),
(12, 'asdfasdaf', '2021-09-02 00:00:00', '2021-09-03 00:00:00', NULL, 0, 'event_12.jpg', '2021-09-03 04:42:54', '2021-09-07 10:02:53'),
(13, 'asdfads', '2021-09-06 00:00:00', '2021-09-09 00:00:00', NULL, 1, 'event_13.jpg', '2021-09-03 05:22:00', '2021-09-07 10:02:20');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invitees`
--

CREATE TABLE `invitees` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_order` int DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invitees`
--

INSERT INTO `invitees` (`id`, `name`, `display_order`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mrs. Felicita Murray', NULL, 0, '2021-08-31 03:48:21', '2021-08-31 03:48:21'),
(2, 'Pamela Jones', NULL, 0, '2021-08-31 03:48:21', '2021-08-31 03:48:21'),
(3, 'Vincenza Tromp', NULL, 1, '2021-08-31 03:48:21', '2021-08-31 03:48:21');

-- --------------------------------------------------------

--
-- Table structure for table `leadership_types`
--

CREATE TABLE `leadership_types` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_order` int DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leadership_types`
--

INSERT INTO `leadership_types` (`id`, `name`, `display_order`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Maryam Kutch', NULL, 1, '2021-08-31 03:48:21', '2021-09-06 04:44:11'),
(2, 'Dr. Gregoria Johns', NULL, 1, '2021-08-31 03:48:22', '2021-09-06 04:44:11'),
(3, 'Ms. Carlie Schmeler II', NULL, 1, '2021-08-31 03:48:22', '2021-09-06 04:44:12'),
(4, 'Created By Arun', NULL, 1, '2021-09-15 07:51:19', '2021-09-15 07:51:19');

-- --------------------------------------------------------

--
-- Table structure for table `logos`
--

CREATE TABLE `logos` (
  `id` bigint UNSIGNED NOT NULL,
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'left',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logos`
--

INSERT INTO `logos` (`id`, `image_url`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'logo_0', 'left', 1, '2021-09-01 14:34:08', '2021-09-01 14:34:08'),
(2, 'logo_1', 'left', 1, '2021-09-01 14:34:08', '2021-09-01 14:34:08'),
(28, 'logo_28.png', 'right', 1, '2021-08-31 06:01:25', '2021-08-31 16:50:08'),
(30, 'logo_30.png', 'left', 1, '2021-08-31 06:09:18', '2021-08-31 16:49:17');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `email`, `mobile_number`, `image_url`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Immanuel Klocko', NULL, NULL, 'member_1.jpg', '<p>Quis rem natus quaerat ullam sapiente fugiat iusto. Incidunt pariatur sed consequatur. Qui eos dolorum voluptatibus id quis inventore.</p>', 1, '2021-09-03 06:28:05', '2021-09-16 07:24:33'),
(2, 'Ms. Annetta Kuhic I', NULL, NULL, 'member_2.jpg', '<p>Ab molestiae et sint dolores quisquam dolorem. Nostrum autem iste quia corrupti quasi. Quos vitae sunt nostrum illo in.</p>', 1, '2021-09-03 06:28:05', '2021-09-16 07:30:55'),
(3, 'Walton Hyatt', NULL, NULL, 'member_3.jpg', '<p>Earum consequatur et ex ipsum atque. Qui praesentium voluptatem illum exercitationem. Maxime sit nihil ratione velit laudantium cum.</p>', 1, '2021-09-03 06:28:05', '2021-09-16 07:32:22'),
(4, 'Dr. Alec Mraz III', NULL, NULL, NULL, 'Alias earum non tempora ab. Reiciendis dolorem voluptatem corrupti atque sapiente enim. Quia labore provident fuga quisquam consectetur. Aliquam eligendi quod placeat eos error vel.', 0, '2021-09-03 06:28:05', '2021-09-16 07:24:12'),
(5, 'Berniece Reinger', NULL, NULL, 'member_5.png', '<p>Suscipit consequuntur non omnis laboriosam possimus enim aut. Veritatis maiores temporibus fuga. Dolore et eligendi provident laborum.</p>', 0, '2021-09-03 06:28:05', '2021-09-16 07:24:12'),
(6, 'abc', NULL, NULL, 'member_6.jpg', '<p>adsf</p>', 0, '2021-09-04 08:47:38', '2021-09-16 07:24:13');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` bigint UNSIGNED NOT NULL DEFAULT '0',
  `page_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `display_order` int DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `slug`, `parent_id`, `page_content`, `display_order`, `status`, `created_at`, `updated_at`) VALUES
(3, 'TeamTeamsTeams', NULL, 0, NULL, NULL, 1, '2021-08-31 07:15:32', '2021-09-23 07:24:41'),
(9, 'Board Of Directors', 'board-of-directors', 3, '<p>welcome&nbsp;</p>', NULL, 0, '2021-08-31 16:58:08', '2021-09-15 07:33:06'),
(10, 'test', NULL, 0, NULL, NULL, 1, '2021-09-06 17:02:23', '2021-09-14 12:43:01'),
(11, 'Description', NULL, 0, NULL, NULL, 1, '2021-09-13 10:12:29', '2021-09-14 12:43:02'),
(12, 'About Us', 'about-us', 0, NULL, NULL, 1, '2021-09-13 10:14:11', '2021-09-14 12:43:03'),
(26, 'hgdgfdi', 'hgdgfdi', 10, NULL, NULL, 1, '2021-09-14 12:47:50', '2021-09-15 05:20:47'),
(27, 'gfhgfht', 'gfhgfht', 11, NULL, NULL, 1, '2021-09-14 12:48:15', '2021-09-14 12:48:25'),
(28, 'yutytryut', 'yutytryut', 10, NULL, NULL, 1, '2021-09-14 12:48:53', '2021-09-14 12:48:53'),
(29, 'chandana', NULL, 0, NULL, 1, 1, '2021-09-15 05:15:34', '2021-09-23 10:47:15'),
(30, 'Abc', 'abc', 29, '<p>Chandana Page</p>', NULL, 0, '2021-09-15 05:16:55', '2021-09-15 07:04:35'),
(31, 'chandana', 'chandana', 3, '<p>welcome to webpage</p>', NULL, 1, '2021-09-15 06:25:09', '2021-09-15 07:00:05'),
(32, 'vani', 'vani', 3, '<p>welcome vani</p>', NULL, 1, '2021-09-15 06:25:29', '2021-09-15 06:53:13'),
(33, 'committee convention', 'committee-convention', 3, '<p>welcome to committee convention</p>', NULL, 1, '2021-09-15 06:31:55', '2021-09-15 06:52:10'),
(34, 'vani', NULL, 0, NULL, 1, 1, '2021-09-23 07:38:13', '2021-09-23 10:41:37'),
(35, 'avc', NULL, 0, NULL, 2, 1, '2021-09-23 10:41:52', '2021-09-23 10:41:52'),
(36, 'obe', 'obe', 3, NULL, 11, 1, '2021-09-23 10:42:10', '2021-09-23 10:42:10');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation_id` bigint UNSIGNED NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `designation_id`, `message`, `image_url`, `status`, `created_at`, `updated_at`) VALUES
(1, 'hari', 4, '<p>Hello All,</p>\r\n\r\n<p>We are there to help you people when ever in need</p>', 'Message_1.jpg', 1, '2021-08-31 03:48:22', '2021-09-07 05:21:36'),
(2, 'mallikarjuna', 5, '<p>hello</p>', 'Message_2.jpg', 1, '2021-08-31 03:48:22', '2021-09-02 05:06:47'),
(3, 'sai', 6, '<p>hello</p>', 'Message_3.jpg', 1, '2021-08-31 03:48:22', '2021-09-02 05:06:57'),
(4, 'anil', 7, '<p>sdfsdf</p>', 'Message_4.jpg', 1, '2021-08-31 07:13:38', '2021-09-02 05:07:06'),
(5, 'Sandeep', 4, '<p>Hello All,</p>\r\n\r\n<p>We are there to help you people when ever in need</p>', 'Message_5.png', 0, '2021-09-03 20:10:45', '2021-09-15 08:14:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_08_23_183230_create_banners_table', 1),
(6, '2021_08_24_061501_create_venues_table', 1),
(7, '2021_08_24_085914_create_designations_table', 1),
(8, '2021_08_24_085918_create_messages_table', 1),
(9, '2021_08_26_073612_create_menus_table', 1),
(10, '2021_08_28_120444_create_donortypes_table', 1),
(11, '2021_08_28_124854_create_invitees_table', 1),
(12, '2021_08_30_065031_create_leadership_types_table', 1),
(13, '2021_08_30_074526_create_sponsor_category_types_table', 1),
(14, '2021_08_30_123957_create_logos_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `payment_amount` double(8,2) NOT NULL,
  `payment_methord` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `unique_id_for_payment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'rererence id for zelle, payment_id for paypal',
  `more_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `user_id`, `payment_amount`, `payment_methord`, `unique_id_for_payment`, `more_info`, `account_status`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, 1, 100.00, '3', 'asdfasd', '{\"cheque_date\":null,\"more_info\":null}', 'Initial Payment', 'Inprocess', '2021-09-23 10:45:56', '2021-09-23 10:45:56');

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

CREATE TABLE `payment_types` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_types`
--

INSERT INTO `payment_types` (`id`, `name`, `note`, `status`, `created_at`, `updated_at`) VALUES
(1, 'paypal', NULL, 1, '2021-09-13 23:48:54', '2021-09-13 23:50:41'),
(2, 'check', NULL, 1, '2021-09-13 23:48:54', '2021-09-13 23:50:24'),
(3, 'zelle', NULL, 1, '2021-09-13 23:48:54', '2021-09-13 23:50:40'),
(4, 'other', NULL, 1, '2021-09-13 23:48:54', '2021-09-13 23:50:39'),
(5, 'abc', NULL, 0, '2021-09-13 23:48:54', '2021-09-15 04:42:25');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` bigint UNSIGNED NOT NULL,
  `chair_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `co_chair_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `program_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Location` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `page_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `chair_name`, `mobile_number`, `co_chair_name`, `program_name`, `Location`, `image_url`, `date`, `page_content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'nnN7TDYcVpYbZfiqM7ukQtXJ7mqTUUC1WyZAkJcGH0HPS6l1O5PI4GfWe6Z8YW0HmTuv6OOzYPbVUWGM8lo8csq8TR3ioWjbb5He', 'BHTTFscP5ZqhxC5ndRio', 'DP6emcPZ7i89eGkmb0egpM3fT8lsrAyNVIkUa8CagZ1hXvVlcHanQmVgEYx50W4tu00vXVsikMfjWtUplRJy9SJloDgmfMgdzg8U', 'Uu5fmnCNGmC7dB5L7ON4', 'VaXZd9xyIc5kfz4jsQoo02zNUohVea9qPiIofUt1wqElVp1nhfCzh7YR0iXCtwud3RMponNwc8ccSbl1Dw2Xfylk4Oq4JXXRyZY2RPJ963QUBueBN1dnHS8FwNpKFL27xYgM98M4jhyKUu8sUR11pZvSeWOvjkPvcwy3CIOeJ0KxhVC8dznHz1p5Z0PTFjx6AP8V2npN', NULL, '2010-03-11', 'Asperiores dolores autem culpa mollitia omnis. Saepe harum et aut voluptas porro occaecati aut libero. Ratione molestiae maxime minima.', 0, '2021-09-01 14:34:08', '2021-09-01 14:34:08'),
(2, 'b6jbSG4D5cejMVzQ6frBkD21fan0BdKsGdzSfCPMuz5Bd2liv6w8LlFbCnS72hrRc1TcErOcw0AHu69s0B3TAKzzxjUxkXLA3mKz', 'khA1O7b9T0MX26LP9jPA', 'OXFq2NGtAH74MA3K5dx83mTye6MNCLrrdW7zWwF4hi4Df31F2pMxcrplo8cL0lt6PLRkJBfXRajZu10Cja2A6hozmR3uFfAgJS1z', 'k5fGeYzif3mRZ3ms0cAo', 'v7MRgAR0BmhhsRqSOO7EjhGolfFjfOd3vJxZGXDH7hWqRVSX6OnlxhHMOpPsvPgbIKFhTeJRcvhYNyuXlZ08dqAckyYFkOx3mhsTO8LlqVuNEez0mVL5xLnhP2f2raJhmwbPQbgqb41ZPOEOoFtehxSiUsQ8qCpO2fRBX4T5mn0xj5GnChVINboUTGl8hRuUYWR78VEp', NULL, '1998-01-26', 'Suscipit architecto molestiae ipsum. Ut hic ea voluptatem animi ipsam et. Accusantium fugit maxime ipsum molestias corporis. Mollitia aspernatur qui consequatur sequi dicta ipsum.', 0, '2021-09-01 14:34:08', '2021-09-01 14:34:08'),
(3, 'Nu6x37hrYaLDzMzf6Mi67IrtDLXwwvSBgrmOegDW7JvappCzNdAjfe4nwChtFOAug1zvJ4mLPGPCzLSOv7oDnrD9q8iIhPzkutrU', 'CAw6qw1V3A7GShN17OZq', 'qblasZ5zdXSB7mzSakTJksY8GP51F8pljRJ3AWSKUJ49pqv7Um4hUZatA77bxhbpF3fqgIZL3yZ6IvCaoWOlfvqxxAL9KJ7ozza0', 'iD9CmsRM6oW0jUUpJ2hr', 'a3HR9HNGdlbDf5IW4gl3QPTSeNd0VOC4UfEVFjCjql3KEq8ZDGN9f8VnYA932VmwyqlAsWjHNEl1yqhQxNT3g1yicpDZbJVOyN4aUjxsCVCl16FNOQN2rLE8RYHTptjxq49vkzF7TFAHKnhZvmbmxVdoviJYmilTAk8Y9gEqzewim2UcPrH0iOp5euhgF6oCHYnqWu5q', NULL, '2011-01-03', 'Ut minima incidunt sed ut rem error. Aliquid aut sequi similique ea deleniti architecto.', 0, '2021-09-01 14:34:08', '2021-09-01 14:34:08'),
(4, '2Nrz4Xvt3qbapWYDuf6BaIFKR9bhNljTZP7pG4uJiZ8oK8kaJloDekDMCrVWLZN3HpQmnD704pp9rQdiMV1tdYZI2gTHvKp9TyoU', 'wOFe8TQs4TjZouWkzWqs', 'uw1wrApxxEqoclgByzTnllHSeimI803ivUn9Qpgl6ByQMibCKUOU87rZ7ykNA7hzG82U6dT1leR52YyF3otqoHN3VOnvdSBnqZkM', 'xEtLga65TMk9L3Vne5ef', 'z3t0cJzq2Q5iYuWtxV5I9mGn3tPbN2SD1snV6RiVr4jkJHJsNn1HGNBtrW50yHKfpC6QPS39ozeEZGw2tHH3iGhwxVxLUyFCVdE9yMs4CLu9gVUsT3G2PYMqyYcYRHWqJqAhKODptJc7a82Xv2LSoRWKMjp7onAB0HkmvgltaY9hyaDRLCt6OjcNpleQkEzu2FKgTzso', NULL, '1970-03-17', 'Odit accusantium consequatur dolorem dolore. Consequatur repellat ea dolorum. Molestiae itaque minus incidunt id a ut veniam quod.', 1, '2021-09-01 14:34:08', '2021-09-01 14:34:08'),
(5, 'QkoJe6WU2SdVzYGAR3EEPNz9JsL89FY3w09swFRDD7sAysk0omey2AE0Pc0pY9Y0gbiLfZdRVWuFkyU5TuzDqeY9e6LAyNrmp7hG', 'H8SJddO1pl3PxklWE77P', 'coVnSc6KichdFoZdxjY0wFwGrug6TGAmJzXrOitAWeR5ynzhGGsGY1Fb1ECD6Q2wPb4lF7SLIwVNxJSVDh6vYNGrLHgdAmI93o3f', 'e0Flajs8jeaybb2uicIG', '2NkI4YSDLRv8XcXDaFpVhc60IzFI7tvIiEJsXQXAL9Uc3Yl44kPa00iSZdI2tytRg3ZMgtHMIEHTyBMcsLeLzhyO22p0RDKVjX3n00SO5vLqnKBO4e1W1JXqmRmZY4azM9GLB5iWM7tjTEXgqXBMbPPXC6nkZ9g6mJQ1DvX4HbOtuLNXS4hMlOrSyT1MAGnFbgPTzeJA', NULL, '2012-03-20', 'Tempore id omnis quia nesciunt vitae. Itaque repudiandae nobis qui officia facilis dolores. Totam occaecati aut in sunt. Ut nam fugiat temporibus voluptatem et perspiciatis qui.', 1, '2021-09-01 14:34:08', '2021-09-01 14:34:08'),
(6, 'dfasd', '987654321', 'asdfasdf', 'asfasdf', 'adsfasdf', 'program_6.jpg', '2021-09-07', '<p>asdf</p>', 1, '2021-09-02 04:41:17', '2021-09-02 04:41:17');

-- --------------------------------------------------------

--
-- Table structure for table `registration_page_content`
--

CREATE TABLE `registration_page_content` (
  `id` bigint UNSIGNED NOT NULL,
  `additional_donor_benefits` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `patment_types` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `check_payable_to` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `registration_page_content`
--

INSERT INTO `registration_page_content` (`id`, `additional_donor_benefits`, `note`, `patment_types`, `check_payable_to`, `created_at`, `updated_at`) VALUES
(1, '<p>adf</p>', '<p>aaaa111</p>', '[\"1\",\"2\",\"3\",\"4\"]', '<p>qwqwq</p>', NULL, '2021-09-09 05:22:55');

-- --------------------------------------------------------

--
-- Table structure for table `roles_and_designations`
--

CREATE TABLE `roles_and_designations` (
  `id` bigint UNSIGNED NOT NULL,
  `member_id` bigint UNSIGNED NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_type_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles_and_designations`
--

INSERT INTO `roles_and_designations` (`id`, `member_id`, `type`, `sub_type_id`, `designation_id`, `created_at`, `updated_at`) VALUES
(19, 1, 'leadershiptype', '1', 4, '2021-09-16 07:28:24', '2021-09-16 07:28:24'),
(20, 1, 'leadershiptype', '2', 5, '2021-09-16 07:28:43', '2021-09-16 07:28:43'),
(21, 2, 'leadershiptype', '1', 6, '2021-09-16 07:29:12', '2021-09-16 07:29:12'),
(22, 2, 'leadershiptype', '2', 7, '2021-09-16 07:29:12', '2021-09-16 07:29:12'),
(23, 2, 'leadershiptype', '4', 7, '2021-09-16 07:29:34', '2021-09-16 07:30:00'),
(24, 2, 'leadershiptype', '3', 4, '2021-09-16 07:30:17', '2021-09-16 07:30:17'),
(25, 3, 'leadershiptype', '2', 7, '2021-09-16 07:32:32', '2021-09-16 07:32:32');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` bigint UNSIGNED NOT NULL,
  `event_id` bigint UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `program_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `event_id`, `date`, `from_time`, `to_time`, `program_name`, `room_no`, `created_at`, `updated_at`) VALUES
(1, 6, '2003-06-17', '02:53:05', '07:08:24', 'zSwed7HVeT', 'bt5OAHp88D', '2021-09-02 06:20:46', '2021-09-02 06:20:46'),
(2, 7, '1977-10-19', '10:23:54', '19:43:11', 'nadJI1rYKw', 'LQcTJzVpqi', '2021-09-02 06:20:46', '2021-09-02 06:20:46'),
(3, 8, '1995-03-12', '08:12:55', '12:20:39', 'dddhqZEQP8', 'WzPRj6oDGx', '2021-09-02 06:20:46', '2021-09-02 06:20:46'),
(4, 9, '1972-05-04', '00:19:59', '14:00:34', '3txY8JWlt4', 'aURZd00oxu', '2021-09-02 06:20:46', '2021-09-02 06:20:46'),
(5, 10, '1977-11-07', '20:19:20', '00:11:30', 'sLoGzWjNVy', 'eyf6rqrfaj', '2021-09-02 06:20:46', '2021-09-02 06:20:46'),
(6, 1, '2021-09-07', '00:56:00', '00:57:00', 'adsf', 'sdfasdf', '2021-09-03 05:23:57', '2021-09-07 10:05:48'),
(7, 1, '2021-09-05', '17:38:00', '15:41:00', 'asdf', 'sfasdfasdf', '2021-09-07 10:09:04', '2021-09-07 10:09:04'),
(8, 1, '2021-09-04', '22:22:00', '12:23:00', 'abbb', '12222', '2021-09-07 14:50:34', '2021-09-07 14:50:34'),
(9, 1, '2021-09-05', '08:00:00', '09:00:00', 'ss', 'ss', '2021-09-11 10:55:14', '2021-09-11 10:55:14'),
(10, 1, '2021-09-05', '11:00:00', '12:00:00', 'sd', 'sdf', '2021-09-11 10:55:14', '2021-09-11 10:55:14');

-- --------------------------------------------------------

--
-- Table structure for table `sponsor_categories`
--

CREATE TABLE `sponsor_categories` (
  `id` bigint UNSIGNED NOT NULL,
  `category_type_id` bigint UNSIGNED NOT NULL,
  `donor_type_id` bigint UNSIGNED NOT NULL COMMENT 'null if Individual',
  `amount` double(8,2) NOT NULL,
  `benefits` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sponsor_categories`
--

INSERT INTO `sponsor_categories` (`id`, `category_type_id`, `donor_type_id`, `amount`, `benefits`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 1111.00, '\"accc\"', 1, '2021-09-08 23:34:53', '2021-09-08 23:34:53'),
(2, 1, 0, 12112.00, '\"adfddf\"', 1, '2021-09-08 23:35:00', '2021-09-08 23:35:00'),
(5, 2, 1, 200.00, '', 1, '2021-09-23 10:45:18', '2021-09-23 10:45:18');

-- --------------------------------------------------------

--
-- Table structure for table `sponsor_category_types`
--

CREATE TABLE `sponsor_category_types` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sponsor_category_types`
--

INSERT INTO `sponsor_category_types` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Family / Individual', 1, '2021-08-31 03:48:22', '2021-09-09 05:19:41'),
(2, 'Donor', 1, '2021-08-31 03:48:22', '2021-09-09 05:19:40'),
(3, 'Alf Borer', 1, '2021-08-31 03:48:22', '2021-09-15 20:32:47'),
(4, 'Arun', 1, '2021-09-15 20:34:32', '2021-09-15 20:34:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spouse_first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spouse_last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not initiated',
  `total_amount` double(8,2) DEFAULT NULL,
  `amount_paid` double(8,2) DEFAULT NULL,
  `registration_type_id` bigint UNSIGNED NOT NULL,
  `individual_registration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sponsorship_category_id` bigint UNSIGNED NOT NULL,
  `registration_note` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `member_id`, `email`, `first_name`, `last_name`, `spouse_first_name`, `spouse_last_name`, `phone_code`, `mobile`, `country`, `state`, `city`, `zip_code`, `image_url`, `payment_status`, `total_amount`, `amount_paid`, `registration_type_id`, `individual_registration`, `sponsorship_category_id`, `registration_note`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '', '13473', 'amarsree007@gmail.com', 'amar', 'sree', '', '', NULL, '(944) 096-9532', 'United States', 'sdfasdf', 'asdfasdf', '544444', NULL, 'partial Paid', 200.00, 100.00, 2, NULL, 5, '<p>abc reg note</p>', NULL, '', NULL, '2021-09-23 10:43:13', '2021-09-23 10:52:32');

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

CREATE TABLE `venues` (
  `id` bigint UNSIGNED NOT NULL,
  `Event_date_time` datetime NOT NULL,
  `end_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `venues`
--

INSERT INTO `venues` (`id`, `Event_date_time`, `end_date`, `location`, `created_at`, `updated_at`) VALUES
(1, '2021-09-15 00:00:00', '2021-09-17 00:00:00', 'Washington', '2021-08-31 03:48:22', '2021-09-15 07:49:15');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint UNSIGNED NOT NULL,
  `video_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_order` int DEFAULT NULL,
  `youtube_video_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `video_url`, `name`, `display_order`, `youtube_video_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ipsum', 'Ned Rohan', NULL, NULL, 1, '2021-09-03 06:28:05', '2021-09-03 06:28:05'),
(2, 'dolorum', 'Mrs. Callie Okuneva Sr.', NULL, NULL, 1, '2021-09-03 06:28:06', '2021-09-03 06:28:06'),
(3, 'et', 'Roxanne Dibbert PhD', NULL, NULL, 1, '2021-09-03 06:28:06', '2021-09-03 06:28:06'),
(4, 'nisi', 'Judah Streich', NULL, NULL, 1, '2021-09-03 06:28:06', '2021-09-03 06:28:06'),
(5, 'eos', 'Toby Quitzon', NULL, NULL, 1, '2021-09-03 06:28:06', '2021-09-03 06:28:06'),
(10, 'https://www.youtube.com/watch?v=1ck_UBkW500', 'video', NULL, '1ck_UBkW500', 1, '2021-09-03 04:29:38', '2021-09-03 04:29:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benefit_types`
--
ALTER TABLE `benefit_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benefit_user`
--
ALTER TABLE `benefit_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benfit_sponsor_category`
--
ALTER TABLE `benfit_sponsor_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `committes`
--
ALTER TABLE `committes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donors`
--
ALTER TABLE `donors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donortypes`
--
ALTER TABLE `donortypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `invitees`
--
ALTER TABLE `invitees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leadership_types`
--
ALTER TABLE `leadership_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logos`
--
ALTER TABLE `logos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_slug_unique` (`slug`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration_page_content`
--
ALTER TABLE `registration_page_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_and_designations`
--
ALTER TABLE `roles_and_designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsor_categories`
--
ALTER TABLE `sponsor_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsor_category_types`
--
ALTER TABLE `sponsor_category_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `venues`
--
ALTER TABLE `venues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `benefit_types`
--
ALTER TABLE `benefit_types`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `benefit_user`
--
ALTER TABLE `benefit_user`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `benfit_sponsor_category`
--
ALTER TABLE `benfit_sponsor_category`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `committes`
--
ALTER TABLE `committes`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `donors`
--
ALTER TABLE `donors`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `donortypes`
--
ALTER TABLE `donortypes`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invitees`
--
ALTER TABLE `invitees`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `leadership_types`
--
ALTER TABLE `leadership_types`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `logos`
--
ALTER TABLE `logos`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `registration_page_content`
--
ALTER TABLE `registration_page_content`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles_and_designations`
--
ALTER TABLE `roles_and_designations`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sponsor_categories`
--
ALTER TABLE `sponsor_categories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sponsor_category_types`
--
ALTER TABLE `sponsor_category_types`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `venues`
--
ALTER TABLE `venues`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
