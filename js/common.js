function convertToSlug(Text) {
    return Text
       .toLowerCase()
        .replace(/ +/g, '-')
      // .replace(/[^\w-]+/g, 'ddd')
}


$(document).ready(function () {

    $(".show_more_btn").click(function () {
        $('#donation_'+$(this).attr('id').split('_')[1]).toggleClass("benfits-height");
       // $('donation_12').toggleClass("speakers-details-height");
        if ($(this).text() == "Show More +") {
            $(this).text("Show Less - ");
        } else {
            $(this).text("Show More +");
        } 
    });

    $('body').on('click', '.add-category-name', function() {
        count = $(this).data('count')
        $(this).attr('data-count', parseInt($(this).attr('data-count')) + 1)
        $(".child-template").find('.main').first().clone()
            .find(".chaild-of-main").attr('id', 'delete_individual_' + $(this).attr('data-count')).end()
            .find(".button-div").empty().end()
            .find(".Kid_name").val('').end()
            .find(".Kid_age").val('').end()
            .find(".button-div").append('<span class="plus_n_minus_icons delete-item" id="btn_delete_individual_' + $(this).attr('data-count') + '" ><i class="fas fa-minus"></i>').end()
            .appendTo($('.child-add-div'));
    })

    $(document).on("click", ".delete-item", function() {
        id = $(this).attr('id').split('_')
        $('#delete_individual_'+ id[3]).parent().remove();
    })


    $('.datatable').DataTable();

    $(".speakers-more-details").click(function () {
        $(this).parent().parent().parent().find(".speakers-content").toggleClass("speakers-details-height");
        if ($(this).text() == "Show More +") {
            $(this).text("Show Less - ");
        } else {
            $(this).text("Show More +");
        }
    });

    $('[name=title], [name=name]').on('keyup', function () {
        var title = $(this).val();
        var slug = convertToSlug(title);
        $('[name=slug]').val(slug);

    });


    $('[name=title], [name=name]').on('blur', function () {
        var title = $(this).val();
        var slug = convertToSlug(title);
        $('[name=slug]').val(slug);

    });


    $('[name=slug]').on('keyup', function () {
        var title = $(this).val();
        var slug = convertToSlug(title);
        $('[name=slug]').val(slug);

    });

    $('[name=slug]').on('blur', function () {
        var title = $(this).val();
        var slug = convertToSlug(title);
        $('[name=slug]').val(slug);
    });
})


function IsCheckBoxChecked(thisVal) {
    if (thisVal.checked) {
           return true
        } else {
            return false
        }
}


function ajaxCall(url, method, data = null, callBack = null) {
    $.ajax({
        url: url,
        type: method,
        data: data,
        success: function (response) {
            if (callBack != null) {
                callBack(response);
            }
        }
    });
}



$('.status-btn').click(function() {
        if ($(this).text() == 'Active') {
        $(this).text('InActive')
            status = 0
        } else {
        $(this).text('Active')
            status = 1
        }
    data = {
        "_token": $('meta[name=csrf-token]').attr('content'),
    id: $(this).attr('data-id'),
    status: status
        }
        ajaxCall($(this).data('url'), 'put', data, afterStatusUpdate)

    function afterStatusUpdate(data) {

    }
    })

$('.delete-btn').click(function () {

    swal({
        title: "Are you sure ?",
        text: "Do you want to delete ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if(willDelete){
            data = {
                        "_token": $('meta[name=csrf-token]').attr('content'),
                        id: $(this).attr('data-id'),
                    }
                ajaxCall($(this).data('url'), 'delete', data, afterdelete)


            }
        });

})

function afterdelete(data) {
    location.reload();
}


$('.dropdown-element').change(function () {
    ajaxCall($(this).data('url') + '/' + $(this).val(), 'get', null , putDataToDropdown)
})


$('.dropdown-element-edit').change(function () {
    ajaxCall($(this).data('url') + '/' + $(this).val()+'/'+$(this).data('id'), 'get', null, putDataToDropdown)
})



function putDataToDropdown(data) {
    $('.dropdown-target').empty()
    $('.dropdown-target').append('<option selected disabled value="">Select</option>')
    for (let i = 0; i < data.length; i++) {
        $('.dropdown-target').append('<option  value="'+ data[i].id+'">'+data[i].name+'</option>')
    }
}
